VERSION 5.00
Object = "{C1A8AF28-1257-101B-8FB0-0020AF039CA3}#1.1#0"; "MCI32.OCX"
Begin VB.Form frmHealLongTerm 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Heal: Long-Term Wounds"
   ClientHeight    =   5430
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8310
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5430
   ScaleWidth      =   8310
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.TextBox txtPoints 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3120
      MaxLength       =   4
      TabIndex        =   2
      Top             =   960
      Width           =   615
   End
   Begin VB.TextBox txtCombatantHealth 
      BackColor       =   &H8000000F&
      Height          =   495
      Left            =   0
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   60
      TabStop         =   0   'False
      Top             =   360
      Width           =   3975
   End
   Begin VB.ComboBox cmbMultipleInjuries 
      Height          =   315
      ItemData        =   "frmHealLongTerm.frx":0000
      Left            =   5760
      List            =   "frmHealLongTerm.frx":001C
      Style           =   2  'Dropdown List
      TabIndex        =   31
      Top             =   3000
      Width           =   1695
   End
   Begin VB.ComboBox cmbRest 
      Height          =   315
      ItemData        =   "frmHealLongTerm.frx":003A
      Left            =   5760
      List            =   "frmHealLongTerm.frx":0067
      Style           =   2  'Dropdown List
      TabIndex        =   29
      Top             =   2640
      Width           =   1695
   End
   Begin VB.ComboBox cmbTechLevel 
      Height          =   315
      ItemData        =   "frmHealLongTerm.frx":00DD
      Left            =   5760
      List            =   "frmHealLongTerm.frx":011A
      TabIndex        =   27
      Text            =   "1.0 (Modern Day)"
      Top             =   2280
      Width           =   1695
   End
   Begin VB.ComboBox cmbPatientEndurance 
      Height          =   315
      ItemData        =   "frmHealLongTerm.frx":0287
      Left            =   6120
      List            =   "frmHealLongTerm.frx":02AC
      TabIndex        =   18
      Text            =   "0"
      Top             =   480
      Width           =   1335
   End
   Begin VB.TextBox txtMisc 
      Height          =   285
      Index           =   4
      Left            =   7560
      MaxLength       =   4
      TabIndex        =   37
      Top             =   4080
      Width           =   735
   End
   Begin VB.TextBox txtMisc 
      Height          =   285
      Index           =   3
      Left            =   7560
      MaxLength       =   4
      TabIndex        =   35
      Top             =   3720
      Width           =   735
   End
   Begin VB.TextBox txtMisc 
      Height          =   285
      Index           =   5
      Left            =   7560
      MaxLength       =   4
      TabIndex        =   33
      Top             =   3360
      Width           =   735
   End
   Begin VB.TextBox txtMisc 
      Height          =   285
      Index           =   2
      Left            =   7560
      MaxLength       =   4
      TabIndex        =   24
      Top             =   1560
      Width           =   735
   End
   Begin VB.TextBox txtMisc 
      Height          =   285
      Index           =   1
      Left            =   7560
      MaxLength       =   4
      TabIndex        =   22
      Top             =   1200
      Width           =   735
   End
   Begin VB.TextBox txtMisc 
      Height          =   285
      Index           =   0
      Left            =   7560
      MaxLength       =   4
      TabIndex        =   20
      Top             =   840
      Width           =   735
   End
   Begin VB.CheckBox chkMisc 
      Caption         =   "Other"
      Height          =   255
      Index           =   4
      Left            =   4200
      TabIndex        =   36
      Top             =   4080
      Width           =   3255
   End
   Begin VB.CheckBox chkMisc 
      Caption         =   "Other"
      Height          =   255
      Index           =   3
      Left            =   4200
      TabIndex        =   34
      Top             =   3720
      Width           =   3255
   End
   Begin VB.CheckBox chkMisc 
      Caption         =   "Realism Level"
      Height          =   255
      Index           =   5
      Left            =   4200
      TabIndex        =   32
      Top             =   3360
      Width           =   3255
   End
   Begin VB.CheckBox chkMultipleInjuries 
      Caption         =   "Multiple Injuries"
      Height          =   255
      Left            =   4200
      TabIndex        =   30
      Top             =   3000
      Width           =   1455
   End
   Begin VB.CheckBox chkRest 
      Caption         =   "Rest"
      Height          =   255
      Left            =   4200
      TabIndex        =   28
      Top             =   2640
      Value           =   1  'Checked
      Width           =   1455
   End
   Begin VB.CheckBox chkTechLevel 
      Caption         =   "Tech Level"
      Height          =   255
      Left            =   4200
      TabIndex        =   26
      Top             =   2280
      Value           =   1  'Checked
      Width           =   1455
   End
   Begin VB.CheckBox chkMedicalAssistance 
      Caption         =   "Medical Assistance (see left)"
      Height          =   255
      Left            =   4200
      TabIndex        =   25
      Top             =   1920
      Value           =   1  'Checked
      Width           =   3255
   End
   Begin VB.CheckBox chkMisc 
      Caption         =   "Item"
      Height          =   255
      Index           =   2
      Left            =   4200
      TabIndex        =   23
      Top             =   1560
      Width           =   3255
   End
   Begin VB.CheckBox chkMisc 
      Caption         =   "Idiosyncrasy"
      Height          =   255
      Index           =   1
      Left            =   4200
      TabIndex        =   21
      Top             =   1200
      Width           =   3255
   End
   Begin VB.CheckBox chkMisc 
      Caption         =   "Idiosyncrasy"
      Height          =   255
      Index           =   0
      Left            =   4200
      TabIndex        =   19
      Top             =   840
      Width           =   3255
   End
   Begin VB.CheckBox chkPatientEndurance 
      Caption         =   "Patient's Endurance"
      Height          =   255
      Left            =   4200
      TabIndex        =   17
      Top             =   480
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.ComboBox cmbWoundType 
      Height          =   315
      ItemData        =   "frmHealLongTerm.frx":02DB
      Left            =   1560
      List            =   "frmHealLongTerm.frx":0300
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   960
      Width           =   1455
   End
   Begin VB.CommandButton cmdOK 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Default         =   -1  'True
      Height          =   375
      Left            =   7680
      TabIndex        =   38
      Top             =   5040
      Width           =   615
   End
   Begin VB.TextBox txtOther2 
      Height          =   285
      Left            =   3240
      MaxLength       =   4
      TabIndex        =   16
      Top             =   4680
      Width           =   735
   End
   Begin VB.TextBox txtOther1 
      Height          =   285
      Left            =   3240
      MaxLength       =   4
      TabIndex        =   14
      Top             =   4320
      Width           =   735
   End
   Begin VB.TextBox txtPhysicianSkill 
      Height          =   285
      Left            =   3240
      MaxLength       =   4
      TabIndex        =   5
      Top             =   2520
      Width           =   735
   End
   Begin VB.TextBox txtRoll 
      Height          =   285
      Left            =   3240
      MaxLength       =   4
      TabIndex        =   4
      Top             =   2160
      Width           =   735
   End
   Begin VB.ComboBox cmbPatientState 
      Height          =   315
      ItemData        =   "frmHealLongTerm.frx":0393
      Left            =   1560
      List            =   "frmHealLongTerm.frx":039E
      Style           =   2  'Dropdown List
      TabIndex        =   12
      Top             =   3960
      Width           =   1575
   End
   Begin VB.ComboBox cmbPhysicianTime 
      Height          =   315
      ItemData        =   "frmHealLongTerm.frx":03BA
      Left            =   1560
      List            =   "frmHealLongTerm.frx":03D0
      Style           =   2  'Dropdown List
      TabIndex        =   10
      Top             =   3600
      Width           =   1575
   End
   Begin VB.ComboBox cmbTreatingSelf 
      Height          =   315
      ItemData        =   "frmHealLongTerm.frx":0420
      Left            =   1560
      List            =   "frmHealLongTerm.frx":0431
      Style           =   2  'Dropdown List
      TabIndex        =   8
      Top             =   3240
      Width           =   1575
   End
   Begin VB.CheckBox chkOther2 
      Caption         =   "Other"
      Height          =   255
      Left            =   0
      TabIndex        =   15
      Top             =   4680
      Width           =   3015
   End
   Begin VB.CheckBox chkOther1 
      Caption         =   "Other"
      Height          =   255
      Left            =   0
      TabIndex        =   13
      Top             =   4320
      Width           =   3015
   End
   Begin VB.CheckBox chkPatientState 
      Caption         =   "Patient state"
      Height          =   255
      Left            =   0
      TabIndex        =   11
      Top             =   3960
      Width           =   1575
   End
   Begin VB.CheckBox chkPhysicianTime 
      Caption         =   "Physician time"
      Height          =   255
      Left            =   0
      TabIndex        =   9
      Top             =   3600
      Value           =   1  'Checked
      Width           =   1575
   End
   Begin VB.CheckBox chkTreatingSelf 
      Caption         =   "Treating yourself"
      Height          =   255
      Left            =   0
      TabIndex        =   7
      Top             =   3240
      Width           =   1695
   End
   Begin VB.CheckBox chkDiagnosis 
      Caption         =   "Diagnosis skill completed?"
      Height          =   255
      Left            =   0
      TabIndex        =   6
      Top             =   2880
      Width           =   3015
   End
   Begin VB.CheckBox chkPhysicianSkill 
      Caption         =   "Physician Skill"
      Height          =   255
      Left            =   0
      TabIndex        =   43
      Top             =   2520
      Value           =   2  'Grayed
      Width           =   3015
   End
   Begin VB.CheckBox chkRoll 
      Height          =   255
      Left            =   0
      TabIndex        =   42
      Top             =   2160
      Value           =   2  'Grayed
      Width           =   210
   End
   Begin VB.CommandButton cmdRoll 
      Caption         =   "Roll"
      Height          =   285
      Left            =   240
      TabIndex        =   3
      Top             =   2160
      Width           =   2895
   End
   Begin VB.ComboBox cmbCombatants 
      Height          =   315
      HelpContextID   =   340
      Left            =   1560
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   0
      Width           =   2415
   End
   Begin MCI.MMControl mciControl 
      Height          =   330
      Left            =   360
      TabIndex        =   39
      TabStop         =   0   'False
      Top             =   360
      Visible         =   0   'False
      Width           =   3540
      _ExtentX        =   6244
      _ExtentY        =   582
      _Version        =   393216
      PlayEnabled     =   -1  'True
      DeviceType      =   "WaveAudio"
      FileName        =   "C:\Program Files\IRIS\dice.wav"
   End
   Begin VB.Label lblPts 
      Caption         =   "pts"
      Height          =   255
      Left            =   3750
      TabIndex        =   61
      Top             =   1005
      Width           =   255
   End
   Begin VB.Label lblTypeOfWound 
      Caption         =   "Type of wound:"
      Height          =   255
      Left            =   0
      TabIndex        =   59
      Top             =   1020
      Width           =   1455
   End
   Begin VB.Label lblOutcome 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   855
      Left            =   4200
      TabIndex        =   58
      Top             =   4560
      Width           =   3375
   End
   Begin VB.Label lblMultipleInjuries 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   7560
      TabIndex        =   57
      Top             =   3000
      Width           =   735
   End
   Begin VB.Label lblRest 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   7560
      TabIndex        =   56
      Top             =   2640
      Width           =   735
   End
   Begin VB.Label lblTechLevel 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "1.0"
      Height          =   255
      Left            =   7560
      TabIndex        =   55
      Top             =   2280
      Width           =   735
   End
   Begin VB.Label lblMedicalAssistance 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   7560
      TabIndex        =   54
      Top             =   1920
      Width           =   735
   End
   Begin VB.Line Line2 
      X1              =   4200
      X2              =   8280
      Y1              =   4440
      Y2              =   4440
   End
   Begin VB.Label lblPatientEndurance 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   7560
      TabIndex        =   53
      Top             =   480
      Width           =   735
   End
   Begin VB.Label lblHealingFactor 
      Alignment       =   2  'Center
      Caption         =   "Healing Factor:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4200
      TabIndex        =   52
      Top             =   120
      Width           =   3975
   End
   Begin VB.Line Line1 
      X1              =   4080
      X2              =   4080
      Y1              =   0
      Y2              =   5400
   End
   Begin VB.Label lblMedicalAssistanceHeading 
      Alignment       =   2  'Center
      Caption         =   "Medical Assistance:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   0
      TabIndex        =   51
      Top             =   1800
      Width           =   3975
   End
   Begin VB.Label lblBaseRateIntro 
      Caption         =   "Base Healing Rate:"
      Height          =   255
      Left            =   0
      TabIndex        =   50
      Top             =   1320
      Width           =   1455
   End
   Begin VB.Image imgDice 
      Height          =   480
      Left            =   7740
      MousePointer    =   4  'Icon
      Picture         =   "frmHealLongTerm.frx":0465
      Top             =   4500
      Width           =   480
   End
   Begin VB.Label lblResultName 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   0
      TabIndex        =   49
      Top             =   5160
      Width           =   3135
   End
   Begin VB.Label lblTotal 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   3240
      TabIndex        =   48
      Top             =   5160
      Width           =   735
   End
   Begin VB.Line linBotSep 
      X1              =   0
      X2              =   3960
      Y1              =   5040
      Y2              =   5040
   End
   Begin VB.Label lblBaseHealingRate 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   1560
      TabIndex        =   41
      Top             =   1320
      Width           =   2415
   End
   Begin VB.Label lblPatientState 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Left            =   3240
      TabIndex        =   47
      Top             =   3960
      Width           =   735
   End
   Begin VB.Label lblPhysicianTime 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Left            =   3240
      TabIndex        =   46
      Top             =   3600
      Width           =   735
   End
   Begin VB.Label lblTreatingSelf 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Left            =   3240
      TabIndex        =   45
      Top             =   3240
      Width           =   735
   End
   Begin VB.Label lblDiagnosis 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "+3"
      Height          =   255
      Left            =   3240
      TabIndex        =   44
      Top             =   2880
      Width           =   735
   End
   Begin VB.Line linTopSep 
      X1              =   0
      X2              =   3960
      Y1              =   1680
      Y2              =   1680
   End
   Begin VB.Label lblCombatant 
      Caption         =   "Combatant To Heal:"
      Height          =   255
      Left            =   0
      TabIndex        =   40
      Top             =   60
      Width           =   1455
   End
End
Attribute VB_Name = "frmHealLongTerm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private UserInput As Boolean
Private OutcomeHours As Integer

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
  
  ' load things that are remembered from instance to instance
  txtPhysicianSkill = ""
  chkMisc(5) = 0
  txtMisc(5) = ""
  txtMisc(5).Enabled = False
  
  ' set up the sound to play
  mciControl.Notify = False
  mciControl.Wait = True
  mciControl.Shareable = False
  mciControl.DeviceType = "WaveAudio"
  mciControl.Filename = App.Path & "\dice.wav"
  mciControl.Command = "Open"
End Sub

Private Sub Form_Activate()
  Dim i As Integer
  If cmdOK.Tag <> "l" Then Exit Sub
  UserInput = False
  cmdOK.Tag = ""
  ' set up the combatants list
  cmbCombatants.Clear
  For i = 1 To NumCombatants
    cmbCombatants.AddItem Combatants(i).Name
    Next i
  ' choose combatant (triggers cmbCombatants_Click)
  If frmIRIS.lisCombatants.ListIndex >= 0 Then
      cmbCombatants.ListIndex = frmIRIS.lisCombatants.ListIndex
    Else
      cmbCombatants.ListIndex = 0
    End If
  ' load other fields
  cmdRoll_Click
  ' allow physician skill field to remain from last time
  chkDiagnosis = 0
  chkTreatingSelf = 0
  cmbTreatingSelf.ListIndex = 1
  cmbTreatingSelf.Enabled = False
  chkPhysicianTime = 1
  cmbPhysicianTime.ListIndex = 2
  cmbPhysicianTime.Enabled = True
  chkPatientState = 0
  cmbPatientState.ListIndex = 0
  cmbPatientState.Enabled = False
  chkOther1 = 0
  txtOther1 = ""
  chkOther2 = 0
  txtOther2 = ""
  chkPatientEndurance = 1
  ' rest of patient endurance was filled in by the choice in cmbCombatants
  For i = 0 To 4 ' 5 is realism level and is left alone
    chkMisc(i) = 0
    txtMisc(i) = ""
    txtMisc(i).Enabled = False
    Next i
  chkMedicalAssistance = 1
  chkTechLevel = 1
  cmbTechLevel.Enabled = True
  ' allow the value to carry over from last time
  chkRest = 1
  cmbRest.ListIndex = 3
  cmbRest.Enabled = True
  chkMultipleInjuries = 0
  cmbMultipleInjuries.ListIndex = 0
  cmbMultipleInjuries.Enabled = False
  txtRoll.SetFocus
  UserInput = True
  lblTotal_Update
End Sub

Private Sub cmbCombatants_Click()
  Dim woundtype As Integer, points As Integer
  Dim healthnotes As String
  txtCombatantHealth = ""
  ' also attempt to guess the wound type and number of points
  woundtype = -1
  points = 0
  With Combatants(cmbCombatants.ListIndex + 1)
    cmbPatientEndurance = IIf(.StatE > 0, "+", "") & CStr(.StatE)
    cmbPatientEndurance_Click ' force an update
    If .Health <> "" Then
        txtCombatantHealth = txtCombatantHealth & .Health
        healthnotes = LCase(.Health)
        If Right(txtCombatantHealth, Len(vbCrLf)) <> vbCrLf Then txtCombatantHealth = txtCombatantHealth & vbCrLf
        ' try to guess wound type from key words
        If InStr(healthnotes, "useless") <> 0 Then ' limb useless
            woundtype = 9
          ElseIf InStr(healthnotes, "coma") <> 0 Then ' coma
            woundtype = 8
          ElseIf InStr(healthnotes, "compound fracture") <> 0 Then ' compound fracture
            woundtype = 7
          ElseIf InStr(healthnotes, "shattered") <> 0 Then ' shattered bone
            woundtype = 6
          ElseIf InStr(healthnotes, "bone") <> 0 Or _
                 InStr(healthnotes, "broken") <> 0 Or _
                 InStr(healthnotes, "fracture") <> 0 Then ' bone broken
            woundtype = 5
          ElseIf InStr(healthnotes, "tendon") <> 0 Then ' tendons
            woundtype = 4
          ElseIf InStr(healthnotes, "muscle") <> 0 Then ' muscles
            woundtype = 3
          ElseIf InStr(healthnotes, "sprain") <> 0 Then ' sprain
            woundtype = 2
          End If
      End If
    If .Penalty < 0 Then
        txtCombatantHealth = txtCombatantHealth & "At a " & CStr(.Penalty) & " to actions." & vbCrLf
        If woundtype = -1 Then woundtype = 2 ' sprain
        If points = 0 Then points = -.Penalty
      End If
    If .CurHp < .OptHp Then
        txtCombatantHealth = txtCombatantHealth & "Down " & CStr(.OptHp - .CurHp) & " hits of " & CStr(.OptHp) & "." & vbCrLf
        If woundtype = -1 Then
            woundtype = 0 ' lost hit points
            points = .OptHp - .CurHp
          End If
      End If
    If txtCombatantHealth = "" Then
        txtCombatantHealth = "No long-term wounds detected!"
      End If
    End With
  If woundtype = -1 Then woundtype = 0
  cmbWoundType.ListIndex = woundtype ' triggers change event
  If OutcomeHours <> 0 Then
      If points = 0 Then txtPoints = "" Else txtPoints = CStr(points)
    End If
End Sub

Private Sub cmbWoundType_Click()
  ' update base healing rate
  ' also record time rate to be used in later calculations of outcome
  txtPoints.Enabled = True
  Select Case cmbWoundType.ListIndex
    Case 0 ' lost Hp
      lblBaseHealingRate = "1 point per 8 hours"
      OutcomeHours = 8
    Case 1 ' bleeding
      lblBaseHealingRate = "1 point per 4 days"
      OutcomeHours = 96
    Case 2 ' sprains
      lblBaseHealingRate = "1 point per 3 days"
      OutcomeHours = 72
    Case 3 ' muscle damage
      lblBaseHealingRate = "1 point per 5 days"
      OutcomeHours = 120
    Case 4 ' tendon damage
      lblBaseHealingRate = "1 point per 8 days"
      OutcomeHours = 192
    Case 5 ' bone broken
      lblBaseHealingRate = "1 point per 10 days"
      OutcomeHours = 240
    Case 6 ' compound fracture
      lblBaseHealingRate = "1 point per 15 days"
      OutcomeHours = 360
    Case 7 ' shattered bone
      lblBaseHealingRate = "1 point per 15 days"
      OutcomeHours = 360
    Case 8 To 10 ' coma, limb useless, disease
      lblBaseHealingRate = "special"
      OutcomeHours = 0
      txtPoints.Enabled = False
    Case Else
      lblBaseHealingRate = "internal error!"
      OutcomeHours = 0
      txtPoints.Enabled = False
    End Select
  lblTotal_Update
End Sub

' dice roll button
Private Sub cmdRoll_Click()
  txtRoll = OpenEndedD20("open")
  mciControl.Command = "Play"
  mciControl.Command = "Prev"
End Sub

' text fields get selected on focus, only allow digits and sometimes minuses or periods
Private Sub txtRoll_GotFocus()
  Call SelectField(txtRoll)
End Sub

Private Sub txtRoll_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtPhysicianSkill_GotFocus()
  Call SelectField(txtPhysicianSkill)
End Sub

Private Sub txtPhysicianSkill_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtOther1_GotFocus()
  Call SelectField(txtOther1)
End Sub

Private Sub txtOther1_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtOther2_GotFocus()
  Call SelectField(txtOther2)
End Sub

Private Sub txtOther2_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtMisc_GotFocus(Index As Integer)
  Call SelectField(txtMisc(Index))
End Sub

Private Sub txtMisc_KeyPress(Index As Integer, KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndPeriod(KeyAscii)
End Sub

Private Sub txtPoints_GotFocus()
  Call SelectField(txtPoints)
End Sub

Private Sub txtPoints_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub cmbPatientEndurance_GotFocus()
  Call SelectField(cmbPatientEndurance)
End Sub

Private Sub cmbPatientEndurance_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub cmbTechLevel_GotFocus()
  Call SelectField(cmbTechLevel)
End Sub

Private Sub cmbTechLevel_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndPeriod(KeyAscii)
End Sub

' all checkboxes update, enable/disable, set focus on their fields
Private Sub chkDiagnosis_Click()
  Call lblTotal_Update
End Sub

Private Sub chkTreatingSelf_Click()
  If chkTreatingSelf = 1 Then cmbTreatingSelf.Enabled = True Else cmbTreatingSelf.Enabled = False
  Call lblTotal_Update
  If UserInput And chkTreatingSelf = 1 Then cmbTreatingSelf.SetFocus
End Sub

Private Sub chkPhysicianTime_Click()
  If chkPhysicianTime = 1 Then cmbPhysicianTime.Enabled = True Else cmbPhysicianTime.Enabled = False
  Call lblTotal_Update
  If UserInput And chkPhysicianTime = 1 Then cmbPhysicianTime.SetFocus
End Sub

Private Sub chkPatientState_Click()
  If chkPatientState = 1 Then cmbPatientState.Enabled = True Else cmbPatientState.Enabled = False
  Call lblTotal_Update
  If UserInput And chkPatientState = 1 Then cmbPatientState.SetFocus
End Sub

Private Sub chkOther1_Click()
  If chkOther1 = 1 Then txtOther1.Enabled = True Else txtOther1.Enabled = False
  Call lblTotal_Update
  If UserInput And chkOther1 = 1 Then txtOther1.SetFocus
End Sub

Private Sub chkOther2_Click()
  If chkOther2 = 1 Then txtOther2.Enabled = True Else txtOther2.Enabled = False
  Call lblTotal_Update
  If UserInput And chkOther2 = 1 Then txtOther2.SetFocus
End Sub

Private Sub chkPatientEndurance_Click()
  If chkPatientEndurance = 1 Then cmbPatientEndurance.Enabled = True Else cmbPatientEndurance.Enabled = False
  Call lblTotal_Update
  If UserInput And chkPatientEndurance = 1 Then cmbPatientEndurance.SetFocus
End Sub

Private Sub chkMisc_Click(Index As Integer)
  If chkMisc(Index) = 1 Then txtMisc(Index).Enabled = True Else txtMisc(Index).Enabled = False
  Call lblTotal_Update
  If UserInput And chkMisc(Index) = 1 Then txtMisc(Index).SetFocus
End Sub

Private Sub chkTechLevel_Click()
  If chkTechLevel = 1 Then cmbTechLevel.Enabled = True Else cmbTechLevel.Enabled = False
  Call lblTotal_Update
  If UserInput And chkTechLevel = 1 Then cmbTechLevel.SetFocus
End Sub

Private Sub chkRest_Click()
  If chkRest = 1 Then cmbRest.Enabled = True Else cmbRest.Enabled = False
  Call lblTotal_Update
  If UserInput And chkRest = 1 Then cmbRest.SetFocus
End Sub

Private Sub chkMultipleInjuries_Click()
  If chkMultipleInjuries = 1 Then cmbMultipleInjuries.Enabled = True Else cmbMultipleInjuries.Enabled = False
  Call lblTotal_Update
  If UserInput And chkMultipleInjuries = 1 Then cmbMultipleInjuries.SetFocus
End Sub

' all middle fields cause updates to their label fields
Private Sub cmbTreatingSelf_Click()
  lblTreatingSelf = cmbTreatingSelf.ItemData(cmbTreatingSelf.ListIndex)
  If Val(lblTreatingSelf) > 0 Then lblTreatingSelf = "+" & lblTreatingSelf
  Call lblTotal_Update
End Sub

Private Sub cmbPhysicianTime_Click()
  lblPhysicianTime = cmbPhysicianTime.ItemData(cmbPhysicianTime.ListIndex)
  If Val(lblPhysicianTime) > 0 Then lblPhysicianTime = "+" & lblPhysicianTime
  Call lblTotal_Update
End Sub

Private Sub cmbPatientState_Click()
  lblPatientState = cmbPatientState.ItemData(cmbPatientState.ListIndex)
  If Val(lblPatientState) > 0 Then lblPatientState = "+" & lblPatientState
  Call lblTotal_Update
End Sub

Private Sub cmbPatientEndurance_Click()
  lblPatientEndurance = CStr(1 + CInt(cmbPatientEndurance.Text) / 10)
  If CDbl(lblPatientEndurance) < 0.1 Then lblPatientEndurance = "0.1"
  If InStr(lblPatientEndurance, ".") = 0 Then lblPatientEndurance = lblPatientEndurance & ".0"
  Call lblTotal_Update
End Sub

Private Sub cmbPatientEndurance_LostFocus()
  Call cmbPatientEndurance_Click
End Sub

Private Sub cmbTechLevel_Click()
  lblTechLevel = CStr(Val(cmbTechLevel.Text))
  If CDbl(lblTechLevel) < 0.1 Then lblTechLevel = "0.1"
  If InStr(lblTechLevel, ".") = 0 Then lblTechLevel = lblTechLevel & ".0"
  Call lblTotal_Update
End Sub

Private Sub cmbTechLevel_LostFocus()
  Call cmbTechLevel_Click
End Sub

Private Sub cmbRest_Click()
  lblRest = CStr(cmbRest.ItemData(cmbRest.ListIndex) / 100)
  If InStr(lblRest, ".") = 0 Then lblRest = lblRest & ".0"
  Call lblTotal_Update
End Sub

Private Sub cmbMultipleInjuries_Click()
  lblMultipleInjuries = CStr(cmbMultipleInjuries.ItemData(cmbMultipleInjuries.ListIndex) / 100)
  If InStr(lblMultipleInjuries, ".") = 0 Then lblMultipleInjuries = lblMultipleInjuries & ".0"
  Call lblTotal_Update
End Sub

' all updates to any remaining fields updates results
Private Sub txtRoll_Change()
  Call lblTotal_Update
End Sub

Private Sub txtPhysicianSkill_Change()
  Call lblTotal_Update
End Sub

Private Sub txtOther1_Change()
  Call lblTotal_Update
End Sub

Private Sub txtOther2_Change()
  Call lblTotal_Update
End Sub

Private Sub txtMisc_Change(Index As Integer)
  Call lblTotal_Update
End Sub

Private Sub txtPoints_Change()
  Call lblTotal_Update
End Sub

' update all the totals and results everywhere
Private Sub lblTotal_Update()
  Dim i As Integer
  Dim rate As Double
  Dim resultname As String
  Dim dummy1 As String
  Dim outcome As String
  
  If UserInput = False Then Exit Sub
  
  ' calculate total
  i = Val(txtRoll) + Val(txtPhysicianSkill)
  If chkDiagnosis Then i = i + Val(lblDiagnosis)
  If chkTreatingSelf Then i = i + Val(lblTreatingSelf)
  If chkPhysicianTime Then i = i + Val(lblPhysicianTime)
  If chkPatientState Then i = i + Val(lblPatientState)
  If chkOther1 Then i = i + Val(txtOther1)
  If chkOther2 Then i = i + Val(txtOther2)
  
  ' fill in total and lookup
  lblTotal = i
  ManeuverChart i, True, dummy1, resultname, outcome
  lblResultName = resultname
  
  ' also update lblMedicalAssistance
  lblOutcome = ""
  Select Case resultname
    Case "Fumble"
      rate = 0.5
    Case "Critical Success"
      rate = 3
    Case "Extraordinary Failure"
      lblOutcome = "The medic makes the wound much worse, or something impossible to heal (e.g., bones don't set right)."
      lblMedicalAssistance = "n/a"
      Exit Sub
    Case "Extraordinary Success"
      If OutcomeHours = 0 Then
          lblOutcome = "The wound heals completely in an amount of time determined by the GM."
        Else
          lblOutcome = "The wound heals completely in " & ShowTime(CDbl(OutcomeHours)) & "."
        End If
      lblMedicalAssistance = "n/a"
      Exit Sub
    Case "Unrelated Success"
      rate = 1.95
    Case Else
      rate = 1 + Int(Left(resultname, Len(resultname) - 1)) / 100
    End Select
  lblMedicalAssistance = rate
  If InStr(lblMedicalAssistance, ".") = 0 Then lblMedicalAssistance = lblMedicalAssistance & ".0"
  
  ' calculate the healing factor
  rate = 1
  If chkPatientEndurance Then rate = rate * CDbl(lblPatientEndurance)
  If chkMedicalAssistance And lblMedicalAssistance <> "n/a" Then rate = rate * CDbl(lblMedicalAssistance)
  If chkTechLevel Then rate = rate * CDbl(lblTechLevel)
  If chkRest Then rate = rate * CDbl(lblRest)
  If chkMultipleInjuries Then rate = rate * CDbl(lblMultipleInjuries)
  For i = 0 To 5
    If chkMisc(i) And IsNumeric(txtMisc(i)) Then rate = rate * CDbl(txtMisc(i))
    Next i
    
  ' update the outcome
  lblOutcome = "Healing factor is " & CStr(rate)
  If InStr(lblOutcome, ".") = 0 Then lblOutcome = lblOutcome & ".0"
  lblOutcome = lblOutcome & ".  Time to heal:" & vbCrLf
  If OutcomeHours = 0 Then
      lblOutcome = lblOutcome & "See the rules for how to interpret this result."
    Else
      lblOutcome = lblOutcome & "� 1pt: " & ShowTime(OutcomeHours / rate) & vbCrLf
      If txtPoints <> "" And txtPoints <> "0" Then
          lblOutcome = lblOutcome & "� fully: " & ShowTime(Val(txtPoints) * OutcomeHours / rate)
        End If
    End If
End Sub

Private Sub cmdOK_Click()
  Me.Hide
End Sub

Private Sub imgDice_Click()
  frmIRIS.imgDice_Click
End Sub

Private Sub Form_Unload(Cancel As Integer)
  mciControl.Command = "Close"
End Sub

