VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmCriticalChartEditor 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Critical Chart Editor"
   ClientHeight    =   7965
   ClientLeft      =   5370
   ClientTop       =   1695
   ClientWidth     =   8445
   Icon            =   "frmCriticalChartEditor.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7965
   ScaleWidth      =   8445
   Begin VB.Frame fraEntry 
      Caption         =   "Critical Chart Entry"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7575
      Left            =   4920
      TabIndex        =   155
      Top             =   360
      Width           =   3495
      Begin VB.CommandButton cmdNext 
         Caption         =   "Next >>"
         Default         =   -1  'True
         Height          =   375
         Left            =   2160
         TabIndex        =   17
         Top             =   7080
         Width           =   1215
      End
      Begin VB.TextBox txtDescription 
         Height          =   1215
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   0
         Top             =   480
         Width           =   3255
      End
      Begin VB.TextBox txtHealthChanges 
         Height          =   495
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   16
         Top             =   6480
         Width           =   3255
      End
      Begin VB.ComboBox cmbNewPosition 
         Height          =   315
         ItemData        =   "frmCriticalChartEditor.frx":0442
         Left            =   1680
         List            =   "frmCriticalChartEditor.frx":0467
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   5880
         Width           =   1695
      End
      Begin VB.TextBox txtAttackerBonus 
         Height          =   285
         Left            =   2640
         TabIndex        =   14
         Top             =   5520
         Width           =   735
      End
      Begin VB.TextBox txtAttackerBonusRounds 
         Height          =   285
         Left            =   1680
         TabIndex        =   13
         Top             =   5520
         Width           =   615
      End
      Begin VB.TextBox txtBonusPenalty 
         Height          =   285
         Left            =   1680
         TabIndex        =   12
         Top             =   5160
         Width           =   1695
      End
      Begin VB.TextBox txtTmpBP 
         Height          =   285
         Left            =   2640
         TabIndex        =   11
         Top             =   4800
         Width           =   735
      End
      Begin VB.TextBox txtTmpBPRounds 
         Height          =   285
         Left            =   1680
         TabIndex        =   10
         Top             =   4800
         Width           =   615
      End
      Begin VB.TextBox txtMustParryBP 
         Height          =   285
         Left            =   2640
         TabIndex        =   9
         Top             =   4440
         Width           =   735
      End
      Begin VB.TextBox txtMustParryRounds 
         Height          =   285
         Left            =   1680
         TabIndex        =   8
         Top             =   4440
         Width           =   615
      End
      Begin VB.TextBox txtRoundsTillDead 
         Height          =   285
         Left            =   1680
         TabIndex        =   7
         Top             =   3960
         Width           =   1695
      End
      Begin VB.TextBox txtRoundsTillDown 
         Height          =   285
         Left            =   1680
         TabIndex        =   6
         Top             =   3600
         Width           =   1695
      End
      Begin VB.TextBox txtLostInits 
         Height          =   285
         Left            =   1680
         TabIndex        =   5
         Top             =   3240
         Width           =   1695
      End
      Begin VB.TextBox txtStunNoParries 
         Height          =   285
         Left            =   1680
         TabIndex        =   4
         Top             =   2880
         Width           =   1695
      End
      Begin VB.TextBox txtStuns 
         Height          =   285
         Left            =   1680
         TabIndex        =   3
         Top             =   2520
         Width           =   1695
      End
      Begin VB.TextBox txtBleeding 
         Height          =   285
         Left            =   1680
         TabIndex        =   2
         Top             =   2160
         Width           =   1695
      End
      Begin VB.TextBox txtBonusHits 
         Height          =   285
         Left            =   1680
         TabIndex        =   1
         Top             =   1800
         Width           =   1695
      End
      Begin VB.Label Label63 
         Caption         =   "Description:"
         Height          =   255
         Left            =   120
         TabIndex        =   172
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label62 
         Caption         =   "Health Changes:"
         Height          =   255
         Left            =   120
         TabIndex        =   171
         Top             =   6240
         Width           =   1215
      End
      Begin VB.Label Label61 
         Alignment       =   1  'Right Justify
         Caption         =   "New Position"
         Height          =   255
         Left            =   120
         TabIndex        =   170
         Top             =   5925
         Width           =   1455
      End
      Begin VB.Label Label60 
         Alignment       =   2  'Center
         Caption         =   "at"
         Height          =   255
         Left            =   2280
         TabIndex        =   169
         Top             =   5565
         Width           =   375
      End
      Begin VB.Label Label59 
         Alignment       =   1  'Right Justify
         Caption         =   "Attacker Bonus"
         Height          =   255
         Left            =   120
         TabIndex        =   168
         Top             =   5565
         Width           =   1455
      End
      Begin VB.Label Label58 
         Alignment       =   1  'Right Justify
         Caption         =   "Bonus/Penalty"
         Height          =   255
         Left            =   120
         TabIndex        =   167
         Top             =   5205
         Width           =   1455
      End
      Begin VB.Label Label57 
         Alignment       =   2  'Center
         Caption         =   "at"
         Height          =   255
         Left            =   2280
         TabIndex        =   166
         Top             =   4845
         Width           =   375
      End
      Begin VB.Label Label56 
         Alignment       =   2  'Center
         Caption         =   "at"
         Height          =   255
         Left            =   2280
         TabIndex        =   165
         Top             =   4485
         Width           =   375
      End
      Begin VB.Label Label55 
         Alignment       =   1  'Right Justify
         Caption         =   "Must Parry"
         Height          =   255
         Left            =   120
         TabIndex        =   164
         Top             =   4485
         Width           =   1455
      End
      Begin VB.Label Label54 
         Alignment       =   1  'Right Justify
         Caption         =   "Dead In"
         Height          =   255
         Left            =   120
         TabIndex        =   163
         Top             =   4005
         Width           =   1455
      End
      Begin VB.Label Label53 
         Alignment       =   1  'Right Justify
         Caption         =   "Down In"
         Height          =   255
         Left            =   120
         TabIndex        =   162
         Top             =   3645
         Width           =   1455
      End
      Begin VB.Label Label52 
         Alignment       =   1  'Right Justify
         Caption         =   "Lost Inits"
         Height          =   255
         Left            =   120
         TabIndex        =   161
         Top             =   3285
         Width           =   1455
      End
      Begin VB.Label Label51 
         Alignment       =   1  'Right Justify
         Caption         =   "Stun/No-Parries"
         Height          =   255
         Left            =   120
         TabIndex        =   160
         Top             =   2925
         Width           =   1455
      End
      Begin VB.Label Label50 
         Alignment       =   1  'Right Justify
         Caption         =   "Stuns"
         Height          =   255
         Left            =   120
         TabIndex        =   159
         Top             =   2565
         Width           =   1455
      End
      Begin VB.Label Label49 
         Alignment       =   1  'Right Justify
         Caption         =   "Bleeding"
         Height          =   255
         Left            =   120
         TabIndex        =   158
         Top             =   2205
         Width           =   1455
      End
      Begin VB.Line Line54 
         X1              =   3360
         X2              =   120
         Y1              =   4320
         Y2              =   4320
      End
      Begin VB.Label Label48 
         Alignment       =   1  'Right Justify
         Caption         =   "Tmp Bonus/Penalty"
         Height          =   255
         Left            =   120
         TabIndex        =   157
         Top             =   4845
         Width           =   1455
      End
      Begin VB.Label Label47 
         Alignment       =   1  'Right Justify
         Caption         =   "Bonus Hits"
         Height          =   255
         Left            =   120
         TabIndex        =   156
         Top             =   1845
         Width           =   1455
      End
   End
   Begin VB.CommandButton cmdE 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   16
      Left            =   4080
      TabIndex        =   108
      Top             =   7560
      Width           =   735
   End
   Begin VB.CommandButton cmdD 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   16
      Left            =   3360
      TabIndex        =   91
      Top             =   7560
      Width           =   735
   End
   Begin VB.CommandButton cmdC 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   16
      Left            =   2640
      TabIndex        =   74
      Top             =   7560
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   16
      Left            =   1920
      TabIndex        =   57
      Top             =   7560
      Width           =   735
   End
   Begin VB.CommandButton cmdA 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   16
      Left            =   1200
      TabIndex        =   40
      Top             =   7560
      Width           =   735
   End
   Begin VB.CommandButton cmdE 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   15
      Left            =   4080
      TabIndex        =   107
      Top             =   6840
      Width           =   735
   End
   Begin VB.CommandButton cmdD 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   15
      Left            =   3360
      TabIndex        =   90
      Top             =   6840
      Width           =   735
   End
   Begin VB.CommandButton cmdC 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   15
      Left            =   2640
      TabIndex        =   73
      Top             =   6840
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   15
      Left            =   1920
      TabIndex        =   56
      Top             =   6840
      Width           =   735
   End
   Begin VB.CommandButton cmdA 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   15
      Left            =   1200
      TabIndex        =   39
      Top             =   6840
      Width           =   735
   End
   Begin VB.CommandButton cmdE 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   14
      Left            =   4080
      TabIndex        =   106
      Top             =   6480
      Width           =   735
   End
   Begin VB.CommandButton cmdD 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   14
      Left            =   3360
      TabIndex        =   89
      Top             =   6480
      Width           =   735
   End
   Begin VB.CommandButton cmdC 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   14
      Left            =   2640
      TabIndex        =   72
      Top             =   6480
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   14
      Left            =   1920
      TabIndex        =   55
      Top             =   6480
      Width           =   735
   End
   Begin VB.CommandButton cmdA 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   14
      Left            =   1200
      TabIndex        =   38
      Top             =   6480
      Width           =   735
   End
   Begin VB.CommandButton cmdE 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   13
      Left            =   4080
      TabIndex        =   105
      Top             =   6120
      Width           =   735
   End
   Begin VB.CommandButton cmdD 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   13
      Left            =   3360
      TabIndex        =   88
      Top             =   6120
      Width           =   735
   End
   Begin VB.CommandButton cmdC 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   13
      Left            =   2640
      TabIndex        =   71
      Top             =   6120
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   13
      Left            =   1920
      TabIndex        =   54
      Top             =   6120
      Width           =   735
   End
   Begin VB.CommandButton cmdA 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   13
      Left            =   1200
      TabIndex        =   37
      Top             =   6120
      Width           =   735
   End
   Begin VB.CommandButton cmdE 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   12
      Left            =   4080
      TabIndex        =   104
      Top             =   5760
      Width           =   735
   End
   Begin VB.CommandButton cmdD 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   12
      Left            =   3360
      TabIndex        =   87
      Top             =   5760
      Width           =   735
   End
   Begin VB.CommandButton cmdC 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   12
      Left            =   2640
      TabIndex        =   70
      Top             =   5760
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   12
      Left            =   1920
      TabIndex        =   53
      Top             =   5760
      Width           =   735
   End
   Begin VB.CommandButton cmdA 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   12
      Left            =   1200
      TabIndex        =   36
      Top             =   5760
      Width           =   735
   End
   Begin VB.CommandButton cmdE 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   11
      Left            =   4080
      TabIndex        =   103
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton cmdD 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   11
      Left            =   3360
      TabIndex        =   86
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton cmdC 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   11
      Left            =   2640
      TabIndex        =   69
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   11
      Left            =   1920
      TabIndex        =   52
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton cmdA 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   11
      Left            =   1200
      TabIndex        =   35
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton cmdE 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   10
      Left            =   4080
      TabIndex        =   102
      Top             =   5040
      Width           =   735
   End
   Begin VB.CommandButton cmdD 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   10
      Left            =   3360
      TabIndex        =   85
      Top             =   5040
      Width           =   735
   End
   Begin VB.CommandButton cmdC 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   10
      Left            =   2640
      TabIndex        =   68
      Top             =   5040
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   10
      Left            =   1920
      TabIndex        =   51
      Top             =   5040
      Width           =   735
   End
   Begin VB.CommandButton cmdA 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   10
      Left            =   1200
      TabIndex        =   34
      Top             =   5040
      Width           =   735
   End
   Begin VB.CommandButton cmdE 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   9
      Left            =   4080
      TabIndex        =   101
      Top             =   4320
      Width           =   735
   End
   Begin VB.CommandButton cmdD 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   9
      Left            =   3360
      TabIndex        =   84
      Top             =   4320
      Width           =   735
   End
   Begin VB.CommandButton cmdC 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   9
      Left            =   2640
      TabIndex        =   67
      Top             =   4320
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   9
      Left            =   1920
      TabIndex        =   50
      Top             =   4320
      Width           =   735
   End
   Begin VB.CommandButton cmdA 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   9
      Left            =   1200
      TabIndex        =   33
      Top             =   4320
      Width           =   735
   End
   Begin VB.CommandButton cmdE 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   8
      Left            =   4080
      TabIndex        =   100
      Top             =   3960
      Width           =   735
   End
   Begin VB.CommandButton cmdD 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   8
      Left            =   3360
      TabIndex        =   83
      Top             =   3960
      Width           =   735
   End
   Begin VB.CommandButton cmdC 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   8
      Left            =   2640
      TabIndex        =   66
      Top             =   3960
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   8
      Left            =   1920
      TabIndex        =   49
      Top             =   3960
      Width           =   735
   End
   Begin VB.CommandButton cmdA 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   8
      Left            =   1200
      TabIndex        =   32
      Top             =   3960
      Width           =   735
   End
   Begin VB.CommandButton cmdE 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   7
      Left            =   4080
      TabIndex        =   99
      Top             =   3600
      Width           =   735
   End
   Begin VB.CommandButton cmdD 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   7
      Left            =   3360
      TabIndex        =   82
      Top             =   3600
      Width           =   735
   End
   Begin VB.CommandButton cmdC 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   7
      Left            =   2640
      TabIndex        =   65
      Top             =   3600
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   7
      Left            =   1920
      TabIndex        =   48
      Top             =   3600
      Width           =   735
   End
   Begin VB.CommandButton cmdA 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   7
      Left            =   1200
      TabIndex        =   31
      Top             =   3600
      Width           =   735
   End
   Begin VB.CommandButton cmdE 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   6
      Left            =   4080
      TabIndex        =   98
      Top             =   3240
      Width           =   735
   End
   Begin VB.CommandButton cmdD 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   6
      Left            =   3360
      TabIndex        =   81
      Top             =   3240
      Width           =   735
   End
   Begin VB.CommandButton cmdC 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   6
      Left            =   2640
      TabIndex        =   64
      Top             =   3240
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   6
      Left            =   1920
      TabIndex        =   47
      Top             =   3240
      Width           =   735
   End
   Begin VB.CommandButton cmdA 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   6
      Left            =   1200
      TabIndex        =   30
      Top             =   3240
      Width           =   735
   End
   Begin VB.CommandButton cmdE 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   5
      Left            =   4080
      TabIndex        =   97
      Top             =   2880
      Width           =   735
   End
   Begin VB.CommandButton cmdD 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   5
      Left            =   3360
      TabIndex        =   80
      Top             =   2880
      Width           =   735
   End
   Begin VB.CommandButton cmdC 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   5
      Left            =   2640
      TabIndex        =   63
      Top             =   2880
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   5
      Left            =   1920
      TabIndex        =   46
      Top             =   2880
      Width           =   735
   End
   Begin VB.CommandButton cmdA 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   5
      Left            =   1200
      TabIndex        =   29
      Top             =   2880
      Width           =   735
   End
   Begin VB.CommandButton cmdE 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   4
      Left            =   4080
      TabIndex        =   96
      Top             =   2520
      Width           =   735
   End
   Begin VB.CommandButton cmdD 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   4
      Left            =   3360
      TabIndex        =   79
      Top             =   2520
      Width           =   735
   End
   Begin VB.CommandButton cmdC 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   4
      Left            =   2640
      TabIndex        =   62
      Top             =   2520
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   4
      Left            =   1920
      TabIndex        =   45
      Top             =   2520
      Width           =   735
   End
   Begin VB.CommandButton cmdA 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   4
      Left            =   1200
      TabIndex        =   28
      Top             =   2520
      Width           =   735
   End
   Begin VB.CommandButton cmdE 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   3
      Left            =   4080
      TabIndex        =   95
      Top             =   2160
      Width           =   735
   End
   Begin VB.CommandButton cmdD 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   3
      Left            =   3360
      TabIndex        =   78
      Top             =   2160
      Width           =   735
   End
   Begin VB.CommandButton cmdC 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   3
      Left            =   2640
      TabIndex        =   61
      Top             =   2160
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   3
      Left            =   1920
      TabIndex        =   44
      Top             =   2160
      Width           =   735
   End
   Begin VB.CommandButton cmdA 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   3
      Left            =   1200
      TabIndex        =   27
      Top             =   2160
      Width           =   735
   End
   Begin VB.CommandButton cmdE 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   4080
      TabIndex        =   94
      Top             =   1800
      Width           =   735
   End
   Begin VB.CommandButton cmdD 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   3360
      TabIndex        =   77
      Top             =   1800
      Width           =   735
   End
   Begin VB.CommandButton cmdC 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   2640
      TabIndex        =   60
      Top             =   1800
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   1920
      TabIndex        =   43
      Top             =   1800
      Width           =   735
   End
   Begin VB.CommandButton cmdA 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   1200
      TabIndex        =   26
      Top             =   1800
      Width           =   735
   End
   Begin VB.CommandButton cmdE 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   4080
      TabIndex        =   93
      Top             =   1440
      Width           =   735
   End
   Begin VB.CommandButton cmdD 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   3360
      TabIndex        =   76
      Top             =   1440
      Width           =   735
   End
   Begin VB.CommandButton cmdC 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   2640
      TabIndex        =   59
      Top             =   1440
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   1920
      TabIndex        =   42
      Top             =   1440
      Width           =   735
   End
   Begin VB.CommandButton cmdA 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   1200
      TabIndex        =   25
      Top             =   1440
      Width           =   735
   End
   Begin VB.CommandButton cmdE 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   4080
      TabIndex        =   92
      Top             =   1080
      Width           =   735
   End
   Begin VB.CommandButton cmdD 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   3360
      TabIndex        =   75
      Top             =   1080
      Width           =   735
   End
   Begin VB.CommandButton cmdC 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   2640
      TabIndex        =   58
      Top             =   1080
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   1920
      TabIndex        =   41
      Top             =   1080
      Width           =   735
   End
   Begin VB.CommandButton cmdA 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   1200
      TabIndex        =   24
      Top             =   1080
      Width           =   735
   End
   Begin VB.TextBox txtCode 
      Height          =   285
      Left            =   4440
      MaxLength       =   3
      TabIndex        =   22
      Top             =   30
      Width           =   495
   End
   Begin VB.TextBox txtName 
      Height          =   285
      Left            =   5760
      TabIndex        =   23
      Top             =   30
      Width           =   2655
   End
   Begin VB.CommandButton cmdSaveAs 
      Caption         =   "Save As..."
      Enabled         =   0   'False
      Height          =   375
      Left            =   2880
      TabIndex        =   21
      Top             =   0
      Width           =   975
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1920
      TabIndex        =   20
      Top             =   0
      Width           =   975
   End
   Begin VB.CommandButton cmdOpen 
      Caption         =   "Open"
      Height          =   375
      Left            =   960
      TabIndex        =   19
      Top             =   0
      Width           =   975
   End
   Begin VB.CommandButton cmdNew 
      Caption         =   "New"
      Height          =   375
      Left            =   0
      TabIndex        =   18
      Top             =   0
      Width           =   975
   End
   Begin MSComDlg.CommonDialog cdlGeneric 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Shape Shape3 
      FillColor       =   &H000000FF&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1200
      Top             =   7560
      Width           =   3615
   End
   Begin VB.Shape Shape2 
      FillColor       =   &H000000FF&
      FillStyle       =   0  'Solid
      Height          =   2175
      Left            =   1200
      Top             =   5040
      Width           =   3615
   End
   Begin VB.Shape Shape1 
      FillColor       =   &H000000FF&
      FillStyle       =   0  'Solid
      Height          =   3615
      Left            =   1200
      Top             =   1080
      Width           =   3615
   End
   Begin VB.Line Line53 
      X1              =   1920
      X2              =   1200
      Y1              =   7200
      Y2              =   7560
   End
   Begin VB.Line Line52 
      X1              =   2640
      X2              =   1920
      Y1              =   7200
      Y2              =   7560
   End
   Begin VB.Line Line51 
      X1              =   3360
      X2              =   2640
      Y1              =   7200
      Y2              =   7560
   End
   Begin VB.Line Line50 
      X1              =   4080
      X2              =   3360
      Y1              =   7200
      Y2              =   7560
   End
   Begin VB.Line Line49 
      X1              =   4800
      X2              =   4080
      Y1              =   7200
      Y2              =   7560
   End
   Begin VB.Line Line48 
      X1              =   4080
      X2              =   4800
      Y1              =   7200
      Y2              =   7560
   End
   Begin VB.Line Line47 
      X1              =   3360
      X2              =   4080
      Y1              =   7200
      Y2              =   7560
   End
   Begin VB.Line Line46 
      X1              =   2640
      X2              =   3360
      Y1              =   7200
      Y2              =   7560
   End
   Begin VB.Line Line45 
      X1              =   1920
      X2              =   2640
      Y1              =   7200
      Y2              =   7560
   End
   Begin VB.Line Line44 
      X1              =   1200
      X2              =   1920
      Y1              =   7200
      Y2              =   7560
   End
   Begin VB.Line Line43 
      X1              =   1920
      X2              =   2640
      Y1              =   4680
      Y2              =   5040
   End
   Begin VB.Line Line42 
      X1              =   3360
      X2              =   2640
      Y1              =   4680
      Y2              =   5040
   End
   Begin VB.Line Line41 
      X1              =   4080
      X2              =   3360
      Y1              =   4680
      Y2              =   5040
   End
   Begin VB.Line Line40 
      X1              =   4800
      X2              =   4080
      Y1              =   4680
      Y2              =   5040
   End
   Begin VB.Line Line39 
      X1              =   4080
      X2              =   4800
      Y1              =   4680
      Y2              =   5040
   End
   Begin VB.Line Line38 
      X1              =   3360
      X2              =   4080
      Y1              =   4680
      Y2              =   5040
   End
   Begin VB.Line Line37 
      X1              =   2640
      X2              =   3360
      Y1              =   4680
      Y2              =   5040
   End
   Begin VB.Line Line36 
      X1              =   1920
      X2              =   2640
      Y1              =   5040
      Y2              =   4680
   End
   Begin VB.Line Line35 
      X1              =   1200
      X2              =   1920
      Y1              =   5040
      Y2              =   4680
   End
   Begin VB.Line Line34 
      X1              =   1200
      X2              =   1920
      Y1              =   4680
      Y2              =   5040
   End
   Begin VB.Line Line33 
      X1              =   720
      X2              =   1200
      Y1              =   7200
      Y2              =   7560
   End
   Begin VB.Line Line32 
      X1              =   720
      X2              =   1200
      Y1              =   7560
      Y2              =   7200
   End
   Begin VB.Line Line31 
      X1              =   720
      X2              =   1200
      Y1              =   4680
      Y2              =   5040
   End
   Begin VB.Line Line30 
      X1              =   720
      X2              =   1200
      Y1              =   5040
      Y2              =   4680
   End
   Begin VB.Label Label46 
      Caption         =   "Name:"
      Height          =   255
      Left            =   5160
      TabIndex        =   154
      Top             =   75
      Width           =   495
   End
   Begin VB.Label Label45 
      Caption         =   "Code:"
      Height          =   255
      Left            =   3960
      TabIndex        =   153
      Top             =   75
      Width           =   495
   End
   Begin VB.Label Label44 
      Alignment       =   2  'Center
      Caption         =   "20"
      Height          =   240
      Left            =   840
      TabIndex        =   152
      Top             =   7635
      Width           =   255
   End
   Begin VB.Label Label43 
      Alignment       =   2  'Center
      Caption         =   "19"
      Height          =   240
      Left            =   840
      TabIndex        =   151
      Top             =   6915
      Width           =   255
   End
   Begin VB.Label Label42 
      Alignment       =   2  'Center
      Caption         =   "18"
      Height          =   240
      Left            =   840
      TabIndex        =   150
      Top             =   6555
      Width           =   255
   End
   Begin VB.Label Label41 
      Alignment       =   2  'Center
      Caption         =   "17"
      Height          =   240
      Left            =   840
      TabIndex        =   149
      Top             =   6195
      Width           =   255
   End
   Begin VB.Label Label40 
      Alignment       =   2  'Center
      Caption         =   "16"
      Height          =   240
      Left            =   840
      TabIndex        =   148
      Top             =   5835
      Width           =   255
   End
   Begin VB.Label Label39 
      Alignment       =   2  'Center
      Caption         =   "15"
      Height          =   240
      Left            =   840
      TabIndex        =   147
      Top             =   5475
      Width           =   255
   End
   Begin VB.Label Label38 
      Alignment       =   2  'Center
      Caption         =   "14"
      Height          =   240
      Left            =   840
      TabIndex        =   146
      Top             =   5115
      Width           =   255
   End
   Begin VB.Label Label37 
      Alignment       =   2  'Center
      Caption         =   "13"
      Height          =   240
      Left            =   840
      TabIndex        =   145
      Top             =   4395
      Width           =   255
   End
   Begin VB.Label Label36 
      Alignment       =   2  'Center
      Caption         =   "12"
      Height          =   240
      Left            =   840
      TabIndex        =   144
      Top             =   4035
      Width           =   255
   End
   Begin VB.Label Label35 
      Alignment       =   2  'Center
      Caption         =   "11"
      Height          =   240
      Left            =   840
      TabIndex        =   143
      Top             =   3675
      Width           =   255
   End
   Begin VB.Label Label34 
      Alignment       =   2  'Center
      Caption         =   "10"
      Height          =   240
      Left            =   840
      TabIndex        =   142
      Top             =   3315
      Width           =   255
   End
   Begin VB.Label Label33 
      Alignment       =   2  'Center
      Caption         =   "5-7"
      Height          =   240
      Left            =   840
      TabIndex        =   141
      Top             =   2595
      Width           =   255
   End
   Begin VB.Label Label32 
      Alignment       =   2  'Center
      Caption         =   "4"
      Height          =   240
      Left            =   840
      TabIndex        =   140
      Top             =   2235
      Width           =   255
   End
   Begin VB.Label Label31 
      Alignment       =   2  'Center
      Caption         =   "3"
      Height          =   240
      Left            =   840
      TabIndex        =   139
      Top             =   1875
      Width           =   255
   End
   Begin VB.Label Label30 
      Alignment       =   2  'Center
      Caption         =   "2"
      Height          =   240
      Left            =   840
      TabIndex        =   138
      Top             =   1515
      Width           =   255
   End
   Begin VB.Label Label29 
      Alignment       =   2  'Center
      Caption         =   "1"
      Height          =   240
      Left            =   840
      TabIndex        =   137
      Top             =   1155
      Width           =   255
   End
   Begin VB.Line Line29 
      X1              =   4800
      X2              =   0
      Y1              =   7560
      Y2              =   7560
   End
   Begin VB.Line Line28 
      X1              =   4800
      X2              =   0
      Y1              =   7200
      Y2              =   7200
   End
   Begin VB.Line Line27 
      X1              =   4800
      X2              =   0
      Y1              =   6840
      Y2              =   6840
   End
   Begin VB.Line Line26 
      X1              =   4800
      X2              =   0
      Y1              =   6480
      Y2              =   6480
   End
   Begin VB.Line Line25 
      X1              =   4800
      X2              =   0
      Y1              =   6120
      Y2              =   6120
   End
   Begin VB.Line Line24 
      X1              =   4800
      X2              =   0
      Y1              =   5760
      Y2              =   5760
   End
   Begin VB.Line Line23 
      X1              =   4800
      X2              =   0
      Y1              =   5400
      Y2              =   5400
   End
   Begin VB.Line Line22 
      X1              =   4800
      X2              =   0
      Y1              =   5040
      Y2              =   5040
   End
   Begin VB.Line Line21 
      X1              =   0
      X2              =   4800
      Y1              =   7920
      Y2              =   7920
   End
   Begin VB.Label Label28 
      Alignment       =   2  'Center
      Caption         =   "100"
      Height          =   255
      Left            =   120
      TabIndex        =   136
      Top             =   7635
      Width           =   495
   End
   Begin VB.Label Label27 
      Alignment       =   2  'Center
      Caption         =   "96-99"
      Height          =   255
      Left            =   120
      TabIndex        =   135
      Top             =   7275
      Width           =   495
   End
   Begin VB.Label Label26 
      Alignment       =   2  'Center
      Caption         =   "91-95"
      Height          =   255
      Left            =   120
      TabIndex        =   134
      Top             =   6915
      Width           =   495
   End
   Begin VB.Label Label25 
      Alignment       =   2  'Center
      Caption         =   "86-90"
      Height          =   255
      Left            =   120
      TabIndex        =   133
      Top             =   6555
      Width           =   495
   End
   Begin VB.Label Label24 
      Alignment       =   2  'Center
      Caption         =   "81-85"
      Height          =   255
      Left            =   120
      TabIndex        =   132
      Top             =   6195
      Width           =   495
   End
   Begin VB.Label Label23 
      Alignment       =   2  'Center
      Caption         =   "76-80"
      Height          =   255
      Left            =   120
      TabIndex        =   131
      Top             =   5835
      Width           =   495
   End
   Begin VB.Label Label22 
      Alignment       =   2  'Center
      Caption         =   "71-75"
      Height          =   255
      Left            =   120
      TabIndex        =   130
      Top             =   5475
      Width           =   495
   End
   Begin VB.Label Label21 
      Alignment       =   2  'Center
      Caption         =   "67-70"
      Height          =   255
      Left            =   120
      TabIndex        =   129
      Top             =   5115
      Width           =   495
   End
   Begin VB.Label Label20 
      Alignment       =   2  'Center
      Caption         =   "66"
      Height          =   255
      Left            =   120
      TabIndex        =   128
      Top             =   4755
      Width           =   495
   End
   Begin VB.Line Line20 
      X1              =   0
      X2              =   4800
      Y1              =   4680
      Y2              =   4680
   End
   Begin VB.Line Line19 
      X1              =   0
      X2              =   4800
      Y1              =   4320
      Y2              =   4320
   End
   Begin VB.Line Line18 
      X1              =   0
      X2              =   4800
      Y1              =   3960
      Y2              =   3960
   End
   Begin VB.Line Line17 
      X1              =   0
      X2              =   4800
      Y1              =   3600
      Y2              =   3600
   End
   Begin VB.Line Line16 
      X1              =   0
      X2              =   4800
      Y1              =   3240
      Y2              =   3240
   End
   Begin VB.Line Line15 
      X1              =   0
      X2              =   4800
      Y1              =   2880
      Y2              =   2880
   End
   Begin VB.Line Line14 
      X1              =   0
      X2              =   4800
      Y1              =   2520
      Y2              =   2520
   End
   Begin VB.Line Line13 
      X1              =   0
      X2              =   4800
      Y1              =   2160
      Y2              =   2160
   End
   Begin VB.Label Label19 
      Alignment       =   2  'Center
      Caption         =   "61-65"
      Height          =   255
      Left            =   120
      TabIndex        =   127
      Top             =   4395
      Width           =   495
   End
   Begin VB.Label Label18 
      Alignment       =   2  'Center
      Caption         =   "56-60"
      Height          =   255
      Left            =   120
      TabIndex        =   126
      Top             =   4035
      Width           =   495
   End
   Begin VB.Label Label17 
      Alignment       =   2  'Center
      Caption         =   "51-55"
      Height          =   255
      Left            =   120
      TabIndex        =   125
      Top             =   3675
      Width           =   495
   End
   Begin VB.Label Label16 
      Alignment       =   2  'Center
      Caption         =   "46-50"
      Height          =   255
      Left            =   120
      TabIndex        =   124
      Top             =   3315
      Width           =   495
   End
   Begin VB.Label Label15 
      Alignment       =   2  'Center
      Caption         =   "36-45"
      Height          =   255
      Left            =   120
      TabIndex        =   123
      Top             =   2955
      Width           =   495
   End
   Begin VB.Label Label14 
      Alignment       =   2  'Center
      Caption         =   "21-35"
      Height          =   255
      Left            =   120
      TabIndex        =   122
      Top             =   2595
      Width           =   495
   End
   Begin VB.Label Label13 
      Alignment       =   2  'Center
      Caption         =   "16-20"
      Height          =   255
      Left            =   120
      TabIndex        =   121
      Top             =   2235
      Width           =   495
   End
   Begin VB.Label Label12 
      Alignment       =   2  'Center
      Caption         =   "11-15"
      Height          =   255
      Left            =   120
      TabIndex        =   120
      Top             =   1875
      Width           =   495
   End
   Begin VB.Line Line12 
      X1              =   4800
      X2              =   0
      Y1              =   1800
      Y2              =   1800
   End
   Begin VB.Label Label11 
      Alignment       =   2  'Center
      Caption         =   "06-10"
      Height          =   255
      Left            =   120
      TabIndex        =   119
      Top             =   1515
      Width           =   495
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      Caption         =   "d20"
      Height          =   210
      Left            =   765
      TabIndex        =   118
      Top             =   840
      Width           =   405
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      Caption         =   "d%"
      Height          =   210
      Left            =   120
      TabIndex        =   117
      Top             =   840
      Width           =   495
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      Caption         =   "Roll"
      Height          =   255
      Left            =   120
      TabIndex        =   116
      Top             =   600
      Width           =   975
   End
   Begin VB.Line Line11 
      X1              =   0
      X2              =   4800
      Y1              =   1440
      Y2              =   1440
   End
   Begin VB.Line Line10 
      X1              =   720
      X2              =   720
      Y1              =   7920
      Y2              =   1080
   End
   Begin VB.Line Line9 
      X1              =   0
      X2              =   0
      Y1              =   7920
      Y2              =   480
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      Caption         =   "8-9"
      Height          =   240
      Left            =   840
      TabIndex        =   115
      Top             =   2955
      Width           =   255
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      Caption         =   "01-05"
      Height          =   255
      Left            =   120
      TabIndex        =   114
      Top             =   1155
      Width           =   495
   End
   Begin VB.Line Line8 
      X1              =   4800
      X2              =   0
      Y1              =   1080
      Y2              =   1080
   End
   Begin VB.Line Line7 
      X1              =   1200
      X2              =   1200
      Y1              =   480
      Y2              =   7920
   End
   Begin VB.Line Line6 
      X1              =   4800
      X2              =   4800
      Y1              =   480
      Y2              =   7920
   End
   Begin VB.Line Line5 
      X1              =   4080
      X2              =   4080
      Y1              =   480
      Y2              =   7920
   End
   Begin VB.Line Line4 
      X1              =   3360
      X2              =   3360
      Y1              =   480
      Y2              =   7920
   End
   Begin VB.Line Line3 
      X1              =   2640
      X2              =   2640
      Y1              =   480
      Y2              =   7920
   End
   Begin VB.Line Line2 
      X1              =   1920
      X2              =   1920
      Y1              =   480
      Y2              =   7920
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Caption         =   "E"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4080
      TabIndex        =   113
      Top             =   720
      Width           =   735
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "D"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3360
      TabIndex        =   112
      Top             =   720
      Width           =   735
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "C"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2640
      TabIndex        =   111
      Top             =   720
      Width           =   735
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "B"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1920
      TabIndex        =   110
      Top             =   720
      Width           =   735
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "A"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1200
      TabIndex        =   109
      Top             =   720
      Width           =   735
   End
   Begin VB.Line Line1 
      X1              =   0
      X2              =   4800
      Y1              =   480
      Y2              =   480
   End
End
Attribute VB_Name = "frmCriticalChartEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Base 1

Const conButtonLength = 15

Dim Filename As String, Dirty As Boolean
Dim CurrentColumn As Integer, CurrentRow As Integer
Dim loadingentry As Boolean

Private Sub HighlightButton(ByVal column As Integer, ByVal row As Integer, ByVal highlight As Boolean)
  If column = 0 Or row = 0 Then Exit Sub
  If column = 1 Then
      cmdA(row - 1).Visible = Not highlight
    ElseIf column = 2 Then
      cmdB(row - 1).Visible = Not highlight
    ElseIf column = 3 Then
      cmdC(row - 1).Visible = Not highlight
    ElseIf column = 4 Then
      cmdD(row - 1).Visible = Not highlight
    Else
      cmdE(row - 1).Visible = Not highlight
    End If
End Sub

Public Function RowName(ByVal row As Integer) As String
  Select Case row
    Case 1
      RowName = "1 (01-05)"
    Case 2
      RowName = "2 (06-10)"
    Case 3
      RowName = "3 (11-15)"
    Case 4
      RowName = "4 (16-20)"
    Case 5
      RowName = "5-7 (21-35)"
    Case 6
      RowName = "8-9 (36-45)"
    Case 7
      RowName = "10 (46-50)"
    Case 8
      RowName = "11 (51-55)"
    Case 9
      RowName = "12 (56-60)"
    Case 10
      RowName = "13 (61-65)"
    Case 11
      RowName = "14 (67-70)"
    Case 12
      RowName = "15 (71-75)"
    Case 13
      RowName = "16 (76-80)"
    Case 14
      RowName = "17 (81-85)"
    Case 15
      RowName = "18 (86-90)"
    Case 16
      RowName = "19 (91-95)"
    Case 17
      RowName = "20 (100)"
    End Select
End Function

Private Sub Form_Load()
  Dim column As Integer, row As Integer
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
  ' initialize variables
  Filename = ""
  Dirty = False
  CurrentColumn = 0
  CurrentRow = 0
  loadingentry = False
  ' start with a blank record
  For column = 1 To 5
    For row = 1 To 17
      ClearCriticalEntry column, row
      Next row
    Next column
  Me.Show
  cmdA_Click (0)
  txtCode.SetFocus
  Form_EnableDisable
End Sub

Private Sub Form_EnableDisable()
  If txtCode <> "" And txtName <> "" Then
      cmdSaveAs.Enabled = True
      If Filename <> "" Then
          cmdSave.Enabled = True
        Else
          cmdSave.Enabled = False
        End If
    Else
      cmdSaveAs.Enabled = False
      cmdSave.Enabled = False
    End If
  Me.Caption = "Critical Chart Editor - " & _
      IIf(Filename = "", "(Untitled)", FilePart(Filename)) & IIf(Dirty, " *", "")
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Dim i As Integer
  If Dirty Then
     i = MsgBox("Save chart first?", vbYesNoCancel + vbQuestion, "Critical Chart Editor")
     If i = vbCancel Then
         Cancel = True
         Exit Sub
       End If
     If i = vbYes Then cmdSave_Click
    End If
End Sub

Private Sub cmdA_Click(Index As Integer)
  LoadCriticalEntry 1, Index + 1
End Sub

Private Sub cmdB_Click(Index As Integer)
  LoadCriticalEntry 2, Index + 1
End Sub

Private Sub cmdC_Click(Index As Integer)
  LoadCriticalEntry 3, Index + 1
End Sub

Private Sub cmdD_Click(Index As Integer)
  LoadCriticalEntry 4, Index + 1
End Sub

Private Sub cmdE_Click(Index As Integer)
  LoadCriticalEntry 5, Index + 1
End Sub

Private Sub cmdNext_Click()
  If CurrentRow = 17 And CurrentColumn = 5 Then
      LoadCriticalEntry 1, 1
    ElseIf CurrentRow = 17 Then
      LoadCriticalEntry CurrentColumn + 1, 1
    Else
      LoadCriticalEntry CurrentColumn, CurrentRow + 1
    End If
End Sub

Public Sub LoadCriticalEntry(ByVal column As Integer, ByVal row As Integer)
  ' load the fields
  loadingentry = True
  With CriticalChart(column, row)
    txtBonusHits = .BonusHits
    txtBleeding = .Bleeding
    txtStuns = .Stuns
    txtStunNoParries = .StunNoParries
    txtLostInits = .LostInits
    txtRoundsTillDown = .RoundsTillDown
    txtRoundsTillDead = .RoundsTillDead
    txtMustParryRounds = .MustParryRounds
    txtMustParryBP = .MustParryBP
    txtTmpBPRounds = .TmpBPRounds
    txtTmpBP = .TmpBP
    txtBonusPenalty = .BonusPenalty
    txtAttackerBonusRounds = .AttackerBonusRounds
    txtAttackerBonus = .AttackerBonus
    cmbNewPosition.ListIndex = .NewPosition
    txtHealthChanges = .HealthChanges
    txtDescription = .Description
    End With
  loadingentry = False
  ' set the frame title
  fraEntry.Caption = RowName(row) & " " & Mid("ABCDE", column, 1) & txtCode & " (" & txtName & ")"
  ' highlight the button for the selected row and column
  HighlightButton CurrentColumn, CurrentRow, False
  CurrentColumn = column
  CurrentRow = row
  HighlightButton CurrentColumn, CurrentRow, True
  txtDescription.SetFocus
End Sub

Public Sub SelectField(o As Object)
  o.SelStart = 0
  o.SelLength = Len(o)
End Sub

Public Function NoQuotes(ByVal KeyAscii As Integer) As Integer
  NoQuotes = KeyAscii
  If KeyAscii = 34 Then NoQuotes = Asc("'")
End Function

Public Function OnlyDigits(ByVal KeyAscii As Integer) As Integer
  OnlyDigits = KeyAscii
  If InStr("0123456789", Chr$(KeyAscii)) = 0 And _
      KeyAscii <> 8 And KeyAscii <> 127 Then
      OnlyDigits = 0
      Beep
    End If
End Function

Public Function OnlyDigitsAndMinus(ByVal KeyAscii As Integer, o As Object) As Integer
  OnlyDigitsAndMinus = KeyAscii
  If (o.SelStart <> 0 Or (Chr$(KeyAscii) <> "-" And Chr$(KeyAscii) <> "+")) And _
      InStr("0123456789", Chr$(KeyAscii)) = 0 And _
      KeyAscii <> 8 And KeyAscii <> 127 Then
      OnlyDigitsAndMinus = 0
      Beep
    End If
End Function

Private Sub txtBonusHits_GotFocus()
  Call SelectField(txtBonusHits)
End Sub

Private Sub txtDescription_KeyPress(KeyAscii As Integer)
  KeyAscii = NoQuotes(KeyAscii)
End Sub

Private Sub txtName_KeyPress(KeyAscii As Integer)
  KeyAscii = NoQuotes(KeyAscii)
End Sub

Private Sub txtHealthChanges_KeyPress(KeyAscii As Integer)
  KeyAscii = NoQuotes(KeyAscii)
End Sub

Private Sub txtCode_KeyPress(KeyAscii As Integer)
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
  If (KeyAscii < Asc("A") Or KeyAscii > Asc("Z")) And _
      KeyAscii <> 8 And KeyAscii <> 127 Then
      KeyAscii = 0
      Beep
    End If
End Sub

Private Sub txtBonusHits_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtBonusHits_Change()
  If loadingentry Then Exit Sub
  CriticalChart(CurrentColumn, CurrentRow).BonusHits = Val(txtBonusHits)
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub txtBleeding_GotFocus()
  Call SelectField(txtBleeding)
End Sub

Private Sub txtBleeding_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtBleeding_Change()
  If loadingentry Then Exit Sub
  CriticalChart(CurrentColumn, CurrentRow).Bleeding = Val(txtBleeding)
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub txtStuns_GotFocus()
  Call SelectField(txtStuns)
End Sub

Private Sub txtStuns_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtStuns_Change()
  If loadingentry Then Exit Sub
  CriticalChart(CurrentColumn, CurrentRow).Stuns = Val(txtStuns)
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub txtStunNoParries_GotFocus()
  Call SelectField(txtStunNoParries)
End Sub

Private Sub txtStunNoParries_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtStunNoParries_Change()
  If loadingentry Then Exit Sub
  CriticalChart(CurrentColumn, CurrentRow).StunNoParries = Val(txtStunNoParries)
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub txtLostInits_GotFocus()
  Call SelectField(txtLostInits)
End Sub

Private Sub txtLostInits_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtLostInits_Change()
  If loadingentry Then Exit Sub
  CriticalChart(CurrentColumn, CurrentRow).LostInits = Val(txtLostInits)
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub txtRoundsTillDown_GotFocus()
  Call SelectField(txtRoundsTillDown)
End Sub

Private Sub txtRoundsTillDown_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtRoundsTillDown_Change()
  If loadingentry Then Exit Sub
  CriticalChart(CurrentColumn, CurrentRow).RoundsTillDown = Val(txtRoundsTillDown)
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub txtRoundsTillDead_GotFocus()
  Call SelectField(txtRoundsTillDead)
End Sub

Private Sub txtRoundsTillDead_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtRoundsTillDead_Change()
  If loadingentry Then Exit Sub
  CriticalChart(CurrentColumn, CurrentRow).RoundsTillDead = Val(txtRoundsTillDead)
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub txtMustParryRounds_GotFocus()
  Call SelectField(txtMustParryRounds)
End Sub

Private Sub txtMustParryRounds_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtMustParryRounds_Change()
  If loadingentry Then Exit Sub
  CriticalChart(CurrentColumn, CurrentRow).MustParryRounds = Val(txtMustParryRounds)
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub txtMustParryBP_GotFocus()
  Call SelectField(txtMustParryBP)
End Sub

Private Sub txtMustParryBP_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii, txtMustParryBP)
End Sub

Private Sub txtMustParryBP_Change()
  If loadingentry Then Exit Sub
  If txtMustParryBP = "-" Then
      CriticalChart(CurrentColumn, CurrentRow).MustParryBP = 0
    Else
      CriticalChart(CurrentColumn, CurrentRow).MustParryBP = Val(txtMustParryBP)
    End If
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub txtTmpBPRounds_GotFocus()
  Call SelectField(txtTmpBPRounds)
End Sub

Private Sub txtTmpBPRounds_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtTmpBPRounds_Change()
  If loadingentry Then Exit Sub
  CriticalChart(CurrentColumn, CurrentRow).TmpBPRounds = Val(txtTmpBPRounds)
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub txtTmpBP_GotFocus()
  Call SelectField(txtTmpBP)
End Sub

Private Sub txtTmpBP_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii, txtTmpBP)
End Sub

Private Sub txtTmpBP_Change()
  If loadingentry Then Exit Sub
  If txtTmpBP = "-" Then
      CriticalChart(CurrentColumn, CurrentRow).TmpBP = 0
    Else
      CriticalChart(CurrentColumn, CurrentRow).TmpBP = Val(txtTmpBP)
    End If
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub txtBonusPenalty_GotFocus()
  Call SelectField(txtBonusPenalty)
End Sub

Private Sub txtBonusPenalty_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii, txtBonusPenalty)
End Sub

Private Sub txtBonusPenalty_Change()
  If loadingentry Then Exit Sub
  If txtBonusPenalty = "-" Then
      CriticalChart(CurrentColumn, CurrentRow).BonusPenalty = 0
    Else
      CriticalChart(CurrentColumn, CurrentRow).BonusPenalty = Val(txtBonusPenalty)
    End If
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub txtAttackerBonusRounds_GotFocus()
  Call SelectField(txtAttackerBonusRounds)
End Sub

Private Sub txtAttackerBonusRounds_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtAttackerBonusRounds_Change()
  If loadingentry Then Exit Sub
  CriticalChart(CurrentColumn, CurrentRow).AttackerBonusRounds = Val(txtAttackerBonusRounds)
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub txtAttackerBonus_GotFocus()
  Call SelectField(txtAttackerBonus)
End Sub

Private Sub txtAttackerBonus_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii, txtAttackerBonus)
End Sub

Private Sub txtAttackerBonus_Change()
  If loadingentry Then Exit Sub
  If txtAttackerBonus = "-" Then
      CriticalChart(CurrentColumn, CurrentRow).AttackerBonus = 0
    Else
      CriticalChart(CurrentColumn, CurrentRow).AttackerBonus = Val(txtAttackerBonus)
    End If
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub cmbNewPosition_Click()
  If loadingentry Then Exit Sub
  CriticalChart(CurrentColumn, CurrentRow).NewPosition = cmbNewPosition.ListIndex
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub txtHealthChanges_GotFocus()
  Call SelectField(txtHealthChanges)
End Sub

Private Sub txtHealthChanges_Change()
  If loadingentry Then Exit Sub
  CriticalChart(CurrentColumn, CurrentRow).HealthChanges = txtHealthChanges
  Dirty = True
  Form_EnableDisable
End Sub

'Private Sub txtDescription_GotFocus()
'  Call SelectField(txtDescription)
'End Sub

Private Sub txtDescription_Change()
  Dim s As String
  If loadingentry Then Exit Sub
  CriticalChart(CurrentColumn, CurrentRow).Description = txtDescription
  If Len(Trim(txtDescription)) < conButtonLength Then
      s = Trim(txtDescription)
    Else
      s = Left(Trim(txtDescription), conButtonLength) & "..."
    End If
  If CurrentColumn = 1 Then
      cmdA(CurrentRow - 1).Caption = s
    ElseIf CurrentColumn = 2 Then
      cmdB(CurrentRow - 1).Caption = s
    ElseIf CurrentColumn = 3 Then
      cmdC(CurrentRow - 1).Caption = s
    ElseIf CurrentColumn = 4 Then
      cmdD(CurrentRow - 1).Caption = s
    Else
      cmdE(CurrentRow - 1).Caption = s
    End If
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub txtCode_GotFocus()
  Call SelectField(txtCode)
End Sub

Private Sub txtCode_Change()
  If loadingentry Then Exit Sub
  fraEntry.Caption = RowName(CurrentRow) & " " & Mid("ABCDE", CurrentColumn, 1) & txtCode & " (" & txtName & ")"
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub txtName_GotFocus()
  Call SelectField(txtName)
End Sub

Private Sub txtName_Change()
  If loadingentry Then Exit Sub
  fraEntry.Caption = RowName(CurrentRow) & " " & Mid("ABCDE", CurrentColumn, 1) & txtCode & " (" & txtName & ")"
  Dirty = True
  Form_EnableDisable
End Sub

Private Sub cmdNew_Click()
  Dim column As Integer, row As Integer
  ' confirm an overwrite if needed
  If Dirty Then
     column = MsgBox("Save chart first?", vbYesNoCancel + vbQuestion, "Critical Chart Editor")
     If column = vbCancel Then Exit Sub
     If column = vbYes Then cmdSave_Click
    Else
      If MsgBox("Really erase everything?", vbYesNo + vbQuestion, "Critical Chart Editor") = vbNo Then Exit Sub
    End If
  ' reset everything
  Dirty = False
  Filename = ""
  loadingentry = True
  txtCode = ""
  txtName = ""
  ' start with a blank record
  For column = 1 To 5
    For row = 1 To 17
      ClearCriticalEntry column, row
      If column = 1 Then
          cmdA(row - 1).Caption = ""
          cmdA(row - 1).Visible = True
        ElseIf column = 2 Then
          cmdB(row - 1).Caption = ""
          cmdB(row - 1).Visible = True
        ElseIf column = 3 Then
          cmdC(row - 1).Caption = ""
          cmdC(row - 1).Visible = True
        ElseIf column = 4 Then
          cmdD(row - 1).Caption = ""
          cmdD(row - 1).Visible = True
        Else
          cmdE(row - 1).Caption = ""
          cmdE(row - 1).Visible = True
        End If
      Next row
    Next column
  CurrentColumn = 0
  CurrentRow = 0
  cmdA_Click (0)
  txtCode.SetFocus
  loadingentry = False
  Form_EnableDisable
End Sub

Private Sub cmdOpen_Click()
  Dim column As Integer, row As Integer
  Dim errorText, Header As String, s1 As String, s2 As String
  ' confirm an overwrite if needed
  If Dirty Then
     column = MsgBox("Save chart first?", vbYesNoCancel + vbQuestion, "Critical Chart Editor")
     If column = vbCancel Then Exit Sub
     If column = vbYes Then cmdSave_Click
      ' reset everything
      Dirty = False
      loadingentry = True
      CurrentColumn = 0
      CurrentRow = 0
      txtCode = ""
      txtName = ""
      ' start with a blank record
      For column = 1 To 5
        For row = 1 To 17
          ClearCriticalEntry column, row
          Next row
        Next column
      loadingentry = False
    End If
  ' set up the common dialog for a load
  cdlGeneric.DialogTitle = "Load chart"
  cdlGeneric.Filename = Filename
  cdlGeneric.DefaultExt = "*.prism"
  cdlGeneric.Filter = "Prism Files (*.prism)|*.prism|All Files (*.*)|*.*"
  cdlGeneric.FilterIndex = 1
  cdlGeneric.Flags = cdlOFNFileMustExist + cdlOFNHideReadOnly + _
      cdlOFNPathMustExist + cdlOFNExplorer + cdlOFNLongNames
  cdlGeneric.CancelError = True
  On Error GoTo cancelled
  cdlGeneric.ShowOpen
  If cdlGeneric.Filename = "" Then Exit Sub
  ' work around a bug: default extension is chopped to three characters
  If Right(cdlGeneric.Filename, 4) = ".pri" Then cdlGeneric.Filename = cdlGeneric.Filename & "sm"
  Filename = cdlGeneric.Filename
  ' open the file (for read)
  On Error GoTo fileError
  ' change the caption to show that it's loading
  Me.Caption = "Loading from file '" & Filename & "'..."
  errorText = " opening file"
  Open Filename For Input As #1
  ' read and check header
  errorText = " reading from file"
  Input #1, Header
  If Header <> "PrismTools - Critical Chart - v1.0" Then
      MsgBox "Error: file is of incorrect format.", vbExclamation
      Close #1
      Form_EnableDisable
      Exit Sub
    End If
  ' read entries, setting the cmd buttons accordingly
  loadingentry = True
  Input #1, s1, s2
  txtCode = s1
  txtName = s2
  ' set up to start on the first empty cell
  CurrentColumn = 0
  CurrentRow = 0
  ' for each entry
  For column = 1 To 5
    For row = 1 To 17
      With CriticalChart(column, row)
        Input #1, s1, .BonusHits, .Bleeding, .Stuns, .StunNoParries, .LostInits, _
            .RoundsTillDown, .RoundsTillDead, .MustParryRounds, .MustParryBP, _
            .TmpBPRounds, .TmpBP, .BonusPenalty, .AttackerBonusRounds, _
            .AttackerBonus, .NewPosition, .HealthChanges, .Description
        ' is this where the cursor should go?
        If CurrentColumn = 0 And CurrentRow = 0 And .Description = "" Then
            CurrentColumn = column
            CurrentRow = row
          End If
        ' set the button
        If Len(Trim(.Description)) < conButtonLength Then
            s1 = Trim(.Description)
          Else
            s1 = Left(Trim(.Description), conButtonLength) & "..."
          End If
        If column = 1 Then
            cmdA(row - 1).Caption = s1
            cmdA(row - 1).Visible = True
          ElseIf column = 2 Then
            cmdB(row - 1).Caption = s1
            cmdB(row - 1).Visible = True
          ElseIf column = 3 Then
            cmdC(row - 1).Caption = s1
            cmdC(row - 1).Visible = True
          ElseIf column = 4 Then
            cmdD(row - 1).Caption = s1
            cmdD(row - 1).Visible = True
          Else
            cmdE(row - 1).Caption = s1
            cmdE(row - 1).Visible = True
          End If
        End With
      Next row
    Next column
  ' if the chart is full, start in the upper left
  If CurrentColumn = 0 And CurrentRow = 0 Then
      CurrentColumn = 1
      CurrentRow = 1
    End If
  loadingentry = False
  ' close the file
  errorText = " closing file"
  Close #1
  ' change the topic back
  Form_EnableDisable
  LoadCriticalEntry CurrentColumn, CurrentRow
  Exit Sub
  ' error message handlers
cancelled:
  Exit Sub
fileError:
  Close #1
  MsgBox "Error" & errorText & " " & cdlGeneric.Filename, vbCritical
End Sub

Private Sub cmdSave_Click()
  Dim errorText As String, s As String
  Dim column As Integer, row As Integer
  If Filename = "" Then
      cmdSaveAs_Click
      Exit Sub
    End If
  ' change the caption to show that it's saving
  Me.Caption = "Saving into file '" & Filename & "'..."
  ' open the file (for rewrite)
  On Error GoTo fileError
  errorText = " opening file"
  Open Filename For Output As #1
  ' write header, then Code and Name
  errorText = " writing to file"
  Write #1, "PrismTools - Critical Chart - v1.0"
  Write #1, txtCode, txtName
  ' for each entry
  For column = 1 To 5
    For row = 1 To 17
      With CriticalChart(column, row)
        s = RowName(row) & " " & Mid$("ABCDE", column, 1)
        Write #1, s, .BonusHits, .Bleeding, .Stuns, .StunNoParries, .LostInits, _
            .RoundsTillDown, .RoundsTillDead, .MustParryRounds, .MustParryBP, _
            .TmpBPRounds, .TmpBP, .BonusPenalty, .AttackerBonusRounds, _
            .AttackerBonus, .NewPosition, .HealthChanges, .Description
        End With
      Next row
    Next column
  ' close the file
  errorText = " closing file"
  Close #1
  Dirty = False
  Form_EnableDisable
  Exit Sub
  ' error message handlers
fileError:
  Close #1
  MsgBox "Error" & errorText & " " & Filename, vbCritical
End Sub

Private Sub cmdSaveAs_Click()
  ' set up the common dialog for a save
  cdlGeneric.DialogTitle = "Save chart as"
  cdlGeneric.Filename = Filename
  cdlGeneric.DefaultExt = "*.prism"
  cdlGeneric.Filter = "Prism Files (*.prism)|*.prism|All Files (*.*)|*.*"
  cdlGeneric.FilterIndex = 1
  cdlGeneric.Flags = cdlOFNHideReadOnly + cdlOFNPathMustExist + cdlOFNExplorer + cdlOFNLongNames + cdlOFNOverwritePrompt
  cdlGeneric.CancelError = True
  ' launch the dialog
  On Error GoTo cancelled
  cdlGeneric.ShowSave
  If cdlGeneric.Filename = "" Then Exit Sub
  ' work around a bug: default extension is chopped to three characters
  If Right(cdlGeneric.Filename, 4) = ".pri" Then cdlGeneric.Filename = cdlGeneric.Filename & "sm"
  Filename = cdlGeneric.Filename
  Call cmdSave_Click
cancelled:
  Exit Sub
End Sub

Function FilePart(ByVal s As String) As String
  Dim i, j As Integer
  FilePart = s
  i = InStr(s, "\")
  If i = 0 Then Exit Function
  Do
    j = i
    i = InStr(i + 1, s, "\")
    Loop While i <> 0
  FilePart = Right$(s, Len(s) - j)
End Function
