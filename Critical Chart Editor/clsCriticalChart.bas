Attribute VB_Name = "clsCriticalChart"
Option Explicit

Public Type CriticalEntry
  BonusHits As Integer
  Bleeding As Integer
  Stuns As Integer
  StunNoParries As Integer
  LostInits As Integer
  RoundsTillDown As Integer
  RoundsTillDead As Integer
  MustParryRounds As Integer
  MustParryBP As Integer
  TmpBPRounds As Integer
  TmpBP As Integer
  BonusPenalty As Integer
  AttackerBonusRounds As Integer
  AttackerBonus As Integer
  NewPosition As Integer
  HealthChanges As String
  Description As String
End Type

Public CriticalChart(5, 17) As CriticalEntry

Public Sub ClearCriticalEntry(ByVal column As Integer, ByVal row As Integer)
  With CriticalChart(column, row)
    .BonusHits = 0
    .Bleeding = 0
    .Stuns = 0
    .StunNoParries = 0
    .LostInits = 0
    .RoundsTillDown = 0
    .RoundsTillDead = 0
    .MustParryRounds = 0
    .MustParryBP = 0
    .TmpBPRounds = 0
    .TmpBP = 0
    .BonusPenalty = 0
    .AttackerBonusRounds = 0
    .AttackerBonus = 0
    .NewPosition = 0
    .HealthChanges = ""
    .Description = ""
    End With
End Sub


