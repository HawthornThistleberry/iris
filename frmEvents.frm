VERSION 5.00
Begin VB.Form frmEvents 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Events"
   ClientHeight    =   3315
   ClientLeft      =   2325
   ClientTop       =   2040
   ClientWidth     =   4215
   ControlBox      =   0   'False
   Icon            =   "frmEvents.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3315
   ScaleWidth      =   4215
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtDescription 
      BackColor       =   &H00C0C0C0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   3000
      Width           =   3135
   End
   Begin VB.TextBox txtPhase 
      BackColor       =   &H00C0C0C0&
      Enabled         =   0   'False
      Height          =   285
      Left            =   600
      MaxLength       =   4
      TabIndex        =   1
      Top             =   2640
      Width           =   2535
   End
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Height          =   375
      Left            =   3240
      TabIndex        =   5
      Top             =   2880
      Width           =   975
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Delete"
      Enabled         =   0   'False
      Height          =   375
      Left            =   3240
      TabIndex        =   4
      Top             =   480
      Width           =   975
   End
   Begin VB.CommandButton cmdNew 
      Caption         =   "New"
      Default         =   -1  'True
      Height          =   375
      Left            =   3240
      TabIndex        =   3
      Top             =   0
      Width           =   975
   End
   Begin VB.ListBox lisEvents 
      Height          =   2595
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3135
   End
   Begin VB.Image imgDice 
      Height          =   480
      Left            =   3480
      MousePointer    =   4  'Icon
      Picture         =   "frmEvents.frx":030A
      Top             =   1560
      Width           =   480
   End
   Begin VB.Label lblPhase 
      Alignment       =   1  'Right Justify
      Caption         =   "Phase"
      Height          =   255
      Left            =   0
      TabIndex        =   6
      Top             =   2685
      Width           =   495
   End
End
Attribute VB_Name = "frmEvents"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private sorting As Boolean

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
End Sub

Private Sub Form_Activate()
  Dim i As Integer
  sorting = False
  lisEvents.Clear
  If NumEvents > 0 Then
      For i = 1 To NumEvents
        lisEvents.AddItem Events(i).Phase & ": " & Events(i).Description
        Next i
      lisEvents.ListIndex = 0
    Else
      txtPhase = txtPhase.Tag
      txtDescription = ""
    End If
  Call frmEvents_EnableDisable
  lisEvents.SetFocus
End Sub

Private Sub frmEvents_EnableDisable()
  If sorting = True Then Exit Sub
  If lisEvents.ListIndex >= 0 Then
      cmdDelete.Enabled = True
      txtPhase.Enabled = True
      txtDescription.Enabled = True
      txtPhase.BackColor = &H80000005
      txtDescription.BackColor = &H80000005
    Else
      cmdDelete.Enabled = False
      txtPhase.Enabled = False
      txtDescription.Enabled = False
      txtPhase.BackColor = &HC0C0C0
      txtDescription.BackColor = &HC0C0C0
    End If
  If NumEvents = conMaxEvents Then
      cmdNew.Enabled = False
    Else
      cmdNew.Enabled = True
    End If
End Sub

Private Sub lisEvents_Click()
  If sorting = True Then Exit Sub
  ' load the clicked item into the fields
  txtDescription = Events(lisEvents.ListIndex + 1).Description
  txtPhase = Events(lisEvents.ListIndex + 1).Phase
End Sub

Private Sub txtPhase_GotFocus()
  If sorting = True Then Exit Sub
  Call SelectField(txtPhase)
End Sub

Private Sub txtDescription_GotFocus()
  If sorting = True Then Exit Sub
  Call SelectField(txtDescription)
End Sub

Private Sub txtPhase_KeyPress(KeyAscii As Integer)
  If sorting = True Then Exit Sub
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtPhase_Change()
  If sorting = True Then Exit Sub
  Dim oldpos, newpos, i As Integer
  If lisEvents.ListCount <= 0 Then Exit Sub
  ' update the item in the array
  Events(lisEvents.ListIndex + 1).Phase = Val(txtPhase)
  ' update it in the listbox
  lisEvents.List(lisEvents.ListIndex) = txtPhase & ": " & txtDescription
  ' figure out if the item should move to keep sorted order
  oldpos = lisEvents.ListIndex + 1
  newpos = 1
  Do While newpos <= NumEvents
    If newpos <> oldpos Then
        If Events(newpos).Phase >= Val(txtPhase) Then
            Exit Do
          End If
      End If
    newpos = newpos + 1
    Loop
  If newpos > oldpos Then newpos = newpos - 1
  If newpos = oldpos Then Exit Sub
  'MsgBox "Trace: oldpos=" & oldpos & ", newpos=" & newpos & ", sorting"
  sorting = True ' don't make changes in fields reflect in array
  ' the item needs to move into slot newpos
  ' first, delete it from oldpos
  lisEvents.RemoveItem oldpos - 1
  ' next, insert it into position newpos
  lisEvents.AddItem txtPhase & ": " & txtDescription, newpos - 1
  lisEvents.ListIndex = newpos - 1
  ' slide things in the array to make room to move it
  If oldpos < newpos Then
      For i = oldpos To newpos - 1
        Call CopyEvent(i, i + 1)
        Next i
    Else
      For i = oldpos To newpos + 1 Step -1
        Call CopyEvent(i, i - 1)
        Next i
    End If
  ' move it
  Events(newpos).Phase = Val(txtPhase)
  Events(newpos).Description = txtDescription
  sorting = False
End Sub

Private Sub txtDescription_KeyPress(KeyAscii As Integer)
  If sorting = True Then Exit Sub
  KeyAscii = NoQuotes(KeyAscii)
End Sub

Private Sub txtDescription_Change()
  If sorting = True Then Exit Sub
  If lisEvents.ListCount <= 0 Then Exit Sub
  ' update the item in the array
  Events(lisEvents.ListIndex + 1).Description = txtDescription
  ' update it in the listbox
  lisEvents.List(lisEvents.ListIndex) = txtPhase & ": " & txtDescription
End Sub

Private Sub cmdNew_Click()
  ' add a blank item
  NumEvents = NumEvents + 1
  Events(NumEvents).Phase = txtPhase.Tag
  Events(NumEvents).Description = ""
  lisEvents.AddItem Events(NumEvents).Phase & ": " & Events(NumEvents).Description
  lisEvents.ListIndex = lisEvents.ListCount - 1
  Call frmEvents_EnableDisable
  txtPhase.SetFocus
End Sub

Private Sub cmdDelete_Click()
  ' delete selected item
  Dim i As Integer
  If MsgBox("Really delete this event?", vbYesNo + vbQuestion, "Confirm Event Deletion") = vbYes Then
      ' delete the item from the array
      DeleteEvent (lisEvents.ListIndex + 1)
      i = lisEvents.ListIndex
      ' delete the item from the lisEvents
      lisEvents.RemoveItem lisEvents.ListIndex
      If lisEvents.ListCount = 0 Then ' clear out all the fields
          lisEvents.Clear
          txtPhase = txtPhase.Tag
          txtDescription = ""
        Else ' pick the next person on the list
          If i >= lisEvents.ListCount Then i = lisEvents.ListCount - 1
          lisEvents.ListIndex = i
        End If
      ' update the enabled state of buttons
      Call frmEvents_EnableDisable
    End If
End Sub

Private Sub imgDice_Click()
  frmIRIS.imgDice_Click
End Sub

Private Sub cmdClose_Click()
  Me.Hide
End Sub

