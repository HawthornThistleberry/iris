VERSION 5.00
Begin VB.Form frmMassHeal 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Mass Heal"
   ClientHeight    =   3285
   ClientLeft      =   2130
   ClientTop       =   1740
   ClientWidth     =   3495
   ControlBox      =   0   'False
   HelpContextID   =   430
   Icon            =   "frmMassHeal.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3285
   ScaleWidth      =   3495
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      HelpContextID   =   430
      Left            =   2400
      TabIndex        =   13
      Top             =   2880
      Width           =   1095
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      HelpContextID   =   430
      Left            =   2400
      TabIndex        =   12
      Top             =   2400
      Width           =   1095
   End
   Begin VB.CheckBox chkSaves 
      Caption         =   "Saves"
      Height          =   255
      HelpContextID   =   430
      Left            =   2400
      TabIndex        =   11
      Top             =   480
      Value           =   1  'Checked
      Width           =   975
   End
   Begin VB.CheckBox chkCapLoss 
      Caption         =   "Cap. Loss"
      Height          =   255
      HelpContextID   =   430
      Left            =   2400
      TabIndex        =   10
      Top             =   240
      Value           =   1  'Checked
      Width           =   1095
   End
   Begin VB.Frame fraHeal 
      Caption         =   "Heal"
      Height          =   3255
      Left            =   0
      TabIndex        =   14
      Top             =   0
      Width           =   2295
      Begin VB.CheckBox chkHealth 
         Caption         =   "Health Notes"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   2880
         Width           =   1335
      End
      Begin VB.CheckBox chkPosition 
         Caption         =   "Position"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   2640
         Value           =   1  'Checked
         Width           =   1215
      End
      Begin VB.CheckBox chkRoundsTillDead 
         Caption         =   "Rounds Until Dead"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   960
         Value           =   1  'Checked
         Width           =   1695
      End
      Begin VB.CheckBox chkRoundsTillDown 
         Caption         =   "Rounds Until Down"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   720
         Value           =   1  'Checked
         Width           =   1695
      End
      Begin VB.CommandButton cmdNone 
         Caption         =   "None"
         Height          =   375
         HelpContextID   =   430
         Left            =   1560
         TabIndex        =   9
         Top             =   2760
         Width           =   615
      End
      Begin VB.CommandButton cmdAll 
         Caption         =   "All"
         Height          =   375
         HelpContextID   =   430
         Left            =   1560
         TabIndex        =   8
         Top             =   2400
         Width           =   615
      End
      Begin VB.CheckBox chkPenalty 
         Caption         =   "Penalty"
         Height          =   255
         HelpContextID   =   430
         Left            =   120
         TabIndex        =   7
         Top             =   2400
         Value           =   1  'Checked
         Width           =   1695
      End
      Begin VB.CheckBox chkBonusPenalty 
         Caption         =   "Tmp Bonus/Pnlty"
         Height          =   255
         HelpContextID   =   430
         Left            =   120
         TabIndex        =   6
         Top             =   2160
         Value           =   1  'Checked
         Width           =   1695
      End
      Begin VB.CheckBox chkMustParry 
         Caption         =   "Must Parry"
         Height          =   255
         HelpContextID   =   430
         Left            =   120
         TabIndex        =   5
         Top             =   1920
         Value           =   1  'Checked
         Width           =   1695
      End
      Begin VB.CheckBox chkLostInits 
         Caption         =   "Lost Inits"
         Height          =   255
         HelpContextID   =   430
         Left            =   120
         TabIndex        =   4
         Top             =   1680
         Value           =   1  'Checked
         Width           =   1695
      End
      Begin VB.CheckBox chkStuns 
         Caption         =   "Stuns"
         Height          =   255
         HelpContextID   =   430
         Left            =   120
         TabIndex        =   3
         Top             =   1440
         Value           =   1  'Checked
         Width           =   1455
      End
      Begin VB.CheckBox chkStunNoParries 
         Caption         =   "Stun/No-Parries"
         Height          =   255
         HelpContextID   =   430
         Left            =   120
         TabIndex        =   2
         Top             =   1200
         Value           =   1  'Checked
         Width           =   1455
      End
      Begin VB.CheckBox chkBleed 
         Caption         =   "Bleeding"
         Height          =   255
         HelpContextID   =   430
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Value           =   1  'Checked
         Width           =   1455
      End
      Begin VB.CheckBox chkHits 
         Caption         =   "Lost Hit Points"
         Height          =   255
         HelpContextID   =   430
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Value           =   1  'Checked
         Width           =   1455
      End
   End
   Begin VB.Image imgDice 
      Height          =   480
      Left            =   2715
      MousePointer    =   4  'Icon
      Picture         =   "frmMassHeal.frx":030A
      Top             =   1800
      Width           =   480
   End
End
Attribute VB_Name = "frmMassHeal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
End Sub

Private Sub Form_Activate()
  cmdCancel.Tag = ""
  Call cmdAll_Click
  chkHealth = 0
  chkCapLoss = 1
  chkSaves = 1
  chkHits.SetFocus
End Sub

Private Sub cmdAll_Click()
  chkHits = 1
  chkBleed = 1
  chkRoundsTillDown = 1
  chkRoundsTillDead = 1
  chkStunNoParries = 1
  chkStuns = 1
  chkLostInits = 1
  chkMustParry = 1
  chkBonusPenalty = 1
  chkPenalty = 1
  chkPosition = 1
  chkHealth = 1
End Sub

Private Sub cmdNone_Click()
  chkHits = 0
  chkBleed = 0
  chkRoundsTillDown = 0
  chkRoundsTillDead = 0
  chkStunNoParries = 0
  chkStuns = 0
  chkLostInits = 0
  chkMustParry = 0
  chkBonusPenalty = 0
  chkPenalty = 0
  chkPosition = 0
  chkHealth = 0
End Sub

Private Sub imgDice_Click()
  frmIRIS.imgDice_Click
End Sub

Private Sub cmdOK_Click()
  Me.Hide
End Sub

Private Sub cmdCancel_Click()
  cmdCancel.Tag = "y"
  Me.Hide
End Sub

