Attribute VB_Name = "clsCriticalChart"
' This module handles Critical Charts as a class.  Each Critical Chart consists of a code, name, filename,
' and 5 * 17 entries, plus an internal-use field used to keep track of which charts should be swapped out
' when new ones need to be loaded in (the module can store 20 charts in memory at once, and will
' automatically swap out older, less-used ones when new ones are needed).  Each entry includes a text
' description plus a set of fields reflecting that description, at least when the description is clear; some
' criticals yield conditional results, etc. so there's still a need for GM hand-tuning.

' It's important that the Constructor method be called once at program startup and not at all thereafter.
' (However, if it is called later, it'll just reset the currently-loaded charts to the default three startup
' charts: Slash, Puncture, and Crush.)

Option Explicit
Option Base 1

Private Const conMaxCriticalCharts = 20

Public Type CriticalEntry ' stores a single critical chart entry
  BonusHits As Integer
  Bleeding As Integer
  Stuns As Integer
  StunNoParries As Integer
  LostInits As Integer
  RoundsTillDown As Integer
  RoundsTillDead As Integer
  MustParryRounds As Integer
  MustParryBP As Integer
  TmpBPRounds As Integer
  TmpBP As Integer
  BonusPenalty As Integer
  AttackerBonusRounds As Integer
  AttackerBonus As Integer
  NewPosition As Integer
  HealthChanges As String
  Description As String
End Type

Public Type CriticalChart  ' stores an entire chart
  Code As String
  Name As String
  Filename As String
  Uses As Integer  ' used to know which one to discard when a slot is needed
  Entries(5, 17) As CriticalEntry
End Type

Private CriticalCharts(conMaxCriticalCharts) As CriticalChart
Private NumCriticalCharts As Integer
Private NullCriticalEntry As CriticalEntry

Public Sub Constructor()
  For NumCriticalCharts = 1 To conMaxCriticalCharts
    ClearCriticalChart NumCriticalCharts
    Next NumCriticalCharts
  NumCriticalCharts = 0
  ClearCriticalEntry NullCriticalEntry
  LoadStartingCharts
End Sub

Private Sub ClearCriticalEntry(entry As CriticalEntry)
  With entry
    .BonusHits = 0
    .Bleeding = 0
    .Stuns = 0
    .StunNoParries = 0
    .LostInits = 0
    .RoundsTillDown = 0
    .RoundsTillDead = 0
    .MustParryRounds = 0
    .MustParryBP = 0
    .TmpBPRounds = 0
    .TmpBP = 0
    .BonusPenalty = 0
    .AttackerBonusRounds = 0
    .AttackerBonus = 0
    .NewPosition = 0
    .HealthChanges = ""
    .Description = ""
    End With
End Sub

Private Sub ClearCriticalChart(ByVal chart As Integer)
  Dim column As Integer, row As Integer
  CriticalCharts(chart).Code = ""
  CriticalCharts(chart).Name = ""
  CriticalCharts(chart).Filename = ""
  CriticalCharts(chart).Uses = 0
  For column = 1 To 5
    For row = 1 To 17
      ClearCriticalEntry CriticalCharts(chart).Entries(column, row)
      Next row
    Next column
End Sub

Private Sub CopyCriticalEntry(Source As CriticalEntry, dest As CriticalEntry)
  dest.BonusHits = Source.BonusHits
  dest.Bleeding = Source.Bleeding
  dest.Stuns = Source.Stuns
  dest.StunNoParries = Source.StunNoParries
  dest.LostInits = Source.LostInits
  dest.RoundsTillDown = Source.RoundsTillDown
  dest.RoundsTillDead = Source.RoundsTillDead
  dest.MustParryRounds = Source.MustParryRounds
  dest.MustParryBP = Source.MustParryBP
  dest.TmpBPRounds = Source.TmpBPRounds
  dest.TmpBP = Source.TmpBP
  dest.BonusPenalty = Source.BonusPenalty
  dest.AttackerBonusRounds = Source.AttackerBonusRounds
  dest.AttackerBonus = Source.AttackerBonus
  dest.NewPosition = Source.NewPosition
  dest.HealthChanges = Source.HealthChanges
  dest.Description = Source.Description
End Sub

Private Sub CopyCriticalChart(ByVal Source As Integer, ByVal dest As Integer)
  Dim column As Integer, row As Integer
  CriticalCharts(dest).Code = CriticalCharts(Source).Code
  CriticalCharts(dest).Name = CriticalCharts(Source).Name
  CriticalCharts(dest).Filename = CriticalCharts(Source).Filename
  CriticalCharts(dest).Uses = CriticalCharts(Source).Uses
  For column = 1 To 5
    For row = 1 To 17
      CopyCriticalEntry CriticalCharts(Source).Entries(column, row), CriticalCharts(dest).Entries(column, row)
      Next row
    Next column
End Sub

Private Function OldestCriticalChart() As Integer
  Dim chart, lowest As Integer
  If NumCriticalCharts = 0 Then
      OldestCriticalChart = 0
      Exit Function
    End If
  lowest = -1
  For chart = 1 To NumCriticalCharts
    If lowest = -1 Or CriticalCharts(chart).Uses < lowest Then
        lowest = CriticalCharts(chart).Uses
        OldestCriticalChart = chart
      End If
    Next chart
End Function

Private Function FindChartByCode(Code As String) As Integer
  Dim i As Integer
  FindChartByCode = 0
  If NumCriticalCharts = 0 Then Exit Function
  For i = 1 To NumCriticalCharts
    If UCase(Code) = CriticalCharts(i).Code Then FindChartByCode = i
    Next i
End Function

Public Function AvailableCriticalCharts() As String
  Dim result As String
  Dim i As Integer
  AvailableCriticalCharts = ""
  If NumCriticalCharts = 0 Then Exit Function
  result = ""
  For i = 1 To NumCriticalCharts
    result = result & Left(CriticalCharts(i).Code & "   ", 3)
    Next i
  AvailableCriticalCharts = result
End Function

Public Function LoadCriticalChart(ByVal Filename As String, ByVal chart As Integer, ByVal silent As Boolean) As String
  ' returns the code of the loaded file, or "" if none
  Dim column As Integer, row As Integer
  Dim errorText, s1 As String, s2 As String
  LoadCriticalChart = ""
  ' open the file (for read)
  On Error GoTo fileError
  ' change the caption to show that it's loading
  errorText = " opening file"
  Open Filename For Input As #1
  ' read and check header
  errorText = " reading from file"
  Input #1, s1
  If s1 <> "PrismTools - Critical Chart - v1.0" Then
      If Not silent Then MsgBox "Error: file " & FilePart(Filename) & " is of incorrect format.", vbExclamation
      Close #1
      Exit Function
    End If
  ' read entries, setting the cmd buttons accordingly
  Input #1, s1, s2
  If chart = 0 Then ' no chart specified, so find one
      chart = FindChartByCode(s1)
      If chart = 0 Then ' there isn't a chart currently loaded with this code
          If NumCriticalCharts < conMaxCriticalCharts Then 'add a new one
              NumCriticalCharts = NumCriticalCharts + 1
              chart = NumCriticalCharts
            Else ' find one to replace
              chart = OldestCriticalChart()
            End If
        End If
    End If
  CriticalCharts(chart).Code = s1
  CriticalCharts(chart).Name = s2
  CriticalCharts(chart).Uses = 0
  CriticalCharts(chart).Filename = Filename
  ' for each entry
  For column = 1 To 5
    For row = 1 To 17
      With CriticalCharts(chart).Entries(column, row)
        Input #1, s1, .BonusHits, .Bleeding, .Stuns, .StunNoParries, .LostInits, _
            .RoundsTillDown, .RoundsTillDead, .MustParryRounds, .MustParryBP, _
            .TmpBPRounds, .TmpBP, .BonusPenalty, .AttackerBonusRounds, _
            .AttackerBonus, .NewPosition, .HealthChanges, .Description
        End With
      Next row
    Next column
  ' close the file
  errorText = " closing file"
  Close #1
  LoadCriticalChart = CriticalCharts(chart).Code
  Exit Function
  ' error message handlers
fileError:
  Close #1
  If Not silent Then MsgBox "Error" & errorText & " " & Filename, vbCritical
End Function

Public Function PromptAndLoadCriticalChart(ByVal title As String, ByVal defaultfilename As String) As String
  ' returns the code of the loaded file, or "" if none
  PromptAndLoadCriticalChart = ""
  ' set up the common dialog for a load
  frmIRIS.cdlGeneric.DialogTitle = title
  frmIRIS.cdlGeneric.Filename = defaultfilename
  frmIRIS.cdlGeneric.DefaultExt = "*.prism"
  frmIRIS.cdlGeneric.Filter = "Prism Files (*.prism)|*.prism|All Files (*.*)|*.*"
  frmIRIS.cdlGeneric.FilterIndex = 1
  frmIRIS.cdlGeneric.Flags = cdlOFNFileMustExist + cdlOFNHideReadOnly + _
      cdlOFNPathMustExist + cdlOFNExplorer + cdlOFNLongNames
  frmIRIS.cdlGeneric.CancelError = True
  On Error GoTo cancelled
  frmIRIS.cdlGeneric.ShowOpen
  If frmIRIS.cdlGeneric.Filename = "" Then Exit Function
  ' work around a bug: default extension is chopped to three characters
  If Right(frmIRIS.cdlGeneric.Filename, 4) = ".pri" Then frmIRIS.cdlGeneric.Filename = frmIRIS.cdlGeneric.Filename & "sm"
  PromptAndLoadCriticalChart = LoadCriticalChart(frmIRIS.cdlGeneric.Filename, 0, False)
  Exit Function
cancelled:
  Exit Function
End Function

Private Sub LoadStartingCharts()
  Dim chartlist As Variant, i As Integer
  chartlist = GetAllSettings(conAppName, "Critical Charts")
  For i = LBound(chartlist, 1) To UBound(chartlist, 1)
    LoadCriticalChart chartlist(i, 1), 0, True
    Next i
End Sub

Private Function RollToRow(ByVal roll As Integer) As Integer
  If roll <= 1 Then
      RollToRow = 1
    ElseIf roll <= 4 Then
      RollToRow = roll
    ElseIf roll <= 7 Then
      RollToRow = 5
    ElseIf roll <= 9 Then
      RollToRow = 6
    ElseIf roll <= 20 Then
      RollToRow = roll - 3
    Else
      RollToRow = 17
    End If
End Function

Public Function CriticalChartLookup(ByVal Code As String, ByVal column As Integer, ByVal roll As Integer) As CriticalEntry
  ' look up a given roll, column, and code, and return an entry, or NullCriticalEntry if none found
  Dim chart As Integer, row As Integer
  CriticalChartLookup = NullCriticalEntry
  chart = FindChartByCode(Code)
  If chart = 0 Then Exit Function
  row = RollToRow(roll)
  CriticalChartLookup = CriticalCharts(chart).Entries(column, row)
  CriticalCharts(chart).Uses = CriticalCharts(chart).Uses + 1
End Function

Public Function CriticalChartName(ByVal Code As String) As String
  Dim chart As Integer
  chart = FindChartByCode(Code)
  If chart <> 0 Then
      CriticalChartName = CriticalCharts(chart).Name
    Else
      CriticalChartName = ""
    End If
End Function

Public Sub SaveCritChartList()
  ' Save a list of all currently loaded critical charts in the registry for restoration later
  Dim i As Integer
  On Error Resume Next
  DeleteSetting conAppName, "Critical Charts"
  If NumCriticalCharts = 0 Then Exit Sub
  For i = 1 To NumCriticalCharts
    Call SaveSetting(conAppName, "Critical Charts", CriticalCharts(i).Code, CriticalCharts(i).Filename)
    Next i
End Sub
