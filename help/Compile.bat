@echo off
copy irisword.rtf iris.rtf
fixrtf iris.rtf
hcp iris.hpj | more
if errorlevel 1 goto theend
start iris.hlp
:theend
