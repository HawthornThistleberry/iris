Attribute VB_Name = "MiscFunctions"
Option Explicit

Public Const conAppName = "IRIS"
Public Const conAppVersion = "1.7"

Public Sub SelectField(o As Object)
  o.SelStart = 0
  o.SelLength = Len(o)
End Sub

Public Function NoQuotes(ByVal KeyAscii As Integer) As Integer
  NoQuotes = KeyAscii
  If KeyAscii = 34 Then NoQuotes = Asc("'")
End Function

Public Function OnlyDigits(ByVal KeyAscii As Integer) As Integer
  OnlyDigits = KeyAscii
  If InStr("0123456789", Chr$(KeyAscii)) = 0 And _
      KeyAscii <> 8 And KeyAscii <> 127 Then
      OnlyDigits = 0
      Beep
    End If
End Function

Public Function OnlyDigitsAndMinus(ByVal KeyAscii As Integer) As Integer
  OnlyDigitsAndMinus = KeyAscii
  If (screen.ActiveControl.SelStart <> 0 Or (Chr$(KeyAscii) <> "-" And Chr$(KeyAscii) <> "+")) And _
      InStr("0123456789", Chr$(KeyAscii)) = 0 And _
      KeyAscii <> 8 And KeyAscii <> 127 Then
      OnlyDigitsAndMinus = 0
      Beep
    End If
End Function

Public Function OnlyDigitsAndPeriod(ByVal KeyAscii As Integer) As Integer
  OnlyDigitsAndPeriod = KeyAscii
  If ((Chr$(KeyAscii) <> "." Or InStr(screen.ActiveControl, ".") <> 0)) And _
      InStr("0123456789", Chr$(KeyAscii)) = 0 And _
      KeyAscii <> 8 And KeyAscii <> 127 Then
      OnlyDigitsAndPeriod = 0
      Beep
    End If
End Function

Function Word(ByVal s As String, ByVal num As Integer) As String
  Dim i, j As Integer
  Dim s2 As String
  Word = ""
  i = 1
  For j = 1 To num - 1
    i = InStr(i, s, " ")
    If i = 0 Then Exit Function
    While i <= Len(s) And Mid$(s, i, 1) = " "
      i = i + 1
      Wend
    If i > Len(s) Then Exit Function
    Next j
  s2 = Mid$(s, i)
  j = InStr(s2, " ")
  If j <> 0 Then s2 = Left$(s2, j - 1)
  Word = s2
End Function

Public Function NextNewName(ByVal Name As String) As String
  ' given a name that is in conflict, builds one that isn't
  Dim lastspace As Integer
  lastspace = Len(Name) - 1
  While Mid$(Name, lastspace, 1) <> " " And lastspace > 1
    lastspace = lastspace - 1
    Wend
  If lastspace = 1 Or Mid$(Name, lastspace + 1, 1) < "0" Or Mid$(Name, lastspace + 1, 1) > "9" Then
      NextNewName = Name & " 1"
      Exit Function
    End If
  NextNewName = Left$(Name, lastspace) & Trim$(Str$(Val(Mid$(Name, lastspace + 1)) + 1))
End Function

Function OpenEndedD20(flavor As String) As Integer
  Dim result, roll As Integer
  result = Int(20 * Rnd + 1)
  If result = 1 And flavor <> "high" Then
      Do
        roll = Int(20 * Rnd + 1)
        result = result - roll
        Loop While roll = 20
    ElseIf result = 20 And flavor <> "low" Then
      Do
        roll = Int(20 * Rnd + 1)
        result = result + roll
        Loop While roll = 20
    End If
  OpenEndedD20 = result
End Function

Function OpenEndedD10() As Integer
  OpenEndedD10 = Int((OpenEndedD20("open") - 1) / 2) + 1
End Function

Function Rnds(ByVal i As Integer) As String
  If i = 1 Then Rnds = "rnd" Else Rnds = "rnds"
End Function

Function CommaList(ByVal dest As String, ByVal Source As String) As String
  If dest = "" Then
      CommaList = Source
    Else
      CommaList = dest & ", " & Source
    End If
End Function

Function MPBPDisplay(ByVal label As String, ByVal mpbp As String) As String
  Dim result, s1, s2 As String
  Dim i As Integer
  s1 = mpbp
  result = ""
  i = InStr(s1, ",")
  While i <> 0
    s2 = Left$(s1, i - 1)
    s1 = Mid$(s1, i + 1)
    i = InStr(s2, " ")
    result = CommaList(result, label & " " & IIf(Left$(s2, i - 1) <> "0", "@" & IIf(Val(Left$(s2, i - 1)) > 0, "+", "") & Left$(s2, i - 1) & " for ", "") & _
      Mid$(s2, i + 1) & " " & Rnds(Val(Mid$(s2, i + 1))))
    i = InStr(s1, ",")
    Wend
  MPBPDisplay = result
End Function

Public Sub ManeuverChart(ByVal roll As Integer, ByVal percentages As Boolean, color As String, result As String, explanation As String)
  If roll <= -6 Then
      color = "\cf8 "
      result = "Extraordinary Failure"
      explanation = "You fail miserably.  If possible, you achieve the opposite of the intended effects.  You're 'in a slump' (at -10) to this and similar actions until you get a full success."
    ElseIf roll <= 1 Then
      color = "\cf3 "
      result = "Fumble"
      explanation = "You fail thoroughly, causing some negative side effect."
    ElseIf roll = 21 Or roll = 22 Then
      color = "\cf13 "
      result = "Unrelated Success"
      explanation = "You fail at the intended effect, but accomplish something else beneficial you weren't even trying to do."
    ElseIf percentages And roll <= 35 Then
      color = "\cf7 "
      result = MovingManeuverChart(roll) & "%"
      explanation = ""
    ElseIf roll <= 16 Then
      color = "\cf5 "
      result = "Failure"
      explanation = "You do not know or you cannot do what you have attempted."
    ElseIf roll <= 18 Then
      color = "\cf11 "
      result = "Partial Success"
      explanation = "You can accomplish about half of your action."
    ElseIf roll <= 20 Then
      color = "\cf9 "
      result = "Near Success"
      explanation = "You can do most of your action, or suffer a side effect."
    ElseIf roll <= 35 Then
      color = "\cf7 "
      result = "Success"
      explanation = "You achieve your maneuver successfully."
    ElseIf roll <= 50 Then
      color = "\cf7 "
      result = "Critical Success"
      explanation = "Your move succeeds dramatically.  If your success cannot be improved, it took half the expected time."
    Else
      color = "\cf7 "
      result = "Extraordinary Success"
      explanation = "You succeed as well as possible!  You're 'in the zone' (at +10) at this and similar actions until your next failure."
    End If
End Sub

Function MovingManeuverChart(ByVal roll As Integer) As Integer
  If roll <= 3 Then
      MovingManeuverChart = 0
    ElseIf roll <= 5 Then
      MovingManeuverChart = 10
    ElseIf roll <= 8 Then
      MovingManeuverChart = 20
    ElseIf roll <= 11 Then
      MovingManeuverChart = 30
    ElseIf roll <= 13 Then
      MovingManeuverChart = 40
    ElseIf roll <= 15 Then
      MovingManeuverChart = 50
    ElseIf roll <= 16 Then
      MovingManeuverChart = 60
    ElseIf roll <= 17 Then
      MovingManeuverChart = 70
    ElseIf roll <= 18 Then
      MovingManeuverChart = 80
    ElseIf roll <= 20 Then
      MovingManeuverChart = 90
    ElseIf roll <= 22 Then
      MovingManeuverChart = 95
    ElseIf roll <= 26 Then
      MovingManeuverChart = 100
    ElseIf roll <= 32 Then
      MovingManeuverChart = 110
    Else
      MovingManeuverChart = 120
    End If
End Function

Function StripPlus(ByVal s As String) As String
  If Left$(s, 1) = "+" Then StripPlus = Mid$(s, 2) Else StripPlus = s
End Function

Function FilePart(ByVal s As String) As String
  Dim i, j As Integer
  FilePart = s
  i = InStr(s, "\")
  If i = 0 Then Exit Function
  Do
    j = i
    i = InStr(i + 1, s, "\")
    Loop While i <> 0
  FilePart = Right$(s, Len(s) - j)
End Function

Function Convert318Stat(stat As Integer) As Integer
  Select Case stat
    Case 2: Convert318Stat = -6
    Case 3: Convert318Stat = -5
    Case 4: Convert318Stat = -4
    Case 5: Convert318Stat = -3
    Case 6, 7: Convert318Stat = -2
    Case 8, 9: Convert318Stat = -1
    Case 10, 11: Convert318Stat = 0
    Case 12, 13: Convert318Stat = 1
    Case 14, 15: Convert318Stat = 2
    Case 16: Convert318Stat = 3
    Case 17: Convert318Stat = 4
    Case 18: Convert318Stat = 5
    Case 19: Convert318Stat = 6
    Case Else: If stat <= 1 Then Convert318Stat = -7 Else Convert318Stat = 7
    End Select
End Function

Function Convertd100Stat(stat As Integer) As Integer
  Convertd100Stat = Int(stat / 10 + 0.5) - 5
End Function

Function Pluralize(n As Integer, unit As String) As String
  Pluralize = CStr(n) & " " & unit & IIf(n = 1, "", "s")
End Function

Public Function ShowTime(hours As Double) As String
  Dim d As Integer, h As Double, m As String
  ' converts a number of hours into a displayable string
  h = hours
  If h <> Int(h) Then
      m = ", " & Pluralize(Int((h - Int(h)) * 60), "minute")
      h = CDbl(Int(h))
    Else
      m = ""
    End If
  If h >= 24 Then
      d = Int(h / 24)
      h = h - 24 * d
      ShowTime = Pluralize(d, "day") & ", " & Pluralize(CInt(h), "hour") & m
    Else
      ShowTime = Pluralize(CInt(h), "hour") & m
    End If
End Function

Function Min(a As Integer, b As Integer)
  If a < b Then Min = a Else Min = b
End Function

Function Max(a As Integer, b As Integer)
  If a > b Then Max = a Else Max = b
End Function

