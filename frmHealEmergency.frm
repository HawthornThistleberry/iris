VERSION 5.00
Object = "{C1A8AF28-1257-101B-8FB0-0020AF039CA3}#1.1#0"; "MCI32.OCX"
Begin VB.Form frmHealEmergency 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Heal: Life-Threatening Emergencies"
   ClientHeight    =   6990
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3990
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6990
   ScaleWidth      =   3990
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   2400
      TabIndex        =   23
      Top             =   6600
      Width           =   1575
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   0
      TabIndex        =   22
      Top             =   6600
      Width           =   1575
   End
   Begin VB.TextBox txtOther2 
      Height          =   285
      Left            =   3240
      MaxLength       =   4
      TabIndex        =   21
      Top             =   5040
      Width           =   735
   End
   Begin VB.TextBox txtOther1 
      Height          =   285
      Left            =   3240
      MaxLength       =   4
      TabIndex        =   19
      Top             =   4680
      Width           =   735
   End
   Begin VB.TextBox txtPhysicianSkill 
      Height          =   285
      Left            =   3240
      MaxLength       =   4
      TabIndex        =   3
      Top             =   1440
      Width           =   735
   End
   Begin VB.TextBox txtRoll 
      Height          =   285
      Left            =   3240
      MaxLength       =   4
      TabIndex        =   2
      Top             =   1080
      Width           =   735
   End
   Begin VB.ComboBox cmbPatientState 
      Height          =   315
      ItemData        =   "frmHealEmergency.frx":0000
      Left            =   1560
      List            =   "frmHealEmergency.frx":000B
      Style           =   2  'Dropdown List
      TabIndex        =   17
      Top             =   4320
      Width           =   1575
   End
   Begin VB.ComboBox cmbEquipmentUsed 
      Height          =   315
      ItemData        =   "frmHealEmergency.frx":0027
      Left            =   1560
      List            =   "frmHealEmergency.frx":004A
      Style           =   2  'Dropdown List
      TabIndex        =   15
      Top             =   3960
      Width           =   1575
   End
   Begin VB.ComboBox cmbWoundType 
      Height          =   315
      ItemData        =   "frmHealEmergency.frx":00BB
      Left            =   1560
      List            =   "frmHealEmergency.frx":00DE
      Style           =   2  'Dropdown List
      TabIndex        =   13
      Top             =   3600
      Width           =   1575
   End
   Begin VB.TextBox txtDiesIn 
      Height          =   285
      Left            =   1560
      MaxLength       =   4
      TabIndex        =   10
      Top             =   2880
      Width           =   495
   End
   Begin VB.TextBox txtBleedingSeverity 
      Height          =   285
      Left            =   1560
      MaxLength       =   4
      TabIndex        =   8
      Top             =   2520
      Width           =   495
   End
   Begin VB.ComboBox cmbTreatingSelf 
      Height          =   315
      ItemData        =   "frmHealEmergency.frx":015E
      Left            =   1560
      List            =   "frmHealEmergency.frx":016F
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   2160
      Width           =   1575
   End
   Begin VB.CheckBox chkDiesInstantly 
      Caption         =   "Dies instantly"
      Height          =   255
      Left            =   0
      TabIndex        =   11
      Top             =   3240
      Width           =   3015
   End
   Begin VB.CheckBox chkOther2 
      Caption         =   "Other"
      Height          =   255
      Left            =   0
      TabIndex        =   20
      Top             =   5040
      Width           =   3135
   End
   Begin VB.CheckBox chkOther1 
      Caption         =   "Other"
      Height          =   255
      Left            =   0
      TabIndex        =   18
      Top             =   4680
      Width           =   3135
   End
   Begin VB.CheckBox chkPatientState 
      Caption         =   "Patient state"
      Height          =   255
      Left            =   0
      TabIndex        =   16
      Top             =   4320
      Width           =   1575
   End
   Begin VB.CheckBox chkEquipmentUsed 
      Caption         =   "Equipment used"
      Height          =   255
      Left            =   0
      TabIndex        =   14
      Top             =   3960
      Value           =   1  'Checked
      Width           =   1575
   End
   Begin VB.CheckBox chkWoundType 
      Caption         =   "Wound type"
      Height          =   255
      Left            =   0
      TabIndex        =   12
      Top             =   3600
      Value           =   1  'Checked
      Width           =   1575
   End
   Begin VB.CheckBox chkDiesIn 
      Caption         =   "Dies in"
      Height          =   255
      Left            =   0
      TabIndex        =   9
      Top             =   2880
      Width           =   1575
   End
   Begin VB.CheckBox chkBleedingSeverity 
      Caption         =   "Bleeding severity"
      Height          =   255
      Left            =   0
      TabIndex        =   7
      Top             =   2520
      Width           =   1575
   End
   Begin VB.CheckBox chkTreatingSelf 
      Caption         =   "Treating yourself"
      Height          =   255
      Left            =   0
      TabIndex        =   5
      Top             =   2160
      Width           =   1695
   End
   Begin VB.CheckBox chkDiagnosis 
      Caption         =   "Diagnosis skill completed?"
      Height          =   255
      Left            =   0
      TabIndex        =   4
      Top             =   1800
      Width           =   3015
   End
   Begin VB.CheckBox chkPhysicianSkill 
      Caption         =   "Physician Skill"
      Height          =   255
      Left            =   0
      TabIndex        =   28
      Top             =   1440
      Value           =   2  'Grayed
      Width           =   3015
   End
   Begin VB.CheckBox chkRoll 
      Height          =   255
      Left            =   0
      TabIndex        =   27
      Top             =   1080
      Value           =   2  'Grayed
      Width           =   210
   End
   Begin VB.CommandButton cmdRoll 
      Caption         =   "Roll"
      Height          =   285
      Left            =   240
      TabIndex        =   1
      Top             =   1080
      Width           =   2895
   End
   Begin VB.ComboBox cmbCombatants 
      Height          =   315
      HelpContextID   =   340
      Left            =   1560
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   0
      Width           =   2415
   End
   Begin MCI.MMControl mciControl 
      Height          =   330
      Left            =   120
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   600
      Visible         =   0   'False
      Width           =   3540
      _ExtentX        =   6244
      _ExtentY        =   582
      _Version        =   393216
      PlayEnabled     =   -1  'True
      DeviceType      =   "WaveAudio"
      FileName        =   "C:\Program Files\IRIS\dice.wav"
   End
   Begin VB.Image imgDice 
      Height          =   480
      Left            =   1740
      MousePointer    =   4  'Icon
      Picture         =   "frmHealEmergency.frx":01A3
      Top             =   6540
      Width           =   480
   End
   Begin VB.Label lblOutcome 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Outcome of healing described here."
      Height          =   660
      Left            =   0
      TabIndex        =   41
      Top             =   5880
      Width           =   3975
   End
   Begin VB.Label lblResultName 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   0
      TabIndex        =   40
      Top             =   5520
      Width           =   3135
   End
   Begin VB.Label lblTotal 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   3240
      TabIndex        =   39
      Top             =   5520
      Width           =   735
   End
   Begin VB.Line linBotSep 
      X1              =   0
      X2              =   3960
      Y1              =   5400
      Y2              =   5400
   End
   Begin VB.Label lblWoundDescription 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Description of the wound to be healed."
      Height          =   495
      Left            =   0
      TabIndex        =   26
      Top             =   360
      Width           =   3975
   End
   Begin VB.Label lblPatientState 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Left            =   3240
      TabIndex        =   38
      Top             =   4320
      Width           =   735
   End
   Begin VB.Label lblEquipmentUsed 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Left            =   3240
      TabIndex        =   37
      Top             =   3960
      Width           =   735
   End
   Begin VB.Label lblWoundType 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Left            =   3240
      TabIndex        =   36
      Top             =   3600
      Width           =   735
   End
   Begin VB.Label lblDiesInstantly 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "-50"
      Height          =   255
      Left            =   3240
      TabIndex        =   35
      Top             =   3240
      Width           =   735
   End
   Begin VB.Label lblDiesIn 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Left            =   3240
      TabIndex        =   34
      Top             =   2880
      Width           =   735
   End
   Begin VB.Label lblBleedingSeverity 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Left            =   3240
      TabIndex        =   33
      Top             =   2520
      Width           =   735
   End
   Begin VB.Label lblTreatingSelf 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Left            =   3240
      TabIndex        =   32
      Top             =   2160
      Width           =   735
   End
   Begin VB.Label lblDiagnosis 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "+3"
      Height          =   255
      Left            =   3240
      TabIndex        =   31
      Top             =   1800
      Width           =   735
   End
   Begin VB.Label lblRounds 
      Caption         =   "rounds"
      Height          =   255
      Left            =   2160
      TabIndex        =   30
      Top             =   2910
      Width           =   735
   End
   Begin VB.Label lblPerRound 
      Caption         =   "per round"
      Height          =   255
      Left            =   2160
      TabIndex        =   29
      Top             =   2550
      Width           =   735
   End
   Begin VB.Line linTopSep 
      X1              =   0
      X2              =   3960
      Y1              =   960
      Y2              =   960
   End
   Begin VB.Label lblCombatant 
      Caption         =   "Combatant To Heal:"
      Height          =   255
      Left            =   0
      TabIndex        =   25
      Top             =   60
      Width           =   1455
   End
End
Attribute VB_Name = "frmHealEmergency"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private UserInput As Boolean
Private OutcomeDeath As Boolean
Private OutcomeDiesIn As Integer
Private OutcomeBleed As Integer
Private OutcomeHits As Integer

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
  
  txtPhysicianSkill = ""
  cmbEquipmentUsed.ListIndex = 2
  
  ' set up the sound to play
  mciControl.Notify = False
  mciControl.Wait = True
  mciControl.Shareable = False
  mciControl.DeviceType = "WaveAudio"
  mciControl.Filename = App.Path & "\dice.wav"
  mciControl.Command = "Open"
End Sub

Private Sub Form_Activate()
  Dim i As Integer
  If cmdCancel.Tag <> "l" Then Exit Sub
  UserInput = False
  cmdCancel.Tag = ""
  cmdOK.Tag = ""
  ' set up the combatants list
  cmbCombatants.Clear
  For i = 1 To NumCombatants
    cmbCombatants.AddItem Combatants(i).Name
    Next i
  ' choose combatant (triggers cmbCombatants_Click)
  If frmIRIS.lisCombatants.ListIndex >= 0 Then
      cmbCombatants.ListIndex = frmIRIS.lisCombatants.ListIndex
    Else
      cmbCombatants.ListIndex = 0
    End If
  ' load other fields
  cmdRoll_Click
  ' allow physician skill field to remain from last time
  chkDiagnosis = 0
  chkTreatingSelf = 0
  cmbTreatingSelf.ListIndex = 1
  cmbTreatingSelf.Enabled = False
  chkWoundType = 1
  cmbWoundType.ListIndex = 6
  cmbWoundType.Enabled = True
  chkEquipmentUsed = 1
  ' allow equipment type to remain from last time
  cmbEquipmentUsed_Click
  cmbEquipmentUsed.Enabled = True
  chkPatientState = 0
  cmbPatientState.ListIndex = 0
  cmbPatientState.Enabled = False
  chkOther1 = 0
  txtOther1 = ""
  chkOther2 = 0
  txtOther2 = ""
  txtRoll.SetFocus
  UserInput = True
End Sub

Private Sub cmbCombatants_Click()
  lblWoundDescription = ""
  With Combatants(cmbCombatants.ListIndex + 1)
    If .RoundsTillDead > 0 Then
        lblWoundDescription = lblWoundDescription & "Dies in " & CStr(.RoundsTillDead) & " rounds" & vbCrLf
        chkDiesIn = 1
        txtDiesIn = CStr(.RoundsTillDead)
        txtDiesIn.Enabled = True
      Else
        chkDiesIn = 0
        txtDiesIn = ""
        txtDiesIn.Enabled = False
      End If
    If .Bleed > 0 Then
        lblWoundDescription = lblWoundDescription & "Bleeds " & CStr(.Bleed) & " hits per round" & vbCrLf
        chkBleedingSeverity = 1
        txtBleedingSeverity = CStr(.Bleed)
        txtBleedingSeverity.Enabled = True
      Else
        chkBleedingSeverity = 0
        txtBleedingSeverity = ""
        txtBleedingSeverity.Enabled = False
      End If
    If lblWoundDescription = "" Then
        lblWoundDescription = "No life-threatening emergencies!"
        cmdOK.Enabled = False
      Else
        cmdOK.Enabled = True
      End If
    End With
End Sub

' dice roll button
Private Sub cmdRoll_Click()
  txtRoll = OpenEndedD20("open")
  mciControl.Command = "Play"
  mciControl.Command = "Prev"
End Sub

' text fields get selected on focus, only allow digits and sometimes minuses
Private Sub txtRoll_GotFocus()
  Call SelectField(txtRoll)
End Sub

Private Sub txtRoll_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtPhysicianSkill_GotFocus()
  Call SelectField(txtPhysicianSkill)
End Sub

Private Sub txtPhysicianSkill_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtBleedingSeverity_GotFocus()
  Call SelectField(txtBleedingSeverity)
End Sub

Private Sub txtBleedingSeverity_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtDiesIn_GotFocus()
  Call SelectField(txtDiesIn)
End Sub

Private Sub txtDiesIn_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtOther1_GotFocus()
  Call SelectField(txtOther1)
End Sub

Private Sub txtOther1_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtOther2_GotFocus()
  Call SelectField(txtOther2)
End Sub

Private Sub txtOther2_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

' all checkboxes update, enable/disable, set focus on their fields
Private Sub chkDiagnosis_Click()
  Call lblTotal_Update
End Sub

Private Sub chkTreatingSelf_Click()
  If chkTreatingSelf = 1 Then cmbTreatingSelf.Enabled = True Else cmbTreatingSelf.Enabled = False
  Call lblTotal_Update
  If UserInput And chkTreatingSelf = 1 Then cmbTreatingSelf.SetFocus
End Sub

Private Sub chkBleedingSeverity_Click()
  If chkBleedingSeverity = 1 Then txtBleedingSeverity.Enabled = True Else txtBleedingSeverity.Enabled = False
  Call lblTotal_Update
  If UserInput And chkBleedingSeverity = 1 Then txtBleedingSeverity.SetFocus
End Sub

Private Sub chkDiesIn_Click()
  If chkDiesIn = 1 Then txtDiesIn.Enabled = True Else txtDiesIn.Enabled = False
  Call lblTotal_Update
  If UserInput And chkDiesIn = 1 Then txtDiesIn.SetFocus
End Sub

Private Sub chkDiesInstantly_Click()
  Call lblTotal_Update
End Sub

Private Sub chkWoundType_Click()
  If chkWoundType = 1 Then cmbWoundType.Enabled = True Else cmbWoundType.Enabled = False
  Call lblTotal_Update
  If UserInput And chkWoundType = 1 Then cmbWoundType.SetFocus
End Sub

Private Sub chkEquipmentUsed_Click()
  If chkEquipmentUsed = 1 Then cmbEquipmentUsed.Enabled = True Else cmbEquipmentUsed.Enabled = False
  Call lblTotal_Update
  If UserInput And chkEquipmentUsed = 1 Then cmbEquipmentUsed.SetFocus
End Sub

Private Sub chkPatientState_Click()
  If chkPatientState = 1 Then cmbPatientState.Enabled = True Else cmbPatientState.Enabled = False
  Call lblTotal_Update
  If UserInput And chkPatientState = 1 Then cmbPatientState.SetFocus
End Sub

Private Sub chkOther1_Click()
  If chkOther1 = 1 Then txtOther1.Enabled = True Else txtOther1.Enabled = False
  Call lblTotal_Update
  If UserInput And chkOther1 = 1 Then txtOther1.SetFocus
End Sub

Private Sub chkOther2_Click()
  If chkOther2 = 1 Then txtOther2.Enabled = True Else txtOther2.Enabled = False
  Call lblTotal_Update
  If UserInput And chkOther2 = 1 Then txtOther2.SetFocus
End Sub

' all middle fields cause updates to their label fields
Private Sub cmbTreatingSelf_Click()
  lblTreatingSelf = cmbTreatingSelf.ItemData(cmbTreatingSelf.ListIndex)
  If Val(lblTreatingSelf) > 0 Then lblTreatingSelf = "+" & lblTreatingSelf
  Call lblTotal_Update
End Sub

Private Sub cmbWoundType_Click()
  lblWoundType = cmbWoundType.ItemData(cmbWoundType.ListIndex)
  If Val(lblWoundType) > 0 Then lblWoundType = "+" & lblWoundType
  Call lblTotal_Update
End Sub

Private Sub cmbEquipmentUsed_Click()
  lblEquipmentUsed = cmbEquipmentUsed.ItemData(cmbEquipmentUsed.ListIndex)
  If Val(lblEquipmentUsed) > 0 Then lblEquipmentUsed = "+" & lblEquipmentUsed
  Call lblTotal_Update
End Sub

Private Sub cmbPatientState_Click()
  lblPatientState = cmbPatientState.ItemData(cmbPatientState.ListIndex)
  If Val(lblPatientState) > 0 Then lblPatientState = "+" & lblPatientState
  Call lblTotal_Update
End Sub

Private Sub txtBleedingSeverity_Change()
  If Val(txtBleedingSeverity) = 0 Then
      lblBleedingSeverity = "0"
    ElseIf Val(txtBleedingSeverity) <= 4 Then
      lblBleedingSeverity = 6 - 3 * Val(txtBleedingSeverity)
    Else
      lblBleedingSeverity = 15 - 5 * Val(txtBleedingSeverity)
    End If
  If Val(lblBleedingSeverity) > 0 Then lblBleedingSeverity = "+" & lblBleedingSeverity
  Call lblTotal_Update
End Sub

Private Sub txtDiesIn_Change()
  If Val(txtDiesIn) = 0 Then
      lblDiesIn = "0"
    Else
      lblDiesIn = Val(txtDiesIn) - 40
    End If
  If Val(lblDiesIn) > 0 Then lblDiesIn = "+" & lblDiesIn
  Call lblTotal_Update
End Sub

' all updates to any remaining fields updates results
Private Sub txtRoll_Change()
  Call lblTotal_Update
End Sub

Private Sub txtPhysicianSkill_Change()
  Call lblTotal_Update
End Sub

Private Sub txtOther1_Change()
  Call lblTotal_Update
End Sub

Private Sub txtOther2_Change()
  Call lblTotal_Update
End Sub

Private Sub lblTotal_Update()
  Dim i As Integer
  Dim resultname As String
  Dim dummy1 As String
  Dim outcome As String
  ' calculate total
  i = Val(txtRoll) + Val(txtPhysicianSkill)
  If chkDiagnosis Then i = i + Val(lblDiagnosis)
  If chkTreatingSelf Then i = i + Val(lblTreatingSelf)
  If chkBleedingSeverity Then i = i + Val(lblBleedingSeverity)
  If chkDiesIn Then i = i + Val(lblDiesIn)
  If chkDiesInstantly Then i = i + Val(lblDiesInstantly)
  If chkWoundType Then i = i + Val(lblWoundType)
  If chkEquipmentUsed Then i = i + Val(lblEquipmentUsed)
  If chkPatientState Then i = i + Val(lblPatientState)
  If chkOther1 Then i = i + Val(txtOther1)
  If chkOther2 Then i = i + Val(txtOther2)
  ' fill in total and lookup
  lblTotal = i
  ManeuverChart i, False, dummy1, resultname, outcome
  lblResultName = resultname
  ' start outcome with no change
  OutcomeDeath = False
  OutcomeDiesIn = Combatants(cmbCombatants.ListIndex + 1).RoundsTillDead
  OutcomeBleed = Combatants(cmbCombatants.ListIndex + 1).Bleed
  OutcomeHits = 0
  outcome = ""
  ' figure out outcome text and values
  If resultname = "Extraordinary Failure" Then
      If (Combatants(cmbCombatants.ListIndex + 1).RoundsTillDead <> 0 And _
         Combatants(cmbCombatants.ListIndex + 1).RoundsTillDead <= 6) Or _
         Combatants(cmbCombatants.ListIndex + 1).Bleed > 6 Then
          outcome = "Victim is now dead."
          OutcomeDeath = True
        Else
          outcome = "Victim will now die in 6 rounds."
          OutcomeDiesIn = 6
        End If
      outcome = outcome & "  Rattled, you are at -10 to all medicine-related rolls for two hours."
    ElseIf resultname = "Fumble" Then
      If Combatants(cmbCombatants.ListIndex + 1).RoundsTillDead <> 0 And _
         Combatants(cmbCombatants.ListIndex + 1).RoundsTillDead <= 6 Then
          OutcomeDiesIn = Int(OutcomeDiesIn / 1.5 + 0.5)
        End If
      If Combatants(cmbCombatants.ListIndex + 1).Bleed > 0 Then
          OutcomeBleed = Int(OutcomeBleed * 1.5 + 0.5)
        End If
      outcome = "The wound is now 50% worse than before."
    ElseIf resultname = "Failure" Then
      outcome = "No effect, save that you might have used up supplies."
    ElseIf resultname = "Partial Success" Then
      If Combatants(cmbCombatants.ListIndex + 1).RoundsTillDead <> 0 And _
         Combatants(cmbCombatants.ListIndex + 1).RoundsTillDead <= 6 Then
          OutcomeDiesIn = Int(OutcomeDiesIn * 2 + 0.5)
        End If
      If Combatants(cmbCombatants.ListIndex + 1).Bleed > 0 Then
          OutcomeBleed = Int(OutcomeBleed / 2 + 0.5)
        End If
      outcome = "Bandages are slowing blood loss by half, but take the bandage off and the victim is right back where he started."
    ElseIf resultname = "Near Success" Or resultname = "Unrelated Success" Then
      If Combatants(cmbCombatants.ListIndex + 1).RoundsTillDead > 0 Then
          OutcomeDiesIn = 0
          OutcomeBleed = 2
        ElseIf Combatants(cmbCombatants.ListIndex + 1).Bleed = 1 Then
          OutcomeHits = -5
          OutcomeDiesIn = 0
          OutcomeBleed = 0
        Else
          OutcomeDiesIn = 0
          OutcomeBleed = 1
        End If
      If resultname = "Unrelated Success" Then OutcomeHits = OutcomeHits + 10
      outcome = "The wound's blood flow is mostly stopped, but there's still blood seeping through the clots."
    ElseIf resultname = "Success" Then
      OutcomeDiesIn = 0
      OutcomeBleed = 0
      outcome = "The blood flow or danger is stopped, but bandages and clots will reopen on any motion greater than a slow, gentle walk."
    ElseIf resultname = "Critical Success" Then
      OutcomeDiesIn = 0
      OutcomeBleed = 0
      outcome = "The danger is stopped and wounds are scabbed or well-sealed (will only open on sudden or violent movements)."
    ElseIf resultname = "Extraordinary Success" Then
      OutcomeDiesIn = 0
      OutcomeBleed = 0
      outcome = "The wound is completely sealed and can be completely ignored."
    End If
  lblOutcome = outcome
End Sub

Private Sub cmdOK_Click()
  Me.Hide
  With Combatants(cmbCombatants.ListIndex + 1)
    If OutcomeDeath Then
        .Position = 10
        frmIRIS.rtbLog_Append "\par\b " & frmIRIS.Phase & "\plain\f2\fs17 : " & .Name & " died while being healed."
      End If
    If OutcomeDiesIn <> .RoundsTillDead Then
        If OutcomeDiesIn = 0 Then
            frmIRIS.rtbLog_Append "\par\b " & frmIRIS.Phase & "\plain\f2\fs17 : " & .Name & " healed, will no longer die in " & .RoundsTillDead & " rounds."
          Else
            frmIRIS.rtbLog_Append "\par\b " & frmIRIS.Phase & "\plain\f2\fs17 : " & .Name & " healed, will die in " & OutcomeDiesIn & " instead of " & .RoundsTillDead & " rounds."
          End If
        .RoundsTillDead = OutcomeDiesIn
      End If
    If OutcomeBleed <> .Bleed Then
        If OutcomeBleed = 0 Then
            frmIRIS.rtbLog_Append "\par\b " & frmIRIS.Phase & "\plain\f2\fs17 : " & .Name & " healed, no longer bleeding " & .Bleed & " per round."
          Else
            frmIRIS.rtbLog_Append "\par\b " & frmIRIS.Phase & "\plain\f2\fs17 : " & .Name & " healed, bleeding " & OutcomeBleed & " instead of " & .Bleed & " per round."
          End If
        .Bleed = OutcomeBleed
      End If
    If OutcomeHits <> 0 Then
        If OutcomeHits > 0 Then
            frmIRIS.rtbLog_Append "\par\b " & frmIRIS.Phase & "\plain\f2\fs17 : " & .Name & " gains " & OutcomeHits & " due to healing."
          Else
            frmIRIS.rtbLog_Append "\par\b " & frmIRIS.Phase & "\plain\f2\fs17 : " & .Name & " loses " & -OutcomeHits & " as a side effect of healing."
          End If
        .CurHp = .CurHp + OutcomeHits
        If .CurHp > .OptHp Then .CurHp = .OptHp
        If .CurHp <= 0 And .Position <> 10 Then
            MsgBox .Name & " is now unconscious.", vbInformation
            .Position = 9
            frmIRIS.rtbLog_Append "\par\b " & frmIRIS.Phase & "\plain\f2\fs17 : " & .Name & " is now unconscious."
          End If
      End If
    End With
End Sub

Private Sub cmdCancel_Click()
  cmdCancel.Tag = "y"
  Me.Hide
End Sub

Private Sub imgDice_Click()
  frmIRIS.imgDice_Click
End Sub

Private Sub Form_Unload(Cancel As Integer)
  mciControl.Command = "Close"
End Sub

