VERSION 5.00
Begin VB.Form frmConvertCoC 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Convert Combatant: Call of Cthulhu"
   ClientHeight    =   4830
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6150
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4830
   ScaleWidth      =   6150
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   4920
      TabIndex        =   26
      Top             =   4440
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   3600
      TabIndex        =   25
      Top             =   4440
      Width           =   1215
   End
   Begin VB.Frame fraNotes 
      Caption         =   "Notes"
      Height          =   1695
      Left            =   2400
      TabIndex        =   42
      Top             =   2640
      Width           =   3735
      Begin VB.TextBox txtNotes 
         Height          =   1335
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   24
         Top             =   240
         Width           =   3495
      End
   End
   Begin VB.Frame fraAttackSkills 
      Caption         =   "Attack Skills"
      Height          =   2055
      Left            =   2400
      TabIndex        =   41
      Top             =   480
      Width           =   3735
      Begin VB.TextBox txtAttackOB 
         Height          =   285
         Index           =   4
         Left            =   3120
         MaxLength       =   4
         TabIndex        =   23
         Top             =   1680
         Width           =   495
      End
      Begin VB.TextBox txtAttackOB 
         Height          =   285
         Index           =   3
         Left            =   3120
         MaxLength       =   4
         TabIndex        =   21
         Top             =   1320
         Width           =   495
      End
      Begin VB.TextBox txtAttackOB 
         Height          =   285
         Index           =   2
         Left            =   3120
         MaxLength       =   4
         TabIndex        =   19
         Top             =   960
         Width           =   495
      End
      Begin VB.TextBox txtAttackOB 
         Height          =   285
         Index           =   1
         Left            =   3120
         MaxLength       =   4
         TabIndex        =   17
         Top             =   600
         Width           =   495
      End
      Begin VB.TextBox txtAttack 
         Height          =   285
         Index           =   4
         Left            =   120
         TabIndex        =   22
         Top             =   1680
         Width           =   2895
      End
      Begin VB.TextBox txtAttack 
         Height          =   285
         Index           =   3
         Left            =   120
         TabIndex        =   20
         Top             =   1320
         Width           =   2895
      End
      Begin VB.TextBox txtAttack 
         Height          =   285
         Index           =   2
         Left            =   120
         TabIndex        =   18
         Top             =   960
         Width           =   2895
      End
      Begin VB.TextBox txtAttack 
         Height          =   285
         Index           =   1
         Left            =   120
         TabIndex        =   16
         Top             =   600
         Width           =   2895
      End
      Begin VB.TextBox txtAttackOB 
         Height          =   285
         Index           =   0
         Left            =   3120
         MaxLength       =   4
         TabIndex        =   15
         Top             =   240
         Width           =   495
      End
      Begin VB.TextBox txtAttack 
         Height          =   285
         Index           =   0
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   2895
      End
   End
   Begin VB.Frame fraVitals 
      Caption         =   "Vitals"
      Height          =   1695
      Left            =   0
      TabIndex        =   37
      Top             =   2640
      Width           =   2295
      Begin VB.TextBox txtHeight 
         Height          =   285
         Left            =   1440
         MaxLength       =   4
         TabIndex        =   13
         Top             =   1320
         Width           =   735
      End
      Begin VB.TextBox txtSanity 
         Height          =   285
         Left            =   1440
         MaxLength       =   4
         TabIndex        =   12
         Top             =   960
         Width           =   735
      End
      Begin VB.TextBox txtDodge 
         BackColor       =   &H80000014&
         Height          =   285
         Left            =   1440
         MaxLength       =   4
         TabIndex        =   11
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox txtSpotHidden 
         BackColor       =   &H80000014&
         Height          =   285
         Left            =   1440
         MaxLength       =   4
         TabIndex        =   10
         Top             =   240
         Width           =   735
      End
      Begin VB.Label lblHeight 
         Alignment       =   1  'Right Justify
         Caption         =   "Height"
         Height          =   255
         Left            =   120
         TabIndex        =   43
         Top             =   1320
         Width           =   1215
      End
      Begin VB.Label lblSanity 
         Alignment       =   1  'Right Justify
         Caption         =   "Sanity"
         Height          =   255
         Left            =   120
         TabIndex        =   40
         Top             =   990
         Width           =   1215
      End
      Begin VB.Label lblDodge 
         Alignment       =   1  'Right Justify
         Caption         =   "Dodge"
         Height          =   255
         Left            =   120
         TabIndex        =   39
         Top             =   630
         Width           =   1215
      End
      Begin VB.Label lblSpotHidden 
         Alignment       =   1  'Right Justify
         Caption         =   "Spot Hidden"
         Height          =   255
         Left            =   120
         TabIndex        =   38
         Top             =   270
         Width           =   1215
      End
   End
   Begin VB.TextBox txtName 
      Height          =   285
      Left            =   720
      TabIndex        =   0
      Top             =   60
      Width           =   5295
   End
   Begin VB.Frame fraAttributes 
      Caption         =   "Attributes"
      Height          =   2055
      Left            =   0
      TabIndex        =   27
      Top             =   480
      Width           =   2295
      Begin VB.ComboBox cmbHPMultiplier 
         Height          =   315
         ItemData        =   "frmConvertCoC.frx":0000
         Left            =   600
         List            =   "frmConvertCoC.frx":000D
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   1680
         Width           =   1575
      End
      Begin VB.TextBox txtHP 
         Height          =   285
         Left            =   1800
         MaxLength       =   4
         TabIndex        =   8
         Top             =   1320
         Width           =   375
      End
      Begin VB.TextBox txtPOW 
         Height          =   285
         Left            =   1800
         MaxLength       =   4
         TabIndex        =   7
         Top             =   960
         Width           =   375
      End
      Begin VB.TextBox txtDEX 
         Height          =   285
         Left            =   1800
         MaxLength       =   4
         TabIndex        =   6
         Top             =   600
         Width           =   375
      End
      Begin VB.TextBox txtINT 
         Height          =   285
         Left            =   1800
         MaxLength       =   4
         TabIndex        =   5
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox txtAPP 
         Height          =   285
         Left            =   600
         MaxLength       =   4
         TabIndex        =   4
         Top             =   1320
         Width           =   375
      End
      Begin VB.TextBox txtSIZ 
         Height          =   285
         Left            =   600
         MaxLength       =   4
         TabIndex        =   3
         Top             =   960
         Width           =   375
      End
      Begin VB.TextBox txtCON 
         Height          =   285
         Left            =   600
         MaxLength       =   4
         TabIndex        =   2
         Top             =   600
         Width           =   375
      End
      Begin VB.TextBox txtSTR 
         Height          =   285
         Left            =   600
         MaxLength       =   4
         TabIndex        =   1
         Top             =   240
         Width           =   375
      End
      Begin VB.Label lblType 
         Caption         =   "Type"
         Height          =   255
         Left            =   120
         TabIndex        =   44
         Top             =   1740
         Width           =   450
      End
      Begin VB.Label lblHP 
         Caption         =   "HP"
         Height          =   255
         Left            =   1200
         TabIndex        =   36
         Top             =   1350
         Width           =   375
      End
      Begin VB.Label lblPOW 
         Caption         =   "POW"
         Height          =   255
         Left            =   1200
         TabIndex        =   35
         Top             =   990
         Width           =   495
      End
      Begin VB.Label lblDEX 
         Caption         =   "DEX"
         Height          =   255
         Left            =   1200
         TabIndex        =   34
         Top             =   630
         Width           =   495
      End
      Begin VB.Label lblINT 
         Caption         =   "INT"
         Height          =   255
         Left            =   1200
         TabIndex        =   33
         Top             =   270
         Width           =   495
      End
      Begin VB.Label lblAPP 
         Caption         =   "APP"
         Height          =   255
         Left            =   120
         TabIndex        =   32
         Top             =   1350
         Width           =   375
      End
      Begin VB.Label lblSIZ 
         Caption         =   "SIZ"
         Height          =   255
         Left            =   120
         TabIndex        =   31
         Top             =   990
         Width           =   375
      End
      Begin VB.Label lblCON 
         Caption         =   "CON"
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   630
         Width           =   375
      End
      Begin VB.Label lblSTR 
         Caption         =   "STR"
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   270
         Width           =   375
      End
   End
   Begin VB.Label lblName 
      Alignment       =   1  'Right Justify
      Caption         =   "Name"
      Height          =   255
      Left            =   0
      TabIndex        =   28
      Top             =   105
      Width           =   615
   End
   Begin VB.Image imgDice 
      Height          =   480
      Left            =   2880
      MousePointer    =   4  'Icon
      Picture         =   "frmConvertCoC.frx":0042
      Top             =   4380
      Width           =   480
   End
End
Attribute VB_Name = "frmConvertCoC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
End Sub

Private Sub Form_Activate()
  Dim i As Integer
  cmdCancel.Tag = ""
  cmdOK.Tag = ""
  ' allow optHumanoid/optCreature to default to where it was last
  ' load other fields
  txtSTR = ""
  txtCON = ""
  txtSIZ = ""
  txtAPP = ""
  txtINT = ""
  txtDEX = ""
  txtPOW = ""
  txtHP = ""
  txtSpotHidden = ""
  txtDodge = ""
  txtSanity = ""
  txtHeight = ""
  cmbHPMultiplier.ListIndex = 0
  For i = 0 To 4
    txtAttack(i) = ""
    txtAttackOB(i) = ""
    Next
  txtNotes = ""
  txtName.SetFocus
End Sub

Private Sub cmdOK_Click()
  If txtSTR = "" Then txtSTR = "10"
  If txtCON = "" Then txtCON = "10"
  If txtSIZ = "" Then txtSIZ = "10"
  If txtAPP = "" Then txtAPP = "10"
  If txtINT = "" Then txtINT = "10"
  If txtDEX = "" Then txtDEX = "10"
  If txtPOW = "" Then txtPOW = "10"
  If txtHP = "" Then txtHP = "10"
  If txtSpotHidden = "" Then txtSpotHidden = "50"
  If txtDodge = "" Then txtDodge = "0"
  If txtSanity = "" Then txtSanity = "50"
  If txtHeight = "" Then txtHeight = "66"
  Me.Hide
End Sub

Private Sub cmdCancel_Click()
  cmdCancel.Tag = "y"
  Me.Hide
End Sub

Private Sub txtName_GotFocus()
  Call SelectField(txtName)
End Sub

Private Sub txtName_KeyPress(KeyAscii As Integer)
  KeyAscii = NoQuotes(KeyAscii)
End Sub

Private Sub txtSTR_GotFocus()
  Call SelectField(txtSTR)
End Sub

Private Sub txtSTR_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtCON_GotFocus()
  Call SelectField(txtCON)
End Sub

Private Sub txtCON_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtSIZ_GotFocus()
  Call SelectField(txtSIZ)
End Sub

Private Sub txtSIZ_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtAPP_GotFocus()
  Call SelectField(txtAPP)
End Sub

Private Sub txtAPP_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtINT_GotFocus()
  Call SelectField(txtINT)
End Sub

Private Sub txtINT_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtDEX_GotFocus()
  Call SelectField(txtDEX)
End Sub

Private Sub txtDEX_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtPOW_GotFocus()
  Call SelectField(txtPOW)
End Sub

Private Sub txtPOW_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtHP_GotFocus()
  Call SelectField(txtHP)
End Sub

Private Sub txtHP_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtSpotHidden_GotFocus()
  Call SelectField(txtSpotHidden)
End Sub

Private Sub txtSpotHidden_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtDodge_GotFocus()
  Call SelectField(txtDodge)
End Sub

Private Sub txtDodge_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtSanity_GotFocus()
  Call SelectField(txtSanity)
End Sub

Private Sub txtSanity_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtHeight_GotFocus()
  Call SelectField(txtHeight)
End Sub

Private Sub txtHeight_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtAttack_GotFocus(Index As Integer)
  Call SelectField(txtAttack(Index))
End Sub

Private Sub txtAttack_KeyPress(Index As Integer, KeyAscii As Integer)
  KeyAscii = NoQuotes(KeyAscii)
End Sub

Private Sub txtAttackOB_GotFocus(Index As Integer)
  Call SelectField(txtAttackOB(Index))
End Sub

Private Sub txtAttackOB_KeyPress(Index As Integer, KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtNotes_KeyPress(KeyAscii As Integer)
  KeyAscii = NoQuotes(KeyAscii)
End Sub

Private Sub imgDice_Click()
  frmIRIS.imgDice_Click
End Sub

