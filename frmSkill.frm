VERSION 5.00
Begin VB.Form frmSkill 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Skill / Maneuver"
   ClientHeight    =   4695
   ClientLeft      =   3645
   ClientTop       =   1560
   ClientWidth     =   5295
   ControlBox      =   0   'False
   Icon            =   "frmSkill.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4695
   ScaleWidth      =   5295
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.TextBox txtManaLevel 
      BackColor       =   &H00C0C0C0&
      Enabled         =   0   'False
      Height          =   315
      Left            =   4920
      TabIndex        =   39
      Text            =   "0"
      Top             =   1920
      Width           =   375
   End
   Begin VB.ComboBox cmbManaLevel 
      Enabled         =   0   'False
      Height          =   315
      ItemData        =   "frmSkill.frx":030A
      Left            =   3840
      List            =   "frmSkill.frx":0320
      Style           =   2  'Dropdown List
      TabIndex        =   38
      Top             =   1920
      Width           =   1095
   End
   Begin VB.TextBox txtTerrain 
      BackColor       =   &H00C0C0C0&
      Enabled         =   0   'False
      Height          =   315
      Left            =   4920
      TabIndex        =   37
      Text            =   "0"
      Top             =   1560
      Width           =   375
   End
   Begin VB.ComboBox cmbTerrain 
      Enabled         =   0   'False
      Height          =   315
      ItemData        =   "frmSkill.frx":034A
      Left            =   3840
      List            =   "frmSkill.frx":035D
      Style           =   2  'Dropdown List
      TabIndex        =   36
      Top             =   1560
      Width           =   1095
   End
   Begin VB.TextBox txtItemQuality 
      BackColor       =   &H00C0C0C0&
      Enabled         =   0   'False
      Height          =   315
      Left            =   4920
      TabIndex        =   35
      Text            =   "0"
      Top             =   1200
      Width           =   375
   End
   Begin VB.ComboBox cmbItemQuality 
      Enabled         =   0   'False
      Height          =   315
      ItemData        =   "frmSkill.frx":0388
      Left            =   3840
      List            =   "frmSkill.frx":039B
      Style           =   2  'Dropdown List
      TabIndex        =   34
      Top             =   1200
      Width           =   1095
   End
   Begin VB.TextBox txtOther4 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3840
      MaxLength       =   4
      TabIndex        =   33
      Top             =   2640
      Width           =   1455
   End
   Begin VB.TextBox txtOther3 
      Enabled         =   0   'False
      Height          =   285
      Left            =   3840
      MaxLength       =   4
      TabIndex        =   32
      Top             =   2280
      Width           =   1455
   End
   Begin VB.ComboBox cmbDifficulty 
      Height          =   315
      ItemData        =   "frmSkill.frx":03C4
      Left            =   3840
      List            =   "frmSkill.frx":03E9
      TabIndex        =   31
      Text            =   "0 (Medium)"
      Top             =   480
      Width           =   1455
   End
   Begin VB.TextBox txtOther2 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1440
      MaxLength       =   4
      TabIndex        =   30
      Top             =   2640
      Width           =   855
   End
   Begin VB.TextBox txtOther1 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1440
      MaxLength       =   4
      TabIndex        =   29
      Top             =   2280
      Width           =   855
   End
   Begin VB.TextBox txtEncumbrance 
      Height          =   285
      Left            =   1440
      MaxLength       =   4
      TabIndex        =   28
      Top             =   1920
      Width           =   855
   End
   Begin VB.TextBox txtPenalty 
      Height          =   285
      Left            =   1440
      MaxLength       =   4
      TabIndex        =   27
      Top             =   1560
      Width           =   855
   End
   Begin VB.TextBox txtSkill 
      Height          =   285
      Left            =   1440
      MaxLength       =   4
      TabIndex        =   26
      Top             =   1200
      Width           =   855
   End
   Begin VB.TextBox txtRoll 
      Height          =   285
      Left            =   1440
      MaxLength       =   4
      TabIndex        =   25
      Top             =   840
      Width           =   855
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   4080
      TabIndex        =   24
      Top             =   4320
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Take"
      Default         =   -1  'True
      Height          =   375
      Left            =   2640
      TabIndex        =   23
      Top             =   4320
      Width           =   1215
   End
   Begin VB.CommandButton cmdWSPlus 
      Caption         =   ">"
      Height          =   285
      Left            =   2280
      TabIndex        =   22
      Top             =   4365
      Width           =   255
   End
   Begin VB.TextBox txtWS 
      Height          =   285
      Left            =   1680
      MaxLength       =   4
      TabIndex        =   21
      Text            =   "0"
      Top             =   4365
      Width           =   615
   End
   Begin VB.CommandButton cmdWSMinus 
      Caption         =   "<"
      Height          =   285
      Left            =   1440
      TabIndex        =   20
      Top             =   4365
      Width           =   255
   End
   Begin VB.CommandButton cmdPushback 
      Caption         =   "Pushback"
      Height          =   375
      Left            =   0
      TabIndex        =   19
      Top             =   4320
      Width           =   1215
   End
   Begin VB.CheckBox chkOther4 
      Caption         =   "Other"
      Height          =   255
      Left            =   2520
      TabIndex        =   18
      Top             =   2640
      Width           =   1215
   End
   Begin VB.CheckBox chkOther3 
      Caption         =   "Other"
      Height          =   255
      Left            =   2520
      TabIndex        =   17
      Top             =   2280
      Width           =   1215
   End
   Begin VB.CheckBox chkManaLevel 
      Caption         =   "Mana Level"
      Height          =   255
      Left            =   2520
      TabIndex        =   16
      Top             =   1920
      Width           =   1215
   End
   Begin VB.CheckBox chkTerrain 
      Caption         =   "Terrain"
      Height          =   255
      Left            =   2520
      TabIndex        =   15
      Top             =   1560
      Width           =   1215
   End
   Begin VB.CheckBox chkItemQuality 
      Caption         =   "Item Quality"
      Height          =   255
      Left            =   2520
      TabIndex        =   14
      Top             =   1200
      Width           =   1215
   End
   Begin VB.CheckBox chkDistraction 
      Caption         =   "Distraction"
      Height          =   255
      Left            =   2520
      TabIndex        =   13
      Top             =   840
      Width           =   1215
   End
   Begin VB.CheckBox chkDifficulty 
      Caption         =   "Difficulty"
      Height          =   255
      Left            =   2520
      TabIndex        =   12
      Top             =   480
      Value           =   1  'Checked
      Width           =   1215
   End
   Begin VB.CheckBox chkOther2 
      Caption         =   "Other"
      Height          =   255
      Left            =   0
      TabIndex        =   11
      Top             =   2640
      Width           =   1335
   End
   Begin VB.CheckBox chkOther1 
      Caption         =   "Other"
      Height          =   255
      Left            =   0
      TabIndex        =   10
      Top             =   2280
      Width           =   1335
   End
   Begin VB.CheckBox chkEncumbrance 
      Caption         =   "Encumbrance"
      Height          =   255
      Left            =   0
      TabIndex        =   9
      Top             =   1920
      Value           =   1  'Checked
      Width           =   1335
   End
   Begin VB.CheckBox chkPenalty 
      Caption         =   "Penalty"
      Height          =   255
      Left            =   0
      TabIndex        =   8
      Top             =   1560
      Value           =   1  'Checked
      Width           =   1335
   End
   Begin VB.CheckBox chkSkill 
      Caption         =   "Skill"
      Height          =   255
      Left            =   0
      TabIndex        =   7
      Top             =   1200
      Value           =   1  'Checked
      Width           =   1335
   End
   Begin VB.CommandButton cmdRoll 
      Caption         =   "Roll"
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   840
      Width           =   1095
   End
   Begin VB.CheckBox chkRoll 
      Enabled         =   0   'False
      Height          =   255
      Left            =   0
      TabIndex        =   5
      Top             =   840
      Value           =   2  'Grayed
      Width           =   255
   End
   Begin VB.ComboBox cmbActionType 
      Height          =   315
      ItemData        =   "frmSkill.frx":0498
      Left            =   3480
      List            =   "frmSkill.frx":04AB
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   0
      Width           =   1815
   End
   Begin VB.ComboBox cmbCharacter 
      Height          =   315
      ItemData        =   "frmSkill.frx":04DE
      Left            =   0
      List            =   "frmSkill.frx":04E5
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   0
      Width           =   2295
   End
   Begin VB.Line Line5 
      X1              =   0
      X2              =   5280
      Y1              =   4200
      Y2              =   4200
   End
   Begin VB.Label txtMoving 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   720
      TabIndex        =   45
      Top             =   3840
      Width           =   4575
   End
   Begin VB.Label txtStatic 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   720
      TabIndex        =   44
      Top             =   3480
      Width           =   4575
   End
   Begin VB.Label lblTotal 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   720
      TabIndex        =   43
      Top             =   3120
      Width           =   4575
   End
   Begin VB.Label lblMovingLabel 
      Alignment       =   1  'Right Justify
      Caption         =   "Moving:"
      Height          =   255
      Left            =   0
      TabIndex        =   42
      Top             =   3870
      Width           =   615
   End
   Begin VB.Label lblStaticLabel 
      Alignment       =   1  'Right Justify
      Caption         =   "Static:"
      Height          =   255
      Left            =   0
      TabIndex        =   41
      Top             =   3510
      Width           =   615
   End
   Begin VB.Label lblTotalLabel 
      Alignment       =   1  'Right Justify
      Caption         =   "Total:"
      Height          =   255
      Left            =   0
      TabIndex        =   40
      Top             =   3150
      Width           =   615
   End
   Begin VB.Line Line4 
      X1              =   0
      X2              =   5280
      Y1              =   3000
      Y2              =   3000
   End
   Begin VB.Line Line3 
      X1              =   2550
      X2              =   5310
      Y1              =   390
      Y2              =   390
   End
   Begin VB.Label lblActionType 
      Alignment       =   1  'Right Justify
      Caption         =   "Action Type:"
      Height          =   255
      Left            =   2460
      TabIndex        =   3
      Top             =   75
      Width           =   975
   End
   Begin VB.Line Line2 
      X1              =   2400
      X2              =   2400
      Y1              =   0
      Y2              =   2880
   End
   Begin VB.Line Line1 
      X1              =   0
      X2              =   2280
      Y1              =   750
      Y2              =   750
   End
   Begin VB.Label lblStatus 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Height          =   300
      Left            =   600
      TabIndex        =   2
      Top             =   360
      Width           =   1695
   End
   Begin VB.Label lblStatusLabel 
      Alignment       =   1  'Right Justify
      Caption         =   "Status:"
      Height          =   255
      Left            =   0
      TabIndex        =   1
      Top             =   390
      Width           =   555
   End
End
Attribute VB_Name = "frmSkill"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-- to do:
' new collection of action types and modifiers
' distractions as separate fields
' tab order
' loading initial fields
' loading fields from character pulldown
' disabling, or enabling/moving to, fields from checkboxes
' select focus
' lookups of modifiers based on action type
' totals
' lookup of result
' pushback (with popup)
' WS
' Take
' Cancel
' handling from where it was called

Private UserInput As Boolean

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
End Sub

Private Sub Form_Activate()
  Dim i As Integer
  If cmdCancel.Tag <> "l" Then Exit Sub
  cmdCancel.Tag = ""
  cmdOK.Tag = ""
  txtWS = Val(frmIRIS.txtWS)
  ' build the cmbAttacker and cmbDefender lists
  cmbCharacter.Clear
  cmbCharacter.AddItem "(none)"
  For i = 1 To NumCombatants
    cmbCharacter.AddItem Combatants(i).Name
    Next i
  If frmIRIS.Acting_Combatant <= 0 Then
      cmbCharacter.ListIndex = 0
      cmdPushback.Enabled = False
      cmdPushback.Visible = False
      cmdWSMinus.Enabled = False
      cmdWSMinus.Visible = False
      txtWS.Enabled = False
      txtWS.Visible = False
      cmdWSPlus.Enabled = False
      cmdWSPlus.Visible = False
      cmdOK.Caption = "OK"
      cmdOK.Cancel = True
      cmdOK.Left = 4080
      cmdCancel.Enabled = False
      cmdCancel.Visible = False
    Else
      cmbCharacter.ListIndex = frmIRIS.Acting_Combatant
      cmdPushback.Enabled = True
      cmdPushback.Visible = True
      cmdWSMinus.Enabled = True
      cmdWSMinus.Visible = True
      txtWS.Enabled = True
      txtWS.Visible = True
      cmdWSPlus.Enabled = True
      cmdWSPlus.Visible = True
      cmdOK.Caption = "Take"
      cmdOK.Cancel = False
      cmdOK.Left = 2640
      cmdCancel.Enabled = True
      cmdCancel.Visible = True
    End If
  ' load other fields
  txtRoll = OpenEndedD20("high")
  '-- etc.
  txtRoll.SetFocus
  UserInput = True
End Sub

Private Sub cmdOK_Click()
  Me.Hide
End Sub



