Attribute VB_Name = "clsCombatant"
Option Explicit

Public Const conMaxCombatants = 100
Public Const conMaxEvents = 100

Public Type Combatant   ' stores all the information on one combatant
  Name As String              ' combatant name, must be unique
  
  PC As Boolean               ' show in the Player window?
  
  StatS As Integer            ' combatant stats
  StatE As Integer
  StatD As Integer
  StatQ As Integer
  StatI As Integer
  StatP As Integer
  StatW As Integer
  StatR As Integer
  
  Skills As String               ' free-form (for now) skills
  
  NextAction As Integer    ' current next action phase
  Saves As Integer            ' number of actions saved
  Parry As Integer             ' amount of parry
  ParryAt As Integer         ' combatant at which combatant is parrying
  LastWS As Integer         ' weapon speed most recently used
  LastAttacked As Integer ' last combatant attacked against
  LastOB As Integer           ' last OB used in an attack
  LastCritType As String     ' last critical used in an attack
  
  Delay As Integer            ' base delay from one action to the next
  MV As Integer               ' movement rate
  MN As Integer               ' movement bonus/penalty
  AT As Integer                ' armour type
  DB As Integer                ' defensive bonus
  
  Attacks As String          ' string of attacks available
  
  CurHp As Integer           ' current Hp
  OptHp As Integer           ' optimum Hp
  Bleed As Integer            ' bleeding rate
  Heal As Integer              ' healing percentage
  Clot As Integer              ' clotting percentage
  RoundsTillDown As Integer ' rounds until unconscious
  RoundsTillDead As Integer   ' rounds until dead
  CapLoss As Integer        ' capability loss
  Stuns As Integer            ' number of actions of stun
  StunNoParries As Integer 'number of actions of stun/no-parry
  LostInits As Integer        ' actions of lost initiative
  Penalty As Integer         ' combat penalty (permanent)
  MustParry As String      ' rounds of must parry with minuses
  BonusPenalty As String  ' rounds of bonus or penalty
  Position As Integer         ' from a list
  Health As String              ' free-form health information
  
  Notes As String             ' free-form notes
End Type

Public Combatants(conMaxCombatants) As Combatant
Public NumCombatants As Integer

Public attacker_bonuses As String

Public Type CombatEvent             ' stores all the information on one event
  Phase As Integer            ' when the event happens
  Description As String      ' the text of the event
End Type

Public Events(conMaxEvents) As CombatEvent
Public NumEvents As Integer

Public Function FindCombatant(Name As String) As Integer
  ' search the combatant list for a name, return the index or 0
  Dim i As Integer
  FindCombatant = 0
  If NumCombatants = 0 Then Exit Function
  For i = 1 To NumCombatants
    If Combatants(i).Name = Name Then
        FindCombatant = i
        Exit Function
      End If
    Next i
  FindCombatant = 0
End Function

Public Sub ClearCombatant(i As Integer)
  Combatants(i).Name = ""
  Combatants(i).PC = False
  Combatants(i).StatS = 0
  Combatants(i).StatE = 0
  Combatants(i).StatD = 0
  Combatants(i).StatQ = 0
  Combatants(i).StatI = 0
  Combatants(i).StatP = 0
  Combatants(i).StatW = 0
  Combatants(i).StatR = 0
  Combatants(i).Skills = ""
  Combatants(i).NextAction = 0
  Combatants(i).Saves = 0
  Combatants(i).Parry = 0
  Combatants(i).ParryAt = 0
  Combatants(i).LastWS = 0
  Combatants(i).LastAttacked = 0
  Combatants(i).LastOB = 0
  Combatants(i).LastCritType = "P"
  Combatants(i).Delay = 0
  Combatants(i).MV = 0
  Combatants(i).MN = 0
  Combatants(i).AT = 1
  Combatants(i).DB = 0
  Combatants(i).Attacks = ""
  Combatants(i).CurHp = 0
  Combatants(i).OptHp = 0
  Combatants(i).Bleed = 0
  Combatants(i).Heal = 0
  Combatants(i).Clot = 0
  Combatants(i).RoundsTillDown = 0
  Combatants(i).RoundsTillDead = 0
  Combatants(i).CapLoss = 0
  Combatants(i).Stuns = 0
  Combatants(i).StunNoParries = 0
  Combatants(i).LostInits = 0
  Combatants(i).Penalty = 0
  Combatants(i).MustParry = ""
  Combatants(i).BonusPenalty = ""
  Combatants(i).Position = 1
  Combatants(i).Health = ""
  Combatants(i).Notes = ""
End Sub

Public Sub DeleteCombatant(i As Integer)
  Dim j As Integer
  ' first, adjust all ParryAts and LastAttackeds
  For j = 1 To NumCombatants
    If Combatants(j).ParryAt = i Then
        Combatants(j).ParryAt = 0
        Combatants(j).Parry = 0
      ElseIf Combatants(j).ParryAt > i Then
        Combatants(j).ParryAt = Combatants(j).ParryAt - 1
      End If
    If Combatants(j).LastAttacked = i Then
        Combatants(j).LastAttacked = 0
        Combatants(j).LastAttacked = 0
      ElseIf Combatants(j).LastAttacked > i Then
        Combatants(j).LastAttacked = Combatants(j).LastAttacked - 1
      End If
    Next j
  ' now actually delete him
  NumCombatants = NumCombatants - 1
  For j = i To NumCombatants
    Call CopyCombatant(j, j + 1)
    Next j
  Call ClearCombatant(NumCombatants + 1)
End Sub

Public Sub CopyCombatant(dest As Integer, src As Integer)
  Combatants(dest).Name = Combatants(src).Name
  Combatants(dest).PC = Combatants(src).PC
  Combatants(dest).StatS = Combatants(src).StatS
  Combatants(dest).StatE = Combatants(src).StatE
  Combatants(dest).StatD = Combatants(src).StatD
  Combatants(dest).StatQ = Combatants(src).StatQ
  Combatants(dest).StatI = Combatants(src).StatI
  Combatants(dest).StatP = Combatants(src).StatP
  Combatants(dest).StatW = Combatants(src).StatW
  Combatants(dest).StatR = Combatants(src).StatR
  Combatants(dest).Skills = Combatants(src).Skills
  Combatants(dest).NextAction = Combatants(src).NextAction
  Combatants(dest).Saves = Combatants(src).Saves
  Combatants(dest).Parry = Combatants(src).Parry
  Combatants(dest).ParryAt = Combatants(src).ParryAt
  Combatants(dest).LastWS = Combatants(src).LastWS
  Combatants(dest).LastAttacked = Combatants(src).LastAttacked
  Combatants(dest).LastOB = Combatants(src).LastOB
  Combatants(dest).LastCritType = Combatants(src).LastCritType
  Combatants(dest).Delay = Combatants(src).Delay
  Combatants(dest).MV = Combatants(src).MV
  Combatants(dest).MN = Combatants(src).MN
  Combatants(dest).AT = Combatants(src).AT
  Combatants(dest).DB = Combatants(src).DB
  Combatants(dest).Attacks = Combatants(src).Attacks
  Combatants(dest).CurHp = Combatants(src).CurHp
  Combatants(dest).OptHp = Combatants(src).OptHp
  Combatants(dest).Bleed = Combatants(src).Bleed
  Combatants(dest).Heal = Combatants(src).Heal
  Combatants(dest).Clot = Combatants(src).Clot
  Combatants(dest).RoundsTillDown = Combatants(src).RoundsTillDown
  Combatants(dest).RoundsTillDead = Combatants(src).RoundsTillDead
  Combatants(dest).CapLoss = Combatants(src).CapLoss
  Combatants(dest).Stuns = Combatants(src).Stuns
  Combatants(dest).StunNoParries = Combatants(src).StunNoParries
  Combatants(dest).LostInits = Combatants(src).LostInits
  Combatants(dest).Penalty = Combatants(src).Penalty
  Combatants(dest).MustParry = Combatants(src).MustParry
  Combatants(dest).BonusPenalty = Combatants(src).BonusPenalty
  Combatants(dest).Position = Combatants(src).Position
  Combatants(dest).Health = Combatants(src).Health
  Combatants(dest).Notes = Combatants(src).Notes
End Sub

Public Function CombatantHpPenalty(ByVal comb As Integer) As Integer
  With Combatants(comb)
    If .CurHp <= 0 Then
        CombatantHpPenalty = -20
      ElseIf .CurHp > .OptHp * 0.7 Or .OptHp = 0 Then
        CombatantHpPenalty = 0
      Else
        CombatantHpPenalty = -Int(8 - 10 * .CurHp / .OptHp)
        If CombatantHpPenalty > 0 Then CombatantHpPenalty = 0
      End If
    End With
End Function

Public Function CombatantStatus(ByVal comb As Integer, ByVal longform As Integer) As String
  Dim i As Integer
  With Combatants(comb)
    If .Position = 10 Then
        CombatantStatus = "dead"
      ElseIf .Position = 9 Then
        CombatantStatus = "unconscious"
      ElseIf .CurHp <= 0 Then
        CombatantStatus = "out"
      ElseIf .StunNoParries > 0 Then
        If longform Then CombatantStatus = "stunned/no-parry" Else CombatantStatus = "stun/np"
      ElseIf .Stuns > 0 Then
        CombatantStatus = "stunned"
      ElseIf .MustParry <> "" Then
        i = Val(Word(.MustParry, 1)) + .Penalty + CombatantHpPenalty(comb)
        CombatantStatus = "must parry" & IIf(i <> 0, " at " & IIf(i > 0, "+", "") & Trim$(Str$(i)), "")
      ElseIf .BonusPenalty <> "" Then
        i = Val(Word(.BonusPenalty, 1)) + .Penalty + CombatantHpPenalty(comb)
        CombatantStatus = "action at " & IIf(i > 0, "+", "") & Trim$(Str$(i))
      ElseIf .Penalty + CombatantHpPenalty(comb) <> 0 Then
        i = .Penalty + CombatantHpPenalty(comb)
        CombatantStatus = "action at " & IIf(i > 0, "+", "") & Trim$(Str$(i))
      ElseIf .RoundsTillDead > 0 Then
        CombatantStatus = "dead in" & Str(.RoundsTillDead) & " rounds"
      ElseIf .RoundsTillDown > 0 Then
        CombatantStatus = "down in" & Str(.RoundsTillDown) & " rounds"
      ElseIf .Bleed > 0 Then
        CombatantStatus = "bleeding" & Str(.Bleed)
      ElseIf .Position = 2 Then
        CombatantStatus = "walking"
      ElseIf .Position = 3 Then
        CombatantStatus = "running"
      ElseIf .Position = 4 Then
        CombatantStatus = "flying"
      ElseIf .Position = 5 Then
        CombatantStatus = "crouching"
      ElseIf .Position = 6 Then
        CombatantStatus = "kneeling"
      ElseIf .Position = 7 Then
        CombatantStatus = "face up"
      ElseIf .Position = 8 Then
        CombatantStatus = "face down"
      Else
        CombatantStatus = ""
      End If
    End With
End Function

Public Function CombatantPosition(ByVal Position As Integer) As String
  Select Case Position
    Case 1
      CombatantPosition = "Standing"
    Case 2
      CombatantPosition = "Walking"
    Case 3
      CombatantPosition = "Running"
    Case 4
      CombatantPosition = "Flying"
    Case 5
      CombatantPosition = "Crouching"
    Case 6
      CombatantPosition = "Kneeling"
    Case 7
      CombatantPosition = "Face Up"
    Case 8
      CombatantPosition = "Face Down"
    Case 9
      CombatantPosition = "Unconscious"
    Case 10
      CombatantPosition = "Dead"
    End Select
End Function

Private Function BPMP_StartPos(ByVal BPMP As String, ByVal Position As Integer) As Integer
  Dim i, j As Integer
  BPMP_StartPos = 0
  i = Position
  j = 1 ' points to the beginning of the next item
  While i > 1
    j = InStr(j, BPMP, ",")
    If j = 0 Or j = Len(BPMP) Then Exit Function
    j = j + 1
    i = i - 1
    Wend
  BPMP_StartPos = j
End Function

Private Function BPMP_Parse(ByVal BPMP As String, ByVal Position As Integer) As String
  Dim i, j As Integer
  BPMP_Parse = ""
  i = BPMP_StartPos(BPMP, Position)
  If i = 0 Then Exit Function
  j = InStr(i, BPMP, ",")
  BPMP_Parse = Mid$(BPMP, i, j - i + 1)
End Function

Private Function BPMP_Add(ByVal BPMP As String, ByVal Position As Integer, _
    ByVal rounds As Integer, ByVal Penalty As Integer, replace As Boolean) As String
  Dim i, j As Integer
  Dim s, result As String
  s = Trim$(Str$(Penalty)) & " " & Trim$(Str$(rounds)) & ","
  BPMP_Add = ""
  i = BPMP_StartPos(BPMP, Position)
  If i = 0 Then Exit Function
  If i = 1 Then result = "" Else result = Left(BPMP, i - 1)
  result = result & s & Mid$(BPMP, IIf(replace, InStr(i, BPMP, ",") + 1, i))
  BPMP_Add = result
End Function

Public Sub AddMustParry(ByVal comb As Integer, ByVal rounds As Integer, _
    ByVal Penalty As Integer)
  Dim i, j As Integer
  Dim s As String
  i = 1
  Do
    s = BPMP_Parse(Combatants(comb).MustParry, i)
    If s = "" Then ' we ran out of entries, so we need to add a new one at the end
        Combatants(comb).MustParry = Combatants(comb).MustParry & _
            Trim$(Str$(Penalty)) & " " & Trim$(Str$(rounds)) & ","
        Exit Sub
      End If
    j = Val(Word(s, 1))
    If j = Penalty Then ' just add it to this entry
        Combatants(comb).MustParry = BPMP_Add(Combatants(comb).MustParry, _
            i, Val(Word(s, 2)) + rounds, Penalty, True)
        Exit Sub
      End If
    If j > Penalty Then ' we need to insert one at this position
        Combatants(comb).MustParry = BPMP_Add(Combatants(comb).MustParry, _
            i, rounds, Penalty, False)
        Exit Sub
      End If
    i = i + 1
    Loop
End Sub

Public Sub AddBonusPenalty(ByVal comb As Integer, ByVal rounds As Integer, _
    ByVal Penalty As Integer)
  Dim i, j As Integer
  Dim s As String
  i = 1
  Do
    ' look at entry #i in the BP list
    s = BPMP_Parse(Combatants(comb).BonusPenalty, i)
    If s = "" Then ' we ran out of entries, so we need to add a new one at the end
        Combatants(comb).BonusPenalty = Combatants(comb).BonusPenalty & _
            Trim$(Str$(Penalty)) & " " & Trim$(Str$(rounds)) & ","
        Exit Sub
      End If
    j = Val(Word(s, 1))  ' the BP itself
    ' if we're adding more rounds at the same bonus/penalty as an existing one
    If j = Penalty Then ' just add it to this entry
        Combatants(comb).BonusPenalty = BPMP_Add(Combatants(comb).BonusPenalty, _
            i, Val(Word(s, 2)) + rounds, Penalty, True)
        Exit Sub
      End If
    If j > Penalty Then ' we need to insert one at this position
        Combatants(comb).BonusPenalty = BPMP_Add(Combatants(comb).BonusPenalty, _
            i, rounds, Penalty, False)
        Exit Sub
      End If
    i = i + 1
    Loop
End Sub

Public Sub DeleteEvent(i As Integer)
  Dim j As Integer
  NumEvents = NumEvents - 1
  For j = i To NumEvents
    Call CopyEvent(j, j + 1)
    Next j
  Events(NumEvents + 1).Phase = 0
  Events(NumEvents + 1).Description = ""
End Sub

Public Sub CopyEvent(dest As Integer, src As Integer)
  Events(dest).Phase = Events(src).Phase
  Events(dest).Description = Events(src).Description
End Sub


