VERSION 5.00
Object = "{C1A8AF28-1257-101B-8FB0-0020AF039CA3}#1.1#0"; "MCI32.OCX"
Begin VB.Form frmDamage 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Damage"
   ClientHeight    =   2910
   ClientLeft      =   1065
   ClientTop       =   1740
   ClientWidth     =   9270
   ControlBox      =   0   'False
   HelpContextID   =   340
   Icon            =   "frmDamage.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2910
   ScaleWidth      =   9270
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear"
      Height          =   375
      Left            =   5640
      TabIndex        =   25
      Top             =   2520
      Width           =   855
   End
   Begin VB.Frame fraCrit 
      Caption         =   "Critical"
      Height          =   2535
      Left            =   0
      TabIndex        =   47
      Top             =   360
      Width           =   3135
      Begin VB.CommandButton cmdLoadCrit 
         Caption         =   ">>"
         Enabled         =   0   'False
         Height          =   1455
         Left            =   2640
         TabIndex        =   7
         Top             =   960
         Width           =   375
      End
      Begin VB.TextBox txtCritDescription 
         BackColor       =   &H00C0C0C0&
         Height          =   1455
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   6
         Top             =   960
         Width           =   2535
      End
      Begin VB.CommandButton cmdLoadCritChart 
         Caption         =   "Load"
         Height          =   315
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Width           =   615
      End
      Begin VB.ComboBox cmbCritChart 
         Enabled         =   0   'False
         Height          =   315
         ItemData        =   "frmDamage.frx":030A
         Left            =   840
         List            =   "frmDamage.frx":0317
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   600
         Width           =   2175
      End
      Begin VB.ComboBox cmbCritColumn 
         Height          =   315
         ItemData        =   "frmDamage.frx":0324
         Left            =   2520
         List            =   "frmDamage.frx":0337
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   240
         Width           =   495
      End
      Begin VB.TextBox txtCritRoll 
         Height          =   285
         Left            =   1080
         MaxLength       =   4
         TabIndex        =   2
         Top             =   240
         Width           =   495
      End
      Begin VB.CommandButton cmdRoll 
         Caption         =   "Roll"
         Height          =   285
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   855
      End
      Begin VB.Label lblColumn 
         Alignment       =   1  'Right Justify
         Caption         =   "Column:"
         Height          =   255
         Left            =   1680
         TabIndex        =   49
         Top             =   285
         Width           =   735
      End
   End
   Begin VB.TextBox txtHealth 
      Height          =   285
      Left            =   5640
      TabIndex        =   24
      Top             =   2160
      Width           =   3615
   End
   Begin VB.ComboBox cmbPosition 
      Height          =   315
      ItemData        =   "frmDamage.frx":034A
      Left            =   7080
      List            =   "frmDamage.frx":036C
      Style           =   2  'Dropdown List
      TabIndex        =   23
      Top             =   1800
      Width           =   2175
   End
   Begin VB.TextBox txtRoundsTillDead 
      Height          =   285
      Left            =   4680
      MaxLength       =   4
      TabIndex        =   15
      Top             =   2520
      Width           =   735
   End
   Begin VB.TextBox txtRoundsTillDown 
      Height          =   285
      Left            =   4680
      MaxLength       =   4
      TabIndex        =   14
      Top             =   2160
      Width           =   735
   End
   Begin VB.ComboBox cmbCombatants 
      Height          =   315
      HelpContextID   =   340
      Left            =   720
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   0
      Width           =   2415
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      HelpContextID   =   340
      Left            =   8280
      TabIndex        =   27
      Top             =   2520
      Width           =   975
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      HelpContextID   =   340
      Left            =   7200
      TabIndex        =   26
      Top             =   2520
      Width           =   975
   End
   Begin VB.TextBox txtAttackerAmount 
      Height          =   285
      HelpContextID   =   340
      Left            =   8160
      MaxLength       =   4
      TabIndex        =   22
      Top             =   1440
      Width           =   1095
   End
   Begin VB.TextBox txtAttackerRounds 
      BackColor       =   &H00FFFFFF&
      Height          =   285
      HelpContextID   =   340
      Left            =   7080
      MaxLength       =   4
      TabIndex        =   21
      Top             =   1440
      Width           =   495
   End
   Begin VB.TextBox txtPenalty 
      Height          =   285
      HelpContextID   =   340
      Left            =   8160
      MaxLength       =   4
      TabIndex        =   20
      Top             =   1080
      Width           =   1095
   End
   Begin VB.TextBox txtBPAmount 
      Height          =   285
      HelpContextID   =   340
      Left            =   8160
      MaxLength       =   4
      TabIndex        =   19
      Top             =   720
      Width           =   1095
   End
   Begin VB.TextBox txtBPRounds 
      Height          =   285
      HelpContextID   =   340
      Left            =   7080
      MaxLength       =   4
      TabIndex        =   18
      Top             =   720
      Width           =   495
   End
   Begin VB.TextBox txtMPAmount 
      Height          =   285
      HelpContextID   =   340
      Left            =   8160
      MaxLength       =   4
      TabIndex        =   17
      Top             =   360
      Width           =   1095
   End
   Begin VB.TextBox txtMPRounds 
      Height          =   285
      HelpContextID   =   340
      Left            =   7080
      MaxLength       =   4
      TabIndex        =   16
      Top             =   360
      Width           =   495
   End
   Begin VB.TextBox txtLostInits 
      Height          =   285
      HelpContextID   =   340
      Left            =   4680
      MaxLength       =   4
      TabIndex        =   13
      Top             =   1800
      Width           =   735
   End
   Begin VB.TextBox txtStunNoParries 
      Height          =   285
      HelpContextID   =   340
      Left            =   4680
      MaxLength       =   4
      TabIndex        =   12
      Top             =   1440
      Width           =   735
   End
   Begin VB.TextBox txtStuns 
      Height          =   285
      HelpContextID   =   340
      Left            =   4680
      MaxLength       =   4
      TabIndex        =   11
      Top             =   1080
      Width           =   735
   End
   Begin VB.TextBox txtBleed 
      Height          =   285
      HelpContextID   =   340
      Left            =   4680
      MaxLength       =   4
      TabIndex        =   10
      Top             =   720
      Width           =   735
   End
   Begin VB.TextBox txtBonusHits 
      Height          =   285
      HelpContextID   =   340
      Left            =   4680
      MaxLength       =   4
      TabIndex        =   9
      Top             =   360
      Width           =   735
   End
   Begin VB.TextBox txtHits 
      Height          =   285
      HelpContextID   =   340
      Left            =   4680
      MaxLength       =   4
      TabIndex        =   8
      Top             =   0
      Width           =   735
   End
   Begin MCI.MMControl mciControl 
      Height          =   330
      Left            =   0
      TabIndex        =   48
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   3540
      _ExtentX        =   6244
      _ExtentY        =   582
      _Version        =   393216
      PlayEnabled     =   -1  'True
      DeviceType      =   "WaveAudio"
      FileName        =   "C:\Program Files\IRIS\dice.wav"
   End
   Begin VB.Line Line1 
      X1              =   3240
      X2              =   3240
      Y1              =   0
      Y2              =   2880
   End
   Begin VB.Label lblPosition 
      Alignment       =   1  'Right Justify
      Caption         =   "New Position"
      Height          =   255
      Left            =   5640
      TabIndex        =   46
      Top             =   1845
      Width           =   1335
   End
   Begin VB.Label lblDeadIn 
      Alignment       =   1  'Right Justify
      Caption         =   "Dead In"
      Height          =   255
      Left            =   3360
      TabIndex        =   45
      Top             =   2565
      Width           =   1215
   End
   Begin VB.Label lblDownIn 
      Alignment       =   1  'Right Justify
      Caption         =   "Down In"
      Height          =   255
      Left            =   3360
      TabIndex        =   44
      Top             =   2205
      Width           =   1215
   End
   Begin VB.Image imgDice 
      Height          =   480
      Left            =   6600
      MousePointer    =   4  'Icon
      Picture         =   "frmDamage.frx":03DC
      Top             =   2460
      Width           =   480
   End
   Begin VB.Label lblAgainst 
      Alignment       =   1  'Right Justify
      Caption         =   "Against:"
      Height          =   255
      Left            =   0
      TabIndex        =   43
      Top             =   60
      Width           =   615
   End
   Begin VB.Label lblAttackerAt 
      Alignment       =   2  'Center
      Caption         =   "rnds at"
      Height          =   255
      Left            =   7560
      TabIndex        =   42
      Top             =   1485
      Width           =   615
   End
   Begin VB.Label lblBPAt 
      Alignment       =   2  'Center
      Caption         =   "rnds at"
      Height          =   255
      Left            =   7560
      TabIndex        =   41
      Top             =   765
      Width           =   615
   End
   Begin VB.Label lblMPAt 
      Alignment       =   2  'Center
      Caption         =   "rnds at"
      Height          =   255
      Left            =   7560
      TabIndex        =   40
      Top             =   405
      Width           =   615
   End
   Begin VB.Label lblBPHeading 
      Alignment       =   2  'Center
      Caption         =   "bonus/penalty"
      Height          =   255
      Left            =   8160
      TabIndex        =   39
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label lblRoundsHeading 
      Alignment       =   2  'Center
      Caption         =   "rounds"
      Height          =   255
      Left            =   6960
      TabIndex        =   38
      Top             =   120
      Width           =   735
   End
   Begin VB.Label lblAttacker 
      Alignment       =   1  'Right Justify
      Caption         =   "Attacker Bonus"
      Height          =   255
      Left            =   5640
      TabIndex        =   37
      Top             =   1485
      Width           =   1335
   End
   Begin VB.Label lblBP 
      Alignment       =   1  'Right Justify
      Caption         =   "Bonus/Penalty"
      Height          =   255
      Left            =   5640
      TabIndex        =   36
      Top             =   1125
      Width           =   1335
   End
   Begin VB.Label lblTmpBP 
      Alignment       =   1  'Right Justify
      Caption         =   "Tmp Bonus/Pnlty"
      Height          =   255
      Left            =   5640
      TabIndex        =   35
      Top             =   765
      Width           =   1335
   End
   Begin VB.Label lblMustParry 
      Alignment       =   1  'Right Justify
      Caption         =   "Must Parry"
      Height          =   255
      Left            =   5640
      TabIndex        =   34
      Top             =   405
      Width           =   1335
   End
   Begin VB.Line linVertical 
      X1              =   5520
      X2              =   5520
      Y1              =   0
      Y2              =   2880
   End
   Begin VB.Label lblBleeding 
      Alignment       =   1  'Right Justify
      Caption         =   "Bleeding"
      Height          =   255
      Left            =   3360
      TabIndex        =   33
      Top             =   765
      Width           =   1215
   End
   Begin VB.Label lblLostInits 
      Alignment       =   1  'Right Justify
      Caption         =   "Lost Inits"
      Height          =   255
      Left            =   3360
      TabIndex        =   32
      Top             =   1845
      Width           =   1215
   End
   Begin VB.Label lblSNP 
      Alignment       =   1  'Right Justify
      Caption         =   "Stun/No-Parries"
      Height          =   255
      Left            =   3360
      TabIndex        =   31
      Top             =   1485
      Width           =   1215
   End
   Begin VB.Label lblStuns 
      Alignment       =   1  'Right Justify
      Caption         =   "Stuns"
      Height          =   255
      Left            =   3360
      TabIndex        =   30
      Top             =   1125
      Width           =   1215
   End
   Begin VB.Label lblBonusHits 
      Alignment       =   1  'Right Justify
      Caption         =   "Bonus Hits"
      Height          =   255
      Left            =   3360
      TabIndex        =   29
      Top             =   405
      Width           =   1215
   End
   Begin VB.Label lblHits 
      Alignment       =   1  'Right Justify
      Caption         =   "Hits"
      Height          =   255
      Left            =   3360
      TabIndex        =   28
      Top             =   45
      Width           =   1215
   End
End
Attribute VB_Name = "frmDamage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim CE As CriticalEntry

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
  ' set up the sound to play
  mciControl.Notify = False
  mciControl.Wait = True
  mciControl.Shareable = False
  mciControl.DeviceType = "WaveAudio"
  mciControl.Filename = App.Path & "\dice.wav"
  mciControl.Command = "Open"
End Sub

Private Sub Form_Activate()
  ' load a blank cancel so the calling form will know if we cancelled
  cmdCancel.Tag = ""
  ' preload the damage fields
  txtHits = ""
  txtBonusHits = ""
  txtBleed = ""
  txtStuns = ""
  txtStunNoParries = ""
  txtLostInits = ""
  txtRoundsTillDown = ""
  txtRoundsTillDead = ""
  txtMPRounds = ""
  txtMPAmount = ""
  txtBPRounds = ""
  txtBPAmount = ""
  txtPenalty = ""
  txtAttackerRounds = ""
  txtAttackerAmount = ""
  txtHealth = ""
  txtHits.SetFocus
  ' preload the critical chart stuff
  cmbCritChart.Enabled = False
  cmbCritChart.Clear
  If txtCritRoll.Tag <> "y" Then ' it hasn't been preloaded
      txtCritRoll = Int(20 * Rnd + 1)
      mciControl.Command = "Play"
      mciControl.Command = "Prev"
    End If
  cmbCritColumn.ListIndex = 0
  BuildChartList "P"
End Sub

Private Sub BuildChartList(selected As String)
  Dim s As String, chartcode As String, chartname As String, i As Integer
  cmbCritChart.Clear
  s = AvailableCriticalCharts()
  If s <> "" Then
      For i = 1 To Len(s) / 3
        chartcode = Trim(Mid(s, i * 3 - 2, 3))
        chartname = CriticalChartName(chartcode)
        cmbCritChart.AddItem chartcode & ": " & chartname
        Next i
      cmbCritChart.ListIndex = 0
      For i = 1 To cmbCritChart.ListCount - 1
        If Left(cmbCritChart.List(i), Len(selected) + 1) = selected & ":" Then cmbCritChart.ListIndex = i
        Next i
      cmbCritChart.Enabled = True
      txtCritRoll_Change
    Else
      cmbCritChart.Enabled = False
    End If
End Sub

Private Sub cmdOK_Click()
  Me.Hide
End Sub

Private Sub cmbCombatants_Click()
  cmbPosition.ListIndex = Combatants(cmbCombatants.ListIndex + 1).Position - 1
End Sub

Private Sub cmdCancel_Click()
  cmdCancel.Tag = "y"
  Me.Hide
End Sub

Private Sub txtHits_GotFocus()
  Call SelectField(txtHits)
End Sub

Private Sub txtBonusHits_GotFocus()
  Call SelectField(txtBonusHits)
End Sub

Private Sub txtBleed_GotFocus()
  Call SelectField(txtBleed)
End Sub

Private Sub txtStuns_GotFocus()
  Call SelectField(txtStuns)
End Sub

Private Sub txtStunNoParries_GotFocus()
  Call SelectField(txtStunNoParries)
End Sub

Private Sub txtLostInits_GotFocus()
  Call SelectField(txtLostInits)
End Sub

Private Sub txtRoundsTillDown_GotFocus()
  Call SelectField(txtRoundsTillDown)
End Sub

Private Sub txtRoundsTillDead_GotFocus()
  Call SelectField(txtRoundsTillDead)
End Sub

Private Sub txtMPRounds_GotFocus()
  Call SelectField(txtMPRounds)
End Sub

Private Sub txtMPAmount_GotFocus()
  Call SelectField(txtMPAmount)
End Sub

Private Sub txtBPRounds_GotFocus()
  Call SelectField(txtBPRounds)
End Sub

Private Sub txtBPAmount_GotFocus()
  Call SelectField(txtBPAmount)
End Sub

Private Sub txtPenalty_GotFocus()
  Call SelectField(txtPenalty)
End Sub

Private Sub txtAttackerRounds_GotFocus()
  Call SelectField(txtAttackerRounds)
End Sub

Private Sub txtAttackerAmount_GotFocus()
  Call SelectField(txtAttackerAmount)
End Sub

Private Sub txtHealth_GotFocus()
  Call SelectField(txtHealth)
End Sub

Private Sub txtHits_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtBonusHits_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtBleed_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtStuns_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtStunNoParries_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtLostInits_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtRoundsTillDown_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtRoundsTillDead_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtMPRounds_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtMPAmount_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtBPRounds_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtBPAmount_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtPenalty_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtAttackerRounds_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtAttackerAmount_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub imgDice_Click()
  frmIRIS.imgDice_Click
End Sub

Private Sub txtCritRoll_GotFocus()
  Call SelectField(txtCritRoll)
End Sub

Private Sub txtCritRoll_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtCritRoll_Change()
  Dim chartcode As String
  If Not cmbCritChart.Enabled Then Exit Sub
  chartcode = cmbCritChart.List(cmbCritChart.ListIndex)
  chartcode = Left(chartcode, InStr(chartcode, ":") - 1)
  'lblChartName = CriticalChartName(cmbCritChart.List(cmbCritChart.ListIndex))
  CE = CriticalChartLookup(chartcode, cmbCritColumn.ListIndex + 1, Val(txtCritRoll))
  txtCritDescription = CE.Description
  cmdLoadCrit.Enabled = (CE.Description <> "")
End Sub

Private Sub cmbCritColumn_Click()
  txtCritRoll_Change
End Sub

Private Sub cmbCritChart_Click()
  txtCritRoll_Change
End Sub

Private Sub cmdRoll_Click()
  txtCritRoll = Int(20 * Rnd + 1)
  mciControl.Command = "Play"
  mciControl.Command = "Prev"
End Sub

Private Sub cmdLoadCritChart_Click()
  Dim newcode As String
  newcode = PromptAndLoadCriticalChart("Load critical chart", "")
  If newcode = "" Then Exit Sub
  BuildChartList newcode
End Sub

Private Sub cmdLoadCrit_Click()
  With CE
    If .BonusHits <> 0 Then txtBonusHits = Val(txtBonusHits) + .BonusHits
    If .Bleeding <> 0 Then txtBleed = Val(txtBleed) + .Bleeding
    If .Stuns <> 0 Then txtStuns = Val(txtStuns) + .Stuns
    If .StunNoParries <> 0 Then txtStunNoParries = Val(txtStunNoParries) + .StunNoParries
    If .LostInits <> 0 Then txtLostInits = Val(txtLostInits) + .LostInits
    If .RoundsTillDown < Val(txtRoundsTillDown) Or txtRoundsTillDown = "" Then txtRoundsTillDown = .RoundsTillDown
    If .RoundsTillDead < Val(txtRoundsTillDead) Or txtRoundsTillDead = "" Then txtRoundsTillDead = .RoundsTillDead
    If .MustParryRounds <> 0 Then
        If txtMPRounds = "" And txtMPAmount = "" Then
            txtMPRounds = .MustParryRounds
            txtMPAmount = .MustParryBP
          ElseIf .MustParryBP = Val(txtMPAmount) Then ' just accumulate rounds
            txtMPRounds = Val(txtMPRounds) + .MustParryRounds
          Else
            MsgBox "Unable to accumulate must parry rounds; add " & .MustParryRounds & " rounds at " & .MustParryBP & " manually.", vbOKOnly + vbExclamation, conAppName
          End If
      End If
    If .TmpBPRounds <> 0 And .TmpBP <> 0 Then
        If txtBPRounds = "" And txtBPAmount = "" Then
            txtBPRounds = .TmpBPRounds
            txtBPAmount = .TmpBP
          ElseIf .TmpBP = Val(txtBPAmount) Then ' just accumulate rounds
            txtBPRounds = Val(txtBPRounds) + .TmpBPRounds
          Else
            MsgBox "Unable to accumulate temporary bonus/penalty rounds; add " & .TmpBPRounds & " rounds at " & .TmpBP & " manually.", vbOKOnly + vbExclamation, conAppName
          End If
      End If
    If .BonusPenalty <> 0 Then txtPenalty = Val(txtPenalty) + .BonusPenalty
    If .AttackerBonusRounds <> 0 And .AttackerBonus <> 0 Then
        If txtAttackerRounds = "" And txtAttackerAmount = "" Then
            txtAttackerRounds = .AttackerBonusRounds
            txtAttackerAmount = .AttackerBonus
          ElseIf .AttackerBonus = Val(txtAttackerAmount) Then ' just accumulate rounds
            txtAttackerRounds = Val(txtAttackerRounds) + .AttackerBonusRounds
          Else
            MsgBox "Unable to accumulate attacker bonus rounds; add " & .AttackerBonusRounds & " rounds at " & .AttackerBonus & " manually.", vbOKOnly + vbExclamation, conAppName
          End If
      End If
    If .NewPosition - 1 > cmbPosition.ListIndex Then cmbPosition.ListIndex = .NewPosition - 1
    If .HealthChanges <> "" Then
        If txtHealth <> "" Then txtHealth = txtHealth & "; "
        txtHealth = txtHealth & .HealthChanges
      End If
    End With
End Sub

Private Sub cmdClear_Click()
  If MsgBox("Really clear damage results?", vbOKCancel + vbQuestion, conAppName) <> vbOK Then Exit Sub
  txtHits = ""
  txtBonusHits = ""
  txtBleed = ""
  txtStuns = ""
  txtStunNoParries = ""
  txtLostInits = ""
  txtRoundsTillDown = ""
  txtRoundsTillDead = ""
  txtMPRounds = ""
  txtMPAmount = ""
  txtBPRounds = ""
  txtBPAmount = ""
  txtPenalty = ""
  txtAttackerRounds = ""
  txtAttackerAmount = ""
  txtHealth = ""
  txtHits.SetFocus
  ' cmbPosition.ListIndex = 1 ' ?
End Sub


