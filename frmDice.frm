VERSION 5.00
Object = "{C1A8AF28-1257-101B-8FB0-0020AF039CA3}#1.1#0"; "MCI32.OCX"
Begin VB.Form frmDice 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Dice"
   ClientHeight    =   990
   ClientLeft      =   1050
   ClientTop       =   1470
   ClientWidth     =   7095
   ForeColor       =   &H8000000F&
   Icon            =   "frmDice.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   990
   ScaleWidth      =   7095
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Height          =   495
      Left            =   5160
      TabIndex        =   12
      Top             =   480
      Width           =   1935
   End
   Begin VB.CommandButton cmdXdY 
      Caption         =   "XdY"
      Height          =   495
      Left            =   4200
      TabIndex        =   11
      Top             =   480
      Width           =   975
   End
   Begin VB.CommandButton cmddX 
      Caption         =   "dX"
      Height          =   495
      Left            =   3240
      TabIndex        =   10
      Top             =   480
      Width           =   975
   End
   Begin VB.CommandButton cmdd100 
      Caption         =   "d%"
      Height          =   495
      Left            =   6600
      TabIndex        =   9
      Top             =   0
      Width           =   495
   End
   Begin VB.CommandButton cmdd30 
      Caption         =   "d30"
      Height          =   495
      Left            =   6120
      TabIndex        =   8
      Top             =   0
      Width           =   495
   End
   Begin VB.CommandButton cmdd20 
      Caption         =   "d20"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5640
      TabIndex        =   7
      Top             =   0
      Width           =   495
   End
   Begin VB.CommandButton cmdd12 
      Caption         =   "d12"
      Height          =   495
      Left            =   5160
      TabIndex        =   6
      Top             =   0
      Width           =   495
   End
   Begin VB.CommandButton cmdd10 
      Caption         =   "d10"
      Height          =   495
      Left            =   4680
      TabIndex        =   5
      Top             =   0
      Width           =   495
   End
   Begin VB.CommandButton cmdd8 
      Caption         =   "d8"
      Height          =   495
      Left            =   4200
      TabIndex        =   4
      Top             =   0
      Width           =   495
   End
   Begin VB.CommandButton cmdd6 
      Caption         =   "d6"
      Height          =   495
      Left            =   3720
      TabIndex        =   3
      Top             =   0
      Width           =   495
   End
   Begin VB.CommandButton cmdd4 
      Caption         =   "d4"
      Height          =   495
      Left            =   3240
      TabIndex        =   2
      Top             =   0
      Width           =   495
   End
   Begin VB.CommandButton cmdOpenEnded 
      Caption         =   "Open-Ended"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   2040
      TabIndex        =   1
      Top             =   0
      Width           =   1095
   End
   Begin MCI.MMControl mciControl 
      Height          =   330
      Left            =   5160
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   600
      Visible         =   0   'False
      Width           =   3540
      _ExtentX        =   6244
      _ExtentY        =   582
      _Version        =   393216
      PlayEnabled     =   -1  'True
      DeviceType      =   "WaveAudio"
      FileName        =   "C:\Program Files\IRIS\dice.wav"
   End
   Begin VB.Label lblLastRoll 
      Alignment       =   2  'Center
      Caption         =   "Open"
      Height          =   255
      Left            =   0
      TabIndex        =   13
      Tag             =   "Open"
      Top             =   600
      Width           =   495
   End
   Begin VB.Image imgDice 
      Height          =   480
      Left            =   45
      Picture         =   "frmDice.frx":030A
      Top             =   0
      Width           =   480
   End
   Begin VB.Label lblRoll 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   39.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   975
      Left            =   600
      TabIndex        =   0
      Top             =   0
      Width           =   1335
   End
End
Attribute VB_Name = "frmDice"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
  
  ' set up the sound to play
  mciControl.Notify = False
  mciControl.Wait = True
  mciControl.Shareable = False
  mciControl.DeviceType = "WaveAudio"
  mciControl.Filename = App.Path & "\dice.wav"
  mciControl.Command = "Open"
End Sub

Private Sub Form_Activate()
  Dim i As Integer
  If cmdCancel.Tag <> "l" Then Exit Sub
  cmdCancel.Tag = ""
  lblRoll = OpenEndedD20("open")
  lblLastRoll = "Open"
  cmdOpenEnded.SetFocus
End Sub

Private Sub cmdOpenEnded_Click()
  lblRoll = OpenEndedD20("open")
  lblLastRoll = "Open"
End Sub

Private Sub cmdd4_Click()
  lblRoll = Int(4 * Rnd + 1)
  lblLastRoll = "d4"
End Sub

Private Sub cmdd6_Click()
  lblRoll = Int(6 * Rnd + 1)
  lblLastRoll = "d6"
End Sub

Private Sub cmdd8_Click()
  lblRoll = Int(8 * Rnd + 1)
  lblLastRoll = "d8"
End Sub

Private Sub cmdd10_Click()
  lblRoll = Int(10 * Rnd + 1)
  lblLastRoll = "d10"
End Sub

Private Sub cmdd12_Click()
  lblRoll = Int(12 * Rnd + 1)
  lblLastRoll = "d12"
End Sub

Private Sub cmdd20_Click()
  lblRoll = Int(20 * Rnd + 1)
  lblLastRoll = "d20"
End Sub

Private Sub cmdd30_Click()
  lblRoll = Int(30 * Rnd + 1)
  lblLastRoll = "d30"
End Sub

Private Sub cmdd100_Click()
  lblRoll = Int(100 * Rnd + 1)
  lblLastRoll = "d%"
End Sub

Private Sub cmddX_Click()
  Dim x As Integer
  x = Val(InputBox("Die size", conAppName, 5))
  If x < 2 Then Exit Sub
  lblRoll = Int(x * Rnd) + 1
  lblLastRoll = "d" & Trim(Str(x))
End Sub

Private Sub cmdXdY_Click()
  Dim i, x, y As Integer
  x = Val(InputBox("Number of dice", conAppName, 3))
  y = Val(InputBox("Die size", conAppName, 6))
  If x < 1 Or y < 2 Then Exit Sub
  lblRoll = 0
  For i = 1 To x
    lblRoll = lblRoll + Int(y * Rnd) + 1
    Next
  lblLastRoll = Trim(Str(x)) & "d" & Trim(Str(y))
End Sub

Private Sub imgDice_Click()
  Call lblLastRoll_Click
End Sub

Private Sub lblLastRoll_Click()
  Dim i, x, y As Integer
  If lblLastRoll = "Open" Then
      Call cmdOpenEnded_Click
    ElseIf lblLastRoll = "d4" Then
      Call cmdd4_Click
    ElseIf lblLastRoll = "d6" Then
      Call cmdd6_Click
    ElseIf lblLastRoll = "d8" Then
      Call cmdd8_Click
    ElseIf lblLastRoll = "d10" Then
      Call cmdd10_Click
    ElseIf lblLastRoll = "d12" Then
      Call cmdd12_Click
    ElseIf lblLastRoll = "d20" Then
      Call cmdd20_Click
    ElseIf lblLastRoll = "d30" Then
      Call cmdd30_Click
    ElseIf lblLastRoll = "d%" Then
      Call cmdd100_Click
    ElseIf Left(lblLastRoll, 1) = "d" Then ' dX
      lblRoll = Int(Val(Mid(lblLastRoll, 2)) * Rnd + 1)
    Else ' XdY
      x = Val(Left(lblLastRoll, InStr(lblLastRoll, "d") - 1))
      y = Val(Mid(lblLastRoll, InStr(lblLastRoll, "d") + 1))
      lblRoll = 0
      For i = 1 To x
        lblRoll = lblRoll + Int(y * Rnd) + 1
        Next
    End If
End Sub

Private Sub lblRoll_Change()
  lblRoll.BackColor = &H8000000F
  If lblRoll.ForeColor = &H80000012 Then lblRoll.ForeColor = &HFF0000 Else lblRoll.ForeColor = &H80000012
  mciControl.Command = "Play"
  mciControl.Command = "Prev"
End Sub

Private Sub lblRoll_Click()
  lblRoll.BackColor = &HFFFF80
  Clipboard.SetText lblRoll, vbCFText
End Sub

Private Sub cmdCancel_Click()
  Me.Hide
End Sub

Private Sub Form_Unload(Cancel As Integer)
  mciControl.Command = "Close"
End Sub


