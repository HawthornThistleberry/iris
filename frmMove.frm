VERSION 5.00
Begin VB.Form frmMove 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Move"
   ClientHeight    =   1845
   ClientLeft      =   2955
   ClientTop       =   2715
   ClientWidth     =   3885
   ControlBox      =   0   'False
   HelpContextID   =   160
   Icon            =   "frmMove.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1845
   ScaleWidth      =   3885
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      HelpContextID   =   160
      Left            =   2400
      TabIndex        =   7
      Top             =   1440
      Width           =   1455
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Move"
      Default         =   -1  'True
      Height          =   375
      HelpContextID   =   160
      Left            =   840
      TabIndex        =   6
      Top             =   1440
      Width           =   1455
   End
   Begin VB.Frame Frame2 
      Caption         =   "Movement:"
      Height          =   1335
      Left            =   2040
      TabIndex        =   11
      Top             =   0
      Width           =   1815
      Begin VB.CheckBox chkAsFarAsPossible 
         Caption         =   "As far as possible"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   960
         Width           =   1575
      End
      Begin VB.TextBox txtDistance 
         Height          =   285
         HelpContextID   =   160
         Left            =   960
         MaxLength       =   4
         TabIndex        =   4
         Text            =   "10"
         Top             =   600
         Width           =   735
      End
      Begin VB.ComboBox cmbPace 
         Height          =   315
         HelpContextID   =   160
         ItemData        =   "frmMove.frx":030A
         Left            =   960
         List            =   "frmMove.frx":0320
         TabIndex        =   3
         Text            =   "2"
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Distance"
         Height          =   255
         Left            =   135
         TabIndex        =   13
         Top             =   630
         Width           =   735
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Pace"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   285
         Width           =   735
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Combatant:"
      Height          =   1335
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   1935
      Begin VB.TextBox txtMods 
         Height          =   285
         Left            =   840
         MaxLength       =   4
         TabIndex        =   2
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox txtMN 
         Height          =   285
         HelpContextID   =   160
         Left            =   840
         MaxLength       =   4
         TabIndex        =   1
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox txtMV 
         Height          =   285
         HelpContextID   =   160
         Left            =   840
         MaxLength       =   4
         TabIndex        =   0
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Modifiers"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   990
         Width           =   615
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Skill"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   630
         Width           =   615
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Rate"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   270
         Width           =   615
      End
   End
   Begin VB.Image imgDice 
      Height          =   480
      Left            =   0
      MousePointer    =   4  'Icon
      Picture         =   "frmMove.frx":0338
      Top             =   1320
      Width           =   480
   End
End
Attribute VB_Name = "frmMove"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
End Sub

Private Sub Form_Activate()
  cmdCancel.Tag = ""
  cmbPace = 2
  txtDistance = 10
  cmbPace.SetFocus
End Sub

Private Sub cmdOK_Click()
  Me.Hide
End Sub

Private Sub cmdCancel_Click()
  cmdCancel.Tag = "y"
  Me.Hide
End Sub

Private Sub txtMV_GotFocus()
  Call SelectField(txtMV)
End Sub

Private Sub txtMN_GotFocus()
  Call SelectField(txtMN)
End Sub

Private Sub txtMods_GotFocus()
  Call SelectField(txtMods)
End Sub

Private Sub txtDistance_GotFocus()
  Call SelectField(txtDistance)
End Sub

Private Sub txtMV_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtMN_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtMods_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub cmbPace_KeyPress(KeyAscii As Integer)
  If InStr("0123456789.", Chr$(KeyAscii)) = 0 And _
      KeyAscii <> 8 And KeyAscii <> 127 Then
      KeyAscii = 0
      Beep
    End If
End Sub

Private Sub txtDistance_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub chkAsFarAsPossible_Click()
  If chkAsFarAsPossible Then
      txtDistance.Enabled = False
      txtDistance.BackColor = &H80000004
    Else
      txtDistance.Enabled = True
      txtDistance.BackColor = &H80000005
    End If
End Sub

Private Sub imgDice_Click()
  frmIRIS.imgDice_Click
End Sub


