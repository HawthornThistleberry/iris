Attribute VB_Name = "clsScreenBounds"
' A few simple functions to enable positioning a form properly in a system with multiple monitors (or a single) while still ensuring it doesn't appear way off-screen.
' by Frank J. Perricone (hawthorn@foobox.com)

Public Type ScreenBoundaries
  Left As Integer
  Right As Integer
  Top As Integer
  Bottom As Integer
End Type

Public ScreenBounds As ScreenBoundaries

Private Declare Function GetSystemMetrics Lib "user32" (ByVal nIndex As Long) As Long

' Call this at the beginning of your Form Load for your main form.
Public Sub CalculateScreenBounds()
  If GetSystemMetrics(80) = 0 Then
    ScreenBounds.Left = 0
    ScreenBounds.Right = screen.Width
    ScreenBounds.Top = 0
    ScreenBounds.Bottom = screen.Height
  Else
    ScreenBounds.Left = GetSystemMetrics(76) * screen.TwipsPerPixelX
    ScreenBounds.Right = (GetSystemMetrics(76) + GetSystemMetrics(78)) * screen.TwipsPerPixelX
    ScreenBounds.Top = GetSystemMetrics(77) * screen.TwipsPerPixelY
    ScreenBounds.Bottom = (GetSystemMetrics(77) + GetSystemMetrics(79)) * screen.TwipsPerPixelY
  End If
End Sub

' Use this to position windows in such a way that you ensure they remain visible.
Public Sub PositionForm(theForm As Form, ByVal proposedLeft As Integer, ByVal proposedTop As Integer)
  If proposedLeft < ScreenBounds.Left Then proposedLeft = ScreenBounds.Left
  If proposedLeft > ScreenBounds.Right - theForm.Width Then proposedLeft = ScreenBounds.Right - theForm.Width
  If proposedTop < ScreenBounds.Top Then proposedTop = ScreenBounds.Top
  If proposedTop > ScreenBounds.Bottom - theForm.Height Then proposedTop = ScreenBounds.Bottom - theForm.Height
  theForm.Left = proposedLeft
  theForm.Top = proposedTop
End Sub
