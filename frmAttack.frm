VERSION 5.00
Object = "{C1A8AF28-1257-101B-8FB0-0020AF039CA3}#1.1#0"; "MCI32.OCX"
Begin VB.Form frmAttack 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Attack"
   ClientHeight    =   5070
   ClientLeft      =   1095
   ClientTop       =   2970
   ClientWidth     =   6135
   ControlBox      =   0   'False
   HelpContextID   =   140
   Icon            =   "frmAttack.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5070
   ScaleWidth      =   6135
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.Frame fraResults 
      Caption         =   "Results"
      Height          =   1215
      HelpContextID   =   140
      Left            =   0
      TabIndex        =   32
      Top             =   3840
      Width           =   6135
      Begin VB.CommandButton cmdCritRoll 
         Caption         =   "Crit Roll"
         Height          =   300
         Left            =   1350
         TabIndex        =   50
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox txtCritRoll 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2280
         MaxLength       =   4
         TabIndex        =   49
         Top             =   240
         Width           =   735
      End
      Begin VB.CommandButton cmdWSPlus 
         Caption         =   ">"
         Height          =   300
         Left            =   4800
         TabIndex        =   48
         Top             =   270
         Width           =   255
      End
      Begin VB.CommandButton cmdWSMinus 
         Caption         =   "<"
         Height          =   300
         Left            =   4065
         TabIndex        =   47
         Top             =   270
         Width           =   255
      End
      Begin VB.TextBox txtWS 
         Height          =   285
         HelpContextID   =   110
         Left            =   4320
         MaxLength       =   4
         TabIndex        =   45
         Top             =   270
         Width           =   495
      End
      Begin VB.CommandButton cmdDamage 
         Caption         =   "Damage"
         Height          =   375
         HelpContextID   =   340
         Left            =   3675
         TabIndex        =   35
         Top             =   720
         Width           =   1380
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         Height          =   375
         HelpContextID   =   140
         Left            =   5160
         TabIndex        =   37
         Top             =   720
         Width           =   855
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "Take"
         Default         =   -1  'True
         Height          =   375
         HelpContextID   =   130
         Left            =   5160
         TabIndex        =   36
         Top             =   240
         Width           =   855
      End
      Begin VB.Label lbl3 
         Alignment       =   1  'Right Justify
         Caption         =   "d%:"
         Height          =   255
         Left            =   120
         TabIndex        =   54
         Top             =   555
         Width           =   375
      End
      Begin VB.Label lblTotald100 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   600
         TabIndex        =   53
         Top             =   555
         Width           =   495
      End
      Begin VB.Label lblTotalRoll 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   600
         TabIndex        =   52
         Top             =   240
         Width           =   495
      End
      Begin VB.Image imgDice 
         Height          =   480
         Left            =   3120
         MousePointer    =   4  'Icon
         Picture         =   "frmAttack.frx":030A
         Top             =   690
         Width           =   480
      End
      Begin VB.Label lblWS 
         Caption         =   "WS:"
         Height          =   255
         Left            =   3675
         TabIndex        =   46
         Top             =   330
         Width           =   375
      End
      Begin VB.Label lblAT 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   600
         TabIndex        =   40
         Top             =   870
         Width           =   495
      End
      Begin VB.Label lbl2 
         Alignment       =   1  'Right Justify
         Caption         =   "AT:"
         Height          =   255
         Left            =   120
         TabIndex        =   34
         Top             =   840
         Width           =   375
      End
      Begin VB.Label lbl1 
         Alignment       =   1  'Right Justify
         Caption         =   "Total:"
         Height          =   255
         Left            =   120
         TabIndex        =   33
         Top             =   270
         Width           =   375
      End
   End
   Begin VB.Frame fraDefender 
      Caption         =   "Defender"
      Height          =   3735
      HelpContextID   =   140
      Left            =   3120
      TabIndex        =   16
      Top             =   0
      Width           =   3015
      Begin VB.TextBox txtDOther 
         Enabled         =   0   'False
         Height          =   285
         HelpContextID   =   140
         Left            =   1560
         MaxLength       =   4
         TabIndex        =   31
         Top             =   3360
         Width           =   1335
      End
      Begin VB.CheckBox chkDOther 
         Caption         =   "Other"
         Height          =   255
         HelpContextID   =   140
         Left            =   120
         TabIndex        =   30
         Top             =   3360
         Width           =   1335
      End
      Begin VB.ComboBox cmbDCover 
         Height          =   315
         HelpContextID   =   140
         ItemData        =   "frmAttack.frx":0614
         Left            =   1560
         List            =   "frmAttack.frx":0624
         TabIndex        =   29
         Text            =   "0 (None)"
         Top             =   3000
         Width           =   1335
      End
      Begin VB.CheckBox chkDCover 
         Caption         =   "Cover"
         Height          =   255
         HelpContextID   =   140
         Left            =   120
         TabIndex        =   28
         Top             =   3000
         Value           =   1  'Checked
         Width           =   1335
      End
      Begin VB.TextBox txtDSurprised 
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         Height          =   285
         HelpContextID   =   140
         Left            =   1560
         TabIndex        =   27
         Text            =   "+4"
         Top             =   1200
         Width           =   1335
      End
      Begin VB.CheckBox chkDSurprised 
         Caption         =   "Surprise"
         Height          =   255
         HelpContextID   =   140
         Left            =   120
         TabIndex        =   18
         Top             =   1200
         Width           =   1335
      End
      Begin VB.ComboBox cmbDPosition 
         Height          =   315
         HelpContextID   =   140
         ItemData        =   "frmAttack.frx":0658
         Left            =   1560
         List            =   "frmAttack.frx":0674
         Style           =   2  'Dropdown List
         TabIndex        =   26
         Top             =   2640
         Width           =   1335
      End
      Begin VB.CheckBox chkDPosition 
         Caption         =   "Position"
         Height          =   255
         HelpContextID   =   140
         Left            =   120
         TabIndex        =   25
         Top             =   2640
         Value           =   1  'Checked
         Width           =   1335
      End
      Begin VB.TextBox txtDParry 
         Height          =   285
         HelpContextID   =   140
         Left            =   1560
         MaxLength       =   4
         TabIndex        =   24
         Top             =   2280
         Width           =   1335
      End
      Begin VB.CheckBox chkDParry 
         Caption         =   "Parry"
         Height          =   255
         HelpContextID   =   140
         Left            =   120
         TabIndex        =   23
         Top             =   2280
         Value           =   1  'Checked
         Width           =   1335
      End
      Begin VB.TextBox txtDDB 
         Height          =   285
         HelpContextID   =   140
         Left            =   1560
         MaxLength       =   4
         TabIndex        =   22
         Top             =   1920
         Width           =   1335
      End
      Begin VB.CheckBox chkDDB 
         Caption         =   "DB"
         Height          =   255
         HelpContextID   =   140
         Left            =   120
         TabIndex        =   21
         Top             =   1920
         Value           =   1  'Checked
         Width           =   1335
      End
      Begin VB.TextBox txtDStunned 
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         Height          =   285
         HelpContextID   =   140
         Left            =   1560
         TabIndex        =   20
         Text            =   "+4"
         Top             =   1560
         Width           =   1335
      End
      Begin VB.CheckBox chkDStunned 
         Caption         =   "Stunned"
         Height          =   255
         HelpContextID   =   140
         Left            =   120
         TabIndex        =   19
         Top             =   1560
         Width           =   1335
      End
      Begin VB.ComboBox cmbDefender 
         Height          =   315
         HelpContextID   =   140
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   240
         Width           =   2775
      End
      Begin VB.Label lblParryMinusD 
         Alignment       =   2  'Center
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1440
         TabIndex        =   44
         Top             =   2295
         Width           =   135
      End
      Begin VB.Line Line1 
         X1              =   120
         X2              =   2880
         Y1              =   1005
         Y2              =   1005
      End
      Begin VB.Label lblStatusD 
         Caption         =   "Status:"
         Height          =   255
         Left            =   120
         TabIndex        =   42
         Top             =   630
         Width           =   615
      End
      Begin VB.Label lblDStatus 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   720
         TabIndex        =   39
         Top             =   600
         Width           =   2175
      End
   End
   Begin VB.Frame fraAttacker 
      Caption         =   "Attacker"
      Height          =   3735
      HelpContextID   =   140
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3015
      Begin VB.TextBox txtAOther 
         Enabled         =   0   'False
         Height          =   285
         HelpContextID   =   140
         Left            =   1440
         MaxLength       =   4
         TabIndex        =   15
         Top             =   3360
         Width           =   1455
      End
      Begin VB.CheckBox chkAOther 
         Caption         =   "Other"
         Height          =   255
         HelpContextID   =   140
         Left            =   120
         TabIndex        =   14
         Top             =   3360
         Width           =   1215
      End
      Begin VB.TextBox txtARangeMod 
         Enabled         =   0   'False
         Height          =   285
         HelpContextID   =   140
         Left            =   1440
         MaxLength       =   4
         TabIndex        =   13
         Top             =   3000
         Width           =   1455
      End
      Begin VB.CheckBox chkARangeMod 
         Caption         =   "Range Mod"
         Height          =   255
         HelpContextID   =   140
         Left            =   120
         TabIndex        =   12
         Top             =   3000
         Width           =   1215
      End
      Begin VB.TextBox txtAPenalty 
         Height          =   285
         HelpContextID   =   140
         Left            =   1440
         MaxLength       =   4
         TabIndex        =   11
         Top             =   2640
         Width           =   1455
      End
      Begin VB.CheckBox chkAPenalty 
         Caption         =   "Penalty"
         Height          =   255
         HelpContextID   =   140
         Left            =   120
         TabIndex        =   10
         Top             =   2640
         Value           =   1  'Checked
         Width           =   1215
      End
      Begin VB.TextBox txtAParry 
         Height          =   285
         HelpContextID   =   140
         Left            =   1440
         MaxLength       =   4
         TabIndex        =   9
         Top             =   2280
         Width           =   1455
      End
      Begin VB.CheckBox chkAParry 
         Caption         =   "Parry"
         Height          =   255
         HelpContextID   =   140
         Left            =   120
         TabIndex        =   8
         Top             =   2280
         Value           =   1  'Checked
         Width           =   1215
      End
      Begin VB.TextBox txtAOB 
         Height          =   285
         HelpContextID   =   140
         Left            =   1440
         MaxLength       =   4
         TabIndex        =   7
         Top             =   1920
         Width           =   1455
      End
      Begin VB.CheckBox chkAOB 
         Caption         =   "OB"
         Height          =   255
         HelpContextID   =   140
         Left            =   120
         TabIndex        =   6
         Top             =   1920
         Value           =   1  'Checked
         Width           =   1215
      End
      Begin VB.TextBox txtARoll 
         Height          =   285
         HelpContextID   =   140
         Left            =   1440
         MaxLength       =   4
         TabIndex        =   5
         Top             =   1560
         Width           =   1455
      End
      Begin VB.CommandButton cmdARoll 
         Caption         =   "Roll"
         Height          =   255
         HelpContextID   =   140
         Left            =   360
         TabIndex        =   4
         Top             =   1560
         Width           =   975
      End
      Begin VB.CheckBox chkARoll 
         Enabled         =   0   'False
         Height          =   255
         HelpContextID   =   140
         Left            =   120
         TabIndex        =   3
         Top             =   1560
         Value           =   2  'Grayed
         Width           =   255
      End
      Begin VB.ComboBox cmbAttacker 
         Height          =   315
         HelpContextID   =   140
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   240
         Width           =   2775
      End
      Begin VB.Label lblParryMinusA 
         Alignment       =   2  'Center
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1320
         TabIndex        =   43
         Top             =   2295
         Width           =   135
      End
      Begin VB.Label lblStatusA 
         Caption         =   "Status:"
         Height          =   255
         Left            =   120
         TabIndex        =   41
         Top             =   630
         Width           =   615
      End
      Begin VB.Label lblAStatus 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   720
         TabIndex        =   38
         Top             =   600
         Width           =   2175
      End
      Begin VB.Label lblAttacks 
         BorderStyle     =   1  'Fixed Single
         Height          =   495
         Left            =   120
         TabIndex        =   2
         Top             =   960
         UseMnemonic     =   0   'False
         Width           =   2775
         WordWrap        =   -1  'True
      End
   End
   Begin MCI.MMControl mciControl 
      Height          =   330
      Left            =   0
      TabIndex        =   51
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   3540
      _ExtentX        =   6244
      _ExtentY        =   582
      _Version        =   393216
      PlayEnabled     =   -1  'True
      DeviceType      =   "WaveAudio"
      FileName        =   "C:\Program Files\IRIS\dice.wav"
   End
End
Attribute VB_Name = "frmAttack"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private UserInput As Boolean

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
  
  ' set up the sound to play
  mciControl.Notify = False
  mciControl.Wait = True
  mciControl.Shareable = False
  mciControl.DeviceType = "WaveAudio"
  mciControl.Filename = App.Path & "\dice.wav"
  mciControl.Command = "Open"
End Sub

Private Sub Form_Activate()
  Dim i As Integer
  If cmdCancel.Tag <> "l" Then Exit Sub
  UserInput = False
  cmdCancel.Tag = ""
  cmdOK.Tag = ""
  txtWS = Val(frmIRIS.txtWS)
  ' build the cmbAttacker and cmbDefender lists
  cmbAttacker.Clear
  cmbDefender.Clear
  For i = 1 To NumCombatants
    cmbAttacker.AddItem Combatants(i).Name
    cmbDefender.AddItem Combatants(i).Name
    Next i
  If frmIRIS.Acting_Combatant <= 0 Then
      cmbAttacker.ListIndex = 0
    Else
      cmbAttacker.ListIndex = frmIRIS.Acting_Combatant - 1
    End If
  If Combatants(cmbAttacker.ListIndex + 1).LastAttacked = 0 Then
      If Combatants(cmbAttacker.ListIndex + 1).ParryAt = 0 Then
          cmbDefender.ListIndex = 0
        Else
          cmbDefender.ListIndex = Combatants(cmbAttacker.ListIndex + 1).ParryAt - 1
        End If
    Else
      cmbDefender.ListIndex = Combatants(cmbAttacker.ListIndex + 1).LastAttacked - 1
    End If
  ' load other fields
  txtARoll = OpenEndedD20("open")
  mciControl.Command = "Play"
  mciControl.Command = "Prev"
  chkAOB = 1
  chkAPenalty = 1
  chkARangeMod = 0
  txtARangeMod = ""
  chkAOther = 0
  txtAOther = ""
  chkDSurprised = 0
  chkDPosition = 1
  cmbDPosition.ListIndex = 0
  chkDCover = 1
  cmbDCover.ListIndex = 0
  chkDOther = 0
  txtDOther = ""
  txtCritRoll = Int(20 * Rnd + 1)
  txtARoll.SetFocus
  UserInput = True
End Sub

Private Sub cmdOK_Click()
  Me.Hide
  frmIRIS.txtWS = Val(txtWS)
  Combatants(cmbAttacker.ListIndex + 1).Parry = IIf(chkAParry = 1, Val(txtAParry), 0)
  If chkAParry = 0 Or Val(txtAParry) = 0 Then _
    Combatants(cmbAttacker.ListIndex + 1).ParryAt = 0 Else _
    Combatants(cmbAttacker.ListIndex + 1).ParryAt = cmbDefender.ListIndex + 1
  Combatants(cmbDefender.ListIndex + 1).Parry = IIf(chkDParry = 1, Val(txtDParry), 0)
  If chkDParry = 0 Or Val(txtDParry) = 0 Then _
    Combatants(cmbDefender.ListIndex + 1).ParryAt = 0 Else _
    Combatants(cmbDefender.ListIndex + 1).ParryAt = cmbAttacker.ListIndex + 1
End Sub

Private Sub cmdCancel_Click()
  cmdCancel.Tag = "y"
  Me.Hide
End Sub

Private Sub cmbAttacker_Click()
  UserInput = False
  lblAStatus = CombatantStatus(cmbAttacker.ListIndex + 1, 1)
  lblAttacks = Combatants(cmbAttacker.ListIndex + 1).Attacks
  If Combatants(cmbAttacker.ListIndex + 1).Parry > 0 Then
      chkAParry = 1
      txtAParry = Combatants(cmbAttacker.ListIndex + 1).Parry
    Else
      chkAParry = 0
      txtAParry = ""
    End If
  txtAOB = Trim$(Str$(Combatants(cmbAttacker.ListIndex + 1).LastOB))
  If Combatants(cmbDefender.ListIndex + 1).Parry > 0 And _
      Combatants(cmbDefender.ListIndex + 1).ParryAt = cmbAttacker.ListIndex + 1 Then
      chkDParry = 1
      txtDParry = Combatants(cmbDefender.ListIndex + 1).Parry
    Else
      chkDParry = 0
      txtDParry = ""
    End If
  txtAPenalty = Combatants(cmbAttacker.ListIndex + 1).Penalty + _
      CombatantHpPenalty(cmbAttacker.ListIndex + 1) + _
      IIf(Combatants(cmbAttacker.ListIndex + 1).BonusPenalty <> "", _
      Val(Word(Combatants(cmbAttacker.ListIndex + 1).BonusPenalty, 1)), 0)
  If txtAPenalty > 0 Then txtAPenalty = "+" & txtAPenalty
  UserInput = True
End Sub

Private Sub cmdARoll_Click()
  txtARoll = OpenEndedD20("open")
  mciControl.Command = "Play"
  mciControl.Command = "Prev"
End Sub

Private Sub txtARoll_GotFocus()
  Call SelectField(txtARoll)
End Sub

Private Sub txtARoll_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtARoll_Change()
  Call lblTotalRoll_Update
End Sub

Private Sub chkAOB_Click()
  Call lblTotalRoll_Update
  If chkAOB = 1 Then txtAOB.Enabled = True Else txtAOB.Enabled = False
  If UserInput And chkAOB = 1 Then txtAOB.SetFocus
End Sub

Private Sub txtAOB_GotFocus()
  Call SelectField(txtAOB)
End Sub

Private Sub txtAOB_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtAOB_Change()
  Call lblTotalRoll_Update
  Combatants(cmbAttacker.ListIndex + 1).LastOB = Val(txtAOB)
End Sub

Private Sub chkAParry_Click()
  Call lblTotalRoll_Update
  If chkAParry = 1 Then txtAParry.Enabled = True Else txtAParry.Enabled = False
  If UserInput And chkAParry = 1 Then txtAParry.SetFocus
End Sub

Private Sub txtAParry_GotFocus()
  Call SelectField(txtAParry)
End Sub

Private Sub txtAParry_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtAParry_Change()
  Call lblTotalRoll_Update
End Sub

Private Sub txtAParry_LostFocus()
  If chkAParry = 1 And Val(txtAParry) >= Val(txtAOB) And _
      chkAOB = 1 And Val(txtAOB) > 0 Then _
      MsgBox "Warning: the parry should be less than your OB.", vbInformation
End Sub

Private Sub chkAPenalty_Click()
  Call lblTotalRoll_Update
  If chkAPenalty = 1 Then txtAPenalty.Enabled = True Else txtAPenalty.Enabled = False
  If UserInput And chkAPenalty = 1 Then txtAPenalty.SetFocus
End Sub

Private Sub txtAPenalty_GotFocus()
  Call SelectField(txtAPenalty)
End Sub

Private Sub txtAPenalty_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtAPenalty_Change()
  Call lblTotalRoll_Update
End Sub

Private Sub chkARangeMod_Click()
  Call lblTotalRoll_Update
  If chkARangeMod = 1 Then txtARangeMod.Enabled = True Else txtARangeMod.Enabled = False
  If UserInput And chkARangeMod = 1 Then txtARangeMod.SetFocus
End Sub

Private Sub txtARangeMod_GotFocus()
  Call SelectField(txtARangeMod)
End Sub

Private Sub txtARangeMod_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtARangeMod_Change()
  Call lblTotalRoll_Update
End Sub

Private Sub chkAOther_Click()
  Call lblTotalRoll_Update
  If chkAOther = 1 Then txtAOther.Enabled = True Else txtAOther.Enabled = False
  If UserInput And chkAOther = 1 Then txtAOther.SetFocus
End Sub

Private Sub txtAOther_GotFocus()
  Call SelectField(txtAOther)
End Sub

Private Sub txtAOther_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtAOther_Change()
  Call lblTotalRoll_Update
End Sub

Private Sub cmbDefender_Click()
  UserInput = False
  Combatants(cmbAttacker.ListIndex + 1).LastAttacked = cmbDefender.ListIndex + 1
  lblDStatus = CombatantStatus(cmbDefender.ListIndex + 1, 1)
  If Combatants(cmbDefender.ListIndex + 1).StunNoParries > 0 Or _
      Combatants(cmbDefender.ListIndex + 1).Stuns > 0 Then _
      chkDStunned = 1 Else chkDStunned = 0
  txtDDB = -Combatants(cmbDefender.ListIndex + 1).DB
  If txtDDB > 0 Then txtDDB = "+" & txtDDB
  If Combatants(cmbDefender.ListIndex + 1).Parry > 0 And _
      Combatants(cmbDefender.ListIndex + 1).ParryAt = cmbAttacker.ListIndex + 1 Then
      chkDParry = 1
      txtDParry = Combatants(cmbDefender.ListIndex + 1).Parry
    Else
      chkDParry = 0
      txtDParry = ""
    End If
  chkDDB = 1
  If Combatants(cmbDefender.ListIndex + 1).StunNoParries > 0 Then
      chkDDB = 0
      txtDDB = ""
      txtDDB.Enabled = False
      chkDParry = 0
      txtDParry = ""
      txtDParry.Enabled = False
    End If
  lblAT = Combatants(cmbDefender.ListIndex + 1).AT
  UserInput = True
End Sub

Private Sub chkDSurprised_Click()
  Call lblTotalRoll_Update
End Sub

Private Sub chkDStunned_Click()
  Call lblTotalRoll_Update
End Sub

Private Sub chkDDB_Click()
  Call lblTotalRoll_Update
  If chkDDB = 1 Then txtDDB.Enabled = True Else txtDDB.Enabled = False
  If UserInput And chkDDB = 1 Then txtDDB.SetFocus
End Sub

Private Sub txtDDB_GotFocus()
  Call SelectField(txtDDB)
End Sub

Private Sub txtDDB_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtDDB_Change()
  Call lblTotalRoll_Update
End Sub

Private Sub chkDParry_Click()
  Call lblTotalRoll_Update
  If chkDParry = 1 Then txtDParry.Enabled = True Else txtDParry.Enabled = False
  If UserInput And chkDParry = 1 Then txtDParry.SetFocus
End Sub

Private Sub txtDParry_GotFocus()
  Call SelectField(txtDParry)
End Sub

Private Sub txtDParry_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtDParry_Change()
  Call lblTotalRoll_Update
End Sub

Private Sub chkDPosition_Click()
  Call lblTotalRoll_Update
  If chkDPosition = 1 Then cmbDPosition.Enabled = True Else cmbDPosition.Enabled = False
  If UserInput And chkDPosition = 1 Then cmbDPosition.SetFocus
End Sub

Private Sub cmbDPosition_Click()
  Call lblTotalRoll_Update
End Sub

Private Sub chkDCover_Click()
  Call lblTotalRoll_Update
  If chkDCover = 1 Then cmbDCover.Enabled = True Else cmbDCover.Enabled = False
  If UserInput And chkDCover = 1 Then cmbDCover.SetFocus
End Sub

Private Sub cmbDCover_GotFocus()
  Call SelectField(cmbDCover)
End Sub

Private Sub cmbDCover_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub cmbDCover_Change()
  Call lblTotalRoll_Update
End Sub

Private Sub cmbDCover_Click()
  Call lblTotalRoll_Update
End Sub

Private Sub chkDOther_Click()
  Call lblTotalRoll_Update
  If chkDOther = 1 Then txtDOther.Enabled = True Else txtDOther.Enabled = False
  If UserInput And chkDOther = 1 Then txtDOther.SetFocus
End Sub

Private Sub txtDOther_GotFocus()
  Call SelectField(txtDOther)
End Sub

Private Sub txtDOther_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtDOther_Change()
  Call lblTotalRoll_Update
End Sub

Private Sub cmdCritRoll_Click()
  txtCritRoll = Int(20 * Rnd + 1)
  mciControl.Command = "Play"
  mciControl.Command = "Prev"
  Call lblTotalRoll_Update
End Sub

Private Sub txtCritRoll_Change()
  Call lblTotalRoll_Update
End Sub

Private Sub txtCritRoll_GotFocus()
  Call SelectField(txtCritRoll)
End Sub

Private Sub lblTotalRoll_Update()
  Dim i As Integer
  i = Val(txtARoll)
  If chkAOB = 1 Then i = i + Val(txtAOB)
  If chkAParry = 1 Then i = i - Val(txtAParry)
  If chkAPenalty = 1 Then i = i + Val(txtAPenalty)
  If chkARangeMod = 1 Then i = i + Val(txtARangeMod)
  If chkAOther = 1 Then i = i + Val(txtAOther)
  If chkDStunned = 1 Then i = i + 4
  If chkDSurprised = 1 Then i = i + 4
  If chkDDB = 1 Then i = i + Val(txtDDB)
  If chkDParry = 1 Then i = i - Val(txtDParry)
  If chkDPosition = 1 Then i = i + Val(cmbDPosition)
  If chkDCover = 1 Then i = i + Val(cmbDCover)
  If chkDOther = 1 Then i = i + Val(txtDOther)
  lblTotalRoll = i
  lblTotald100 = i * 5 - 2 + Int(5 * Rnd + 1)
End Sub

Private Sub cmdDamage_Click()
  Dim i As Integer
  Dim logtext, logtext2 As String
  With frmDamage
    .cmbCombatants.Clear
    For i = 1 To NumCombatants
      .cmbCombatants.AddItem Combatants(i).Name
      Next i
    .cmbCombatants.ListIndex = cmbDefender.ListIndex
    .txtAttackerRounds.Enabled = True
    .txtAttackerRounds.BackColor = &H80000005
    .txtAttackerAmount.Enabled = True
    .txtAttackerAmount.BackColor = &H80000005
    .txtCritRoll = txtCritRoll
    .txtCritRoll.Tag = "y"
    .Show 1
    If .cmdCancel.Tag = "y" Then Exit Sub
    logtext = ""
    logtext2 = ""
    If Val(.txtAttackerRounds) <> 0 And Val(.txtAttackerAmount) <> 0 Then
        attacker_bonuses = attacker_bonuses & Trim(Str(.txtAttackerAmount)) & " " & _
            Trim(Str(.txtAttackerRounds)) & ","
        logtext2 = "attacker at " & .txtAttackerAmount & " for " & .txtAttackerRounds & _
          " action" & IIf(Val(.txtAttackerRounds) <> 1, "s", "")
      End If
    Combatants(.cmbCombatants.ListIndex + 1).CurHp = _
        Combatants(.cmbCombatants.ListIndex + 1).CurHp - Val(.txtHits) - Val(.txtBonusHits)
    If Val(.txtHits) + Val(.txtBonusHits) <> 0 Then logtext = _
        CommaList(logtext, Trim$(Str$(Val(.txtHits) + Val(.txtBonusHits))) & " Hp")
    If Combatants(.cmbCombatants.ListIndex + 1).CurHp <= 0 Then
        logtext = CommaList(logtext, "\b out!\plain\f2\fs17")
        Combatants(.cmbCombatants.ListIndex + 1).Position = 9
        MsgBox Combatants(.cmbCombatants.ListIndex + 1).Name & " is out!", vbInformation
      End If
    Combatants(.cmbCombatants.ListIndex + 1).Bleed = _
        Combatants(.cmbCombatants.ListIndex + 1).Bleed + Val(.txtBleed)
    If Val(.txtBleed) <> 0 Then logtext = CommaList(logtext, .txtBleed & " bleeding")
    Combatants(.cmbCombatants.ListIndex + 1).Stuns = _
        Combatants(.cmbCombatants.ListIndex + 1).Stuns + Val(.txtStuns)
    If Val(.txtStuns) <> 0 Then logtext = CommaList(logtext, .txtStuns & " stun" & _
        IIf(Val(.txtStuns) <> 1, "s", ""))
    Combatants(.cmbCombatants.ListIndex + 1).StunNoParries = _
        Combatants(.cmbCombatants.ListIndex + 1).StunNoParries + Val(.txtStunNoParries)
    If Val(.txtStunNoParries) <> 0 Then logtext = CommaList(logtext, .txtStunNoParries & _
        " stun/no-parr" & IIf(Val(.txtStunNoParries) <> 1, "ies", "y"))
    Combatants(.cmbCombatants.ListIndex + 1).LostInits = _
        Combatants(.cmbCombatants.ListIndex + 1).LostInits + Val(.txtLostInits)
    If Val(.txtLostInits) <> 0 Then logtext = CommaList(logtext, .txtLostInits & _
        " lost init" & IIf(Val(.txtLostInits) <> 1, "s", ""))
    If Val(.txtRoundsTillDown) > 0 And _
        (Val(Combatants(.cmbCombatants.ListIndex + 1).RoundsTillDown) = 0 Or _
        Val(.txtRoundsTillDown) < Val(Combatants(.cmbCombatants.ListIndex + 1).RoundsTillDown)) _
        Then Combatants(.cmbCombatants.ListIndex + 1).RoundsTillDown = .txtRoundsTillDown
    If Val(Combatants(.cmbCombatants.ListIndex + 1).RoundsTillDown) <> 0 Then _
        logtext = CommaList(logtext, "down in " & _
        Combatants(.cmbCombatants.ListIndex + 1).RoundsTillDown & " round" & _
        IIf(Val(Combatants(.cmbCombatants.ListIndex + 1).RoundsTillDown) <> 1, "s", ""))
    If Val(.txtRoundsTillDead) > 0 And _
        (Val(Combatants(.cmbCombatants.ListIndex + 1).RoundsTillDead) = 0 Or _
        Val(.txtRoundsTillDead) < Val(Combatants(.cmbCombatants.ListIndex + 1).RoundsTillDead)) _
        Then Combatants(.cmbCombatants.ListIndex + 1).RoundsTillDead = .txtRoundsTillDead
    If Val(Combatants(.cmbCombatants.ListIndex + 1).RoundsTillDead) <> 0 Then _
        logtext = CommaList(logtext, "dead in " & _
        Combatants(.cmbCombatants.ListIndex + 1).RoundsTillDead & " round" & _
        IIf(Val(Combatants(.cmbCombatants.ListIndex + 1).RoundsTillDead) <> 1, "s", ""))
    If Val(.txtMPRounds) <> 0 Then
        Call AddMustParry(.cmbCombatants.ListIndex + 1, Val(.txtMPRounds), Val(.txtMPAmount))
        logtext = CommaList(logtext, .txtMPRounds & " round" & IIf(Val(.txtMPRounds) <> 1, "s", "") & " of must parry" & IIf(Val(.txtMPAmount) <> 0, " at " & .txtMPAmount, ""))
      End If
    If Val(.txtBPRounds) <> 0 And Val(.txtBPAmount) <> 0 Then
        Call AddBonusPenalty(.cmbCombatants.ListIndex + 1, Val(.txtBPRounds), Val(.txtBPAmount))
        logtext = CommaList(logtext, .txtBPRounds & " round" & IIf(Val(.txtBPRounds) <> 1, "s", "") & " at" & .txtBPAmount)
      End If
    Combatants(.cmbCombatants.ListIndex + 1).Penalty = _
        Combatants(.cmbCombatants.ListIndex + 1).Penalty + Val(.txtPenalty)
    If Val(.txtPenalty) <> 0 Then logtext = CommaList(logtext, "at " & .txtPenalty)
    If .cmbPosition.ListIndex + 1 <> Combatants(.cmbCombatants.ListIndex + 1).Position Then
        Combatants(.cmbCombatants.ListIndex + 1).Position = .cmbPosition.ListIndex + 1
        logtext = CommaList(logtext, LCase(CombatantPosition(.cmbPosition.ListIndex + 1)))
      End If
    If .txtHealth <> "" Then
        Combatants(.cmbCombatants.ListIndex + 1).Health = _
            Combatants(.cmbCombatants.ListIndex + 1).Health & .txtHealth & Chr$(13) & Chr$(10)
        logtext = CommaList(logtext, LCase(.txtHealth))
      End If
    If logtext2 <> "" Then logtext = CommaList(logtext, logtext2)
    If logtext <> "" Then
        cmdOK.Tag = ", defender damaged (" & logtext & ")"
      End If
    End With
End Sub

Private Sub txtWS_GotFocus()
  Call SelectField(txtWS)
End Sub

Private Sub cmdWSMinus_Click()
  txtWS = txtWS - 1
End Sub

Private Sub cmdWSPlus_Click()
  txtWS = txtWS + 1
End Sub

Private Sub imgDice_Click()
  frmIRIS.imgDice_Click
End Sub

Private Sub Form_Unload(Cancel As Integer)
  mciControl.Command = "Close"
End Sub


