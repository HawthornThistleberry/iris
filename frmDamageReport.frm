VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "Richtx32.ocx"
Begin VB.Form frmDamageReport 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Damage Report"
   ClientHeight    =   4710
   ClientLeft      =   1155
   ClientTop       =   1800
   ClientWidth     =   6630
   ControlBox      =   0   'False
   HelpContextID   =   440
   Icon            =   "frmDamageReport.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4710
   ScaleWidth      =   6630
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.CommandButton cmdOK 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Default         =   -1  'True
      Height          =   375
      HelpContextID   =   440
      Left            =   4680
      TabIndex        =   2
      Top             =   4320
      Width           =   1935
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print..."
      Height          =   375
      HelpContextID   =   440
      Left            =   0
      TabIndex        =   1
      Top             =   4320
      Width           =   1935
   End
   Begin RichTextLib.RichTextBox rtbDamageReport 
      Height          =   4215
      HelpContextID   =   440
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6615
      _ExtentX        =   11668
      _ExtentY        =   7435
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   6400
      TextRTF         =   $"frmDamageReport.frx":030A
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmDamageReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
End Sub

Private Sub Form_Activate()
  cmdOK.Tag = ""
  cmdOK.SetFocus
End Sub

Private Sub cmdOK_Click()
  Me.Hide
End Sub

Private Sub cmdPrint_Click()
  Dim orig, body As String
  Dim i As Integer
  On Local Error GoTo printError
  frmIRIS.cdlGeneric.CancelError = True
  frmIRIS.cdlGeneric.Flags = cdlPDReturnDC + cdlPDNoPageNums
  If rtbDamageReport.SelLength = 0 Then
      frmIRIS.cdlGeneric.Flags = frmIRIS.cdlGeneric.Flags + cdlPDAllPages
    Else
      frmIRIS.cdlGeneric.Flags = frmIRIS.cdlGeneric.Flags + cdlPDSelection
    End If
  frmIRIS.cdlGeneric.ShowPrinter
  On Local Error Resume Next
  orig = rtbDamageReport.TextRTF
  body = rtbDamageReport.TextRTF
  ' change all the \fs17s to \fs22s in body
  i = InStr(body, "\fs17")
  Do While i <> 0
    body = Left$(body, i - 1) & "\fs24" & Mid$(body, i + 5)
    i = InStr(body, "\fs17")
    Loop
  Printer.Print ""
  i = rtbDamageReport.RightMargin
  rtbDamageReport.RightMargin = 12000
  rtbDamageReport.TextRTF = body
  rtbDamageReport.SelPrint Printer.hDC
  rtbDamageReport.TextRTF = orig
  rtbDamageReport.RightMargin = i
  Printer.EndDoc
  Exit Sub
printError:
  If Err.Number <> cdlCancel Then
      MsgBox "Print Error: " & Err & "; " & Error, vbExclamation
    End If
End Sub


