VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "Richtx32.ocx"
Object = "{C1A8AF28-1257-101B-8FB0-0020AF039CA3}#1.1#0"; "MCI32.OCX"
Begin VB.Form frmMassRoll 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Mass Roll"
   ClientHeight    =   2790
   ClientLeft      =   1140
   ClientTop       =   1605
   ClientWidth     =   8805
   Icon            =   "frmMassRoll.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2790
   ScaleWidth      =   8805
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.CheckBox chkShowDetails 
      Caption         =   "Show Roll Details"
      Height          =   255
      Left            =   0
      TabIndex        =   8
      Top             =   2280
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.Frame fraResults 
      Caption         =   "Results"
      Height          =   2775
      Left            =   4080
      TabIndex        =   16
      Top             =   0
      Width           =   4695
      Begin RichTextLib.RichTextBox rtbResults 
         Height          =   2415
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   4455
         _ExtentX        =   7858
         _ExtentY        =   4260
         _Version        =   393217
         BackColor       =   16777215
         Enabled         =   -1  'True
         ReadOnly        =   -1  'True
         ScrollBars      =   2
         RightMargin     =   4100
         TextRTF         =   $"frmMassRoll.frx":030A
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Height          =   375
      Left            =   2760
      TabIndex        =   10
      Top             =   2400
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Roll >>"
      Default         =   -1  'True
      Height          =   375
      Left            =   2760
      TabIndex        =   9
      Top             =   1920
      Width           =   1215
   End
   Begin VB.CheckBox chkPercentage 
      Caption         =   "Percentage Results"
      Height          =   255
      Left            =   0
      TabIndex        =   7
      Top             =   1920
      Width           =   1815
   End
   Begin VB.Frame fraMassRoll 
      Caption         =   "Conditions"
      Height          =   1815
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   3975
      Begin VB.TextBox txtSkill 
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         Height          =   315
         Left            =   2040
         TabIndex        =   1
         Top             =   240
         Width           =   1815
      End
      Begin VB.TextBox txtMinus2 
         Height          =   285
         Left            =   2160
         MaxLength       =   4
         TabIndex        =   6
         Top             =   1320
         Width           =   1695
      End
      Begin VB.TextBox txtMinus1 
         Height          =   285
         Left            =   240
         MaxLength       =   4
         TabIndex        =   5
         Top             =   1320
         Width           =   1695
      End
      Begin VB.TextBox txtPlus 
         Height          =   285
         Left            =   2160
         MaxLength       =   4
         TabIndex        =   4
         Top             =   960
         Width           =   1695
      End
      Begin VB.ComboBox cmbDifficulty 
         Height          =   315
         ItemData        =   "frmMassRoll.frx":038C
         Left            =   120
         List            =   "frmMassRoll.frx":03B1
         TabIndex        =   3
         Text            =   "0 (Medium)"
         Top             =   915
         Width           =   1815
      End
      Begin VB.CheckBox chkStatusModifiers 
         Caption         =   "Apply status modifiers"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   600
         Value           =   1  'Checked
         Width           =   1815
      End
      Begin VB.ComboBox cmbStat 
         Height          =   315
         ItemData        =   "frmMassRoll.frx":0460
         Left            =   120
         List            =   "frmMassRoll.frx":047F
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label lblMinus2 
         Alignment       =   2  'Center
         Caption         =   "-"
         Height          =   255
         Left            =   1995
         TabIndex        =   15
         Top             =   1365
         Width           =   135
      End
      Begin VB.Label lblMinus1 
         Alignment       =   2  'Center
         Caption         =   "-"
         Height          =   255
         Left            =   75
         TabIndex        =   14
         Top             =   1365
         Width           =   135
      End
      Begin VB.Label lblPlus 
         Alignment       =   2  'Center
         Caption         =   "+"
         Height          =   255
         Left            =   1995
         TabIndex        =   13
         Top             =   1005
         Width           =   135
      End
   End
   Begin MCI.MMControl mciControl 
      Height          =   330
      Left            =   0
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   3540
      _ExtentX        =   6244
      _ExtentY        =   582
      _Version        =   393216
      PlayEnabled     =   -1  'True
      DeviceType      =   "WaveAudio"
      FileName        =   "C:\Program Files\IRIS\dice.wav"
   End
   Begin VB.Image imgDice 
      Height          =   480
      Left            =   2160
      MousePointer    =   4  'Icon
      Picture         =   "frmMassRoll.frx":04E2
      Top             =   1875
      Width           =   480
   End
End
Attribute VB_Name = "frmMassRoll"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const conRTFColorTable = "{\colortbl\red0\green0\blue0;\red128\green128\blue128;\red192\green192\blue192;\red128\green0\blue0;\red255\green255\blue255;\red128\green128\blue0;\red255\green255\blue0;\red0\green128\blue0;\red255\green0\blue0;\red0\green128\blue128;\red0\green255\blue255;\red0\green0\blue128;\red0\green255\blue0;\red128\green0\blue128;\red255\green0\blue255;\red0\green0\blue255;}"

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
  ' load things only on the first entry
  cmbStat.ListIndex = 5 ' default is Perception
  txtSkill = ""
  txtPlus = ""
  txtMinus1 = ""
  txtMinus2 = ""
  ' set up the sound to play
  mciControl.Notify = False
  mciControl.Wait = True
  mciControl.Shareable = False
  mciControl.DeviceType = "WaveAudio"
  mciControl.Filename = App.Path & "\dice.wav"
  mciControl.Command = "Open"
End Sub

Private Sub Form_Activate()
  cmbStat.SetFocus
End Sub

Private Sub cmbStat_Click()
  If cmbStat.ListIndex <> 8 Then
      txtSkill.Enabled = False
      txtSkill.BackColor = &HC0C0C0
    Else
      txtSkill.Enabled = True
      txtSkill.BackColor = &H80000005
      txtSkill.SetFocus
    End If
End Sub

Private Sub txtSkill_GotFocus()
  Call SelectField(txtSkill)
End Sub

Private Sub txtPlus_GotFocus()
  Call SelectField(txtPlus)
End Sub

Private Sub txtPlus_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtMinus1_GotFocus()
  Call SelectField(txtMinus1)
End Sub

Private Sub txtMinus1_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtMinus2_GotFocus()
  Call SelectField(txtMinus2)
End Sub

Private Sub txtMinus2_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub imgDice_Click()
  frmIRIS.imgDice_Click
End Sub

Private Sub cmdOK_Click()
  Dim i As Integer
  Dim roll As Integer
  Dim stat As Integer
  Dim mods As Integer
  Dim status As Integer
  Dim total As Integer
  Dim result As String
  Dim color As String
  Dim explanation As String
  Dim outcome As String
  mciControl.Command = "Play"
  mciControl.Command = "Prev"
  mods = Val(cmbDifficulty) + Val(txtPlus) - Val(txtMinus1) - Val(txtMinus2)
  For i = 1 To NumCombatants
    With Combatants(i)
      roll = OpenEndedD20("high")
      Select Case cmbStat.ListIndex
        Case 0
          stat = Combatants(i).StatS
        Case 1
          stat = Combatants(i).StatE
        Case 2
          stat = Combatants(i).StatD
        Case 3
          stat = Combatants(i).StatQ
        Case 4
          stat = Combatants(i).StatI
        Case 5
          stat = Combatants(i).StatP
        Case 6
          stat = Combatants(i).StatW
        Case 7
          stat = Combatants(i).StatR
        Case 8
          stat = InStr(Combatants(i).Skills, txtSkill)
          If stat = 0 Then
              stat = -5
            Else
              stat = stat + Len(txtSkill)
              Do While stat <= Len(Combatants(i).Skills)
                result = Mid(Combatants(i).Skills, stat, 1)
                If result >= "0" And result <= "9" Then Exit Do
                stat = stat + 1
                Loop
              If stat > Len(Combatants(i).Skills) Then
                  stat = -5
                Else
                  stat = Val(Trim(Mid(Combatants(i).Skills, stat)))
                End If
            End If
        End Select
      If chkStatusModifiers Then
          status = Combatants(i).Penalty + CombatantHpPenalty(i) + _
          IIf(Combatants(i).BonusPenalty <> "", Val(Word(Combatants(i).BonusPenalty, 1)), 0)
        Else
          status = 0
        End If
      total = roll + stat + mods + status
      ManeuverChart total, chkPercentage, color, result, explanation
      outcome = outcome & "\b " & Combatants(i).Name & "\plain\f2\fs17 : \b " & _
          color & result & "\plain\f2\fs17\cf1  (\cf14 " & total & "\cf1 )" & "\par\cf0 " & _
          IIf(chkShowDetails, "\plain\f2\fs17\cf1     = roll " & roll & " + " & _
          LCase$(cmbStat.List(cmbStat.ListIndex) & _
          IIf(frmMassRoll.cmbStat.ListIndex = 8, " " & frmMassRoll.txtSkill, "")) & " " & _
          stat & " + mods " & mods + status & "\par\cf0 ", "")
      End With
    Next i
  ' assemble the RTF around results and load it into the form
  rtbResults = _
    "{\rtf1\ansi\deff0\deftab720{\fonttbl{\f0\fswiss MS Sans Serif;}" & _
    "{\f1\froman\fcharset2 Symbol;}" & _
    "{\f2\fswiss MS Sans Serif;}}" & Chr$(13) & Chr$(10) & _
     conRTFColorTable & Chr$(13) & Chr$(10) & _
    "\deflang1033\pard\plain\f2\fs17" & outcome & Chr$(13) & Chr$(10) & _
    "\par }"
End Sub

Private Sub cmdCancel_Click()
  Me.Hide
End Sub

