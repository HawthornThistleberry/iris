VERSION 5.00
Begin VB.Form frmSurprise 
   Caption         =   "Surprise Combatants"
   ClientHeight    =   1830
   ClientLeft      =   1515
   ClientTop       =   1905
   ClientWidth     =   3630
   ControlBox      =   0   'False
   HelpContextID   =   100
   Icon            =   "frmSurprise.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1830
   ScaleWidth      =   3630
   Visible         =   0   'False
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel Begin"
      Height          =   375
      HelpContextID   =   100
      Left            =   2520
      TabIndex        =   5
      Top             =   1440
      Width           =   1095
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Begin"
      Default         =   -1  'True
      Height          =   375
      HelpContextID   =   100
      Left            =   1440
      TabIndex        =   4
      Top             =   1440
      Width           =   1095
   End
   Begin VB.CommandButton cmdClearAll 
      Caption         =   "Clear All"
      Height          =   375
      HelpContextID   =   100
      Left            =   0
      TabIndex        =   3
      Top             =   1440
      Width           =   735
   End
   Begin VB.Frame fraAdjustments 
      Caption         =   "Surprise:"
      Height          =   1335
      Left            =   2040
      TabIndex        =   6
      Top             =   0
      Width           =   1575
      Begin VB.ComboBox cmbSurprise 
         Height          =   315
         HelpContextID   =   100
         ItemData        =   "frmSurprise.frx":030A
         Left            =   840
         List            =   "frmSurprise.frx":0320
         TabIndex        =   1
         Text            =   "0"
         Top             =   600
         Width           =   615
      End
      Begin VB.ComboBox cmbSaves 
         Height          =   315
         HelpContextID   =   100
         ItemData        =   "frmSurprise.frx":033A
         Left            =   840
         List            =   "frmSurprise.frx":034A
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   960
         Width           =   615
      End
      Begin VB.Label lblName 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label lblSaves 
         Alignment       =   1  'Right Justify
         Caption         =   "Saves"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   1005
         Width           =   615
      End
      Begin VB.Label lblSurprise 
         Alignment       =   1  'Right Justify
         Caption         =   "Surprise"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   645
         Width           =   615
      End
   End
   Begin VB.ListBox lisCombatants 
      Height          =   1425
      HelpContextID   =   100
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   1935
   End
   Begin VB.Image imgDice 
      Height          =   480
      Left            =   840
      MousePointer    =   4  'Icon
      Picture         =   "frmSurprise.frx":035A
      Top             =   1410
      Width           =   480
   End
End
Attribute VB_Name = "frmSurprise"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const conSurpriseIndicator = "�"

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
End Sub

Private Sub Form_Activate()
  Dim i As Integer
  cmdCancel.Tag = ""
  lisCombatants.Clear
  For i = 1 To NumCombatants
    lisCombatants.AddItem IIf(Combatants(i).NextAction <> 0, _
      conSurpriseIndicator, "  ") & " " & Combatants(i).Name
    Next i
  lisCombatants.ListIndex = 0
  Call lisCombatants_Click
  lisCombatants.SetFocus
End Sub

Private Sub cmdOK_Click()
  Me.Hide
End Sub

Private Sub cmdCancel_Click()
  cmdCancel.Tag = "y"
  Me.Hide
End Sub

Private Sub lisCombatants_Click()
  ' load the surprise (NextAction) and saves into the fields
  lblName = Trim$(Mid$(lisCombatants.List(lisCombatants.ListIndex), 3))
  cmbSurprise = Combatants(lisCombatants.ListIndex + 1).NextAction
  cmbSaves = Combatants(lisCombatants.ListIndex + 1).Saves
End Sub

Private Sub cmbSurprise_Click()
  Combatants(lisCombatants.ListIndex + 1).NextAction = cmbSurprise
  lisCombatants.List(lisCombatants.ListIndex) = IIf(cmbSurprise = 0, "  ", conSurpriseIndicator) & _
      " " & Combatants(lisCombatants.ListIndex + 1).Name
End Sub

Private Sub cmbSaves_Click()
  Combatants(lisCombatants.ListIndex + 1).Saves = cmbSaves
End Sub

Private Sub cmdClearAll_Click()
  Dim i As Integer
  If MsgBox("Clear all surprise and saves?", vbYesNo + vbQuestion) = vbNo Then Exit Sub
  For i = 1 To NumCombatants
    Combatants(i).NextAction = 0
    Combatants(i).Saves = 0
    lisCombatants.List(i - 1) = "   " & Combatants(i).Name
    Next i
  Call lisCombatants_Click
End Sub

Private Sub imgDice_Click()
  frmIRIS.imgDice_Click
End Sub


