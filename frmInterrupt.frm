VERSION 5.00
Begin VB.Form frmInterrupt 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Select the Interrupter"
   ClientHeight    =   3375
   ClientLeft      =   2790
   ClientTop       =   2145
   ClientWidth     =   3630
   ControlBox      =   0   'False
   HelpContextID   =   190
   Icon            =   "frmInterrupt.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3375
   ScaleWidth      =   3630
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.TextBox txtWhen 
      Height          =   285
      HelpContextID   =   190
      Left            =   1920
      MaxLength       =   4
      TabIndex        =   8
      Top             =   1440
      Width           =   1695
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      HelpContextID   =   190
      Left            =   1920
      TabIndex        =   6
      Top             =   3000
      Width           =   1695
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Interrupt"
      Default         =   -1  'True
      Height          =   375
      HelpContextID   =   190
      Left            =   1920
      TabIndex        =   5
      Top             =   2520
      Width           =   1695
   End
   Begin VB.ListBox lisCombatants 
      Height          =   3375
      HelpContextID   =   190
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   1815
   End
   Begin VB.Image imgDice 
      Height          =   480
      Left            =   2520
      MousePointer    =   4  'Icon
      Picture         =   "frmInterrupt.frx":030A
      Top             =   1920
      Width           =   480
   End
   Begin VB.Label Label3 
      Caption         =   "Interrupt on phase:"
      Height          =   255
      Left            =   1920
      TabIndex        =   7
      Top             =   1200
      Width           =   1455
   End
   Begin VB.Label lblNextAction 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   1920
      TabIndex        =   4
      Top             =   840
      Width           =   1695
   End
   Begin VB.Label Label5 
      Caption         =   "Next Action:"
      Height          =   255
      Left            =   1920
      TabIndex        =   3
      Top             =   600
      Width           =   975
   End
   Begin VB.Label lblSaves 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   1920
      TabIndex        =   2
      Top             =   240
      Width           =   1695
   End
   Begin VB.Label Label2 
      Caption         =   "Saves:"
      Height          =   255
      Left            =   1920
      TabIndex        =   1
      Top             =   0
      Width           =   615
   End
End
Attribute VB_Name = "frmInterrupt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
End Sub

Private Sub Form_Activate()
  Dim i As Integer
  cmdCancel.Tag = ""
  lisCombatants.Clear
  For i = 1 To NumCombatants
    If Combatants(i).Saves > 0 Then
        lisCombatants.AddItem Combatants(i).Name
        lisCombatants.ItemData(lisCombatants.ListCount - 1) = i
      End If
    Next i
  lisCombatants.ListIndex = 0
  lisCombatants.SetFocus
End Sub

Private Sub cmdOK_Click()
  Me.Hide
End Sub

Private Sub cmdCancel_Click()
  cmdCancel.Tag = "y"
  Me.Hide
End Sub

Private Sub lisCombatants_Click()
  With Combatants(lisCombatants.ItemData(lisCombatants.ListIndex))
    lblSaves = .Saves
    lblNextAction = .NextAction
    End With
End Sub

Private Sub txtWhen_GotFocus()
  Call SelectField(txtWhen)
End Sub

Private Sub txtWhen_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub imgDice_Click()
  frmIRIS.imgDice_Click
End Sub


