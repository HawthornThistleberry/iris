VERSION 5.00
Begin VB.Form frmBonusPenalty 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Bonus/Penalty"
   ClientHeight    =   1110
   ClientLeft      =   2010
   ClientTop       =   1590
   ClientWidth     =   2415
   ControlBox      =   0   'False
   HelpContextID   =   350
   Icon            =   "frmBonusPenalty.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1110
   ScaleWidth      =   2415
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      HelpContextID   =   350
      Left            =   1320
      TabIndex        =   5
      Top             =   720
      Width           =   1095
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      HelpContextID   =   350
      Left            =   0
      TabIndex        =   4
      Top             =   720
      Width           =   1095
   End
   Begin VB.TextBox txtNumRounds 
      Height          =   285
      HelpContextID   =   350
      Left            =   1560
      MaxLength       =   4
      TabIndex        =   3
      Top             =   360
      Width           =   855
   End
   Begin VB.TextBox txtBonusPenalty 
      Height          =   285
      HelpContextID   =   350
      Left            =   1560
      MaxLength       =   4
      TabIndex        =   2
      Top             =   0
      Width           =   855
   End
   Begin VB.Label lblNumRounds 
      Alignment       =   1  'Right Justify
      Caption         =   "Number of Rounds"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   405
      Width           =   1335
   End
   Begin VB.Label lblBonusPenalty 
      Alignment       =   1  'Right Justify
      Caption         =   "Bonus/Penalty"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   45
      Width           =   1335
   End
End
Attribute VB_Name = "frmBonusPenalty"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()
  cmdCancel.Tag = "y"
  Me.Hide
End Sub

Private Sub cmdOK_Click()
  If txtBonusPenalty = "" Or txtNumRounds = "" Then
      MsgBox "You must enter both a bonus/penalty and a number of rounds.", vbExclamation
      Exit Sub
    End If
  Me.Hide
End Sub

Private Sub Form_Activate()
  cmdCancel.Tag = ""
  txtBonusPenalty.SetFocus
End Sub

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
End Sub

Private Sub txtBonusPenalty_GotFocus()
  Call SelectField(txtBonusPenalty)
End Sub

Private Sub txtBonusPenalty_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtNumRounds_GotFocus()
  Call SelectField(txtNumRounds)
End Sub

Private Sub txtNumRounds_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

