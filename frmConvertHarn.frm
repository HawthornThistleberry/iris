VERSION 5.00
Begin VB.Form frmConvertHarn 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Convert Combatant : HârnMaster"
   ClientHeight    =   4605
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7605
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4605
   ScaleWidth      =   7605
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   6120
      TabIndex        =   54
      Top             =   4200
      Width           =   1455
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   4560
      TabIndex        =   53
      Top             =   4200
      Width           =   1455
   End
   Begin VB.Frame fraNotes 
      Caption         =   "Notes"
      Height          =   1695
      Left            =   3840
      TabIndex        =   51
      Top             =   2400
      Width           =   3735
      Begin VB.TextBox txtNotes 
         Height          =   1335
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   30
         Top             =   240
         Width           =   3495
      End
   End
   Begin VB.Frame fraAttackSkills 
      Caption         =   "Attack Skills"
      Height          =   2055
      Left            =   3840
      TabIndex        =   50
      Top             =   240
      Width           =   3735
      Begin VB.TextBox txtAttackOB 
         Height          =   285
         Index           =   4
         Left            =   3120
         MaxLength       =   4
         TabIndex        =   29
         Top             =   1680
         Width           =   495
      End
      Begin VB.TextBox txtAttackOB 
         Height          =   285
         Index           =   3
         Left            =   3120
         MaxLength       =   4
         TabIndex        =   27
         Top             =   1320
         Width           =   495
      End
      Begin VB.TextBox txtAttackOB 
         Height          =   285
         Index           =   2
         Left            =   3120
         MaxLength       =   4
         TabIndex        =   25
         Top             =   960
         Width           =   495
      End
      Begin VB.TextBox txtAttackOB 
         Height          =   285
         Index           =   1
         Left            =   3120
         MaxLength       =   4
         TabIndex        =   23
         Top             =   600
         Width           =   495
      End
      Begin VB.TextBox txtAttack 
         Height          =   285
         Index           =   4
         Left            =   120
         TabIndex        =   28
         Top             =   1680
         Width           =   2895
      End
      Begin VB.TextBox txtAttack 
         Height          =   285
         Index           =   3
         Left            =   120
         TabIndex        =   26
         Top             =   1320
         Width           =   2895
      End
      Begin VB.TextBox txtAttack 
         Height          =   285
         Index           =   2
         Left            =   120
         TabIndex        =   24
         Top             =   960
         Width           =   2895
      End
      Begin VB.TextBox txtAttack 
         Height          =   285
         Index           =   1
         Left            =   120
         TabIndex        =   22
         Top             =   600
         Width           =   2895
      End
      Begin VB.TextBox txtAttackOB 
         Height          =   285
         Index           =   0
         Left            =   3120
         MaxLength       =   4
         TabIndex        =   21
         Top             =   240
         Width           =   495
      End
      Begin VB.TextBox txtAttack 
         Height          =   285
         Index           =   0
         Left            =   120
         TabIndex        =   20
         Top             =   240
         Width           =   2895
      End
   End
   Begin VB.Frame fraCombatSkills 
      Caption         =   "Combat Skills"
      Height          =   1695
      Left            =   1800
      TabIndex        =   46
      Top             =   2400
      Width           =   1935
      Begin VB.ComboBox cmbShieldType 
         Height          =   315
         ItemData        =   "frmConvertHarn.frx":0000
         Left            =   120
         List            =   "frmConvertHarn.frx":0016
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   1320
         Width           =   1695
      End
      Begin VB.TextBox txtShield 
         Height          =   285
         Left            =   1320
         MaxLength       =   4
         TabIndex        =   18
         Top             =   960
         Width           =   495
      End
      Begin VB.TextBox txtDodge 
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1320
         MaxLength       =   4
         TabIndex        =   17
         Top             =   600
         Width           =   495
      End
      Begin VB.TextBox txtInitiative 
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1320
         MaxLength       =   4
         TabIndex        =   16
         Top             =   240
         Width           =   495
      End
      Begin VB.Label lblShield 
         Caption         =   "Shield"
         Height          =   255
         Left            =   120
         TabIndex        =   49
         Top             =   990
         Width           =   930
      End
      Begin VB.Label lblDodge 
         Caption         =   "Dodge"
         Height          =   255
         Left            =   120
         TabIndex        =   48
         Top             =   630
         Width           =   975
      End
      Begin VB.Label lblInitiative 
         Caption         =   "Initiative"
         Height          =   255
         Left            =   120
         TabIndex        =   47
         Top             =   270
         Width           =   975
      End
   End
   Begin VB.Frame fraCombatAttributes 
      Caption         =   "Combat Attributes"
      Height          =   1695
      Left            =   0
      TabIndex        =   43
      Top             =   2400
      Width           =   1695
      Begin VB.TextBox txtHeight 
         Height          =   285
         Left            =   1080
         MaxLength       =   4
         TabIndex        =   15
         Top             =   960
         Width           =   495
      End
      Begin VB.TextBox txtMove 
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1080
         MaxLength       =   4
         TabIndex        =   14
         Top             =   600
         Width           =   495
      End
      Begin VB.TextBox txtEndurance 
         Height          =   285
         Left            =   1080
         MaxLength       =   4
         TabIndex        =   13
         Top             =   240
         Width           =   495
      End
      Begin VB.Label lblHeight 
         Caption         =   "Height (in.)"
         Height          =   255
         Left            =   120
         TabIndex        =   52
         Top             =   990
         Width           =   855
      End
      Begin VB.Label lblMove 
         Caption         =   "Move"
         Height          =   255
         Left            =   120
         TabIndex        =   45
         Top             =   630
         Width           =   855
      End
      Begin VB.Label lblEndurance 
         Caption         =   "Endurance"
         Height          =   255
         Left            =   120
         TabIndex        =   44
         Top             =   270
         Width           =   855
      End
   End
   Begin VB.OptionButton optCreature 
      Caption         =   "Creature"
      Height          =   255
      Left            =   2160
      TabIndex        =   2
      Top             =   360
      Width           =   1575
   End
   Begin VB.OptionButton optHumanoid 
      Caption         =   "Human-like"
      Height          =   255
      Left            =   720
      TabIndex        =   1
      Top             =   360
      Value           =   -1  'True
      Width           =   1455
   End
   Begin VB.TextBox txtName 
      Height          =   285
      Left            =   720
      TabIndex        =   0
      Top             =   30
      Width           =   3015
   End
   Begin VB.Frame fraAttributes 
      Caption         =   "Attributes"
      Height          =   1695
      Left            =   0
      TabIndex        =   31
      Top             =   600
      Width           =   3735
      Begin VB.TextBox txtWIL 
         Height          =   285
         Left            =   2520
         MaxLength       =   4
         TabIndex        =   12
         Top             =   960
         Width           =   375
      End
      Begin VB.TextBox txtAUR 
         Height          =   285
         Left            =   2520
         MaxLength       =   4
         TabIndex        =   11
         Top             =   600
         Width           =   375
      End
      Begin VB.TextBox txtINT 
         Height          =   285
         Left            =   2520
         MaxLength       =   4
         TabIndex        =   10
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox txtSML 
         Height          =   285
         Left            =   1320
         MaxLength       =   4
         TabIndex        =   9
         Top             =   960
         Width           =   375
      End
      Begin VB.TextBox txtHRG 
         Height          =   285
         Left            =   1320
         MaxLength       =   4
         TabIndex        =   8
         Top             =   600
         Width           =   375
      End
      Begin VB.TextBox txtEYE 
         Height          =   285
         Left            =   1320
         MaxLength       =   4
         TabIndex        =   7
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox txtAGL 
         Height          =   285
         Left            =   120
         MaxLength       =   4
         TabIndex        =   6
         Top             =   1320
         Width           =   375
      End
      Begin VB.TextBox txtDEX 
         Height          =   285
         Left            =   120
         MaxLength       =   4
         TabIndex        =   5
         Top             =   960
         Width           =   375
      End
      Begin VB.TextBox txtSTA 
         Height          =   285
         Left            =   120
         MaxLength       =   4
         TabIndex        =   4
         Top             =   600
         Width           =   375
      End
      Begin VB.TextBox txtSTR 
         Height          =   285
         Left            =   120
         MaxLength       =   4
         TabIndex        =   3
         Top             =   240
         Width           =   375
      End
      Begin VB.Label lblWIL 
         Caption         =   "WIL"
         Height          =   255
         Left            =   3000
         TabIndex        =   42
         Top             =   990
         Width           =   375
      End
      Begin VB.Label lblAUR 
         Caption         =   "AUR"
         Height          =   255
         Left            =   3000
         TabIndex        =   41
         Top             =   630
         Width           =   375
      End
      Begin VB.Label lblINT 
         Caption         =   "INT"
         Height          =   255
         Left            =   3000
         TabIndex        =   40
         Top             =   270
         Width           =   375
      End
      Begin VB.Label lblSML 
         Caption         =   "SML"
         Height          =   255
         Left            =   1800
         TabIndex        =   39
         Top             =   990
         Width           =   495
      End
      Begin VB.Label lblHRG 
         Caption         =   "HRG"
         Height          =   255
         Left            =   1800
         TabIndex        =   38
         Top             =   630
         Width           =   495
      End
      Begin VB.Label lblEYE 
         Caption         =   "EYE"
         Height          =   255
         Left            =   1800
         TabIndex        =   37
         Top             =   270
         Width           =   495
      End
      Begin VB.Label lblAGL 
         Caption         =   "AGL"
         Height          =   255
         Left            =   600
         TabIndex        =   36
         Top             =   1350
         Width           =   375
      End
      Begin VB.Label lblDEX 
         Caption         =   "DEX"
         Height          =   255
         Left            =   600
         TabIndex        =   35
         Top             =   990
         Width           =   375
      End
      Begin VB.Label lblSTA 
         Caption         =   "STA"
         Height          =   255
         Left            =   600
         TabIndex        =   34
         Top             =   630
         Width           =   375
      End
      Begin VB.Label lblSTR 
         Caption         =   "STR"
         Height          =   255
         Left            =   600
         TabIndex        =   33
         Top             =   270
         Width           =   375
      End
   End
   Begin VB.Label lblName 
      Alignment       =   1  'Right Justify
      Caption         =   "Name"
      Height          =   255
      Left            =   0
      TabIndex        =   32
      Top             =   75
      Width           =   615
   End
   Begin VB.Image imgDice 
      Height          =   480
      Left            =   3840
      MousePointer    =   4  'Icon
      Picture         =   "frmConvertHarn.frx":0062
      Top             =   4140
      Width           =   480
   End
End
Attribute VB_Name = "frmConvertHarn"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
End Sub

Private Sub Form_Activate()
  Dim i As Integer
  cmdCancel.Tag = ""
  cmdOK.Tag = ""
  ' allow optHumanoid/optCreature to default to where it was last
  ' load other fields
  txtSTR = ""
  txtSTA = ""
  txtDEX = ""
  txtAGL = ""
  txtEYE = ""
  txtHRG = ""
  txtSML = ""
  txtINT = ""
  txtAUR = ""
  txtWIL = ""
  txtEndurance = ""
  txtMove = ""
  txtHeight = ""
  txtInitiative = ""
  txtDodge = ""
  txtShield = ""
  cmbShieldType.ListIndex = 0
  For i = 0 To 4
    txtAttack(i) = ""
    txtAttackOB(i) = ""
    Next
  txtNotes = ""
  txtName.SetFocus
End Sub

Private Sub cmdOK_Click()
  If txtSTR = "" Then txtSTR = "10"
  If txtSTA = "" Then txtSTA = "10"
  If txtDEX = "" Then txtDEX = "10"
  If txtAGL = "" Then txtAGL = "10"
  If txtEYE = "" Then txtEYE = "10"
  If txtHRG = "" Then txtHRG = "10"
  If txtSML = "" Then txtSML = "10"
  If txtINT = "" Then txtINT = "10"
  If txtAUR = "" Then txtAUR = "10"
  If txtSTR = "" Then txtSTR = "10"
  If txtWIL = "" Then txtWIL = "10"
  If txtEndurance = "" Then txtEndurance = "10"
  If txtMove = "" Then txtMove = "10"
  If txtHeight = "" Then txtHeight = "66"
  If txtInitiative = "" Then txtInitiative = "0"
  If txtDodge = "" Then txtDodge = "0"
  If txtShield = "" Then txtDodge = "0"
  Me.Hide
End Sub

Private Sub cmdCancel_Click()
  cmdCancel.Tag = "y"
  Me.Hide
End Sub

Private Sub optHumanoid_Click()
  txtHeight.Enabled = True
  txtHeight.BackColor = &H80000005
  txtInitiative.Enabled = False
  txtInitiative.BackColor = &H8000000F
  txtMove.Enabled = False
  txtMove.BackColor = &H8000000F
  txtDodge.Enabled = False
  txtDodge.BackColor = &H8000000F
End Sub

Private Sub optCreature_Click()
  txtHeight.Enabled = False
  txtHeight.BackColor = &H8000000F
  txtInitiative.Enabled = True
  txtInitiative.BackColor = &H80000005
  txtMove.Enabled = True
  txtMove.BackColor = &H80000005
  txtDodge.Enabled = True
  txtDodge.BackColor = &H80000005
End Sub

Private Sub txtName_GotFocus()
  Call SelectField(txtName)
End Sub

Private Sub txtName_KeyPress(KeyAscii As Integer)
  KeyAscii = NoQuotes(KeyAscii)
End Sub

Private Sub txtSTR_GotFocus()
  Call SelectField(txtSTR)
End Sub

Private Sub txtSTR_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtSTA_GotFocus()
  Call SelectField(txtSTA)
End Sub

Private Sub txtSTA_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtDEX_GotFocus()
  Call SelectField(txtDEX)
End Sub

Private Sub txtDEX_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtAGL_GotFocus()
  Call SelectField(txtAGL)
End Sub

Private Sub txtAGL_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtEYE_GotFocus()
  Call SelectField(txtEYE)
End Sub

Private Sub txtEYE_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtHRG_GotFocus()
  Call SelectField(txtHRG)
End Sub

Private Sub txtHRG_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtSML_GotFocus()
  Call SelectField(txtSML)
End Sub

Private Sub txtSML_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtINT_GotFocus()
  Call SelectField(txtINT)
End Sub

Private Sub txtINT_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtAUR_GotFocus()
  Call SelectField(txtAUR)
End Sub

Private Sub txtAUR_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtWIL_GotFocus()
  Call SelectField(txtWIL)
End Sub

Private Sub txtWIL_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtEndurance_GotFocus()
  Call SelectField(txtEndurance)
End Sub

Private Sub txtEndurance_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtMove_GotFocus()
  Call SelectField(txtMove)
End Sub

Private Sub txtMove_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtHeight_GotFocus()
  Call SelectField(txtHeight)
End Sub

Private Sub txtHeight_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtInitiative_GotFocus()
  Call SelectField(txtInitiative)
End Sub

Private Sub txtInitiative_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtDodge_GotFocus()
  Call SelectField(txtDodge)
End Sub

Private Sub txtDodge_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtShield_GotFocus()
  Call SelectField(txtShield)
End Sub

Private Sub txtShield_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtAttack_GotFocus(Index As Integer)
  Call SelectField(txtAttack(Index))
End Sub

Private Sub txtAttack_KeyPress(Index As Integer, KeyAscii As Integer)
  KeyAscii = NoQuotes(KeyAscii)
End Sub

Private Sub txtAttackOB_GotFocus(Index As Integer)
  Call SelectField(txtAttackOB(Index))
End Sub

Private Sub txtAttackOB_KeyPress(Index As Integer, KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtNotes_KeyPress(KeyAscii As Integer)
  KeyAscii = NoQuotes(KeyAscii)
End Sub

Private Sub imgDice_Click()
  frmIRIS.imgDice_Click
End Sub

