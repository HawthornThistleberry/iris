VERSION 5.00
Begin VB.Form frmConvertCandT 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Convert Combatant: Creatures & Treasures"
   ClientHeight    =   4830
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6150
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4830
   ScaleWidth      =   6150
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   4920
      TabIndex        =   27
      Top             =   4440
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   3600
      TabIndex        =   26
      Top             =   4440
      Width           =   1215
   End
   Begin VB.Frame fraNotes 
      Caption         =   "Notes"
      Height          =   1695
      Left            =   2400
      TabIndex        =   43
      Top             =   2640
      Width           =   3735
      Begin VB.TextBox txtNotes 
         Height          =   1335
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   25
         Top             =   240
         Width           =   3495
      End
   End
   Begin VB.Frame fraAttackSkills 
      Caption         =   "Attacks"
      Height          =   2055
      Left            =   2400
      TabIndex        =   42
      Top             =   480
      Width           =   3735
      Begin VB.TextBox txtAttackOB 
         Height          =   285
         Index           =   4
         Left            =   120
         MaxLength       =   4
         TabIndex        =   23
         Top             =   1680
         Width           =   495
      End
      Begin VB.TextBox txtAttackOB 
         Height          =   285
         Index           =   3
         Left            =   120
         MaxLength       =   4
         TabIndex        =   21
         Top             =   1320
         Width           =   495
      End
      Begin VB.TextBox txtAttackOB 
         Height          =   285
         Index           =   2
         Left            =   120
         MaxLength       =   4
         TabIndex        =   19
         Top             =   960
         Width           =   495
      End
      Begin VB.TextBox txtAttackOB 
         Height          =   285
         Index           =   1
         Left            =   120
         MaxLength       =   4
         TabIndex        =   17
         Top             =   600
         Width           =   495
      End
      Begin VB.TextBox txtAttack 
         Height          =   285
         Index           =   4
         Left            =   720
         TabIndex        =   24
         Top             =   1680
         Width           =   2895
      End
      Begin VB.TextBox txtAttack 
         Height          =   285
         Index           =   3
         Left            =   720
         TabIndex        =   22
         Top             =   1320
         Width           =   2895
      End
      Begin VB.TextBox txtAttack 
         Height          =   285
         Index           =   2
         Left            =   720
         TabIndex        =   20
         Top             =   960
         Width           =   2895
      End
      Begin VB.TextBox txtAttack 
         Height          =   285
         Index           =   1
         Left            =   720
         TabIndex        =   18
         Top             =   600
         Width           =   2895
      End
      Begin VB.TextBox txtAttackOB 
         Height          =   285
         Index           =   0
         Left            =   120
         MaxLength       =   4
         TabIndex        =   15
         Top             =   240
         Width           =   495
      End
      Begin VB.TextBox txtAttack 
         Height          =   285
         Index           =   0
         Left            =   720
         TabIndex        =   16
         Top             =   240
         Width           =   2895
      End
   End
   Begin VB.Frame fraVitals 
      Caption         =   "Vitals"
      Height          =   2055
      Left            =   0
      TabIndex        =   38
      Top             =   480
      Width           =   2295
      Begin VB.ComboBox cmbSpeedAQ 
         Height          =   315
         ItemData        =   "frmConvertCandT.frx":0000
         Left            =   1080
         List            =   "frmConvertCandT.frx":0028
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   960
         Width           =   1095
      End
      Begin VB.ComboBox cmbAT 
         Height          =   315
         ItemData        =   "frmConvertCandT.frx":00B8
         Left            =   1080
         List            =   "frmConvertCandT.frx":00F8
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   1680
         Width           =   615
      End
      Begin VB.TextBox txtDB 
         Height          =   285
         Left            =   1680
         TabIndex        =   6
         Top             =   1680
         Width           =   495
      End
      Begin VB.TextBox txtHits 
         Height          =   285
         Left            =   1080
         MaxLength       =   4
         TabIndex        =   4
         Top             =   1320
         Width           =   1095
      End
      Begin VB.TextBox txtMNBonus 
         BackColor       =   &H80000014&
         Height          =   285
         Left            =   1080
         MaxLength       =   4
         TabIndex        =   2
         Top             =   600
         Width           =   1095
      End
      Begin VB.TextBox txtBaseRate 
         BackColor       =   &H80000014&
         Height          =   285
         Left            =   1080
         MaxLength       =   4
         TabIndex        =   1
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label lblATDB 
         Alignment       =   1  'Right Justify
         Caption         =   "AT/DB"
         Height          =   255
         Left            =   120
         TabIndex        =   45
         Top             =   1710
         Width           =   855
      End
      Begin VB.Label lblHits 
         Alignment       =   1  'Right Justify
         Caption         =   "Hits"
         Height          =   255
         Left            =   120
         TabIndex        =   44
         Top             =   1350
         Width           =   855
      End
      Begin VB.Label lblSpeedAQ 
         Alignment       =   1  'Right Justify
         Caption         =   "Speed AQ"
         Height          =   255
         Left            =   120
         TabIndex        =   41
         Top             =   990
         Width           =   855
      End
      Begin VB.Label lblMNBonus 
         Alignment       =   1  'Right Justify
         Caption         =   "MN Bonus"
         Height          =   255
         Left            =   120
         TabIndex        =   40
         Top             =   630
         Width           =   855
      End
      Begin VB.Label lblBaseRate 
         Alignment       =   1  'Right Justify
         Caption         =   "Base Rate"
         Height          =   255
         Left            =   120
         TabIndex        =   39
         Top             =   270
         Width           =   855
      End
   End
   Begin VB.TextBox txtName 
      Height          =   285
      Left            =   720
      TabIndex        =   0
      Top             =   60
      Width           =   5295
   End
   Begin VB.Frame fraAttributes 
      Caption         =   "Stats (optional)"
      Height          =   1695
      Left            =   0
      TabIndex        =   28
      Top             =   2640
      Width           =   2295
      Begin VB.TextBox txtR 
         Height          =   285
         Left            =   1800
         MaxLength       =   4
         TabIndex        =   14
         Top             =   1320
         Width           =   375
      End
      Begin VB.TextBox txtW 
         Height          =   285
         Left            =   1800
         MaxLength       =   4
         TabIndex        =   13
         Top             =   960
         Width           =   375
      End
      Begin VB.TextBox txtP 
         Height          =   285
         Left            =   1800
         MaxLength       =   4
         TabIndex        =   12
         Top             =   600
         Width           =   375
      End
      Begin VB.TextBox txtI 
         Height          =   285
         Left            =   1800
         MaxLength       =   4
         TabIndex        =   11
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox txtQ 
         Height          =   285
         Left            =   600
         MaxLength       =   4
         TabIndex        =   10
         Top             =   1320
         Width           =   375
      End
      Begin VB.TextBox txtD 
         Height          =   285
         Left            =   600
         MaxLength       =   4
         TabIndex        =   9
         Top             =   960
         Width           =   375
      End
      Begin VB.TextBox txtE 
         Height          =   285
         Left            =   600
         MaxLength       =   4
         TabIndex        =   8
         Top             =   600
         Width           =   375
      End
      Begin VB.TextBox txtS 
         Height          =   285
         Left            =   600
         MaxLength       =   4
         TabIndex        =   7
         Top             =   240
         Width           =   375
      End
      Begin VB.Label lblHP 
         Alignment       =   1  'Right Justify
         Caption         =   "R"
         Height          =   255
         Left            =   1200
         TabIndex        =   37
         Top             =   1350
         Width           =   495
      End
      Begin VB.Label lblPOW 
         Alignment       =   1  'Right Justify
         Caption         =   "W"
         Height          =   255
         Left            =   1200
         TabIndex        =   36
         Top             =   990
         Width           =   495
      End
      Begin VB.Label lblDEX 
         Alignment       =   1  'Right Justify
         Caption         =   "P"
         Height          =   255
         Left            =   1200
         TabIndex        =   35
         Top             =   630
         Width           =   495
      End
      Begin VB.Label lblINT 
         Alignment       =   1  'Right Justify
         Caption         =   "I"
         Height          =   255
         Left            =   1200
         TabIndex        =   34
         Top             =   270
         Width           =   495
      End
      Begin VB.Label lblAPP 
         Alignment       =   1  'Right Justify
         Caption         =   "Q"
         Height          =   255
         Left            =   120
         TabIndex        =   33
         Top             =   1350
         Width           =   375
      End
      Begin VB.Label lblSIZ 
         Alignment       =   1  'Right Justify
         Caption         =   "D"
         Height          =   255
         Left            =   120
         TabIndex        =   32
         Top             =   990
         Width           =   375
      End
      Begin VB.Label lblCON 
         Alignment       =   1  'Right Justify
         Caption         =   "E"
         Height          =   255
         Left            =   120
         TabIndex        =   31
         Top             =   630
         Width           =   375
      End
      Begin VB.Label lblSTR 
         Alignment       =   1  'Right Justify
         Caption         =   "S"
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   270
         Width           =   375
      End
   End
   Begin VB.Label lblName 
      Alignment       =   1  'Right Justify
      Caption         =   "Name"
      Height          =   255
      Left            =   0
      TabIndex        =   29
      Top             =   105
      Width           =   615
   End
   Begin VB.Image imgDice 
      Height          =   480
      Left            =   2880
      MousePointer    =   4  'Icon
      Picture         =   "frmConvertCandT.frx":0143
      Top             =   4380
      Width           =   480
   End
End
Attribute VB_Name = "frmConvertCandT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  ' center the form on the screen
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
End Sub

Private Sub Form_Activate()
  Dim i As Integer
  cmdCancel.Tag = ""
  cmdOK.Tag = ""
  ' load fields with defaults
  txtBaseRate = ""
  txtMNBonus = ""
  cmbSpeedAQ.ListIndex = 4
  txtHits = ""
  cmbAT.ListIndex = 0
  txtDB = ""
  txtS = ""
  txtE = ""
  txtD = ""
  txtQ = ""
  txtI = ""
  txtP = ""
  txtW = ""
  txtR = ""
  For i = 0 To 4
    txtAttack(i) = ""
    txtAttackOB(i) = ""
    Next
  txtNotes = ""
  txtName.SetFocus
End Sub

Private Sub cmdOK_Click()
  ' force values in blank fields
  If txtS = "" Then txtS = "0"
  If txtE = "" Then txtE = "0"
  If txtD = "" Then txtD = "0"
  If txtQ = "" Then txtQ = "0"
  If txtI = "" Then txtI = "0"
  If txtP = "" Then txtP = "0"
  If txtW = "" Then txtW = "0"
  If txtR = "" Then txtR = "0"
  If txtBaseRate = "" Then txtBaseRate = "83"
  If txtMNBonus = "" Then txtMNBonus = "0"
  If txtHits = "" Then txtHits = "50"
  If txtDB = "" Then txtDB = "0"
  Me.Hide
End Sub

Private Sub cmdCancel_Click()
  cmdCancel.Tag = "y"
  Me.Hide
End Sub

Private Sub txtName_GotFocus()
  Call SelectField(txtName)
End Sub

Private Sub txtName_KeyPress(KeyAscii As Integer)
  KeyAscii = NoQuotes(KeyAscii)
End Sub

' select and validate fields

Private Sub txtBaseRate_GotFocus()
  Call SelectField(txtBaseRate)
End Sub

Private Sub txtBaseRate_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtMNBonus_GotFocus()
  Call SelectField(txtMNBonus)
End Sub

Private Sub txtMNBonus_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtHits_GotFocus()
  Call SelectField(txtHits)
End Sub

Private Sub txtHits_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtS_GotFocus()
  Call SelectField(txtS)
End Sub

Private Sub txtS_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtE_GotFocus()
  Call SelectField(txtE)
End Sub

Private Sub txtE_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtD_GotFocus()
  Call SelectField(txtD)
End Sub

Private Sub txtD_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtQ_GotFocus()
  Call SelectField(txtQ)
End Sub

Private Sub txtQ_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtI_GotFocus()
  Call SelectField(txtI)
End Sub

Private Sub txtI_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtP_GotFocus()
  Call SelectField(txtP)
End Sub

Private Sub txtP_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtW_GotFocus()
  Call SelectField(txtW)
End Sub

Private Sub txtW_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtR_GotFocus()
  Call SelectField(txtR)
End Sub

Private Sub txtR_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtAttackOB_GotFocus(Index As Integer)
  Call SelectField(txtAttackOB(Index))
End Sub

Private Sub txtAttackOB_KeyPress(Index As Integer, KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtNotes_KeyPress(KeyAscii As Integer)
  KeyAscii = NoQuotes(KeyAscii)
End Sub

Private Sub imgDice_Click()
  frmIRIS.imgDice_Click
End Sub

