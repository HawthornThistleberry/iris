VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmPlayerDisplay 
   Caption         =   "IRIS Player Display"
   ClientHeight    =   5430
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12750
   ControlBox      =   0   'False
   HelpContextID   =   10
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5430
   ScaleWidth      =   12750
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lisPLDPlayerList 
      BeginProperty Font 
         Name            =   "Andale Mono"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1410
      Left            =   0
      Sorted          =   -1  'True
      TabIndex        =   5
      Top             =   2160
      Width           =   12735
   End
   Begin RichTextLib.RichTextBox rtbPLDPlayerDetails 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   ""
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   0
      EndProperty
      Height          =   1815
      Left            =   0
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   3600
      Width           =   12735
      _ExtentX        =   22463
      _ExtentY        =   3201
      _Version        =   393217
      BackColor       =   -2147483633
      Enabled         =   -1  'True
      ScrollBars      =   2
      TextRTF         =   $"frmPlayerDisplay.frx":0000
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraPLDCurrentStatus 
      Caption         =   "Current Status"
      Height          =   1695
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   12735
      Begin VB.Label lblPLDPlayerStatus 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   24
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   525
         Left            =   2520
         TabIndex        =   3
         Top             =   1080
         Width           =   10095
      End
      Begin VB.Label lblPLDAction 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   29.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   855
         Left            =   2520
         TabIndex        =   2
         Top             =   240
         Width           =   10095
      End
      Begin VB.Label lblPLDPhase 
         Caption         =   "n/a"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   54
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1335
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   2295
      End
   End
   Begin VB.Label lblPLDPlayerDisplayHeading 
      Caption         =   "Nxt Name                   Hp   Sv Status"
      BeginProperty Font 
         Name            =   "Andale Mono"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   0
      TabIndex        =   6
      Top             =   1680
      Width           =   12735
   End
End
Attribute VB_Name = "frmPlayerDisplay"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
  Width = GetSetting(conAppName, "PlayerDisplay", "Width", Width)
  Height = GetSetting(conAppName, "PlayerDisplay", "Height", Height)
  PositionForm Me, GetSetting(conAppName, "PlayerDisplay", "Left", (Screen.Width - Width) / 2), GetSetting(conAppName, "PlayerDisplay", "Top", (Screen.Height - Height) / 2)
  Call Form_Refresh
End Sub

Private Sub Form_Resize()
  If Width < 12870 Then Width = 12870
  If Height < 5835 Then Height = 5835
  ' Resize things based on width:
  fraPLDCurrentStatus.Width = Width - 135
  lblPLDAction.Width = Width - 2775
  lblPLDPlayerStatus.Width = Width - 2775
  lisPLDPlayerList.Width = Width - 135
  rtbPLDPlayerDetails.Width = Width - 135
  ' Resize things based on height:
  rtbPLDPlayerDetails.Top = Height - 2235
  lisPLDPlayerList.Height = Height - 4425
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Call SaveSetting(conAppName, "PlayerDisplay", "Left", Left)
  Call SaveSetting(conAppName, "PlayerDisplay", "Top", Top)
  Call SaveSetting(conAppName, "PlayerDisplay", "Height", Height)
  Call SaveSetting(conAppName, "PlayerDisplay", "Width", Width)
End Sub

Public Sub Form_Refresh()
  Dim i As Integer, j As Integer
  If frmIRIS.lblPhase = "none" Then lblPLDPhase = "n/a" Else lblPLDPhase = frmIRIS.lblPhase
  lisPLDPlayerList.Clear
  For i = 1 To NumCombatants
    With Combatants(i)
      If .PC Then
          j = .NextAction
          If .LostInits > 0 Then j = j + 10
          lisPLDPlayerList.AddItem Right("000" & CStr(j), 3) & " " & Left(.Name & "                    ", 20) & " " & Right("   " & CStr(.CurHp), 3) & "/" & Left(CStr(.OptHp) & "   ", 3) & " " + CStr(.Saves) & " " & CombatantStatus(i, 0)
          lisPLDPlayerList.ItemData(lisPLDPlayerList.NewIndex) = i
        End If
      End With
    Next i
  If lisPLDPlayerList.ListCount >= 1 Then lisPLDPlayerList.ListIndex = 0 Else rtbPLDPlayerDetails = ""
End Sub

Private Sub lblPLDAction_Click()
  Call Form_Refresh
End Sub

Private Sub lblPLDPhase_Click()
  Call Form_Refresh
End Sub

Private Sub lblPLDPlayerStatus_Click()
  Call Form_Refresh
End Sub

Private Sub lisPLDPlayerList_Click()
  Dim s As String, i As Integer
  With Combatants(lisPLDPlayerList.ItemData(lisPLDPlayerList.ListIndex))
    If .Position = 10 Then s = CommaList(s, "\b\cf8 Dead!\plain\f2\fs48") Else _
    If .Position = 9 Then s = CommaList(s, "\b\cf5 Unconscious!\plain\f2\fs48")
    If .CurHp <= 0 Then s = CommaList(s, "\b\cf3 Out!\plain\f2\fs48") Else _
    If .CurHp < .OptHp Then s = CommaList(s, "Hp " & .CurHp & " out of " & .OptHp & " [" & Int(.CurHp * 100 / .OptHp) & "%]")
    If .RoundsTillDead > 0 Then s = CommaList(s, "\b Dead in " & .RoundsTillDead & " rounds")
    If .RoundsTillDown > 0 Then s = CommaList(s, "\b Down in " & .RoundsTillDown & " rounds")
    If .Bleed > 0 Then s = CommaList(s, "Bleeding " & .Bleed & "/rnd")
    If .StunNoParries > 0 Then s = CommaList(s, "S/NP " & .StunNoParries & " " & Rnds(.StunNoParries))
    If .Stuns > 0 Then s = CommaList(s, "Stunned " & .Stuns & " " & Rnds(.Stuns))
    If .MustParry <> "" Then s = CommaList(s, MPBPDisplay("Must parry", .MustParry))
    If .LostInits > 0 Then s = CommaList(s, "Lost Inits " & .LostInits & " " & Rnds(.LostInits))
    If .Penalty <> 0 Then s = CommaList(s, "@" & .Penalty)
    If .BonusPenalty <> "" Then s = CommaList(s, MPBPDisplay("Tmp Bonus/Pnlty", .BonusPenalty))
    If .CapLoss > 0 Then s = CommaList(s, "Cap. Loss " & .CapLoss)
    If s = "" Then s = "No damage"
    If .Saves > 0 Then s = s & "; " & CStr(.Saves) & " saved actions."
  End With
  rtbPLDPlayerDetails = "{\rtf1\fbidis\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fswiss\fcharset0 MS Sans Serif;}{\f1\froman\fprq2\fcharset0 Arrus BT;}{\f2\fswiss MS Sans Serif;}}\viewkind4\uc1\pard\ltrpar\b\f0\fs48 " & Combatants(lisPLDPlayerList.ItemData(lisPLDPlayerList.ListIndex)).Name & "\b0 :\par " & s & Chr$(13) & Chr$(10) & "\par }"
End Sub
