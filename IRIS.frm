VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{7A080CC8-26E2-101B-AEBD-04021C009402}#1.0#0"; "gauge32.ocx"
Begin VB.Form frmIRIS 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "IRIS"
   ClientHeight    =   7230
   ClientLeft      =   480
   ClientTop       =   2085
   ClientWidth     =   11790
   HelpContextID   =   10
   Icon            =   "IRIS.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   482
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   786
   Begin VB.Frame fraLog 
      Caption         =   "Log"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   0
      TabIndex        =   109
      Top             =   6000
      Width           =   11775
      Begin VB.CommandButton cmdLogClear 
         Caption         =   "Clear..."
         Enabled         =   0   'False
         Height          =   375
         Left            =   10680
         TabIndex        =   112
         Top             =   720
         Width           =   975
      End
      Begin VB.CommandButton cmdLogPrint 
         Caption         =   "Print..."
         Enabled         =   0   'False
         Height          =   375
         Left            =   10680
         TabIndex        =   111
         Top             =   240
         Width           =   975
      End
      Begin RichTextLib.RichTextBox rtbLog 
         Height          =   930
         Left            =   120
         TabIndex        =   110
         Top             =   210
         Width           =   10455
         _ExtentX        =   18441
         _ExtentY        =   1640
         _Version        =   393217
         BackColor       =   -2147483633
         ScrollBars      =   2
         TextRTF         =   $"IRIS.frx":030A
      End
   End
   Begin VB.Frame fraCombatants 
      Caption         =   "Combatants"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4695
      Left            =   0
      TabIndex        =   67
      Top             =   1320
      Width           =   11775
      Begin VB.CheckBox chkPC 
         Caption         =   "Check1"
         Height          =   255
         Left            =   9840
         TabIndex        =   113
         Top             =   240
         Width           =   255
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "Delete"
         Enabled         =   0   'False
         Height          =   375
         HelpContextID   =   330
         Left            =   1200
         TabIndex        =   18
         Top             =   4200
         Width           =   975
      End
      Begin VB.TextBox txtNotes 
         Enabled         =   0   'False
         Height          =   855
         Left            =   2280
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   107
         Top             =   3720
         Width           =   5535
      End
      Begin VB.TextBox txtSkills 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1935
         Left            =   6120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   28
         Top             =   1680
         Width           =   1695
      End
      Begin VB.Frame fraStatsSkills 
         Caption         =   "Stats"
         Height          =   2295
         Left            =   2280
         TabIndex        =   97
         Top             =   600
         Width           =   1695
         Begin VB.TextBox txtR 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1080
            MaxLength       =   4
            TabIndex        =   27
            Top             =   1920
            Width           =   495
         End
         Begin VB.TextBox txtW 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1080
            MaxLength       =   4
            TabIndex        =   26
            Top             =   1680
            Width           =   495
         End
         Begin VB.TextBox txtP 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1080
            MaxLength       =   4
            TabIndex        =   25
            Top             =   1440
            Width           =   495
         End
         Begin VB.TextBox txtI 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1080
            MaxLength       =   4
            TabIndex        =   24
            Top             =   1200
            Width           =   495
         End
         Begin VB.TextBox txtQ 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1080
            MaxLength       =   4
            TabIndex        =   23
            Top             =   960
            Width           =   495
         End
         Begin VB.TextBox txtD 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1080
            MaxLength       =   4
            TabIndex        =   22
            Top             =   720
            Width           =   495
         End
         Begin VB.TextBox txtE 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1080
            MaxLength       =   4
            TabIndex        =   21
            Top             =   480
            Width           =   495
         End
         Begin VB.TextBox txtS 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1080
            MaxLength       =   4
            TabIndex        =   20
            Top             =   240
            Width           =   495
         End
         Begin VB.Label lblR 
            Alignment       =   1  'Right Justify
            Caption         =   "Resistance"
            Height          =   255
            Left            =   120
            TabIndex        =   105
            Top             =   1965
            Width           =   855
         End
         Begin VB.Label lblW 
            Alignment       =   1  'Right Justify
            Caption         =   "Will"
            Height          =   255
            Left            =   120
            TabIndex        =   104
            Top             =   1725
            Width           =   855
         End
         Begin VB.Label lblP 
            Alignment       =   1  'Right Justify
            Caption         =   "Perception"
            Height          =   255
            Left            =   120
            TabIndex        =   103
            Top             =   1485
            Width           =   855
         End
         Begin VB.Label lblI 
            Alignment       =   1  'Right Justify
            Caption         =   "Intelligence"
            Height          =   255
            Left            =   120
            TabIndex        =   102
            Top             =   1245
            Width           =   855
         End
         Begin VB.Label lblQ 
            Alignment       =   1  'Right Justify
            Caption         =   "Quickness"
            Height          =   255
            Left            =   120
            TabIndex        =   101
            Top             =   1005
            Width           =   855
         End
         Begin VB.Label lblD 
            Alignment       =   1  'Right Justify
            Caption         =   "Dexterity"
            Height          =   255
            Left            =   120
            TabIndex        =   100
            Top             =   765
            Width           =   855
         End
         Begin VB.Label lblE 
            Alignment       =   1  'Right Justify
            Caption         =   "Endurance"
            Height          =   255
            Left            =   120
            TabIndex        =   99
            Top             =   525
            Width           =   855
         End
         Begin VB.Label lblS 
            Alignment       =   1  'Right Justify
            Caption         =   "Strength"
            Height          =   255
            Left            =   120
            TabIndex        =   98
            Top             =   285
            Width           =   855
         End
      End
      Begin VB.TextBox txtName 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2880
         TabIndex        =   19
         Top             =   240
         Width           =   6855
      End
      Begin VB.Frame fraAttacks 
         Caption         =   "Attacks"
         Height          =   855
         Left            =   7920
         TabIndex        =   74
         Top             =   600
         Width           =   3735
         Begin VB.TextBox txtAttacks 
            Enabled         =   0   'False
            Height          =   525
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   39
            Top             =   240
            Width           =   3495
         End
      End
      Begin VB.Frame fraHealth 
         Caption         =   "Health"
         Height          =   3165
         Left            =   7920
         TabIndex        =   79
         Top             =   1440
         Width           =   3735
         Begin VB.TextBox txtHealth 
            Enabled         =   0   'False
            Height          =   885
            Left            =   1320
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   59
            Top             =   2160
            Width           =   2295
         End
         Begin VB.ComboBox cmbPosition 
            Enabled         =   0   'False
            Height          =   315
            ItemData        =   "IRIS.frx":038C
            Left            =   120
            List            =   "IRIS.frx":03AE
            Style           =   2  'Dropdown List
            TabIndex        =   52
            Top             =   2760
            Width           =   1095
         End
         Begin VB.TextBox txtRoundsTillDead 
            Enabled         =   0   'False
            Height          =   285
            Left            =   720
            MaxLength       =   4
            TabIndex        =   46
            Top             =   2040
            Width           =   495
         End
         Begin VB.TextBox txtRoundsTillDown 
            Enabled         =   0   'False
            Height          =   285
            Left            =   720
            MaxLength       =   4
            TabIndex        =   45
            Top             =   1680
            Width           =   495
         End
         Begin VB.ListBox lisBonusPenalty 
            Enabled         =   0   'False
            Height          =   645
            HelpContextID   =   350
            Left            =   2760
            TabIndex        =   56
            Top             =   1440
            Width           =   855
         End
         Begin VB.ListBox lisMustParry 
            Enabled         =   0   'False
            Height          =   645
            HelpContextID   =   350
            Left            =   2760
            TabIndex        =   53
            Top             =   600
            Width           =   855
         End
         Begin VB.CommandButton cmdBPMinus 
            Caption         =   "-"
            Enabled         =   0   'False
            Height          =   285
            HelpContextID   =   350
            Left            =   2400
            TabIndex        =   58
            Top             =   1800
            Width           =   375
         End
         Begin VB.CommandButton cmdBPPlus 
            Caption         =   "+"
            Enabled         =   0   'False
            Height          =   255
            HelpContextID   =   350
            Left            =   2400
            TabIndex        =   57
            Top             =   1560
            Width           =   375
         End
         Begin VB.CommandButton cmdMPMinus 
            Caption         =   "-"
            Enabled         =   0   'False
            Height          =   285
            HelpContextID   =   350
            Left            =   2400
            TabIndex        =   55
            Top             =   960
            Width           =   375
         End
         Begin VB.CommandButton cmdMPPlus 
            Caption         =   "+"
            Enabled         =   0   'False
            Height          =   255
            HelpContextID   =   350
            Left            =   2400
            TabIndex        =   54
            Top             =   720
            Width           =   375
         End
         Begin VB.TextBox txtPenalty 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1800
            MaxLength       =   4
            TabIndex        =   50
            Top             =   1680
            Width           =   495
         End
         Begin VB.TextBox txtLostInits 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1800
            MaxLength       =   4
            TabIndex        =   49
            Top             =   1320
            Width           =   495
         End
         Begin VB.TextBox txtStunNoParries 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1800
            MaxLength       =   4
            TabIndex        =   48
            Top             =   960
            Width           =   495
         End
         Begin VB.TextBox txtStuns 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1800
            MaxLength       =   4
            TabIndex        =   47
            Top             =   600
            Width           =   495
         End
         Begin VB.TextBox txtCapLoss 
            Enabled         =   0   'False
            Height          =   285
            Left            =   720
            MaxLength       =   4
            TabIndex        =   51
            Top             =   2400
            Width           =   495
         End
         Begin VB.TextBox txtClot 
            Enabled         =   0   'False
            Height          =   285
            Left            =   720
            MaxLength       =   4
            TabIndex        =   44
            Top             =   1320
            Width           =   495
         End
         Begin VB.TextBox txtHeal 
            Enabled         =   0   'False
            Height          =   285
            Left            =   720
            MaxLength       =   4
            TabIndex        =   43
            Top             =   960
            Width           =   495
         End
         Begin VB.TextBox txtBleed 
            Enabled         =   0   'False
            Height          =   285
            Left            =   720
            MaxLength       =   4
            TabIndex        =   42
            Top             =   600
            Width           =   495
         End
         Begin VB.TextBox txtOptHp 
            Enabled         =   0   'False
            Height          =   285
            Left            =   960
            MaxLength       =   4
            TabIndex        =   41
            Top             =   240
            Width           =   375
         End
         Begin VB.TextBox txtCurHp 
            Enabled         =   0   'False
            Height          =   285
            Left            =   360
            MaxLength       =   4
            TabIndex        =   40
            Top             =   240
            Width           =   375
         End
         Begin GaugeLib.Gauge gauHp 
            Height          =   255
            Left            =   1440
            TabIndex        =   106
            TabStop         =   0   'False
            Top             =   240
            Width           =   1695
            _Version        =   65536
            _ExtentX        =   2990
            _ExtentY        =   450
            _StockProps     =   73
            ForeColor       =   49152
            BackColor       =   192
            Enabled         =   0   'False
            InnerTop        =   0
            InnerLeft       =   0
            InnerRight      =   0
            InnerBottom     =   0
            Value           =   100
            NeedleWidth     =   1
         End
         Begin VB.Label lblDeadIn 
            Alignment       =   1  'Right Justify
            Caption         =   "Dead In"
            Height          =   255
            Left            =   45
            TabIndex        =   96
            Top             =   2085
            Width           =   615
         End
         Begin VB.Label lblDownIn 
            Alignment       =   1  'Right Justify
            Caption         =   "Down In"
            Height          =   255
            Left            =   45
            TabIndex        =   95
            Top             =   1725
            Width           =   615
         End
         Begin VB.Label lblHpStatus 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   3240
            TabIndex        =   92
            Top             =   270
            Width           =   375
         End
         Begin VB.Label lblBP 
            Alignment       =   2  'Center
            Caption         =   "B/P"
            Height          =   255
            Left            =   2400
            TabIndex        =   91
            Top             =   1320
            Width           =   375
         End
         Begin VB.Label lblMP 
            Alignment       =   2  'Center
            Caption         =   "MP"
            Height          =   255
            Left            =   2400
            TabIndex        =   90
            Top             =   525
            Width           =   375
         End
         Begin VB.Label lblPenalty 
            Alignment       =   1  'Right Justify
            Caption         =   "@"
            Height          =   255
            Index           =   1
            Left            =   1245
            TabIndex        =   89
            Top             =   1725
            Width           =   495
         End
         Begin VB.Label lblLI 
            Alignment       =   1  'Right Justify
            Caption         =   "L.I."
            Height          =   255
            Left            =   1245
            TabIndex        =   88
            Top             =   1365
            Width           =   495
         End
         Begin VB.Label lblSNP 
            Alignment       =   1  'Right Justify
            Caption         =   "S/NP"
            Height          =   255
            Left            =   1245
            TabIndex        =   87
            Top             =   1005
            Width           =   495
         End
         Begin VB.Label lblStun 
            Alignment       =   1  'Right Justify
            Caption         =   "Stun"
            Height          =   255
            Left            =   1365
            TabIndex        =   86
            Top             =   645
            Width           =   375
         End
         Begin VB.Label lblCL 
            Alignment       =   2  'Center
            Caption         =   "Fatigue"
            Height          =   255
            Left            =   120
            TabIndex        =   85
            Top             =   2400
            Width           =   615
         End
         Begin VB.Label lblClot 
            Alignment       =   1  'Right Justify
            Caption         =   "Clot"
            Height          =   255
            Left            =   285
            TabIndex        =   84
            Top             =   1365
            Width           =   375
         End
         Begin VB.Label lblHeal 
            Alignment       =   1  'Right Justify
            Caption         =   "Heal"
            Height          =   255
            Left            =   285
            TabIndex        =   83
            Top             =   1005
            Width           =   375
         End
         Begin VB.Label lblBleed 
            Alignment       =   1  'Right Justify
            Caption         =   "Bleed"
            Height          =   255
            Left            =   210
            TabIndex        =   82
            Top             =   645
            Width           =   450
         End
         Begin VB.Label lblHp2 
            Alignment       =   2  'Center
            Caption         =   "of"
            Height          =   255
            Left            =   720
            TabIndex        =   81
            Top             =   285
            Width           =   255
         End
         Begin VB.Label lblHp 
            Caption         =   "Hp"
            Height          =   255
            Left            =   120
            TabIndex        =   80
            Top             =   285
            Width           =   255
         End
      End
      Begin VB.Frame fraStatus 
         Caption         =   "Status"
         Height          =   975
         Left            =   4080
         TabIndex        =   69
         Top             =   600
         Width           =   3735
         Begin VB.ComboBox cmbSaves 
            Enabled         =   0   'False
            Height          =   315
            ItemData        =   "IRIS.frx":0412
            Left            =   2520
            List            =   "IRIS.frx":0422
            Style           =   2  'Dropdown List
            TabIndex        =   30
            Top             =   240
            Width           =   1095
         End
         Begin VB.TextBox txtNextAction 
            Enabled         =   0   'False
            Height          =   285
            Left            =   720
            MaxLength       =   4
            TabIndex        =   29
            Top             =   240
            Width           =   1095
         End
         Begin VB.ComboBox cmbParryAt 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            ItemData        =   "IRIS.frx":0432
            Left            =   2115
            List            =   "IRIS.frx":0439
            Style           =   2  'Dropdown List
            TabIndex        =   32
            Top             =   600
            Width           =   1500
         End
         Begin VB.TextBox txtParry 
            Enabled         =   0   'False
            Height          =   285
            Left            =   720
            MaxLength       =   4
            TabIndex        =   31
            Top             =   600
            Width           =   1095
         End
         Begin VB.Label lblParryAt 
            Alignment       =   1  'Right Justify
            Caption         =   "at"
            Height          =   255
            Left            =   1860
            TabIndex        =   73
            Top             =   645
            Width           =   195
         End
         Begin VB.Label lblParry 
            Alignment       =   1  'Right Justify
            Caption         =   "Parry"
            Height          =   255
            Left            =   120
            TabIndex        =   72
            Top             =   645
            Width           =   495
         End
         Begin VB.Label lblSaves 
            Alignment       =   1  'Right Justify
            Caption         =   "Saves"
            Height          =   255
            Left            =   1920
            TabIndex        =   71
            Top             =   300
            Width           =   495
         End
         Begin VB.Label lblNext 
            Alignment       =   1  'Right Justify
            Caption         =   "Next"
            Height          =   255
            Left            =   120
            TabIndex        =   70
            Top             =   285
            Width           =   495
         End
      End
      Begin VB.Frame fraStats 
         Caption         =   "Vitals"
         Height          =   2085
         Left            =   4080
         TabIndex        =   75
         Top             =   1560
         Width           =   1935
         Begin VB.CommandButton cmdDelay 
            Caption         =   "Delay"
            Enabled         =   0   'False
            Height          =   285
            Left            =   80
            TabIndex        =   33
            Top             =   240
            Width           =   600
         End
         Begin VB.TextBox txtDB 
            Enabled         =   0   'False
            Height          =   285
            Left            =   720
            MaxLength       =   4
            TabIndex        =   38
            Top             =   1680
            Width           =   1095
         End
         Begin VB.ComboBox cmbAT 
            Enabled         =   0   'False
            Height          =   315
            ItemData        =   "IRIS.frx":0445
            Left            =   720
            List            =   "IRIS.frx":048F
            Style           =   2  'Dropdown List
            TabIndex        =   37
            Top             =   1320
            Width           =   1095
         End
         Begin VB.TextBox txtMN 
            Enabled         =   0   'False
            Height          =   285
            Left            =   720
            MaxLength       =   4
            TabIndex        =   36
            Top             =   960
            Width           =   1095
         End
         Begin VB.TextBox txtMV 
            Enabled         =   0   'False
            Height          =   285
            Left            =   720
            MaxLength       =   4
            TabIndex        =   35
            Top             =   600
            Width           =   1095
         End
         Begin VB.TextBox txtDelay 
            Enabled         =   0   'False
            Height          =   285
            Left            =   720
            MaxLength       =   4
            TabIndex        =   34
            Top             =   240
            Width           =   1095
         End
         Begin VB.Label txtMVSkill 
            Alignment       =   1  'Right Justify
            Caption         =   "Skill"
            Height          =   255
            Left            =   120
            TabIndex        =   108
            Top             =   1005
            Width           =   495
         End
         Begin VB.Label lblMV 
            Alignment       =   1  'Right Justify
            Caption         =   "Rate"
            Height          =   255
            Left            =   120
            TabIndex        =   78
            Top             =   645
            Width           =   495
         End
         Begin VB.Label lblDB 
            Alignment       =   1  'Right Justify
            Caption         =   "DB"
            Height          =   255
            Left            =   120
            TabIndex        =   77
            Top             =   1725
            Width           =   495
         End
         Begin VB.Label lblAt 
            Alignment       =   1  'Right Justify
            Caption         =   "AT"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   76
            Top             =   1365
            Width           =   495
         End
      End
      Begin VB.ListBox lisCombatants 
         Height          =   3570
         HelpContextID   =   300
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   2055
      End
      Begin VB.CommandButton cmdDamage 
         Caption         =   "Damage"
         Enabled         =   0   'False
         Height          =   375
         HelpContextID   =   340
         Left            =   120
         TabIndex        =   17
         Top             =   4200
         Width           =   1095
      End
      Begin VB.CommandButton cmdClone 
         Caption         =   "Clone"
         Enabled         =   0   'False
         Height          =   375
         HelpContextID   =   320
         Left            =   1200
         TabIndex        =   16
         Top             =   3840
         Width           =   975
      End
      Begin VB.CommandButton cmdNew 
         Caption         =   "New"
         Height          =   375
         HelpContextID   =   310
         Left            =   120
         TabIndex        =   15
         Top             =   3840
         Width           =   1095
      End
      Begin MSComDlg.CommonDialog cdlGeneric 
         Left            =   11160
         Top             =   600
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Label lblCombStatus 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Height          =   300
         Left            =   10080
         TabIndex        =   94
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label lblName 
         Alignment       =   1  'Right Justify
         Caption         =   "Name"
         Height          =   255
         Left            =   2280
         TabIndex        =   68
         Top             =   285
         Width           =   495
      End
   End
   Begin VB.Frame fraBattle 
      Caption         =   "Battle"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   0
      TabIndex        =   60
      Top             =   0
      Width           =   8265
      Begin VB.CommandButton cmdWSPlus 
         Caption         =   ">"
         Enabled         =   0   'False
         Height          =   255
         Left            =   2640
         TabIndex        =   3
         Top             =   450
         Width           =   255
      End
      Begin VB.CommandButton cmdWSMinus 
         Caption         =   "<"
         Enabled         =   0   'False
         Height          =   255
         Left            =   1920
         TabIndex        =   1
         Top             =   450
         Width           =   255
      End
      Begin VB.CommandButton cmdInterrupt 
         Caption         =   "Interrupt"
         Enabled         =   0   'False
         Height          =   375
         HelpContextID   =   190
         Left            =   6600
         TabIndex        =   11
         Top             =   720
         Width           =   855
      End
      Begin VB.CommandButton cmdNothing 
         Caption         =   "Nothing"
         Enabled         =   0   'False
         Height          =   375
         HelpContextID   =   180
         Left            =   5880
         TabIndex        =   10
         Top             =   720
         Width           =   735
      End
      Begin VB.CommandButton cmdPushback 
         Caption         =   "Pushback"
         Enabled         =   0   'False
         Height          =   375
         HelpContextID   =   170
         Left            =   4920
         TabIndex        =   9
         Top             =   720
         Width           =   975
      End
      Begin VB.CommandButton cmdMove 
         Caption         =   "Move"
         Enabled         =   0   'False
         Height          =   375
         HelpContextID   =   160
         Left            =   4080
         TabIndex        =   8
         Top             =   720
         Width           =   855
      End
      Begin VB.CommandButton cmdSaveAction 
         Caption         =   "Save"
         Enabled         =   0   'False
         Height          =   375
         HelpContextID   =   150
         Left            =   3240
         TabIndex        =   7
         Top             =   720
         Width           =   855
      End
      Begin VB.CommandButton cmdAttack 
         Caption         =   "Attack"
         Enabled         =   0   'False
         Height          =   375
         HelpContextID   =   140
         Left            =   2400
         TabIndex        =   6
         Top             =   720
         Width           =   855
      End
      Begin VB.TextBox txtWS 
         Enabled         =   0   'False
         Height          =   285
         HelpContextID   =   110
         Left            =   2160
         MaxLength       =   4
         TabIndex        =   2
         Top             =   435
         Width           =   495
      End
      Begin VB.CommandButton cmdRefresh 
         Caption         =   "�"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Wingdings"
            Size            =   14.25
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         HelpContextID   =   120
         Left            =   7800
         TabIndex        =   4
         Top             =   225
         Width           =   375
      End
      Begin VB.CommandButton cmdFetch 
         Caption         =   "�"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Wingdings"
            Size            =   12
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         HelpContextID   =   200
         Left            =   7800
         TabIndex        =   13
         Top             =   720
         Width           =   375
      End
      Begin VB.CheckBox chkAutoFetch 
         Height          =   255
         HelpContextID   =   200
         Left            =   7560
         TabIndex        =   12
         Top             =   720
         Value           =   1  'Checked
         Width           =   255
      End
      Begin VB.CommandButton cmdBeginEnd 
         Caption         =   "Begin"
         Enabled         =   0   'False
         Height          =   375
         HelpContextID   =   100
         Left            =   120
         TabIndex        =   0
         Top             =   720
         Width           =   735
      End
      Begin VB.CommandButton cmdTake 
         Caption         =   "Take"
         Enabled         =   0   'False
         Height          =   375
         HelpContextID   =   130
         Left            =   1560
         TabIndex        =   5
         Top             =   720
         Width           =   855
      End
      Begin VB.Image imgDice 
         Height          =   480
         Left            =   885
         MousePointer    =   4  'Icon
         Picture         =   "IRIS.frx":04DA
         Top             =   675
         Width           =   480
      End
      Begin VB.Label lblWS 
         Caption         =   "WS"
         Height          =   255
         Left            =   1560
         TabIndex        =   93
         Top             =   480
         Width           =   255
      End
      Begin VB.Label lblUpkeep 
         Alignment       =   2  'Center
         Caption         =   "Upkeep Phase"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   120
         TabIndex        =   66
         Top             =   480
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Label lblStatus 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   3540
         TabIndex        =   65
         Top             =   480
         Width           =   4155
      End
      Begin VB.Line linSeparator 
         BorderColor     =   &H00000000&
         X1              =   1440
         X2              =   1440
         Y1              =   240
         Y2              =   1080
      End
      Begin VB.Label lblStatusLabel 
         Caption         =   "Status:"
         Height          =   255
         Left            =   3000
         TabIndex        =   64
         Top             =   480
         Width           =   495
      End
      Begin VB.Label lblAction 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   1560
         TabIndex        =   63
         Top             =   240
         Width           =   6135
      End
      Begin VB.Label lblPhase 
         Alignment       =   2  'Center
         Caption         =   "none"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   840
         TabIndex        =   62
         Top             =   240
         Width           =   495
      End
      Begin VB.Label lblPhaseLabel 
         Caption         =   "Phase"
         Height          =   255
         Left            =   120
         TabIndex        =   61
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.Menu mnuBattle 
      Caption         =   "&Battle"
      HelpContextID   =   400
      Begin VB.Menu mnuNew 
         Caption         =   "&New"
         Enabled         =   0   'False
         HelpContextID   =   410
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         HelpContextID   =   420
         Shortcut        =   ^O
      End
      Begin VB.Menu mnuAppend 
         Caption         =   "&Append"
         Enabled         =   0   'False
         HelpContextID   =   420
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Save"
         Enabled         =   0   'False
         HelpContextID   =   430
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuSaveAs 
         Caption         =   "Sa&ve As"
         Enabled         =   0   'False
         HelpContextID   =   430
      End
      Begin VB.Menu mnuSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuOpenRecent 
         Caption         =   "&1 - (none)"
         Index           =   1
      End
      Begin VB.Menu mnuOpenRecent 
         Caption         =   "&2 - (none)"
         Index           =   2
      End
      Begin VB.Menu mnuOpenRecent 
         Caption         =   "&3 - (none)"
         Index           =   3
      End
      Begin VB.Menu mnuOpenRecent 
         Caption         =   "&4 - (none)"
         Index           =   4
      End
      Begin VB.Menu mnuOpenRecent 
         Caption         =   "&5 - (none)"
         Index           =   5
      End
      Begin VB.Menu mnuOpenRecent 
         Caption         =   "&6 - (none)"
         Index           =   6
      End
      Begin VB.Menu mnuOpenRecent 
         Caption         =   "&7 - (none)"
         Index           =   7
      End
      Begin VB.Menu mnuOpenRecent 
         Caption         =   "&8 - (none)"
         Index           =   8
      End
      Begin VB.Menu mnuOpenRecent 
         Caption         =   "&9 - (none)"
         Index           =   9
      End
      Begin VB.Menu mnuOpenRecent 
         Caption         =   "1&0 - (none)"
         Index           =   10
      End
      Begin VB.Menu mnuSep13 
         Caption         =   "-"
      End
      Begin VB.Menu mnuBeginEnd 
         Caption         =   "Begin &Battle"
         Enabled         =   0   'False
         HelpContextID   =   100
      End
      Begin VB.Menu mnuSep10 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "&Exit"
         HelpContextID   =   440
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      HelpContextID   =   500
      Begin VB.Menu mnuRefresh 
         Caption         =   "&Refresh"
         Enabled         =   0   'False
         HelpContextID   =   120
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuFetch 
         Caption         =   "&Fetch acting combatant"
         Enabled         =   0   'False
         HelpContextID   =   200
         Shortcut        =   ^F
      End
      Begin VB.Menu mnuSep3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEvents 
         Caption         =   "&Events"
         HelpContextID   =   510
         Shortcut        =   ^E
      End
      Begin VB.Menu mnuSep4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDamageReport 
         Caption         =   "&Damage Report"
         Enabled         =   0   'False
         HelpContextID   =   520
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuSep12 
         Caption         =   "-"
      End
      Begin VB.Menu mnuResetWindows 
         Caption         =   "Reset &Windows"
         Shortcut        =   ^W
      End
   End
   Begin VB.Menu mnuCombatant 
      Caption         =   "&Combatant"
      HelpContextID   =   600
      Begin VB.Menu mnuCombatantNew 
         Caption         =   "&New"
         HelpContextID   =   310
      End
      Begin VB.Menu mnuCombatantConvert 
         Caption         =   "Con&vert from"
         Begin VB.Menu mnuConvertCandT 
            Caption         =   "Creatures && &Treasures..."
            Shortcut        =   ^T
         End
         Begin VB.Menu mnuCombatantConvertHarn 
            Caption         =   "&H�rnMaster (Lens of Pvara)..."
            Shortcut        =   ^H
         End
         Begin VB.Menu mnuCombatantConvertCoC 
            Caption         =   "Call of &Cthulhu..."
         End
      End
      Begin VB.Menu mnuCombatantClone 
         Caption         =   "&Clone"
         Enabled         =   0   'False
         HelpContextID   =   320
      End
      Begin VB.Menu mnuCombatantDelete 
         Caption         =   "D&elete"
         Enabled         =   0   'False
         HelpContextID   =   330
      End
      Begin VB.Menu mnuSep9 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCombatantDamage 
         Caption         =   "&Damage"
         Enabled         =   0   'False
         HelpContextID   =   340
      End
      Begin VB.Menu mnuHeal 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHealEmergency 
         Caption         =   "Life-Threatening &Emergencies..."
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuHealLongTerm 
         Caption         =   "&Long-Term Wounds..."
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuMassHeal 
         Caption         =   "&Mass Heal..."
         Enabled         =   0   'False
         HelpContextID   =   610
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuSep29 
         Caption         =   "-"
      End
      Begin VB.Menu mnuMassRoll 
         Caption         =   "Mass &Roll"
         Enabled         =   0   'False
         Shortcut        =   ^P
      End
   End
   Begin VB.Menu mnuActions 
      Caption         =   "&Actions"
      HelpContextID   =   700
      Begin VB.Menu mnuTake 
         Caption         =   "&Take"
         Enabled         =   0   'False
         HelpContextID   =   130
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuAttack 
         Caption         =   "&Attack"
         Enabled         =   0   'False
         HelpContextID   =   140
         Shortcut        =   {F3}
      End
      Begin VB.Menu mnuSaveAction 
         Caption         =   "&Save"
         Enabled         =   0   'False
         HelpContextID   =   150
         Shortcut        =   {F4}
      End
      Begin VB.Menu mnuMove 
         Caption         =   "&Move"
         Enabled         =   0   'False
         HelpContextID   =   160
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuPushback 
         Caption         =   "&Pushback"
         Enabled         =   0   'False
         HelpContextID   =   170
         Shortcut        =   {F6}
      End
      Begin VB.Menu mnuNothing 
         Caption         =   "&Nothing"
         Enabled         =   0   'False
         HelpContextID   =   180
         Shortcut        =   {F7}
      End
      Begin VB.Menu mnuSep5 
         Caption         =   "-"
      End
      Begin VB.Menu mnuInterrupt 
         Caption         =   "&Interrupt"
         Enabled         =   0   'False
         HelpContextID   =   190
         Shortcut        =   {F8}
      End
      Begin VB.Menu mnuSep11 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSkill 
         Caption         =   "S&kill/Maneuver"
         Enabled         =   0   'False
         Shortcut        =   {F9}
      End
   End
   Begin VB.Menu mnuPreferences 
      Caption         =   "&Preferences"
      HelpContextID   =   800
      Begin VB.Menu mnuPrompt 
         Caption         =   "&Prompt for log actions"
         Checked         =   -1  'True
         HelpContextID   =   530
      End
      Begin VB.Menu mnuAutosave 
         Caption         =   "Automatically &save battles"
         Checked         =   -1  'True
         HelpContextID   =   430
      End
      Begin VB.Menu mnuAutofetch 
         Caption         =   "Automatically &fetch combatants"
         Checked         =   -1  'True
         HelpContextID   =   200
      End
      Begin VB.Menu mnuShowPlayerDisplay 
         Caption         =   "Show player &display"
         Checked         =   -1  'True
         HelpContextID   =   200
      End
   End
End
Attribute VB_Name = "frmIRIS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private loading_combatant As Boolean
Private current_listindex As Integer
Public Phase, Next_AutoSave, Last_Bleeding_Phase, Acting_Combatant, Interrupter As Integer
Public PrismDicePath As String
Const FILENAME_HISTORY = 10
Private LastFilename(FILENAME_HISTORY), Last_AutoSave_Filename, Log As String
Const conAutoSaveRate = 25

Private Sub Form_Load()
  Dim i As Integer
  On Error Resume Next
  ' Set up the form's position on the screen
  CalculateScreenBounds
  Me.Height = 7875
  PositionForm Me, GetSetting(conAppName, "Startup", "Left", (Screen.Width - Width) / 2), GetSetting(conAppName, "Startup", "Top", (Screen.Height - Height) / 2)
  ' Load Preferences to the menu
  mnuPrompt.Checked = GetSetting(conAppName, "Startup", "Prompt", True)
  mnuAutosave.Checked = GetSetting(conAppName, "Startup", "Autosave", True)
  mnuAutofetch.Checked = GetSetting(conAppName, "Startup", "Autofetch", True)
  chkAutoFetch.Value = mnuAutofetch.Checked
  mnuShowPlayerDisplay.Checked = GetSetting(conAppName, "Startup", "ShowPlayerDisplay", True)
  If mnuShowPlayerDisplay.Checked Then frmPlayerDisplay.Show Else Unload frmPlayerDisplay
  ' Convert an old "LastFilename" to a new "LastFilename1"
  If GetSetting(conAppName, "Startup", "LastFilename1", "") = "" Then
    Call SaveSetting(conAppName, "Startup", "LastFilename1", GetSetting(conAppName, "Startup", "LastFilename", ""))
    Call DeleteSetting(conAppName, "Startup", "LastFilename")
  End If
  ' Load all the last filenames
  For i = 1 To FILENAME_HISTORY
    SetLastFilename i, GetSetting(conAppName, "Startup", "LastFilename" + CStr(i), "")
    Next
  ' Find other settings
  PrismDicePath = GetSetting("Prism Dice", "Startup", "Path", "C:\Program Files\Prism Dice\Prism Dice.exe")
  Last_AutoSave_Filename = GetSetting(conAppName, "Startup", "AutoSaveFilename", "")
  ' Set up the window
  Caption = conAppName & " v" & conAppVersion
  Me.Show
  cmdNew.SetFocus
  ' Load some starting variables
  NumCombatants = 0
  NumEvents = 0
  Acting_Combatant = 0
  Phase = -1
  Interrupter = 0
  Last_Bleeding_Phase = 0
  Next_AutoSave = conAutoSaveRate
  loading_combatant = False
  Log = ""
  rtbLog_Update
  attacker_bonuses = ""
  Randomize Timer
  clsCriticalChart.Constructor
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Dim i As Integer
  If NumCombatants + NumEvents > 0 And Last_AutoSave_Filename <> "" Then
      Call mnuSave_DoIt(Last_AutoSave_Filename)
    End If
  Unload frmPlayerDisplay
  Unload frmBonusPenalty
  Unload frmDamage
  Unload frmMassHeal
  Unload frmDamageReport
  Unload frmSurprise
  Unload frmMove
  Unload frmInterrupt
  Unload frmAttack
  Unload frmSkill
  Unload frmEvents
  Unload frmMassRoll
  Unload frmConvertHarn
  Unload frmConvertCoC
  Unload frmConvertCandT
  Unload frmHealEmergency
  Unload frmHealLongTerm
  Call SaveSetting(conAppName, "Startup", "Left", Left)
  Call SaveSetting(conAppName, "Startup", "Top", Top)
  Call SaveSetting(conAppName, "Startup", "Prompt", mnuPrompt.Checked)
  Call SaveSetting(conAppName, "Startup", "Autosave", mnuAutosave.Checked)
  Call SaveSetting(conAppName, "Startup", "Autofetch", mnuAutofetch.Checked)
  Call SaveSetting(conAppName, "Startup", "ShowPlayerDisplay", mnuShowPlayerDisplay.Checked)
  For i = 1 To FILENAME_HISTORY
    Call SaveSetting(conAppName, "Startup", "LastFilename" + CStr(i), LastFilename(i))
    Next i
  Call SaveSetting(conAppName, "Startup", "AutoSaveFilename", Last_AutoSave_Filename)
  Call SaveSetting(conAppName, "Startup", "LastVersion", conAppVersion)
  clsCriticalChart.SaveCritChartList
End Sub

Public Sub imgDice_Click()
  On Error GoTo launch
  AppActivate "Prism Dice"
  On Error GoTo 0
  Exit Sub
launch:
  On Error GoTo findit
  If Shell(PrismDicePath, 1) = 0 Then
      MsgBox "Can't find Prism Dice.", vbExclamation
      Exit Sub
    End If
  On Error GoTo 0
  Exit Sub
findit:
  MsgBox "Can't find Prism Dice, please run it manually once first.", vbExclamation
  On Error GoTo 0
  ' frmDice.cmdCancel.Tag = "l" ' flag telling it to load in Form_Activate
  ' frmDice.Show 1
End Sub

Private Sub mnuExit_Click()
  Unload Me
End Sub

Private Sub mnuContents_Click()
  fraCombatants.ShowWhatsThis
End Sub

Private Sub fraCombatants_EnableDisable()
  If NumCombatants = 0 Then
      lisCombatants.Enabled = False
      txtName.Enabled = False
      chkPC.Enabled = False
      txtS.Enabled = False
      txtE.Enabled = False
      txtD.Enabled = False
      txtQ.Enabled = False
      txtI.Enabled = False
      txtP.Enabled = False
      txtW.Enabled = False
      txtR.Enabled = False
      txtSkills.Enabled = False
      txtNextAction.Enabled = False
      cmbSaves.Enabled = False
      cmdDelay.Enabled = False
      txtParry.Enabled = False
      cmbParryAt.Enabled = False
      txtDelay.Enabled = False
      txtMV.Enabled = False
      txtMN.Enabled = False
      cmbAT.Enabled = False
      txtDB.Enabled = False
      txtAttacks.Enabled = False
      txtCurHp.Enabled = False
      txtOptHp.Enabled = False
      txtBleed.Enabled = False
      txtHeal.Enabled = False
      txtClot.Enabled = False
      txtRoundsTillDown.Enabled = False
      txtRoundsTillDead.Enabled = False
      cmbPosition.Enabled = False
      txtHealth.Enabled = False
      txtCapLoss.Enabled = False
      txtStuns.Enabled = False
      txtStunNoParries.Enabled = False
      txtLostInits.Enabled = False
      txtPenalty.Enabled = False
      lisMustParry.Enabled = False
      cmdMPPlus.Enabled = False
      cmdMPMinus.Enabled = False
      lisBonusPenalty.Enabled = False
      cmdBPPlus.Enabled = False
      cmdBPMinus.Enabled = False
      txtNotes.Enabled = False
      cmdClone.Enabled = False
      mnuCombatantClone.Enabled = False
      cmdDelete.Enabled = False
      mnuCombatantDelete.Enabled = False
      cmdDamage.Enabled = False
      mnuCombatantDamage.Enabled = False
      mnuHealEmergency.Enabled = False
      mnuHealLongTerm.Enabled = False
      mnuMassHeal.Enabled = False
      mnuDamageReport.Enabled = False
      mnuMassRoll.Enabled = False
    Else
      lisCombatants.Enabled = True
      txtName.Enabled = True
      chkPC.Enabled = True
      txtS.Enabled = True
      txtE.Enabled = True
      txtD.Enabled = True
      txtQ.Enabled = True
      txtI.Enabled = True
      txtP.Enabled = True
      txtW.Enabled = True
      txtR.Enabled = True
      txtSkills.Enabled = True
      txtNextAction.Enabled = True
      cmbSaves.Enabled = True
      txtParry.Enabled = True
      cmbParryAt.Enabled = True
      cmdDelay.Enabled = True
      txtDelay.Enabled = True
      txtMV.Enabled = True
      txtMN.Enabled = True
      cmbAT.Enabled = True
      txtDB.Enabled = True
      txtAttacks.Enabled = True
      txtCurHp.Enabled = True
      txtOptHp.Enabled = True
      txtBleed.Enabled = True
      txtHeal.Enabled = True
      txtClot.Enabled = True
      txtRoundsTillDown.Enabled = True
      txtRoundsTillDead.Enabled = True
      cmbPosition.Enabled = True
      txtHealth.Enabled = True
      txtCapLoss.Enabled = True
      txtStuns.Enabled = True
      txtStunNoParries.Enabled = True
      txtLostInits.Enabled = True
      txtPenalty.Enabled = True
      lisMustParry.Enabled = True
      cmdMPPlus.Enabled = True
      If lisMustParry.ListCount > 0 Then cmdMPMinus.Enabled = True Else cmdMPMinus.Enabled = False
      lisBonusPenalty.Enabled = True
      cmdBPPlus.Enabled = True
      If lisBonusPenalty.ListCount > 0 Then cmdBPMinus.Enabled = True Else cmdBPMinus.Enabled = False
      txtNotes.Enabled = True
      cmdClone.Enabled = True
      mnuCombatantClone.Enabled = True
      cmdDelete.Enabled = True
      mnuCombatantDelete.Enabled = True
      cmdDamage.Enabled = True
      mnuCombatantDamage.Enabled = True
      mnuHealEmergency.Enabled = True
      mnuHealLongTerm.Enabled = True
      mnuMassHeal.Enabled = True
      mnuDamageReport.Enabled = True
      mnuMassRoll.Enabled = True
    End If
  If NumCombatants + NumEvents = 0 Then
      cmdBeginEnd.Enabled = False
      mnuBeginEnd.Enabled = False
      mnuSave.Enabled = False
      mnuSaveAs.Enabled = False
      mnuNew.Enabled = False
    Else
      cmdBeginEnd.Enabled = True
      mnuBeginEnd.Enabled = True
      mnuSave.Enabled = True
      mnuSaveAs.Enabled = True
      mnuNew.Enabled = True
    End If
  If NumCombatants = conMaxCombatants Then
      cmdNew.Enabled = False
      mnuCombatantNew.Enabled = False
      mnuCombatantConvert.Enabled = False
      mnuAppend.Enabled = False
    Else
      cmdNew.Enabled = True
      mnuCombatantNew.Enabled = True
      mnuCombatantConvert.Enabled = True
      mnuAppend.Enabled = True
    End If
End Sub

Private Sub lisCombatants_Click()
  ' loads the form variables with the values from the appropriate
  ' entry in the array
  loading_combatant = True
  With Combatants(lisCombatants.ListIndex + 1)
    txtName = .Name
    chkPC.Value = IIf(.PC, 1, 0)
    txtS = .StatS
    txtE = .StatE
    txtD = .StatD
    txtQ = .StatQ
    txtI = .StatI
    txtP = .StatP
    txtW = .StatW
    txtR = .StatR
    txtSkills = .Skills
    txtNextAction = .NextAction
    cmbSaves = .Saves
    txtParry = .Parry
    cmbParryAt.ListIndex = .ParryAt
    txtDelay = .Delay
    txtMV = .MV
    txtMN = .MN
    cmbAT.ListIndex = .AT - 1
    txtDB = .DB
    txtAttacks = .Attacks
    txtCurHp = .CurHp
    txtOptHp = .OptHp
    Call gauHp_Update
    txtBleed = .Bleed
    txtHeal = .Heal
    txtClot = .Clot
    txtRoundsTillDown = .RoundsTillDown
    txtRoundsTillDead = .RoundsTillDead
    cmbPosition.ListIndex = .Position - 1
    txtHealth = .Health
    txtCapLoss = .CapLoss
    txtStuns = .Stuns
    txtStunNoParries = .StunNoParries
    txtLostInits = .LostInits
    txtPenalty = IIf(.Penalty > 0, "+", "") & .Penalty
    lisMustParry_Build (.MustParry)
    lisBonusPenalty_Build (.BonusPenalty)
    txtNotes = .Notes
    End With
  loading_combatant = False
  current_listindex = lisCombatants.ListIndex
  Call fraCombatants_EnableDisable
End Sub

Private Sub cmdNew_Click()
  Dim newname As String
  ' pick an Unnamed name
  newname = "Unnamed Combatant"
  While FindCombatant(newname) <> 0
    newname = NextNewName(newname)
    Wend
  ' add one into the array
  NumCombatants = NumCombatants + 1
  ' load it up with default values including the name chosen
  Combatants(NumCombatants).Name = newname
  Combatants(NumCombatants).ParryAt = 0
  Combatants(NumCombatants).Delay = 30
  Combatants(NumCombatants).MV = 50
  Combatants(NumCombatants).AT = 1
  Combatants(NumCombatants).Position = 1
  ' add it to the lisCombatants and cmbParryAt lists
  lisCombatants.AddItem newname
  lisCombatants.ListIndex = lisCombatants.ListCount - 1
  lisCombatants_UpdateLabel NumCombatants
  cmbParryAt.AddItem newname
  ' call the fraCombatants_EnableDisable function
  Call fraCombatants_EnableDisable
  Call frmPlayerDisplay.Form_Refresh
  ' position the cursor on the Name
  txtName.SetFocus
End Sub

Private Sub mnuCombatantNew_Click()
  Call cmdNew_Click
End Sub

Private Sub mnuCombatantConvertHarn_Click()
  Dim newname As String, perception As Integer, melee As Integer, missile As Integer, shieldmultiplier As Double, i As Integer
  ' pick an Unnamed name as a default
  newname = "Unnamed Combatant"
  While FindCombatant(newname) <> 0
    newname = NextNewName(newname)
    Wend
  frmConvertHarn.txtName = newname
  ' launch the form
  frmConvertHarn.Show 1
  If frmConvertHarn.cmdCancel.Tag = "y" Then Exit Sub
  ' correct the name if it is a duplicate
  newname = frmConvertHarn.txtName
  While FindCombatant(newname) <> 0
    newname = NextNewName(newname)
    Wend
  ' combine perception stats
  perception = Int((CInt(frmConvertHarn.txtEYE) + CInt(frmConvertHarn.txtHRG) + CInt(frmConvertHarn.txtSML)) / 3 + 0.5)
  ' add one into the array
  NumCombatants = NumCombatants + 1
  With Combatants(NumCombatants)
    ' convert the character traits, easy ones first
    .Name = newname
    .Notes = frmConvertHarn.txtNotes
    ' those that depend on humanoid/creature next
    If frmConvertHarn.optHumanoid Then
        ' stats
        .StatS = Convert318Stat(Val(frmConvertHarn.txtSTR))
        .StatE = Convert318Stat(Val(frmConvertHarn.txtSTA))
        .StatD = Convert318Stat(Val(frmConvertHarn.txtDEX))
        .StatQ = Convert318Stat(Val(frmConvertHarn.txtAGL))
        .StatI = Convert318Stat(Val(frmConvertHarn.txtINT))
        .StatP = Convert318Stat(perception)
        .StatW = Convert318Stat(Val(frmConvertHarn.txtAUR))
        .StatR = Convert318Stat(Val(frmConvertHarn.txtWIL))
        ' vital stats
        .Delay = 30 - 2 * .StatQ
        .MV = 32 + .StatQ * 3 + Int(Val(frmConvertHarn.txtHeight) / 4 + 0.5)
        .MN = .StatQ
        .DB = .StatQ
      Else
        ' stats
        .StatS = Int(Val(frmConvertHarn.txtSTR) * 2 / 3 + 0.5) - 7
        .StatE = Int(Val(frmConvertHarn.txtSTA) * 2 / 3 + 0.5) - 7
        .StatD = Int(Val(frmConvertHarn.txtDEX) * 2 / 3 + 0.5) - 7
        .StatQ = Int(Val(frmConvertHarn.txtAGL) * 2 / 3 + 0.5) - 7
        .StatI = Int(Val(frmConvertHarn.txtINT) * 2 / 3 + 0.5) - 7
        .StatP = Int(perception * 2 / 3 + 0.5) - 7
        .StatW = Int(Val(frmConvertHarn.txtAUR) * 2 / 3 + 0.5) - 7
        .StatR = Int(Val(frmConvertHarn.txtWIL) * 2 / 3 + 0.5) - 7
        ' vital stats
        .Delay = 80 - Val(frmConvertHarn.txtInitiative)
        If .Delay < 2 Then .Delay = 2
        .MV = 5 * Val(frmConvertHarn.txtMove)
        .MN = Int(Val(frmConvertHarn.txtDodge) / 5)
        .DB = Int(Val(frmConvertHarn.txtDodge) / 6 + 0.5)
      End If
    If Val(frmConvertHarn.txtShield) > 0 And frmConvertHarn.cmbShieldType.ListIndex > 0 Then
        ' figure out shield inherent DB for missile
        Select Case frmConvertHarn.cmbShieldType.ListIndex
          Case 1: melee = 3: missile = 2  ' buckler
          Case 2: melee = 3: missile = 3  ' knight shield
          Case 3: melee = 4: missile = 4  ' roundshield
          Case 4: melee = 5: missile = 5  ' kite shield
          Case 5: melee = 6: missile = 8  ' tower shield
          End Select
        shieldmultiplier = Val(frmConvertHarn.txtShield) / 100 + 0.5
        .Notes = "Shield: melee " & Int(shieldmultiplier * melee + 0.5) & ", missile " & Int(shieldmultiplier * missile + 0.5) & vbCrLf & .Notes
      End If
    ' next, the bits that are the same for everyone
    ' attacks
    .Attacks = ""
    For i = 0 To 4
      If frmConvertHarn.txtAttack(i) <> "" And Val(frmConvertHarn.txtAttackOB(i)) <> 0 Then
        .Attacks = .Attacks & Int(Val(frmConvertHarn.txtAttackOB(i)) / 5 + 0.5) & frmConvertHarn.txtAttack(i) & " "
        End If
      Next
    .Attacks = Trim(.Attacks)
    ' hit points
    .OptHp = CInt(frmConvertHarn.txtEndurance) * 4
    .CurHp = .OptHp
    ' fill in other defaults
    .ParryAt = 0
    .AT = 1
    .Position = 1
    End With
  ' add it to the lisCombatants and cmbParryAt lists
  lisCombatants.AddItem newname
  lisCombatants.ListIndex = lisCombatants.ListCount - 1
  lisCombatants_UpdateLabel NumCombatants
  cmbParryAt.AddItem newname
  ' call the fraCombatants_EnableDisable function
  Call fraCombatants_EnableDisable
  Call frmPlayerDisplay.Form_Refresh
  ' position the cursor on the Skills
  txtSkills.SetFocus
End Sub

Private Sub mnuCombatantConvertCoC_Click()
  Dim newname As String, perception As Integer, melee As Integer, missile As Integer, shieldmultiplier As Double, i As Integer
  ' pick an Unnamed name as a default
  newname = "Unnamed Combatant"
  While FindCombatant(newname) <> 0
    newname = NextNewName(newname)
    Wend
  frmConvertCoC.txtName = newname
  ' launch the form
  frmConvertCoC.Show 1
  If frmConvertCoC.cmdCancel.Tag = "y" Then Exit Sub
  ' correct the name if it is a duplicate
  newname = frmConvertCoC.txtName
  While FindCombatant(newname) <> 0
    newname = NextNewName(newname)
    Wend
  ' add one into the array
  NumCombatants = NumCombatants + 1
  With Combatants(NumCombatants)
    ' convert the character traits, easy ones first
    .Name = newname
    .Notes = frmConvertCoC.txtNotes
    ' stats
    .StatS = Convert318Stat(Val(frmConvertCoC.txtSTR))
    .StatE = Convert318Stat(Val(frmConvertCoC.txtCON))
    .StatD = Convert318Stat(Val(frmConvertCoC.txtDEX))
    .StatQ = Convert318Stat(Val(frmConvertCoC.txtDEX))
    .StatI = Convert318Stat(Val(frmConvertCoC.txtINT))
    .StatP = Convertd100Stat(Val(frmConvertCoC.txtSpotHidden))
    .StatW = Convert318Stat(Val(frmConvertCoC.txtPOW))
    .StatR = Int((Val(frmConvertCoC.txtSanity) - 50) / 8 + 0.5)
    ' vital stats
    .Delay = 30 - 2 * .StatQ
    .MV = 32 + .StatQ * 3 + Int(Val(frmConvertCoC.txtHeight) / 4 + 0.5)
    .MN = .StatQ
    .DB = Convertd100Stat(Val(frmConvertCoC.txtDodge)) + 5
    ' attacks
    .Attacks = ""
    For i = 0 To 4
      If frmConvertCoC.txtAttack(i) <> "" And Val(frmConvertCoC.txtAttackOB(i)) <> 0 Then
        .Attacks = .Attacks & Int(Val(frmConvertCoC.txtAttackOB(i)) / 5 + 0.5) & frmConvertCoC.txtAttack(i) & " "
        End If
      Next
    .Attacks = Trim(.Attacks)
    ' hit points
    .OptHp = CInt(frmConvertCoC.txtHP) * (Val(frmConvertCoC.cmbHPMultiplier.ListIndex) + 2) + 4
    .CurHp = .OptHp
    ' fill in other defaults
    .ParryAt = 0
    .AT = 1
    .Position = 1
    End With
  ' add it to the lisCombatants and cmbParryAt lists
  lisCombatants.AddItem newname
  lisCombatants.ListIndex = lisCombatants.ListCount - 1
  lisCombatants_UpdateLabel NumCombatants
  cmbParryAt.AddItem newname
  ' call the fraCombatants_EnableDisable function
  Call fraCombatants_EnableDisable
  Call frmPlayerDisplay.Form_Refresh
  ' position the cursor on the Skills
  txtSkills.SetFocus
End Sub

Private Sub mnuConvertCandT_Click()
  Dim newname As String, perception As Integer, melee As Integer, missile As Integer, shieldmultiplier As Double, i As Integer
  ' pick an Unnamed name as a default
  newname = "Unnamed Combatant"
  While FindCombatant(newname) <> 0
    newname = NextNewName(newname)
    Wend
  frmConvertCandT.txtName = newname
  ' launch the form
  frmConvertCandT.Show 1
  If frmConvertCandT.cmdCancel.Tag = "y" Then Exit Sub
  ' correct the name if it is a duplicate
  newname = frmConvertCandT.txtName
  While FindCombatant(newname) <> 0
    newname = NextNewName(newname)
    Wend
  ' add one into the array
  NumCombatants = NumCombatants + 1
  With Combatants(NumCombatants)
    ' convert the character traits, easy ones first
    .Name = newname
    .Notes = frmConvertCandT.txtNotes
    ' stats
    .StatS = Val(frmConvertCandT.txtS)
    .StatE = Val(frmConvertCandT.txtE)
    .StatD = Val(frmConvertCandT.txtD)
    .StatQ = Val(frmConvertCandT.txtQ)
    .StatI = Val(frmConvertCandT.txtI)
    .StatP = Val(frmConvertCandT.txtP)
    .StatW = Val(frmConvertCandT.txtW)
    .StatR = Val(frmConvertCandT.txtR)
    ' vital stats
    .Delay = frmConvertCandT.cmbSpeedAQ.ItemData(frmConvertCandT.cmbSpeedAQ.ListIndex)
    .MV = Int(Val(frmConvertCandT.txtBaseRate) * 0.6 + 0.5)
    .MN = Int(Val(frmConvertCandT.txtMNBonus) / 5 + 0.5)
    .AT = frmConvertCandT.cmbAT.ListIndex + 1
    .DB = Int(Val(frmConvertCandT.txtDB) / 5 + 0.5)
    ' attacks
    .Attacks = ""
    For i = 0 To 4
      If frmConvertCandT.txtAttack(i) <> "" And Val(frmConvertCandT.txtAttackOB(i)) <> 0 Then
          .Attacks = .Attacks & Int(Val(frmConvertCandT.txtAttackOB(i)) / 5 + 0.5) & frmConvertCandT.txtAttack(i) & " "
        ElseIf frmConvertCandT.txtAttack(i) <> "" Then
          .Attacks = .Attacks & frmConvertCandT.txtAttack(i) & " "
        End If
      Next
    .Attacks = Trim(.Attacks)
    ' hit points
    .OptHp = Val(frmConvertCandT.txtHits)
    .CurHp = .OptHp
    ' fill in other defaults
    .ParryAt = 0
    .Position = 1
    End With
  ' add it to the lisCombatants and cmbParryAt lists
  lisCombatants.AddItem newname
  lisCombatants.ListIndex = lisCombatants.ListCount - 1
  lisCombatants_UpdateLabel NumCombatants
  cmbParryAt.AddItem newname
  ' call the fraCombatants_EnableDisable function
  Call fraCombatants_EnableDisable
  Call frmPlayerDisplay.Form_Refresh
  ' position the cursor on the Skills
  txtSkills.SetFocus
End Sub

Private Sub cmdClone_Click()
  Dim newname As String
  ' pick an Unnamed name
  newname = Mid(lisCombatants.List(lisCombatants.ListIndex), 5) ' strip off phase
  While FindCombatant(newname) <> 0
    newname = NextNewName(newname)
    Wend
  ' add one into the array
  NumCombatants = NumCombatants + 1
  ' copy the combatant into that position
  Call CopyCombatant(NumCombatants, lisCombatants.ListIndex + 1)
  ' load it the new name
  Combatants(NumCombatants).Name = newname
  ' add it to the lisCombatants and cmbParryAt lists
  lisCombatants.AddItem newname
  lisCombatants.ListIndex = lisCombatants.ListCount - 1
  lisCombatants_UpdateLabel NumCombatants
  cmbParryAt.AddItem newname
  ' call the fraCombatants_EnableDisable function
  Call fraCombatants_EnableDisable
  Call frmPlayerDisplay.Form_Refresh
  ' position the cursor on the Name
  txtName.SetFocus
End Sub

Private Sub mnuCombatantClone_Click()
  Call cmdClone_Click
End Sub

Private Sub mnuHealEmergency_Click()
  frmHealEmergency.cmdCancel.Tag = "l" ' flag telling it to load in Form_Activate
  frmHealEmergency.Show 1
  lisCombatants_Click
End Sub

Private Sub mnuHealLongTerm_Click()
  frmHealLongTerm.cmdOK.Tag = "l" ' flag telling it to load in Form_Activate
  frmHealLongTerm.Show 1
  lisCombatants_Click
End Sub

Private Sub txtName_GotFocus()
  Call SelectField(txtName)
End Sub

Private Sub txtName_KeyPress(KeyAscii As Integer)
  If loading_combatant Then Exit Sub
  KeyAscii = NoQuotes(KeyAscii)
End Sub

Private Sub txtName_Change()
  If loading_combatant Then Exit Sub
  ' update the array variable
  Combatants(lisCombatants.ListIndex + 1).Name = txtName
  ' update the entry in lisCombatants and cmbParryAt
  'lisCombatants.List(lisCombatants.ListIndex) = txtName
  lisCombatants_UpdateLabel lisCombatants.ListIndex + 1
  cmbParryAt.List(lisCombatants.ListIndex + 1) = txtName
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub txtName_LostFocus()
  Dim i As Integer
  ' check to see if this causes a duplication
  If NumCombatants = 1 Then Exit Sub
  For i = 1 To NumCombatants
    If i <> current_listindex + 1 And Combatants(i).Name = txtName Then
        MsgBox "Duplicate combatant name " & txtName & " not allowed.", vbExclamation
        txtName.SetFocus
        lisCombatants.ListIndex = current_listindex
        Exit Sub
      End If
    Next i
  ' Guess it didn't.  All right, you can go now.  :)
End Sub

Private Sub txtNextAction_KeyPress(KeyAscii As Integer)
  If loading_combatant Then Exit Sub
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtNextAction_GotFocus()
  Call SelectField(txtNextAction)
End Sub

Private Sub txtNextAction_Change()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).NextAction = Val(txtNextAction)
  lisCombatants_UpdateLabel lisCombatants.ListIndex + 1
End Sub

Private Sub chkPC_Click()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).PC = IIf(chkPC.Value = 1, True, False)
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub txtS_GotFocus()
  Call SelectField(txtS)
End Sub

Private Sub txtS_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtS_Change()
  If loading_combatant Then Exit Sub
  If txtS = "-" Then
      Combatants(lisCombatants.ListIndex + 1).StatS = 0
    Else
      Combatants(lisCombatants.ListIndex + 1).StatS = Val(txtS)
    End If
End Sub

Private Sub txtE_GotFocus()
  Call SelectField(txtE)
End Sub

Private Sub txtE_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtE_Change()
  If loading_combatant Then Exit Sub
  If txtE = "-" Then
      Combatants(lisCombatants.ListIndex + 1).StatE = 0
    Else
      Combatants(lisCombatants.ListIndex + 1).StatE = Val(txtE)
    End If
End Sub

Private Sub txtD_GotFocus()
  Call SelectField(txtD)
End Sub

Private Sub txtD_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtD_Change()
  If loading_combatant Then Exit Sub
  If txtD = "-" Then
      Combatants(lisCombatants.ListIndex + 1).StatD = 0
    Else
      Combatants(lisCombatants.ListIndex + 1).StatD = Val(txtD)
    End If
End Sub

Private Sub txtQ_GotFocus()
  Call SelectField(txtQ)
End Sub

Private Sub txtQ_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtQ_Change()
  If loading_combatant Then Exit Sub
  If txtQ = "-" Then
      Combatants(lisCombatants.ListIndex + 1).StatQ = 0
    Else
      Combatants(lisCombatants.ListIndex + 1).StatQ = Val(txtQ)
    End If
End Sub

Private Sub txtI_GotFocus()
  Call SelectField(txtI)
End Sub

Private Sub txtI_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtI_Change()
  If loading_combatant Then Exit Sub
  If txtI = "-" Then
      Combatants(lisCombatants.ListIndex + 1).StatI = 0
    Else
      Combatants(lisCombatants.ListIndex + 1).StatI = Val(txtI)
    End If
End Sub

Private Sub txtP_GotFocus()
  Call SelectField(txtP)
End Sub

Private Sub txtP_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtP_Change()
  If loading_combatant Then Exit Sub
  If txtP = "-" Then
      Combatants(lisCombatants.ListIndex + 1).StatP = 0
    Else
      Combatants(lisCombatants.ListIndex + 1).StatP = Val(txtP)
    End If
End Sub

Private Sub txtW_GotFocus()
  Call SelectField(txtW)
End Sub

Private Sub txtW_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtW_Change()
  If loading_combatant Then Exit Sub
  If txtW = "-" Then
      Combatants(lisCombatants.ListIndex + 1).StatW = 0
    Else
      Combatants(lisCombatants.ListIndex + 1).StatW = Val(txtW)
    End If
End Sub

Private Sub txtR_GotFocus()
  Call SelectField(txtR)
End Sub

Private Sub txtR_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtR_Change()
  If loading_combatant Then Exit Sub
  If txtR = "-" Then
      Combatants(lisCombatants.ListIndex + 1).StatR = 0
    Else
      Combatants(lisCombatants.ListIndex + 1).StatR = Val(txtR)
    End If
End Sub

Private Sub txtSkills_KeyPress(KeyAscii As Integer)
  If loading_combatant Then Exit Sub
  KeyAscii = NoQuotes(KeyAscii)
End Sub

'Private Sub txtSkills_GotFocus()
'  Call SelectField(txtSkills)
'End Sub

Private Sub txtSkills_Change()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).Skills = txtSkills
End Sub

Private Sub cmbSaves_Click()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).Saves = cmbSaves.ListIndex
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub txtParry_GotFocus()
  Call SelectField(txtParry)
End Sub

Private Sub txtParry_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtParry_Change()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).Parry = Val(txtParry)
End Sub

Private Sub cmbParryAt_Click()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).ParryAt = cmbParryAt.ListIndex
End Sub

Private Sub cmdDelay_Click()
  txtDelay = 30 - 2 * Val(txtQ)
End Sub

Private Sub txtDelay_GotFocus()
  Call SelectField(txtDelay)
End Sub

Private Sub txtDelay_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtDelay_Change()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).Delay = Val(txtDelay)
End Sub

Private Sub txtMV_GotFocus()
  Call SelectField(txtMV)
End Sub

Private Sub txtMV_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtMV_Change()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).MV = Val(txtMV)
End Sub

Private Sub txtMN_GotFocus()
  Call SelectField(txtMN)
End Sub

Private Sub txtMN_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtMN_Change()
  If loading_combatant Then Exit Sub
  If txtMN = "-" Then
      Combatants(lisCombatants.ListIndex + 1).MN = 0
    Else
      Combatants(lisCombatants.ListIndex + 1).MN = Val(txtMN)
    End If
End Sub

Private Sub cmbAT_Click()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).AT = cmbAT.ListIndex + 1
End Sub

Private Sub txtDB_GotFocus()
  Call SelectField(txtDB)
End Sub

Private Sub txtDB_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtDB_Change()
  If loading_combatant Then Exit Sub
  If txtDB = "-" Then
      Combatants(lisCombatants.ListIndex + 1).DB = 0
    Else
      Combatants(lisCombatants.ListIndex + 1).DB = Val(txtDB)
    End If
End Sub

Private Sub txtAttacks_GotFocus()
  Call SelectField(txtAttacks)
End Sub

Private Sub txtAttacks_KeyPress(KeyAscii As Integer)
  If loading_combatant Then Exit Sub
  KeyAscii = NoQuotes(KeyAscii)
End Sub

Private Sub txtAttacks_Change()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).Attacks = txtAttacks
End Sub

Private Sub txtCurHp_GotFocus()
  Call SelectField(txtCurHp)
End Sub

Private Sub txtCurHp_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtCurHp_Change()
  If loading_combatant Then Exit Sub
  If txtCurHp = "-" Then
      Combatants(lisCombatants.ListIndex + 1).CurHp = 0
    Else
      Combatants(lisCombatants.ListIndex + 1).CurHp = Val(txtCurHp)
    End If
  Call gauHp_Update
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub txtOptHp_GotFocus()
  Call SelectField(txtOptHp)
End Sub

Private Sub txtOptHp_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtOptHp_Change()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).OptHp = Val(txtOptHp)
  Call gauHp_Update
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub gauHp_Update()
  Dim i As Integer
  i = 1
  If Val(txtCurHp) > i Then i = Val(txtCurHp)
  If Val(txtOptHp) > i Then i = Val(txtOptHp)
  gauHp.Max = i
  i = Val(txtCurHp)
  If i < 0 Then gauHp.Value = 0 Else gauHp.Value = i
  i = CombatantHpPenalty(lisCombatants.ListIndex + 1)
  If i = -20 Then
      lblHpStatus = "Out"
    ElseIf i = 0 Then
      lblHpStatus = ""
    Else
      lblHpStatus = i
    End If
  lblCombStatus = CombatantStatus(lisCombatants.ListIndex + 1, 1)
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub txtBleed_GotFocus()
  Call SelectField(txtBleed)
End Sub

Private Sub txtBleed_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtBleed_Change()
  If loading_combatant Then Exit Sub
  If txtBleed = "-" Then
      Combatants(lisCombatants.ListIndex + 1).Bleed = 0
    Else
      Combatants(lisCombatants.ListIndex + 1).Bleed = Val(txtBleed)
    End If
  Call gauHp_Update
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub txtHeal_GotFocus()
  Call SelectField(txtHeal)
End Sub

Private Sub txtHeal_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtHeal_Change()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).Heal = Val(txtHeal)
End Sub

Private Sub txtHeal_LostFocus()
  If Val(txtHeal) < 0 Or Val(txtHeal) > 100 Then
      MsgBox "Heal rates should range from 0 to 100.", vbExclamation
      txtHeal.SetFocus
      lisCombatants.ListIndex = current_listindex
    End If
End Sub

Private Sub txtClot_GotFocus()
  Call SelectField(txtClot)
End Sub

Private Sub txtClot_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtClot_Change()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).Clot = Val(txtClot)
End Sub

Private Sub txtClot_LostFocus()
  If Val(txtClot) < 0 Or Val(txtClot) > 100 Then
      MsgBox "Clot rates should range from 0 to 100.", vbExclamation
      txtClot.SetFocus
      lisCombatants.ListIndex = current_listindex
    End If
End Sub

Private Sub txtRoundsTillDown_GotFocus()
  Call SelectField(txtRoundsTillDown)
End Sub

Private Sub txtRoundsTillDown_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtRoundsTillDown_Change()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).RoundsTillDown = Val(txtRoundsTillDown)
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub txtRoundsTillDead_GotFocus()
  Call SelectField(txtRoundsTillDead)
End Sub

Private Sub txtRoundsTillDead_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtRoundsTillDead_Change()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).RoundsTillDead = Val(txtRoundsTillDead)
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub cmbPosition_Click()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).Position = cmbPosition.ListIndex + 1
  Call gauHp_Update
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub txtCapLoss_GotFocus()
  Call SelectField(txtCapLoss)
End Sub

Private Sub txtCapLoss_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtCapLoss_Change()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).CapLoss = Val(txtCapLoss)
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub txtStuns_GotFocus()
  Call SelectField(txtStuns)
End Sub

Private Sub txtStuns_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtStuns_Change()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).Stuns = Val(txtStuns)
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub txtStunNoParries_GotFocus()
  Call SelectField(txtStunNoParries)
End Sub

Private Sub txtStunNoParries_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtStunNoParries_Change()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).StunNoParries = Val(txtStunNoParries)
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub txtLostInits_GotFocus()
  Call SelectField(txtLostInits)
End Sub

Private Sub txtLostInits_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigits(KeyAscii)
End Sub

Private Sub txtLostInits_Change()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).LostInits = Val(txtLostInits)
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub txtPenalty_GotFocus()
  Call SelectField(txtPenalty)
End Sub

Private Sub txtPenalty_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub txtPenalty_Change()
  If loading_combatant Then Exit Sub
  If txtPenalty = "-" Then
      Combatants(lisCombatants.ListIndex + 1).Penalty = 0
    Else
      Combatants(lisCombatants.ListIndex + 1).Penalty = Val(txtPenalty)
    End If
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub lisMustParry_Build(ByVal MustParry As String)
  Dim s1, s2 As String
  Dim i As Integer
  ' translate the .MustParry into the list
  lisMustParry.Clear
  s1 = MustParry
  i = InStr(s1, ",")
  While i <> 0
    s2 = Left$(s1, i - 1)
    s1 = Mid$(s1, i + 1)
    i = InStr(s2, " ")
    lisMustParry.AddItem IIf(Val(Left$(s2, i - 1)) > 0, "+", "") & Left$(s2, i - 1) & " (" & Mid$(s2, i + 1) & " " & Rnds(Val(Mid$(s2, i + 1))) & ")"
    i = InStr(s1, ",")
    Wend
End Sub

Private Sub lisMustParry_Unbuild()
  Dim result As String
  Dim i As Integer
  result = ""
  For i = 0 To lisMustParry.ListCount - 1
    result = result & StripPlus(Word(lisMustParry.List(i), 1)) & " " & Mid$(Word(lisMustParry.List(i), 2), 2) & ","
    Next i
  Combatants(lisCombatants.ListIndex + 1).MustParry = result
End Sub

Private Sub lisMustParry_Add(ByVal rounds As Integer, ByVal Penalty As Integer)
  Call AddMustParry(lisCombatants.ListIndex + 1, rounds, Penalty)
  Call lisMustParry_Build(Combatants(lisCombatants.ListIndex + 1).MustParry)
  Call fraCombatants_EnableDisable
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub cmdMPPlus_Click()
  ' query the user for the amount and rounds
  frmBonusPenalty.Caption = "Must Parry"
  frmBonusPenalty.txtBonusPenalty = ""
  frmBonusPenalty.txtNumRounds = "1"
  frmBonusPenalty.Show 1
  If frmBonusPenalty.cmdCancel.Tag = "y" Then Exit Sub
  Call lisMustParry_Add(Val(frmBonusPenalty.txtNumRounds), Val(frmBonusPenalty.txtBonusPenalty))
End Sub

Private Sub lisMustParry_DblClick()
  Dim s As String
  Dim i, paren As Integer
  ' edit the must parry item
  If lisMustParry.ListIndex = -1 Then lisMustParry.ListIndex = 0
  frmBonusPenalty.Caption = "Must Parry"
  frmBonusPenalty.txtBonusPenalty = Word(lisMustParry.List(lisMustParry.ListIndex), 1)
  frmBonusPenalty.txtNumRounds = Mid$(Word(lisMustParry.List(lisMustParry.ListIndex), 2), 2)
  frmBonusPenalty.Show 1
  If frmBonusPenalty.cmdCancel.Tag = "y" Then Exit Sub
  lisMustParry.RemoveItem lisMustParry.ListIndex
  s = frmBonusPenalty.txtBonusPenalty & " (" & frmBonusPenalty.txtNumRounds & " " & Rnds(Val(frmBonusPenalty.txtNumRounds)) & ")"
  i = 0
  While i < lisMustParry.ListCount
    If Val(lisMustParry.List(i)) = Val(frmBonusPenalty.txtBonusPenalty) Then
        ' simply add rounds to the existing entry
        paren = InStr(lisMustParry.List(i), "(")
        lisMustParry.List(i) = frmBonusPenalty.txtBonusPenalty & " (" & _
          Trim$(Str$(Val(frmBonusPenalty.txtNumRounds) + _
          Val(Mid$(lisMustParry.List(i), paren + 1)))) & Rnds(Val(frmBonusPenalty.txtNumRounds)) & ")"
        Call lisMustParry_Unbuild
        Call fraCombatants_EnableDisable
        Exit Sub
      ElseIf Val(lisMustParry.List(i)) > Val(frmBonusPenalty.txtBonusPenalty) Then
        ' insert the new item into position i
        lisMustParry.AddItem s, i
        Call lisMustParry_Unbuild
        Call fraCombatants_EnableDisable
        Exit Sub
      End If
    i = i + 1
    Wend
  ' insert the new item at the end
  lisMustParry.AddItem s
  Call lisMustParry_Unbuild
  Call fraCombatants_EnableDisable
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub cmdMPMinus_Click()
  ' remove from the array and string
  If lisMustParry.ListIndex = -1 Then lisMustParry.ListIndex = 0
  lisMustParry.RemoveItem lisMustParry.ListIndex
  Call lisMustParry_Unbuild
  Call fraCombatants_EnableDisable
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub lisBonusPenalty_Build(ByVal BonusPenalty As String)
  Dim s1, s2 As String
  Dim i As Integer
  lisBonusPenalty.Clear
  s1 = BonusPenalty
  i = InStr(s1, ",")
  While i <> 0
    s2 = Left$(s1, i - 1)
    s1 = Mid$(s1, i + 1)
    i = InStr(s2, " ")
    lisBonusPenalty.AddItem IIf(Val(Left$(s2, i - 1)) > 0, "+", "") & Left$(s2, i - 1) & " (" & Mid$(s2, i + 1) & " " & Rnds(Val(Mid$(s2, i + 1))) & ")"
    i = InStr(s1, ",")
    Wend
End Sub

Private Sub lisBonusPenalty_Unbuild()
  Dim result As String
  Dim i As Integer
  result = ""
  For i = 0 To lisBonusPenalty.ListCount - 1
    result = result & StripPlus(Word(lisBonusPenalty.List(i), 1)) & " " & Mid$(Word(lisBonusPenalty.List(i), 2), 2) & ","
    Next i
  Combatants(lisCombatants.ListIndex + 1).BonusPenalty = result
End Sub

Private Sub lisBonusPenalty_Add(ByVal rounds As Integer, ByVal Penalty As Integer)
  Call AddBonusPenalty(lisCombatants.ListIndex + 1, rounds, Penalty)
  Call lisBonusPenalty_Build(Combatants(lisCombatants.ListIndex + 1).BonusPenalty)
  Call fraCombatants_EnableDisable
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub cmdBPPlus_Click()
  ' query the user for the amount and rounds
  frmBonusPenalty.Caption = "Bonus/Penalty"
  frmBonusPenalty.txtBonusPenalty = ""
  frmBonusPenalty.txtNumRounds = "1"
  frmBonusPenalty.Show 1
  If frmBonusPenalty.cmdCancel.Tag = "y" Then Exit Sub
  Call lisBonusPenalty_Add(Val(frmBonusPenalty.txtNumRounds), Val(frmBonusPenalty.txtBonusPenalty))
End Sub

Private Sub lisBonusPenalty_DblClick()
  Dim s As String
  Dim i, paren As Integer
  ' edit the must parry item
  If lisBonusPenalty.ListIndex = -1 Then lisBonusPenalty.ListIndex = 0
  frmBonusPenalty.Caption = "Must Parry"
  frmBonusPenalty.txtBonusPenalty = Word(lisBonusPenalty.List(lisBonusPenalty.ListIndex), 1)
  frmBonusPenalty.txtNumRounds = Mid$(Word(lisBonusPenalty.List(lisBonusPenalty.ListIndex), 2), 2)
  frmBonusPenalty.Show 1
  If frmBonusPenalty.cmdCancel.Tag = "y" Then Exit Sub
  lisBonusPenalty.RemoveItem lisBonusPenalty.ListIndex
  s = frmBonusPenalty.txtBonusPenalty & " (" & frmBonusPenalty.txtNumRounds & " " & Rnds(Val(frmBonusPenalty.txtNumRounds)) & ")"
  i = 0
  While i < lisBonusPenalty.ListCount
    If Val(lisBonusPenalty.List(i)) = Val(frmBonusPenalty.txtBonusPenalty) Then
        ' simply add rounds to the existing entry
        paren = InStr(lisBonusPenalty.List(i), "(")
        lisBonusPenalty.List(i) = frmBonusPenalty.txtBonusPenalty & " (" & _
          Trim$(Str$(Val(frmBonusPenalty.txtNumRounds) + _
          Val(Mid$(lisBonusPenalty.List(i), paren + 1)))) & " " & Rnds(Val(frmBonusPenalty.txtNumRounds)) & ")"
        Call lisMustParry_Unbuild
        Call fraCombatants_EnableDisable
        Exit Sub
      ElseIf Val(lisBonusPenalty.List(i)) > Val(frmBonusPenalty.txtBonusPenalty) Then
        ' insert the new item into position i
        lisBonusPenalty.AddItem s, i
        Call lisMustParry_Unbuild
        Call fraCombatants_EnableDisable
        Exit Sub
      End If
    i = i + 1
    Wend
  ' insert the new item at the end
  lisBonusPenalty.AddItem s
  Call lisBonusPenalty_Unbuild
  Call fraCombatants_EnableDisable
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub cmdBPMinus_Click()
  ' remove from the array and string
  If lisBonusPenalty.ListIndex = -1 Then lisBonusPenalty.ListIndex = 0
  lisBonusPenalty.RemoveItem lisBonusPenalty.ListIndex
  Call lisBonusPenalty_Unbuild
  Call fraCombatants_EnableDisable
  Call frmPlayerDisplay.Form_Refresh
End Sub

'Private Sub txtHealth_GotFocus()
'  Call SelectField(txtHealth)
'End Sub

Private Sub txtHealth_KeyPress(KeyAscii As Integer)
  If loading_combatant Then Exit Sub
  KeyAscii = NoQuotes(KeyAscii)
End Sub

Private Sub txtHealth_Change()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).Health = txtHealth
End Sub

'Private Sub txtNotes_GotFocus()
'  Call SelectField(txtNotes)
'End Sub

Private Sub txtNotes_KeyPress(KeyAscii As Integer)
  If loading_combatant Then Exit Sub
  KeyAscii = NoQuotes(KeyAscii)
End Sub

Private Sub txtNotes_Change()
  If loading_combatant Then Exit Sub
  Combatants(lisCombatants.ListIndex + 1).Notes = txtNotes
End Sub

Private Sub cmdDelete_Click()
  Dim i As Integer
  If MsgBox("Really delete " & Combatants(lisCombatants.ListIndex + 1).Name & "?", vbYesNo + vbQuestion, "Confirm Combatant Deletion") = vbYes Then
     rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & txtName & " deleted."
     ' delete the item from the array
      DeleteCombatant (lisCombatants.ListIndex + 1)
      i = lisCombatants.ListIndex
      ' delete the item from the cmbParryAt
      cmbParryAt.RemoveItem i + 1
      ' delete the item from the lisCombatants
      lisCombatants.RemoveItem lisCombatants.ListIndex
      If lisCombatants.ListCount = 0 Then ' clear out all the fields
          Call ClearFields
          lisCombatants.Clear
        Else ' pick the next person on the list
          If i >= lisCombatants.ListCount Then i = lisCombatants.ListCount - 1
          lisCombatants.ListIndex = i
        End If
      ' update the enabled state of buttons
      Call fraCombatants_EnableDisable
      Call frmPlayerDisplay.Form_Refresh
    End If
End Sub

Private Sub mnuCombatantDelete_Click()
  Call cmdDelete_Click
End Sub

Private Sub ClearFields()
  txtName = ""
  chkPC = False
  txtS = ""
  txtE = ""
  txtD = ""
  txtQ = ""
  txtI = ""
  txtP = ""
  txtW = ""
  txtR = ""
  txtSkills = ""
  txtNextAction = ""
  cmbSaves.ListIndex = 0
  txtParry = ""
  cmbParryAt.ListIndex = 0
  cmbParryAt.List(0) = "(none)"
  txtDelay = ""
  txtMV = ""
  txtMN = ""
  cmbAT.ListIndex = 0
  txtDB = ""
  txtAttacks = ""
  txtCurHp = ""
  txtOptHp = ""
  gauHp.Min = 0
  gauHp.Max = 1
  gauHp.Value = 1
  txtBleed = ""
  txtHeal = ""
  txtClot = ""
  txtRoundsTillDown = ""
  txtRoundsTillDead = ""
  cmbPosition.ListIndex = 0
  txtHealth = ""
  txtCapLoss = ""
  txtStuns = ""
  txtStunNoParries = ""
  txtLostInits = ""
  txtPenalty = ""
  lisMustParry_Build ("")
  lisBonusPenalty_Build ("")
  txtNotes = ""
End Sub

Private Sub mnuSave_DoIt(ByVal Filename As String)
  Dim errorText As String
  Dim i As Integer
  ' change the caption to show that it's saving
  Me.Caption = "Saving into file '" & cdlGeneric.Filename & "'..."
  ' open the file (for rewrite)
  On Error GoTo fileError
  errorText = " opening file"
  Open Filename For Output As #1
  ' write header, then Phase and NumCombatants
  errorText = " writing to file"
  Write #1, "PrismTools - " & conAppName & " - v" & conAppVersion
  Write #1, Phase, Last_Bleeding_Phase, NumCombatants, NumEvents
  ' for each combatant, write the combatant data
  For i = 1 To NumCombatants
    With Combatants(i)
      Write #1, .Name, .NextAction, .Saves, .Parry, .ParryAt, .LastWS, .LastAttacked, .LastOB, .Delay, .MV, .MN, .AT, .DB, .Attacks, .CurHp, .OptHp, .PC
      Write #1, .Bleed, .Heal, .Clot, .CapLoss, .Stuns, .StunNoParries, .LostInits, .Penalty, .MustParry, .BonusPenalty, .Notes
      Write #1, .RoundsTillDown, .RoundsTillDead, .Position, .Health
      Write #1, .StatS, .StatE, .StatD, .StatQ, .StatI, .StatP, .StatW, .StatR, .Skills
      End With
    Next i
  For i = 1 To NumEvents
    With Events(i)
      Write #1, .Phase, .Description
      End With
    Next i
  ' close the file
  errorText = " closing file"
  Close #1
  ' change the topic back
  Me.Caption = conAppName & " v" & conAppVersion & " - " & _
      FilePart(IIf(Filename = Last_AutoSave_Filename, LastFilename(1), Filename))
  Exit Sub
  ' error message handlers
fileError:
  Close #1
  MsgBox "Error" & errorText & " " & Filename, vbCritical
  RemoveLastFilename Filename
End Sub

Private Sub SetLastFilename(ByVal Position As Integer, ByVal Filename As String)
  Dim NumberPart As String
  LastFilename(Position) = Filename
  If Position = 10 Then NumberPart = "1&0" Else NumberPart = "&" & Trim$(Str$(Position))
  If Filename = "" Then
      mnuOpenRecent(Position).Caption = NumberPart & " - (none)"
      mnuOpenRecent(Position).Enabled = False
    Else
      mnuOpenRecent(Position).Caption = NumberPart & " - " & FilePart$(Filename)
      mnuOpenRecent(Position).Enabled = True
    End If
End Sub

Private Sub NewLastFilename(ByVal newfilename As String)
  ' update the LastFilename array and list
  Dim i, j As Integer
  ' if it's already the most recent, do nothing
  If LastFilename(1) = newfilename Then Exit Sub
  ' see if it's already in the list
  For i = 2 To FILENAME_HISTORY
    If LastFilename(i) = newfilename Then
        ' move stuff around to get it to the top
        For j = i To 2 Step -1
          SetLastFilename j, LastFilename(j - 1)
          Next j
        SetLastFilename 1, newfilename
        Exit Sub
      End If
    Next i
  ' add it to the beginning of the list
  For i = FILENAME_HISTORY To 2 Step -1
    SetLastFilename i, LastFilename(i - 1)
    Next i
  SetLastFilename 1, newfilename
End Sub

Private Sub RemoveLastFilename(ByVal newfilename As String)
  ' remove a filename from the LastFilename list because it had an error opening
  Dim i, j As Integer
  If LastFilename(FILENAME_HISTORY) = newfilename Then
      ' remove the last one
      SetLastFilename FILENAME_HISTORY, ""
      Exit Sub
    End If
  For i = 1 To FILENAME_HISTORY - 1
    If LastFilename(i) = newfilename Then
        For j = i To FILENAME_HISTORY - 1
          SetLastFilename j, LastFilename(j + 1)
          Next j
        SetLastFilename FILENAME_HISTORY, ""
        Exit Sub
      End If
    Next i
  ' it wasn't here, so no need to remove it
End Sub

Private Sub mnuSave_Click()
  Call mnuSave_DoIt(LastFilename(1))
End Sub

Private Sub mnuSaveAs_Click()
  ' set up the common dialog for a save
  cdlGeneric.DialogTitle = "Save combat as"
  cdlGeneric.Filename = LastFilename(1)
  cdlGeneric.DefaultExt = "*.prism"
  cdlGeneric.Filter = "Prism Files (*.prism)|*.prism|Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
  cdlGeneric.FilterIndex = 1
  cdlGeneric.Flags = cdlOFNHideReadOnly + cdlOFNPathMustExist + cdlOFNExplorer + cdlOFNLongNames
  cdlGeneric.CancelError = True
  ' launch the dialog
  On Error GoTo cancelled
  cdlGeneric.ShowSave
  If cdlGeneric.Filename = "" Then Exit Sub
  ' work around bug: CDL trims default extension
  If Right(cdlGeneric.Filename, 4) = ".pri" Then cdlGeneric.Filename = cdlGeneric.Filename & "sm"
  Call mnuSave_DoIt(cdlGeneric.Filename)
  NewLastFilename cdlGeneric.Filename
cancelled:
  Exit Sub
End Sub

Private Sub mnuOpen_Click()
  ' confirm an overwrite if needed
  If NumCombatants + NumEvents > 0 Then
      If MsgBox("Really load over current data?", vbYesNo + vbQuestion, conAppName) = vbNo Then Exit Sub
      Call ClearFields
      Call mnuNew_DoIt
      Call fraCombatants_EnableDisable
      Call fraBattle_EnableDisable
    End If
  ' set up the common dialog for a load
  ' front end to generate the Open file requester
  cdlGeneric.DialogTitle = "Load combat"
  cdlGeneric.Filename = LastFilename(1)
  cdlGeneric.DefaultExt = "*.prism"
  cdlGeneric.Filter = "Prism Files (*.prism)|*.prism|Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
  cdlGeneric.FilterIndex = 1
  cdlGeneric.Flags = cdlOFNFileMustExist + cdlOFNHideReadOnly + _
      cdlOFNPathMustExist + cdlOFNExplorer + cdlOFNLongNames
  cdlGeneric.CancelError = True
  On Error GoTo cancelled
  cdlGeneric.ShowOpen
  If cdlGeneric.Filename = "" Then Exit Sub
  Call mnuOpen_DoIt(cdlGeneric.Filename)
cancelled:
  Exit Sub
End Sub

Private Sub mnuOpen_DoIt(ByVal Filename As String)
  Dim errorText, Header, fileversion As String
  Dim i As Integer
  ' open the file (for read)
  On Error GoTo fileError
  ' change the caption to show that it's loading
  Me.Caption = "Loading from file '" & cdlGeneric.Filename & "'..."
  errorText = " opening file"
  Open Filename For Input As #1
  ' read and check header
  errorText = " reading from file"
  Input #1, Header
  If Header = "PrismTools - IRIS - v1.0" Then
      fileversion = "v1.0"
    ElseIf Header = "PrismTools - IRIS - v1.1" Then
      fileversion = "v1.1"
    ElseIf Header = "PrismTools - IRIS - v1.5" Then
      fileversion = "v1.5"
    ElseIf Header = "PrismTools - IRIS - v1.6" Then
      fileversion = "v1.6"
    ElseIf Header = "PrismTools - IRIS - v1.7" Then
      fileversion = "v1.7"
    Else
      MsgBox "Error: file is of incorrect format.", vbExclamation
      Close #1
      Me.Caption = conAppName & " v" & conAppVersion
      Exit Sub
    End If
  ' read Phase and NumCombatants
  Input #1, Phase, Last_Bleeding_Phase, NumCombatants, NumEvents
  Next_AutoSave = Int(Phase / conAutoSaveRate) * conAutoSaveRate + conAutoSaveRate
  If Next_AutoSave < 0 Then Next_AutoSave = 0
  ' for each combatant, read the combatant data
  For i = 1 To NumCombatants
    With Combatants(i)
      If fileversion = "v1.7" Then
          Input #1, .Name, .NextAction, .Saves, .Parry, .ParryAt, .LastWS, .LastAttacked, .LastOB, .Delay, .MV, .MN, .AT, .DB, .Attacks, .CurHp, .OptHp, .PC
        Else
          Input #1, .Name, .NextAction, .Saves, .Parry, .ParryAt, .LastWS, .LastAttacked, .LastOB, .Delay, .MV, .MN, .AT, .DB, .Attacks, .CurHp, .OptHp
        End If
      Input #1, .Bleed, .Heal, .Clot, .CapLoss, .Stuns, .StunNoParries, .LostInits, .Penalty, .MustParry, .BonusPenalty, .Notes
      If fileversion = "v1.0" Then
          .RoundsTillDown = 0
          .RoundsTillDead = 0
          .Position = 1
          .Health = ""
        Else
          Input #1, .RoundsTillDown, .RoundsTillDead, .Position, .Health
        End If
      If fileversion = "v1.5" Or fileversion = "v1.6" Or fileversion = "v1.7" Then
          Input #1, .StatS, .StatE, .StatD, .StatQ, .StatI, .StatP, .StatW, .StatR, .Skills
        Else
          .StatS = 0
          .StatE = 0
          .StatD = 0
          .StatQ = 0
          .StatI = 0
          .StatP = 0
          .StatW = 0
          .StatR = 0
          .Skills = ""
        End If
      lisCombatants.AddItem .Name
      lisCombatants_UpdateLabel lisCombatants.ListCount
      cmbParryAt.AddItem .Name
      End With
    Next i
  For i = 1 To NumEvents
    With Events(i)
      Input #1, .Phase, .Description
      End With
    Next i
  ' close the file
  errorText = " closing file"
  Close #1
  ' change the topic back
  NewLastFilename Filename
  Me.Caption = conAppName & " v" & conAppVersion & " - " & FilePart(LastFilename(1))
  If NumCombatants > 0 Then lisCombatants.ListIndex = 0
  Call fraCombatants_EnableDisable
  Call lblAction_FindNext
  Call fraBattle_EnableDisable
  Call frmPlayerDisplay.Form_Refresh
  Exit Sub
  ' error message handlers
fileError:
  Close #1
  MsgBox "Error" & errorText & " " & cdlGeneric.Filename, vbCritical
  RemoveLastFilename Filename
End Sub

Private Sub mnuAppend_Click()
  ' switch to an Open if there's nothing to append to
  If NumCombatants + NumEvents = 0 Then
      Call mnuOpen_Click
      Exit Sub
    End If
  ' set up the common dialog for a load
  ' front end to generate the Open file requester
  cdlGeneric.DialogTitle = "Append combatants and events from"
  cdlGeneric.Filename = LastFilename(1)
  cdlGeneric.DefaultExt = "*.prism"
  cdlGeneric.Filter = "Prism Files (*.prism)|*.prism|Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
  cdlGeneric.FilterIndex = 1
  cdlGeneric.Flags = cdlOFNFileMustExist + cdlOFNHideReadOnly + _
      cdlOFNPathMustExist + cdlOFNExplorer + cdlOFNLongNames
  cdlGeneric.CancelError = True
  On Error GoTo cancelled
  cdlGeneric.ShowOpen
  If cdlGeneric.Filename = "" Then Exit Sub
  ' change the caption to show that it's loading
  Me.Caption = "Appending from file '" & cdlGeneric.Filename & "'..."
  Call mnuAppend_DoIt(cdlGeneric.Filename)
cancelled:
  Exit Sub
End Sub

Private Sub mnuAppend_DoIt(ByVal Filename As String)
  Dim errorText, Header, fileversion As String
  Dim i, j, k, thiscombs, thisevents As Integer
  Dim discardcomb As Combatant
  Dim readevent As CombatEvent
  ' open the file (for read)
  On Error GoTo fileError
  errorText = " opening file"
  Open Filename For Input As #1
  ' read and check header
  errorText = " reading from file"
  Input #1, Header
  If Header = "PrismTools - IRIS - v1.0" Then
      fileversion = "v1.0"
    ElseIf Header = "PrismTools - IRIS - v1.1" Then
      fileversion = "v1.1"
    ElseIf Header = "PrismTools - IRIS - v1.5" Then
      fileversion = "v1.5"
    ElseIf Header = "PrismTools - IRIS - v1.6" Then
      fileversion = "v1.6"
    ElseIf Header = "PrismTools - IRIS - v1.7" Then
      fileversion = "v1.7"
    Else
      MsgBox "Error: file is of incorrect format.", vbExclamation
      Close #1
      Me.Caption = conAppName & " v" & conAppVersion & " - " & FilePart(LastFilename(1))
      Exit Sub
    End If
  ' read Phase, NumCombatants, and NumEvents
  Input #1, i, j, thiscombs, thisevents
  ' for each combatant, read the combatant data
  j = NumCombatants
  For i = 1 To thiscombs
    If NumCombatants >= conMaxCombatants Then
        With discardcomb
          If fileversion = "v1.7" Then
             Input #1, .Name, .NextAction, .Saves, .Parry, .ParryAt, .LastWS, .LastAttacked, _
              .LastOB, .Delay, .MV, .MN, .AT, .DB, .Attacks, .CurHp, .OptHp, .PC
            Else
              Input #1, .Name, .NextAction, .Saves, .Parry, .ParryAt, .LastWS, .LastAttacked, _
              .LastOB, .Delay, .MV, .MN, .AT, .DB, .Attacks, .CurHp, .OptHp
            End If
          Input #1, .Bleed, .Heal, .Clot, .CapLoss, .Stuns, .StunNoParries, .LostInits, _
              .Penalty, .MustParry, .BonusPenalty, .Notes
          If fileversion = "v1.0" Then
              .RoundsTillDown = 0
              .RoundsTillDead = 0
              .Position = 1
              .Health = ""
            Else
              Input #1, .RoundsTillDown, .RoundsTillDead, .Position, .Health
            End If
          If fileversion = "v1.5" Or fileversion = "v1.6" Or fileversion = "v1.7" Then
              Input #1, .StatS, .StatE, .StatD, .StatQ, .StatI, .StatP, .StatW, .StatR, .Skills
            Else
              .StatS = 0
              .StatE = 0
              .StatD = 0
              .StatQ = 0
              .StatI = 0
              .StatP = 0
              .StatW = 0
              .StatR = 0
              .Skills = ""
            End If
          End With
      Else
        With Combatants(NumCombatants + 1)
          ' read in the combatant
          If fileversion = "v1.7" Then
             Input #1, .Name, .NextAction, .Saves, .Parry, .ParryAt, .LastWS, .LastAttacked, _
              .LastOB, .Delay, .MV, .MN, .AT, .DB, .Attacks, .CurHp, .OptHp, .PC
            Else
              Input #1, .Name, .NextAction, .Saves, .Parry, .ParryAt, .LastWS, .LastAttacked, _
              .LastOB, .Delay, .MV, .MN, .AT, .DB, .Attacks, .CurHp, .OptHp
            End If
          Input #1, .Bleed, .Heal, .Clot, .CapLoss, .Stuns, .StunNoParries, .LostInits, _
              .Penalty, .MustParry, .BonusPenalty, .Notes
          If fileversion = "v1.0" Then
              .RoundsTillDown = 0
              .RoundsTillDead = 0
              .Position = 1
              .Health = ""
            Else
              Input #1, .RoundsTillDown, .RoundsTillDead, .Position, .Health
            End If
          If fileversion = "v1.5" Or fileversion = "v1.6" Or fileversion = "v1.7" Then
              Input #1, .StatS, .StatE, .StatD, .StatQ, .StatI, .StatP, .StatW, .StatR, .Skills
            Else
              .StatS = 0
              .StatE = 0
              .StatD = 0
              .StatQ = 0
              .StatI = 0
              .StatP = 0
              .StatW = 0
              .StatR = 0
              .Skills = ""
            End If
          ' make sure combatant isn't already in the file
          While FindCombatant(.Name) <> 0
            .Name = NextNewName(.Name)
            Wend
          lisCombatants.AddItem .Name
          cmbParryAt.AddItem .Name
          If .ParryAt <> 0 Then .ParryAt = .ParryAt + j
          If .LastAttacked <> 0 Then .LastAttacked = .LastAttacked + j
          If Phase >= 0 And .NextAction < Phase Then .NextAction = Phase + .Delay + OpenEndedD10()
          lisCombatants_UpdateLabel lisCombatants.ListCount
          End With
        NumCombatants = NumCombatants + 1
      End If
    Next i
  For i = 1 To thisevents
    With readevent
      Input #1, .Phase, .Description
      .Phase = .Phase + IIf(Phase > 0, Phase, 0)
      End With
    If NumEvents < conMaxEvents Then
        ' find the position into which this should be inserted
        j = 1
        Do While j <= NumEvents
          If Events(j).Phase > readevent.Phase Then Exit Do
          j = j + 1
          Loop
        If j <= NumEvents Then ' make room for it
            For k = NumEvents To j Step -1
              Events(k + 1).Phase = Events(k).Phase
              Events(k + 1).Description = Events(k).Description
              Next k
          End If
        With Events(j) ' add this event
          .Phase = readevent.Phase
          .Description = readevent.Description
          End With
        NumEvents = NumEvents + 1
      End If
    Next i
  ' close the file
  errorText = " closing file"
  Close #1
  ' change the topic back
  Me.Caption = conAppName & " v" & conAppVersion & " - " & LastFilename(1)
  If NumCombatants > 0 Then lisCombatants.ListIndex = 0
  Call fraCombatants_EnableDisable
  Call frmPlayerDisplay.Form_Refresh
  ' call fraBattle_EnableDisable
  Exit Sub
  ' error message handlers
fileError:
  Close #1
  MsgBox "Error" & errorText & " " & cdlGeneric.Filename, vbCritical
  RemoveLastFilename Filename
End Sub


Private Sub mnuOpenRecent_Click(Index As Integer)
  Dim i As Integer
  ' if there's nothing loaded, just load this file, otherwise ask first if you should load or append
  If NumCombatants + NumEvents > 0 Then
      i = MsgBox("Load over current data?  (No means to Append to current data.)", vbYesNoCancel + vbQuestion, conAppName)
      If i = vbCancel Then Exit Sub
      If i = vbYes Then
          ' clear things
          Call ClearFields
          Call mnuNew_DoIt
          Call fraCombatants_EnableDisable
          Call fraBattle_EnableDisable
          ' do the load
          Call mnuOpen_DoIt(LastFilename(Index))
        Else
          Call mnuAppend_DoIt(LastFilename(Index))
        End If
    Else
      ' do a load
      Call mnuOpen_DoIt(LastFilename(Index))
    End If
End Sub

Private Sub mnuNew_Click()
  If MsgBox("Really zap all current data?", vbYesNo + vbQuestion, conAppName) = vbNo Then Exit Sub
  Call ClearFields
  Call mnuNew_DoIt
  Call fraCombatants_EnableDisable
  Call fraBattle_EnableDisable
End Sub

Private Sub mnuNew_DoIt()
  Dim i As Integer
  For i = 1 To NumCombatants
    Call ClearCombatant(i)
    Next i
  NumCombatants = 0
  For i = 1 To NumEvents
    Events(i).Phase = 0
    Events(i).Description = ""
    Next i
  NumEvents = 0
  Log = ""
  rtbLog_Update
  lisCombatants.Clear
  cmbParryAt.Clear
  cmbParryAt.AddItem "(none)"
  Phase = -1
  Next_AutoSave = 0
  Last_Bleeding_Phase = 0
  Me.Caption = conAppName & " v" & conAppVersion
End Sub

Private Sub cmdDamage_Click()
  Dim i As Integer
  Dim logtext, logtext2 As String
  With frmDamage
    .cmbCombatants.Clear
    For i = 1 To NumCombatants
      .cmbCombatants.AddItem Combatants(i).Name
      Next i
    .cmbCombatants.ListIndex = lisCombatants.ListIndex
    If Acting_Combatant <= 0 Then
        .txtAttackerRounds.Enabled = False
        .txtAttackerRounds.BackColor = &HC0C0C0
        .txtAttackerAmount.Enabled = False
        .txtAttackerAmount.BackColor = &HC0C0C0
      Else
        .txtAttackerRounds.Enabled = True
        .txtAttackerRounds.BackColor = &H80000005
        .txtAttackerAmount.Enabled = True
        .txtAttackerAmount.BackColor = &H80000005
      End If
    .txtCritRoll.Tag = "n"
    .Show 1
    If .cmdCancel.Tag = "y" Then Exit Sub
    logtext2 = ""
    i = lisCombatants.ListIndex
    If Val(.txtAttackerRounds) <> 0 And Val(.txtAttackerAmount) <> 0 Then
        Call AddBonusPenalty(Acting_Combatant, .txtAttackerRounds, .txtAttackerAmount)
        logtext2 = "attacker at " & .txtAttackerAmount & " for " & .txtAttackerRounds & _
          " action" & IIf(Val(.txtAttackerRounds) <> 1, "s", "")
      End If
    lisCombatants.ListIndex = .cmbCombatants.ListIndex
    logtext = ""
    txtCurHp = txtCurHp - Val(.txtHits) - Val(.txtBonusHits)
    If Val(.txtHits) + Val(.txtBonusHits) <> 0 Then logtext = CommaList(logtext, Trim$(Str$(Val(.txtHits) + Val(.txtBonusHits))) & " Hp")
    If txtCurHp <= 0 Then
        MsgBox txtName & " is out!", vbInformation
        logtext = CommaList(logtext, "\b out!\plain\f2\fs17")
        cmbPosition.ListIndex = 8
      End If
    txtBleed = txtBleed + Val(.txtBleed)
    If Val(.txtBleed) <> 0 Then logtext = CommaList(logtext, .txtBleed & " bleeding")
    txtStuns = txtStuns + Val(.txtStuns)
    If Val(.txtStuns) <> 0 Then logtext = CommaList(logtext, .txtStuns & " stun" & _
        IIf(Val(.txtStuns) <> 1, "s", ""))
    txtStunNoParries = txtStunNoParries + Val(.txtStunNoParries)
    If Val(.txtStunNoParries) <> 0 Then logtext = CommaList(logtext, .txtStunNoParries & _
        " stun/no-parr" & IIf(Val(.txtStunNoParries) <> 1, "ies", "y"))
    txtLostInits = txtLostInits + Val(.txtLostInits)
    If Val(.txtLostInits) <> 0 Then logtext = CommaList(logtext, .txtLostInits & " lost init" & _
        IIf(Val(.txtLostInits) <> 1, "s", ""))
    If Val(.txtRoundsTillDown) > 0 And (Val(txtRoundsTillDown) = 0 Or _
        Val(.txtRoundsTillDown) < Val(txtRoundsTillDown)) Then txtRoundsTillDown = .txtRoundsTillDown
    If Val(txtRoundsTillDown) <> 0 Then logtext = CommaList(logtext, "down in " & _
        txtRoundsTillDown & " round" & IIf(Val(txtRoundsTillDown) <> 1, "s", ""))
    If Val(.txtRoundsTillDead) > 0 And (Val(txtRoundsTillDead) = 0 Or _
        Val(.txtRoundsTillDead) < Val(txtRoundsTillDead)) Then txtRoundsTillDead = .txtRoundsTillDead
    If Val(txtRoundsTillDead) <> 0 Then logtext = CommaList(logtext, "dead in " & _
        txtRoundsTillDead & " round" & IIf(Val(txtRoundsTillDead) <> 1, "s", ""))
    If Val(.txtMPRounds) <> 0 Then
        Call lisMustParry_Add(Val(.txtMPRounds), Val(.txtMPAmount))
        logtext = CommaList(logtext, .txtMPRounds & " round" & IIf(Val(.txtMPRounds) <> 1, _
            "s", "") & " of must parry" & IIf(Val(.txtMPAmount) <> 0, " at " _
            & .txtMPAmount, ""))
      End If
    If Val(.txtBPRounds) <> 0 And Val(.txtBPAmount) <> 0 Then
        Call lisBonusPenalty_Add(Val(.txtBPRounds), Val(.txtBPAmount))
        logtext = CommaList(logtext, .txtBPRounds & " round" & IIf(Val(.txtBPRounds) <> 1, "s", "") & " at" & .txtBPAmount)
      End If
    txtPenalty = txtPenalty + Val(.txtPenalty)
    If Val(.txtPenalty) <> 0 Then logtext = CommaList(logtext, "at " & .txtPenalty)
    If .cmbPosition.ListIndex <> cmbPosition.ListIndex Then
        cmbPosition.ListIndex = .cmbPosition.ListIndex
        logtext = CommaList(logtext, LCase(CombatantPosition(cmbPosition.ListIndex + 1)))
      End If
    If .txtHealth <> "" Then
        txtHealth = txtHealth & .txtHealth & Chr$(13) & Chr$(10)
        logtext = CommaList(logtext, LCase(.txtHealth))
      End If
    If logtext2 <> "" Then logtext = CommaList(logtext, logtext2)
    If logtext <> "" Then
        rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & txtName & " damaged (" & logtext & ")"
      End If
    End With
  lisCombatants.ListIndex = i
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub mnuCombatantDamage_Click()
  Call cmdDamage_Click
End Sub

Private Sub mnuEvents_Click()
  frmEvents.txtPhase.Tag = IIf(Phase < 0, 0, Phase)
  frmEvents.Show 1
  Call fraCombatants_EnableDisable
  Call lblAction_FindNext
End Sub

Private Sub mnuMassHeal_Click()
  Dim i As Integer
  frmMassHeal.Show 1
  If frmMassHeal.cmdCancel.Tag = "y" Then Exit Sub
  rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : mass healing done"
  For i = 1 To NumCombatants
    With Combatants(i)
      If frmMassHeal.chkHits = 1 Then .CurHp = .OptHp
      If frmMassHeal.chkBleed = 1 Then .Bleed = 0
      If frmMassHeal.chkRoundsTillDown = 1 Then .RoundsTillDown = 0
      If frmMassHeal.chkRoundsTillDead = 1 Then .RoundsTillDead = 0
      If frmMassHeal.chkStunNoParries = 1 Then .StunNoParries = 0
      If frmMassHeal.chkStuns = 1 Then .Stuns = 0
      If frmMassHeal.chkLostInits = 1 Then .LostInits = 0
      If frmMassHeal.chkMustParry = 1 Then .MustParry = ""
      If frmMassHeal.chkBonusPenalty = 1 Then .BonusPenalty = ""
      If frmMassHeal.chkPenalty = 1 Then .Penalty = 0
      If frmMassHeal.chkPosition = 1 Then .Position = 1
      If frmMassHeal.chkHealth = 1 Then .Health = ""
      If frmMassHeal.chkCapLoss = 1 Then .CapLoss = 0
      If frmMassHeal.chkSaves = 1 Then .Saves = 0
      End With
    Next i
  Call lisCombatants_Click
End Sub

Private Sub mnuDamageReport_Click()
  Dim dmgrpt, combdmg As String
  Dim i As Integer
  ' build a string in dmgrpt which contains all the damage
  dmgrpt = ""
  For i = 1 To NumCombatants
    combdmg = ""
    With Combatants(i)
      If .Position = 10 Then combdmg = CommaList(combdmg, "\b\cf8 Dead!\plain\f2\fs17") Else _
          If .Position = 9 Then combdmg = CommaList(combdmg, "\b\cf5 Unconscious!\plain\f2\fs17")
      If .CurHp <= 0 Then combdmg = CommaList(combdmg, "\b\cf3 Out!\plain\f2\fs17") Else _
          If .CurHp < .OptHp Then combdmg = CommaList(combdmg, "Hp " & .CurHp & " out of " & .OptHp & " [" & Int(.CurHp * 100 / .OptHp) & "%]")
      If .RoundsTillDead > 0 Then combdmg = CommaList(combdmg, "\b Dead in " & .RoundsTillDead & " rounds")
      If .RoundsTillDown > 0 Then combdmg = CommaList(combdmg, "\b Down in " & .RoundsTillDown & " rounds")
      If .Bleed > 0 Then combdmg = CommaList(combdmg, "Bleeding " & .Bleed & "/rnd")
      If .StunNoParries > 0 Then combdmg = CommaList(combdmg, "S/NP " & .StunNoParries & " " & Rnds(.StunNoParries))
      If .Stuns > 0 Then combdmg = CommaList(combdmg, "Stunned " & .Stuns & " " & Rnds(.Stuns))
      If .MustParry <> "" Then combdmg = CommaList(combdmg, MPBPDisplay("Must parry", .MustParry))
      If .LostInits > 0 Then combdmg = CommaList(combdmg, "Lost Inits " & .LostInits & " " & Rnds(.LostInits))
      If .Penalty <> 0 Then combdmg = CommaList(combdmg, "@" & .Penalty)
      If .BonusPenalty <> "" Then combdmg = CommaList(combdmg, MPBPDisplay("Tmp Bonus/Pnlty", .BonusPenalty))
      If .CapLoss > 0 Then combdmg = CommaList(combdmg, "Cap. Loss " & .CapLoss)
      End With
    If combdmg <> "" Then dmgrpt = dmgrpt & "\b " & Combatants(i).Name & _
      "\plain\f2\fs17 : " & combdmg & "\par "
    Next i
  ' if there isn't one, report an error and quit
  If dmgrpt = "" Then
      MsgBox "No one is damaged.", vbInformation
      Exit Sub
    End If
  frmDamageReport.Caption = "Damage Report"
  ' assemble the RTF around dmgrpt and load it into the form
  frmDamageReport.rtbDamageReport = "{\rtf1\ansi\deff0\deftab720{\fonttbl{\f0\fswiss MS Sans Serif;}{\f1\froman\fcharset2 Symbol;}" & _
    "{\f2\fswiss MS Sans Serif;}}" & Chr$(13) & Chr$(10) & _
    "{\colortbl\red0\green0\blue0;\red128\green128\blue128;\red192\green192\blue192;\red128\green0\blue0;\red255\green255\blue255;\red128\green128\blue0;\red255\green255\blue0;\red0\green128\blue0;\red255\green0\blue0;\red0\green128\blue128;\red0\green255\blue255;\red0\green0\blue128;\red0\green255\blue0;\red128\green0\blue128;\red255\green0\blue255;\red0\green0\blue255;}" & Chr$(13) & Chr$(10) & _
    "\deflang1033\pard\plain\f2\fs17" & dmgrpt & Chr$(13) & Chr$(10) & "\par }"
  ' pop up the form
  frmDamageReport.Show 1
End Sub

Private Sub mnuMassRoll_Click()
  frmMassRoll.Show 1
End Sub

Private Sub fraBattle_EnableDisable()
  Dim i As Integer
  If NumCombatants + NumEvents = 0 Or Phase = -1 Then
      lblAction = ""
      lblStatus = ""
      frmPlayerDisplay.lblPLDAction = ""
      frmPlayerDisplay.lblPLDPlayerStatus = ""
      lblPhase = "none"
      lblUpkeep.Visible = False
      cmdBeginEnd.Caption = "Begin"
      mnuBeginEnd.Caption = "Begin &Battle"
      If NumCombatants + NumEvents = 0 Then
          cmdBeginEnd.Enabled = False
          mnuBeginEnd.Enabled = False
        Else
          cmdBeginEnd.Enabled = True
          mnuBeginEnd.Enabled = True
        End If
      cmdRefresh.Enabled = False
      txtWS.Enabled = False
      cmdWSMinus.Enabled = False
      cmdWSPlus.Enabled = False
      cmdTake.Enabled = False
      mnuTake.Enabled = False
      cmdAttack.Enabled = False
      mnuAttack.Enabled = False
      cmdSaveAction.Enabled = False
      mnuSaveAction.Enabled = False
      cmdMove.Enabled = False
      mnuMove.Enabled = False
      cmdPushback.Enabled = False
      mnuPushback.Enabled = False
      cmdNothing.Enabled = False
      mnuNothing.Enabled = False
      cmdInterrupt.Enabled = False
      mnuInterrupt.Enabled = False
      cmdFetch.Enabled = False
      mnuFetch.Enabled = False
      Exit Sub
    End If
  cmdBeginEnd.Caption = "End"
  mnuBeginEnd.Caption = "End &Battle"
  cmdBeginEnd.Enabled = True
  mnuBeginEnd.Enabled = True
  cmdRefresh.Enabled = True
  mnuRefresh.Enabled = True
  lblPhase = Trim$(Str$(Phase))
  If Phase > 0 And Phase / 50 = Int(Phase / 50) Then lblUpkeep.Visible = True Else lblUpkeep.Visible = False
  cmdTake.Enabled = True
  mnuTake.Enabled = True
  If Acting_Combatant > 0 Then
      If Combatants(Acting_Combatant).StunNoParries > 0 Or Combatants(Acting_Combatant).Stuns > 0 Then
          txtWS.Enabled = False
          cmdWSMinus.Enabled = False
          cmdWSPlus.Enabled = False
        Else
          txtWS.Enabled = True
          cmdWSMinus.Enabled = True
          cmdWSPlus.Enabled = True
        End If
      If Combatants(Acting_Combatant).StunNoParries > 0 Or Combatants(Acting_Combatant).Stuns > 0 Or Combatants(Acting_Combatant).MustParry <> "" Then
          cmdSaveAction.Enabled = False
          mnuSaveAction.Enabled = False
          cmdAttack.Enabled = False
          mnuAttack.Enabled = False
        Else
          If Combatants(Acting_Combatant).Saves = 3 Then
              cmdSaveAction.Enabled = False
              mnuSaveAction.Enabled = False
            Else
              cmdSaveAction.Enabled = True
              mnuSaveAction.Enabled = True
            End If
          cmdAttack.Enabled = True
          mnuAttack.Enabled = True
        End If
      cmdMove.Enabled = True
      mnuMove.Enabled = True
      cmdPushback.Enabled = True
      mnuPushback.Enabled = True
      cmdNothing.Enabled = True
      mnuNothing.Enabled = True
      cmdFetch.Enabled = True
      mnuFetch.Enabled = True
    Else
      txtWS.Enabled = False
      cmdWSMinus.Enabled = False
      cmdWSPlus.Enabled = False
      cmdAttack.Enabled = True ' combat initiation action can be an attack
      mnuAttack.Enabled = True ' here too
      cmdSaveAction.Enabled = False
      mnuSaveAction.Enabled = False
      cmdMove.Enabled = False
      mnuMove.Enabled = False
      cmdPushback.Enabled = False
      mnuPushback.Enabled = False
      cmdNothing.Enabled = False
      mnuNothing.Enabled = False
      cmdFetch.Enabled = False
      mnuFetch.Enabled = False
    End If
  cmdInterrupt.Enabled = False
  mnuInterrupt.Enabled = False
  For i = 1 To NumCombatants
    If Combatants(i).Saves > 0 Then
        cmdInterrupt.Enabled = True
        mnuInterrupt.Enabled = True
      End If
    Next i
End Sub

Private Sub cmdBeginEnd_Click()
  Dim i As Integer
  If Phase <> -1 Then ' this is the end... my only friend, the end
      If MsgBox("Terminate current battle?", vbYesNo + vbQuestion) = vbNo Then Exit Sub
      Phase = -1
      Acting_Combatant = 0
      lblAction = ""
      lblStatus = ""
      frmPlayerDisplay.lblPLDAction = ""
      frmPlayerDisplay.lblPLDPlayerStatus = ""
      txtWS = 0
      For i = 1 To NumCombatants
        Combatants(i).LastAttacked = 0
        Combatants(i).NextAction = 0
        lisCombatants_UpdateLabel i
        Next i
      Call fraBattle_EnableDisable
      Call lisCombatants_Click
      Call frmPlayerDisplay.Form_Refresh
      Exit Sub
    End If
  ' from here on, we're doing the beginning of a battle
  Call mnuMassHeal_Click
  ' handle surprise
  For i = 1 To NumCombatants
    Combatants(i).NextAction = 0
    lisCombatants_UpdateLabel i
    Next i
  frmSurprise.Show 1
  If frmSurprise.cmdCancel.Tag = "y" Then Exit Sub
  Log = "\b Battle begun!\plain\f2\fs17  First actions: "
  rtbLog_Update
  ' generate first actions
  For i = 1 To NumCombatants
    Combatants(i).NextAction = Combatants(i).NextAction + Combatants(i).Delay + OpenEndedD10()
    lisCombatants_UpdateLabel i
    rtbLog_Append "\par   " & Combatants(i).Name & " on " & Combatants(i).NextAction
    Next i
  lisCombatants_Click ' force an update to that display
  Phase = 0
  Last_Bleeding_Phase = 0
  Next_AutoSave = conAutoSaveRate
  Acting_Combatant = 0
  lblAction = "Combat Initiation Action"
  lblStatus = ""
  frmPlayerDisplay.lblPLDAction = "Combat Initiation Action"
  frmPlayerDisplay.lblPLDPlayerStatus = ""
  Call fraBattle_EnableDisable
  Call frmPlayerDisplay.Form_Refresh
  cmdTake.SetFocus
End Sub

Private Sub mnuBeginEnd_Click()
  Call cmdBeginEnd_Click
End Sub

Private Sub lblAction_FindNext()
  Dim i, nextphase, nextupkeep, nextcomb, nextevent As Integer
  If Phase = -1 Then
      lblAction = ""
      lblStatus = ""
      frmPlayerDisplay.lblPLDAction = ""
      frmPlayerDisplay.lblPLDPlayerStatus = ""
      txtWS = 0
      Call fraBattle_EnableDisable
      Exit Sub
    End If
  Do
    nextphase = Int(Phase / 50) * 50 + 50
    nextcomb = 0
    If Interrupter = 0 Then
        For i = 1 To NumCombatants
          If Combatants(i).NextAction + IIf(Combatants(i).LostInits > 0, 10, 0) < nextphase Then
              nextphase = Combatants(i).NextAction + IIf(Combatants(i).LostInits > 0, 10, 0)
              nextcomb = i
            End If
          Next i
      Else
        nextcomb = Interrupter
        nextphase = Combatants(Interrupter).NextAction
        Interrupter = 0
      End If
    If nextcomb = 0 Then Exit Do ' go straight to the upkeep phase
    If Combatants(nextcomb).CurHp > 0 And Combatants(nextcomb).Position <> 9 And _
        Combatants(nextcomb).Position <> 10 Then Exit Do
    ' next combatant is out, unconscious or dead
    Combatants(nextcomb).StunNoParries = 0
    Combatants(nextcomb).Stuns = 0
    Combatants(nextcomb).LostInits = 0
    Combatants(nextcomb).MustParry = ""
    Combatants(nextcomb).BonusPenalty = ""
    If Combatants(nextcomb).Position = 9 Then Combatants(nextcomb).RoundsTillDown = 0
    rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(nextcomb).Name & " is out,"
    Call cmdTake_TakeAction(nextcomb, -10, False, True)
    Loop
  nextupkeep = Int(Phase / 50) * 50 + 50
  If nextphase > nextupkeep Then
      nextphase = nextupkeep
      nextcomb = 0
    End If
  ' find the next event
  nextupkeep = nextphase
  nextevent = 0
  For i = 1 To NumEvents
    If Events(i).Phase <= nextupkeep Then
        nextupkeep = Events(i).Phase
        nextevent = i
      End If
    Next i
  If nextphase > nextupkeep Then
      nextphase = nextupkeep
      nextcomb = 0
    End If
  ' implement the action
  Phase = nextphase
  Acting_Combatant = nextcomb
  If Phase >= Next_AutoSave And mnuAutosave.Checked = True Then
      Next_AutoSave = Next_AutoSave + conAutoSaveRate
      If Last_AutoSave_Filename = "" Then
          ' set up the common dialog for a save
          cdlGeneric.DialogTitle = "Select Auto-Save Location"
          cdlGeneric.Filename = "C:\IRIS-AutoSave.prism"
          cdlGeneric.DefaultExt = "*.prism"
          cdlGeneric.Filter = "Prism Files (*.prism)|*.prism|Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
          cdlGeneric.FilterIndex = 1
          cdlGeneric.Flags = cdlOFNHideReadOnly + cdlOFNPathMustExist + cdlOFNExplorer + cdlOFNLongNames
          cdlGeneric.CancelError = True
          ' launch the dialog
          On Error GoTo cancelled
          cdlGeneric.ShowSave
          If cdlGeneric.Filename = "" Then GoTo cancelled
          ' work around bug: CDL trims default extension
          If Right(cdlGeneric.Filename, 4) = ".pri" Then cdlGeneric.Filename = cdlGeneric.Filename & "sm"
          Last_AutoSave_Filename = cdlGeneric.Filename
          GoTo getout
cancelled:
          Last_AutoSave_Filename = ""
getout:
        End If
      If Last_AutoSave_Filename <> "" Then Call mnuSave_DoIt(Last_AutoSave_Filename)
    End If
  If nextevent <> 0 Then
      Acting_Combatant = -nextevent
      lblAction = Events(nextevent).Description
      lblStatus = ""
      frmPlayerDisplay.lblPLDAction = "An event"
      frmPlayerDisplay.lblPLDPlayerStatus = ""
      txtWS = 0
    ElseIf Acting_Combatant = 0 Then
      lblAction = "Upkeep Phase"
      lblStatus = ""
      frmPlayerDisplay.lblPLDAction = "Upkeep Phase"
      frmPlayerDisplay.lblPLDPlayerStatus = ""
      txtWS = 0
    Else
      lblAction = Combatants(Acting_Combatant).Name
      If Combatants(Acting_Combatant).StunNoParries > 0 Or Combatants(Acting_Combatant).Stuns > 0 Then txtWS = 0 Else txtWS = Combatants(Acting_Combatant).LastWS
      ' if the combatant has been moving, she stops
      If Combatants(Acting_Combatant).Position = 2 Or _
          Combatants(Acting_Combatant).Position = 3 Then _
          Combatants(Acting_Combatant).Position = 1
      If Acting_Combatant = lisCombatants.ListIndex + 1 Then cmbPosition.ListIndex = Combatants(Acting_Combatant).Position - 1
      If chkAutoFetch.Value = 1 Then Call cmdFetch_Click
      lblStatus = CombatantStatus(Acting_Combatant, 1)
      If Combatants(Acting_Combatant).PC Then
        frmPlayerDisplay.lblPLDAction = Combatants(Acting_Combatant).Name
        frmPlayerDisplay.lblPLDPlayerStatus = lblStatus
      Else
        frmPlayerDisplay.lblPLDAction = "An NPC"
        frmPlayerDisplay.lblPLDPlayerStatus = ""
      End If
    End If
  If Phase / 50 = Int(Phase / 50) And Phase > Last_Bleeding_Phase Then Call BleedingPhase
  Call fraBattle_EnableDisable
  Call frmPlayerDisplay.Form_Refresh
End Sub

Private Sub BleedingPhase()
  Dim i As Integer, oldHp As Integer, amt As Integer
  Dim conscious As Boolean
  Last_Bleeding_Phase = Phase
  rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : Upkeep phase."
  For i = 1 To NumCombatants
    With Combatants(i)
      oldHp = .CurHp
      If .CurHp > 0 Then conscious = True Else conscious = False
      If .Position = 9 Or .Position = 10 Then conscious = False
      If .RoundsTillDead > 0 Then
          .RoundsTillDead = .RoundsTillDead - 1
          If .RoundsTillDead = 0 Then
              MsgBox .Name & " is now dead.", vbInformation
              .Position = 10
              rtbLog_Append "\par   " & .Name & " died."
            End If
        End If
      If .RoundsTillDown > 0 Then
          .RoundsTillDown = .RoundsTillDown - 1
          If .RoundsTillDown = 0 Then
              MsgBox .Name & " is now unconscious.", vbInformation
              .Position = 9
              rtbLog_Append "\par   " & .Name & " dropped and is down."
            End If
        End If
      If .Bleed > 0 Then
          .CurHp = .CurHp - .Bleed
          If .Clot > 0 Then
              amt = Int(.Clot * .Bleed / 100)
              If Int(100 * Rnd + 1) <= .Clot * .Bleed - 100 * amt Then amt = amt + 1
              If amt > .Bleed Then amt = .Bleed
              .Bleed = .Bleed - amt
            Else
              amt = 0
            End If
          If amt > 0 Then
              rtbLog_Append "\par   " & .Name & " bled " & .Bleed & " Hp, clotted " & amt & " Hp/rnd."
            Else
              rtbLog_Append "\par   " & .Name & " bled " & .Bleed & " Hp."
            End If
        End If
      If .Heal > 0 Then
          amt = Int(.Heal * (.OptHp - .CurHp) / 100)
          If Int(100 * Rnd + 1) <= .Heal * (.OptHp - .CurHp) - 100 * amt Then amt = amt + 1
          If amt > (.OptHp - .CurHp) Then amt = (.OptHp - .CurHp)
          .CurHp = .CurHp + amt
          If amt <> 0 Then
              rtbLog_Append "\par   " & .Name & " healed " & amt & " Hp."
            End If
        End If
      If conscious And .CurHp <= 0 And .Position <> 10 Then
          MsgBox .Name & " is now unconscious.", vbInformation
          .Position = 9
          rtbLog_Append "\par   " & .Name & " is now unconscious."
        End If
      If Not conscious And .CurHp > 0 And .Position <> 10 And oldHp <= 0 Then
          MsgBox .Name & " is now conscious.", vbInformation
          If .Position = 9 Then .Position = 7
          rtbLog_Append "\par   " & .Name & " is now conscious."
        End If
      End With
    Next i
  lisCombatants_Click
End Sub

Private Sub txtWS_GotFocus()
  Call SelectField(txtWS)
End Sub

Private Sub cmdWSMinus_Click()
  txtWS = txtWS - 1
End Sub

Private Sub cmdWSPlus_Click()
  txtWS = txtWS + 1
End Sub

Private Sub txtWS_KeyPress(KeyAscii As Integer)
  KeyAscii = OnlyDigitsAndMinus(KeyAscii)
End Sub

Private Sub cmdTake_TakeAction(ByVal comb As Integer, ByVal delaymod As Integer, _
    ByVal burnstuns As Boolean, ByVal lognext As Boolean)
  Dim i, rounds, bp As Integer
  If Combatants(comb).LostInits > 0 Then Combatants(comb).LostInits = Combatants(comb).LostInits - 1
  If burnstuns Then ' the action was actually taken (not saved, pushed back, etc.); use up stuns, must parries, etc.
      With Combatants(comb)
        If .StunNoParries > 0 Then
            .StunNoParries = .StunNoParries - 1
          ElseIf .Stuns > 0 Then
            .Stuns = .Stuns - 1
          ElseIf .MustParry <> "" Then
            i = Val(Word(.MustParry, 2))
            If i = 1 Then
                If InStr(.MustParry, ",") = 0 Then .MustParry = "" _
                  Else .MustParry = Mid$(.MustParry, InStr(.MustParry, ",") + 1)
              Else
                .MustParry = Word(.MustParry, 1) & " " & Trim$(Str$(i - 1)) & _
                  IIf(InStr(.MustParry, ",") = 0, "", "," & Mid$(.MustParry, InStr(.MustParry, ",") + 1))
              End If
          ElseIf .BonusPenalty <> "" Then
            i = Val(Word(.BonusPenalty, 2))
            If i = 1 Then
                If InStr(.BonusPenalty, ",") = 0 Then .BonusPenalty = "" _
                  Else .BonusPenalty = Mid$(.BonusPenalty, InStr(.BonusPenalty, ",") + 1)
              Else
                .BonusPenalty = Word(.BonusPenalty, 1) & " " & Trim$(Str$(i - 1)) & _
                  IIf(InStr(.BonusPenalty, ",") = 0, "", "," & Mid$(.BonusPenalty, InStr(.BonusPenalty, ",") + 1))
              End If
          End If
        End With
      End If
    ' apply any pending attacker bonuses
    Do While attacker_bonuses <> ""
      ' parse off one, removing it from the string
      i = InStr(attacker_bonuses, " ")
      If i = 0 Then Exit Do
      bp = Val(Left(attacker_bonuses, i - 1))
      attacker_bonuses = Mid(attacker_bonuses, i + 1)
      i = InStr(attacker_bonuses, ",")
      If i = 0 Then Exit Do
      rounds = Val(Left(attacker_bonuses, i - 1))
      attacker_bonuses = Mid(attacker_bonuses, i + 1)
      ' apply it
      Call AddBonusPenalty(comb, rounds, bp)
      Loop
    attacker_bonuses = "" ' just in case we bailed on an Exit Do above
    Combatants(comb).NextAction = Combatants(comb).NextAction + _
      Combatants(comb).Delay + delaymod + OpenEndedD10()
    lisCombatants_UpdateLabel comb
    If lognext Then rtbLog_Append " acts next on phase " & Combatants(comb).NextAction & "."
    lisCombatants_Click
End Sub

Private Sub cmdTake_Click()
  Dim actiontext As String
  If Acting_Combatant < 0 Then ' this is an event
      rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & lblAction & IIf(Right$(lblAction, 1) <> ",", ",", "")
      Call DeleteEvent(-Acting_Combatant)
      Acting_Combatant = 0
    ElseIf Acting_Combatant > 0 Then ' it's a combatant
      If Combatants(Acting_Combatant).StunNoParries = 0 And _
          Combatants(Acting_Combatant).Stuns = 0 Then
          Combatants(Acting_Combatant).LastWS = Val(txtWS)
          If mnuPrompt.Checked Then
              actiontext = InputBox("Action taken (e.g., 'opened the door'):", conAppName & " v" & conAppVersion, " ")
              If actiontext = "" Then Exit Sub
              If actiontext = " " Then actiontext = ""
            Else
              actiontext = ""
            End If
          rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(Acting_Combatant).Name & _
              " " & IIf(actiontext <> "", actiontext, "took an action") & ","
       Else
          rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(Acting_Combatant).Name & _
              " took a stun" & _
              IIf(Combatants(Acting_Combatant).StunNoParries = 0, "", "/no-parry") & " action,"
       End If
      Call cmdTake_TakeAction(Acting_Combatant, Val(txtWS), True, True)
    End If
  Call lblAction_FindNext
End Sub

Private Sub mnuTake_Click()
  Call cmdTake_Click
End Sub

Private Sub cmdAttack_Click()
  attacker_bonuses = ""
  frmAttack.cmdCancel.Tag = "l" ' flag telling it to load in Form_Activate
  frmAttack.Show 1
  Call lisCombatants_Click
  If frmAttack.cmdCancel.Tag = "y" Then Exit Sub
  rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(frmAttack.cmbAttacker.ListIndex + 1).Name & _
    " attacked " & Combatants(frmAttack.cmbDefender.ListIndex + 1).Name & _
    IIf(frmAttack.chkAParry.Value = 1 And Val(frmAttack.txtAParry) <> 0, ", parried -" & frmAttack.txtAParry, "") & _
    ", total roll " & frmAttack.lblTotalRoll & " (" & frmAttack.lblTotald100 & ") against AT" & frmAttack.lblAt & _
    frmAttack.cmdOK.Tag & ","
  Combatants(Acting_Combatant).LastWS = Val(txtWS)
  Call cmdTake_TakeAction(Acting_Combatant, Val(txtWS), True, True)
  Call lblAction_FindNext
End Sub

Private Sub mnuAttack_Click()
  Call cmdAttack_Click
End Sub

Private Sub cmdSaveAction_Click()
  rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(Acting_Combatant).Name & _
    " saved an action,"
  Combatants(Acting_Combatant).Saves = Combatants(Acting_Combatant).Saves + 1
  Call cmdTake_TakeAction(Acting_Combatant, 0, False, True)
  Call lblAction_FindNext
End Sub

Private Sub mnuSaveAction_Click()
  Call cmdSaveAction_Click
End Sub

Private Sub cmdMove_Click()
  Dim roll As Integer, pushback As Integer, i As Integer, doMove As Boolean, effective_rate As Double
  With Combatants(Acting_Combatant)
    If .StunNoParries > 0 Then
        frmMove.txtMV = Int(.MV / 4)
      ElseIf .Stuns > 0 Then
        frmMove.txtMV = Int(.MV / 2)
      Else
        frmMove.txtMV = .MV
      End If
    frmMove.txtMN = .MN
    frmMove.txtMods = (IIf(.MustParry <> "", Val(Word(.MustParry, 1)), 0) + .Penalty + Val(lblHpStatus))
    frmMove.Caption = "Move " & .Name
    End With
  frmMove.Show 1
  If frmMove.cmdCancel.Tag = "y" Then Exit Sub
  doMove = False
  roll = OpenEndedD20("open") + frmMove.txtMN + frmMove.txtMods - 6 * frmMove.cmbPace + 15
  If roll < -4 Then
      rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(Acting_Combatant).Name & _
        " had an extraordinary failure trying to move,"
      MsgBox "Extraordinary failure!  No movement, plus a fumble or crash.", vbInformation
      Call cmdTake_TakeAction(Acting_Combatant, 10, True, True)
    ElseIf roll < 2 Then
      rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(Acting_Combatant).Name & _
        " had a fumble trying to move,"
      MsgBox "Fumble!  No movement, at -5 to next try at the same movement.", vbInformation
      Call cmdTake_TakeAction(Acting_Combatant, 5, True, True)
    ElseIf roll < 4 Then
      rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(Acting_Combatant).Name & _
        " failed to move,"
      Call cmdTake_TakeAction(Acting_Combatant, 0, True, True)
    ElseIf roll > 50 Then
      rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(Acting_Combatant).Name & _
        " had an extraordinary success moving, can act now."
      MsgBox "Extraordinary success, " & Combatants(Acting_Combatant).Name & " can act now.", vbInformation
      Exit Sub
    ElseIf roll > 35 Then
      rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(Acting_Combatant).Name & _
        " had a critical success moving,"
      doMove = True
      roll = 100
      frmMove.cmbPace = CDbl(frmMove.cmbPace) + 1
    Else
      roll = MovingManeuverChart(roll)
      doMove = True
    End If
  If doMove Then
      effective_rate = (roll / 100) * frmMove.txtMV * frmMove.cmbPace ' 50 phases worth
      If frmMove.chkAsFarAsPossible Then
          effective_rate = Int(effective_rate + 0.5)
          rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(Acting_Combatant).Name & _
            " moved " & Trim$(Str$(effective_rate)) & "',"
          Combatants(Acting_Combatant).NextAction = Combatants(Acting_Combatant).NextAction + 50
          lisCombatants_UpdateLabel Acting_Combatant
          If Acting_Combatant = lisCombatants.ListIndex + 1 Then cmbPosition.ListIndex = Combatants(Acting_Combatant).Position - 1
          Combatants(Acting_Combatant).Position = IIf(frmMove.cmbPace >= 2, 3, 2)
        Else
          pushback = Int(frmMove.txtDistance / (effective_rate) * 50 + 0.5)
          If pushback > 50 Then
              effective_rate = Int(effective_rate + 0.5)
              rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(Acting_Combatant).Name & _
                " moved " & Trim$(Str$(effective_rate)) & "' (tried to move " & frmMove.txtDistance & _
                "'),"
              MsgBox Combatants(Acting_Combatant).Name & " can only move " & _
                Trim$(Str$(effective_rate)) & " this action.", vbInformation
              Combatants(Acting_Combatant).Position = IIf(frmMove.cmbPace >= 2, 3, 2)
              If Acting_Combatant = lisCombatants.ListIndex + 1 Then cmbPosition.ListIndex = Combatants(Acting_Combatant).Position - 1
              Combatants(Acting_Combatant).NextAction = Combatants(Acting_Combatant).NextAction + 50
              lisCombatants_UpdateLabel Acting_Combatant
            Else ' pushback
              rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(Acting_Combatant).Name & _
                " moved " & frmMove.txtDistance & "', pushed back " & Trim$(Str$(pushback)) & _
                " phase" & IIf(pushback <> 1, "s", "") & ","
              Combatants(Acting_Combatant).Position = IIf(frmMove.cmbPace >= 2, 3, 2)
              If Acting_Combatant = lisCombatants.ListIndex + 1 Then cmbPosition.ListIndex = Combatants(Acting_Combatant).Position - 1
              Combatants(Acting_Combatant).NextAction = Combatants(Acting_Combatant).NextAction + pushback
              lisCombatants_UpdateLabel Acting_Combatant
            End If
        End If
      rtbLog_Append " acts next on phase " & Combatants(Acting_Combatant).NextAction & "."
    End If
  Call lblAction_FindNext
End Sub

Private Sub mnuMove_Click()
  Call cmdMove_Click
End Sub

Private Sub cmdPushback_Click()
  ' if we're doing it the InputBox way...
  Dim pushback As Integer
  Dim s As String
  s = InputBox("Pushback", , 5)
  If s = "" Then Exit Sub
  pushback = Val(s)
  Combatants(Acting_Combatant).NextAction = Combatants(Acting_Combatant).NextAction + pushback
  lisCombatants_UpdateLabel Acting_Combatant
  rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(Acting_Combatant).Name & _
    " pushed back " & pushback & " phase" & IIf(pushback <> 1, "s", "") & ", acts next on phase " & _
    Combatants(Acting_Combatant).NextAction & "."
  lisCombatants_Click
  Call lblAction_FindNext
End Sub

Private Sub mnuPushback_Click()
  Call cmdPushback_Click
End Sub

Private Sub cmdNothing_Click()
  If Combatants(Acting_Combatant).StunNoParries > 0 Or Combatants(Acting_Combatant).Stuns > 0 Then
      rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(Acting_Combatant).Name & _
        " did nothing on a stun" & IIf(Combatants(Acting_Combatant).StunNoParries > 0, "/no-parry", "") & _
        " action,"
      Call cmdTake_TakeAction(Acting_Combatant, 0, True, True)
    Else
      rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(Acting_Combatant).Name & _
        " did nothing,"
      Call cmdTake_TakeAction(Acting_Combatant, -10, False, True)
    End If
  Call lblAction_FindNext
End Sub

Private Sub mnuNothing_Click()
  Call cmdNothing_Click
End Sub

Private Sub cmdInterrupt_Click()
  frmInterrupt.txtWhen = Phase
  frmInterrupt.Show 1
  If frmInterrupt.cmdCancel.Tag = "y" Then Exit Sub
  Interrupter = frmInterrupt.lisCombatants.ItemData(frmInterrupt.lisCombatants.ListIndex)
  With Combatants(Interrupter)
    .Saves = .Saves - 1
    .NextAction = frmInterrupt.txtWhen
    lisCombatants_UpdateLabel Interrupter
    If Acting_Combatant > 0 Then
        rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(Acting_Combatant).Name & _
          " was interrupted by " & .Name & " acting on phase " & frmInterrupt.txtWhen & "."
      Else
        rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & .Name & " interrupted, acting on phase " & frmInterrupt.txtWhen & "."
      End If
    End With
  Call lblAction_FindNext
End Sub

Private Sub mnuInterrupt_Click()
  Call cmdInterrupt_Click
End Sub

Private Sub mnuSkill_Click()
  frmSkill.cmdCancel.Tag = "l" ' flag telling it to load in Form_Activate
  frmSkill.Show 1
  If frmAttack.cmdCancel.Tag = "y" Then Exit Sub
  '-- deal with the Take and Pushback possibilities; log; etc.
  ' rtbLog_Append "\par\b " & Phase & "\plain\f2\fs17 : " & Combatants(Acting_Combatant).name & _
    " attacked " & Combatants(frmAttack.cmbDefender.ListIndex + 1).name & _
    IIf(frmAttack.chkAParry.Value = 1 And Val(frmAttack.txtAParry) <> 0, ", parried -" & frmAttack.txtAParry, "") & _
    ", total roll " & frmAttack.lblTotalRoll & " against AT" & frmAttack.lblAt & _
    frmAttack.cmdOK.Tag & ","
  ' Combatants(Acting_Combatant).LastWS = Val(txtWS)
  ' Call cmdTake_TakeAction(Acting_Combatant, Val(txtWS), True, True)
  ' Call lblAction_FindNext
End Sub

Private Sub cmdRefresh_Click()
  Call lblAction_FindNext
End Sub

Private Sub mnuRefresh_Click()
  Call cmdRefresh_Click
End Sub

Private Sub mnuResetWindows_Click()
  Me.Left = 0
  Me.Top = 0
  frmPlayerDisplay.Left = 0
  frmPlayerDisplay.Top = 0
End Sub

Private Sub cmdFetch_Click()
  lisCombatants.ListIndex = Acting_Combatant - 1
End Sub

Private Sub mnuFetch_Click()
  Call cmdFetch_Click
End Sub

Private Sub mnuPrompt_Click()
  If mnuPrompt.Checked Then mnuPrompt.Checked = False Else mnuPrompt.Checked = True
End Sub

Private Sub mnuAutosave_Click()
  If mnuAutosave.Checked Then mnuAutosave.Checked = False Else mnuAutosave.Checked = True
End Sub

Private Sub chkAutoFetch_Click()
  mnuAutofetch.Checked = IIf(chkAutoFetch.Value = 1, True, False)
End Sub

Private Sub mnuAutofetch_Click()
  If mnuAutofetch.Checked Then mnuAutofetch.Checked = False Else mnuAutofetch.Checked = True
  chkAutoFetch.Value = IIf(mnuAutofetch.Checked, 1, 0)
End Sub

Private Sub mnuShowPlayerDisplay_Click()
  If mnuShowPlayerDisplay.Checked Then
    mnuShowPlayerDisplay.Checked = False
    Unload frmPlayerDisplay
  Else
    mnuShowPlayerDisplay.Checked = True
    frmPlayerDisplay.Show
  End If
End Sub

Private Sub lblPhase_Change()
  If lblPhase = "none" Then frmPlayerDisplay.lblPLDPhase = "n/a" Else frmPlayerDisplay.lblPLDPhase = lblPhase
End Sub

Private Sub lblAction_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
      PopupMenu mnuActions, vbPopupMenuRightButton
    End If
End Sub

Private Sub lblStatus_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
      PopupMenu mnuActions, vbPopupMenuRightButton
    End If
End Sub

Private Sub lisCombatants_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
      PopupMenu mnuCombatant, vbPopupMenuRightButton
    End If
End Sub

Private Sub lblPhaseLabel_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
      PopupMenu mnuBattle, vbPopupMenuRightButton
    End If
End Sub

Private Sub lblPhase_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
      PopupMenu mnuBattle, vbPopupMenuRightButton
    End If
End Sub

Private Sub lblUpkeep_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
      PopupMenu mnuBattle, vbPopupMenuRightButton
    End If
End Sub

Private Sub cmdBeginEnd_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
      PopupMenu mnuBattle, vbPopupMenuRightButton
    End If
End Sub

Private Sub rtbLog_Update()
  rtbLog = "{\rtf1\ansi\deff0\deftab720{\fonttbl{\f0\fswiss MS Sans Serif;}{\f1\froman\fcharset2 Symbol;}" & _
    "{\f2\fswiss MS Sans Serif;}}" & Chr$(13) & Chr$(10) & _
    "{\colortbl\red0\green0\blue0;}" & Chr$(13) & Chr$(10) & _
    "\deflang1033\pard\plain\f2\fs17" & Log & Chr$(13) & Chr$(10) & "\par }"
  rtbLog.SelStart = Len(rtbLog)
  If Log = "" Then
      cmdLogClear.Enabled = False
      cmdLogPrint.Enabled = False
    Else
      cmdLogClear.Enabled = True
      cmdLogPrint.Enabled = True
    End If
End Sub

Public Sub rtbLog_Append(ByVal s As String)
  Log = Log & s
  rtbLog_Update
End Sub

Private Sub cmdLogClear_Click()
  If MsgBox("Really clear the combat log?", vbYesNo + vbQuestion, conAppName & " v" & conAppVersion) <> vbYes Then Exit Sub
  Log = "\b Log cleared on phase " & Phase & ".\plain\f2\fs17 "
  rtbLog_Update
End Sub

Private Sub cmdLogPrint_Click()
  Dim orig, body As String
  Dim i As Integer
  On Local Error GoTo printError
  cdlGeneric.CancelError = True
  cdlGeneric.Flags = cdlPDReturnDC + cdlPDNoPageNums
  If rtbLog.SelLength = 0 Then
      cdlGeneric.Flags = cdlGeneric.Flags + cdlPDAllPages
    Else
      cdlGeneric.Flags = cdlGeneric.Flags + cdlPDSelection
    End If
  cdlGeneric.ShowPrinter
  On Local Error Resume Next
  orig = rtbLog.TextRTF
  body = rtbLog.TextRTF
  ' change all the \fs17s to \fs24s in body
  i = InStr(body, "\fs17")
  Do While i <> 0
    body = Left$(body, i - 1) & "\fs24" & Mid$(body, i + 5)
    i = InStr(body, "\fs17")
    Loop
  Printer.Print ""
  i = rtbLog.RightMargin
  rtbLog.RightMargin = 12000
  rtbLog.TextRTF = body
  rtbLog.SelPrint Printer.hDC
  rtbLog.TextRTF = orig
  rtbLog.RightMargin = i
  Printer.EndDoc
  Exit Sub
printError:
  If Err.Number <> cdlCancel Then
      MsgBox "Print Error: " & Err & "; " & Error, vbExclamation
    End If
End Sub

Private Function lisCombatants_Label(ByVal i As Integer) As String
  lisCombatants_Label = Right("000" & CStr(Combatants(i).NextAction), 3) & " " & Combatants(i).Name
End Function

Private Sub lisCombatants_UpdateLabel(ByVal i As Integer)
  If i = 0 Or i > lisCombatants.ListCount Then Exit Sub
  lisCombatants.List(i - 1) = lisCombatants_Label(i)
End Sub
